# pan-common

![Static Badge](https://img.shields.io/badge/new:version-0.9.3.6-greey) 
![Static Badge](https://img.shields.io/badge/JDK-8+-blud) 
![Static Badge](https://img.shields.io/badge/华为代码健康度-A-greey) 
![Static Badge](https://img.shields.io/badge/license-MulanPSL2-blue) 

##
[![Security Status](https://www.murphysec.com/platform3/v31/badge/1749267692458442752.svg)](https://www.murphysec.com/console/report/1749267682312421376/1749267692458442752)
## &#x1f4dd; 简介

​	**pan-common** 是一个通用的Java工具库，由一位独立开发者持续努力的成果，宗旨是让开发人员更关注业务，简化常见的编程任务，提高开发效率。

​	此工具类涵盖了常见的**集合、字符串、日期时间、缓存、对比、配置文件、IO、文件、加解密、Net相关(HTTP/FTP)处理、反射**等，为开发者提供了一系列实用的工具方法。

### &#x1F525; 主要特点

- **简单易用：** 提供简洁而强大的API，降低编程复杂性。
- **功能丰富：** 涵盖了多个常见任务的工具方法。
- **持续更新：** 持续不断迭代、持续更新、优化该工具类。

  ------

  

## &#x1F9F0; 安装

- ### 1️⃣  Maven

在项目中的pom.xml中添加以下内容即可引用

```maven
<dependency>
    <groupId>com.gitee.apanlh</groupId>
    <artifactId>apanlh-common</artifactId>
    <version>0.9.3.6</version>
</dependency>
```

- ### 2️⃣  Gradle

```
implementation group: 'com.gitee.apanlh', name: 'apanlh-common', version: '0.9.3.6'
```

- ### 3️⃣  下载JAR


​	[点击此处下载-Maven](https://repo1.maven.org/maven2/com/gitee/apanlh/apanlh-common/0.9.3.6/)  **点击apanlh-common-x.x.x.jar进行下载**

------



## &#x1f4c3; API文档

- **Javadoc文档** [点击此处查看-API文档](https://apidoc.gitee.com/apanlh/pan-common) 

------



## &#x1f4cb; 组件使用文档



[TOC]



------



## &#x1F397; 贡献

- ### 🐞 **提供bug反馈或建议**

​	如果发现了BUG，或者有新的功能建议，欢迎在 [Issues](https://gitee.com/your-username/pan-common/issues) 中提出。欢迎社区的每一位贡献者一同改进和完善该工具库。

------



## &#x1f4e7; **联系方式**

- #### **作者：Pan**
- **邮箱：[582084583@qq.com]()**
  
  

##  &#x1F496; **支持**

​	如果觉得该工具类不错，可以点击一下右上角的 	&#x2B50;小星星，感谢(*^▽^*)&#x1F60B;，希望它能在你的项目中发挥重要作用&#x1f60a;&#x1f60a;

------

