package com.gitee.apanlh.spring.web.resolver;

import com.gitee.apanlh.web.model.RequestDataType;
import com.gitee.apanlh.web.model.vo.RequestVO;
import com.gitee.apanlh.web.util.ServletUtils;
import jakarta.servlet.http.HttpServletRequest;

/**	
 * 	Form解析器
 * 
 * 	@author Pan
 */
class FormResolver implements RequestParamResolverStrategy<RequestVO> {
	
	/**
	 * 	默认构造函数
	 * 
	 * 	@author Pan
	 */
	FormResolver() {
		super();
	}
	
	@Override
	public RequestVO resolve() {
		return null;
	}
	
	@Override
	public RequestVO resolve(HttpServletRequest request, RequestVO params, RequestDataType dataType) {
		params.setParams(ServletUtils.getParams(request));
		dataType.setHasForm(true);
		return params;
	}
}
