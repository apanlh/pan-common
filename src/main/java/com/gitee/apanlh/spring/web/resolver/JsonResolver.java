package com.gitee.apanlh.spring.web.resolver;

import com.alibaba.fastjson.TypeReference;
import com.gitee.apanlh.util.dataformat.JsonUtils;
import com.gitee.apanlh.util.valid.ValidParam;
import com.gitee.apanlh.web.model.RequestDataType;
import com.gitee.apanlh.web.model.vo.RequestVO;
import com.gitee.apanlh.web.util.ServletUtils;
import jakarta.servlet.http.HttpServletRequest;

import java.util.Map;

/**	
 * 	JSON解析器
 * 
 * 	@author Pan
 */
class JsonResolver implements RequestParamResolverStrategy<RequestVO> {
	
	/**
	 * 	默认构造函数
	 * 
	 * 	@author Pan
	 */
	JsonResolver() {
		super();
	}
	
	@Override
	public RequestVO resolve() {
		return null;
	}
	
	@Override
	public RequestVO resolve(HttpServletRequest request, RequestVO params, RequestDataType dataType) {
		String body = ServletUtils.readBodyString(request);
		if (!ValidParam.isEmpty(body)) {
			params.setBody(body);
			params.setJsonParams(JsonUtils.toBean(body, new TypeReference<Map<String, Object>>(){}));
			dataType.setHasJson(true);
		}
		return params;
	}
}
