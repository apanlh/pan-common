package com.gitee.apanlh.spring.web.resolver;

import com.gitee.apanlh.web.model.RequestDataType;
import com.gitee.apanlh.web.model.vo.RequestVO;
import jakarta.servlet.http.HttpServletRequest;

/**	
 * 	解析器类型
 * 
 * 	@author Pan
 */
public interface RequestParamResolverStrategy<T> {
	
	/**	
	 * 	解析
	 * 	
	 * 	@author Pan
	 * 	@return	T
	 */
	T resolve();
	
	/**	
	 * 	解析
	 * 	
	 * 	@author Pan
	 * 	@param 	request		HttpServletRequest对象
	 * 	@param 	params		参数对象
	 * 	@param 	dataType	请求类型
	 * 	@return	T	
	 */
	T resolve(HttpServletRequest request, RequestVO params, RequestDataType dataType);
}
