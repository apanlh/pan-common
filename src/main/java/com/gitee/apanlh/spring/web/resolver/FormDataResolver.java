package com.gitee.apanlh.spring.web.resolver;

import com.gitee.apanlh.web.model.RequestDataType;
import com.gitee.apanlh.web.model.WebFile;
import com.gitee.apanlh.web.model.vo.RequestVO;
import com.gitee.apanlh.web.util.ServletUtils;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**	
 * 	FormData对象解析
 * 
 * 	@author Pan
 */
class FormDataResolver implements RequestParamResolverStrategy<RequestVO> {
	
	/**
	 * 	默认构造函数
	 * 
	 * 	@author Pan
	 */
	FormDataResolver() {
		super();
	}
	
	@Override
	public RequestVO resolve() {
		return null;
	}
	
	@Override
	public RequestVO resolve(HttpServletRequest request, RequestVO params, RequestDataType dataType) {
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		params.setParams(ServletUtils.getParams(request));
		WebFile webFile = new WebFile(multipartRequest.getMultiFileMap());
		
		if (webFile.getSize() > 0) {
			params.setWebFile(webFile);
			dataType.setHasFile(true);
		}
		
		dataType.setHasFormData(true);
		return params;
	}
}
