package com.gitee.apanlh.spring.web.resolver;

import com.gitee.apanlh.util.base.MapUtils;
import com.gitee.apanlh.util.io.IOUtils;
import com.gitee.apanlh.web.model.RequestDataType;
import com.gitee.apanlh.web.model.vo.RequestVO;
import com.gitee.apanlh.web.util.ServletUtils;
import jakarta.servlet.http.HttpServletRequest;

import java.util.Map;

/**	
 * 	文本解析
 * 
 * 	@author Pan
 */
class TextResolver implements RequestParamResolverStrategy<RequestVO> {
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	TextResolver() {
		super();
	}

	@Override
	public RequestVO resolve() {
		return null;
	}
	
	@Override
	public RequestVO resolve(HttpServletRequest request, RequestVO params, RequestDataType dataType) {
		MapUtils.newHashMap((Map<String, String> map) -> {
			map.put(RequestVO.TEXT_KEY, IOUtils.readString(ServletUtils.getInputStream(request), false));
			params.setParams(map);
			dataType.setHasText(true);
		}, 1);
		return params;
	}
}
