package com.gitee.apanlh.spring.web.resolver;

import com.gitee.apanlh.util.base.MapUtils;
import com.gitee.apanlh.web.model.RequestDataType;
import com.gitee.apanlh.web.model.WebFile;
import com.gitee.apanlh.web.model.vo.RequestVO;
import com.gitee.apanlh.web.util.ServletUtils;
import jakarta.servlet.http.HttpServletRequest;

import java.util.Map;

/**	
 * 	文件流解析器
 * 	
 * 	@author Pan
 */
class FileStreamResolver implements RequestParamResolverStrategy<RequestVO> {
	
	private static final Map<String, String> EMPTY_MAP = MapUtils.newHashMap(1);
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	FileStreamResolver() {
		super();
	}
	
	@Override
	public RequestVO resolve() {
		return null;
	}
	
	@Override
	public RequestVO resolve(HttpServletRequest request, RequestVO params, RequestDataType dataType) {
		params.setParams(EMPTY_MAP);
		params.setWebFile(new WebFile(ServletUtils.getInputStream(request)));
		dataType.setHasFile(true);
		return params;
	}
}
