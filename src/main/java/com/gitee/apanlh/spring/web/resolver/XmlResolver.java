package com.gitee.apanlh.spring.web.resolver;

import com.gitee.apanlh.util.base.MapUtils;
import com.gitee.apanlh.web.model.RequestDataType;
import com.gitee.apanlh.web.model.vo.RequestVO;
import com.gitee.apanlh.web.util.ServletUtils;
import jakarta.servlet.http.HttpServletRequest;

import java.util.Map;

/**	
 * 	XML文本解析
 * 
 * 	@author Pan
 */
class XmlResolver implements RequestParamResolverStrategy<RequestVO> {
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	XmlResolver() {
		super();
	}
	
	@Override
	public RequestVO resolve() {
		return null;
	}
	
	@Override
	public RequestVO resolve(HttpServletRequest request, RequestVO params, RequestDataType dataType) {
		MapUtils.newHashMap((Map<String, String> map) -> {
			map.put(RequestVO.XML_KEY, ServletUtils.readString());
			params.setParams(map);
			dataType.setHasXml(true);
		}, 1);
		return params;
	}
}
