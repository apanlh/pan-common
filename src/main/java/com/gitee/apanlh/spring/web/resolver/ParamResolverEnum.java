package com.gitee.apanlh.spring.web.resolver;

import com.gitee.apanlh.exp.ParamParserException;
import com.gitee.apanlh.util.base.MapUtils;
import com.gitee.apanlh.util.base.SingletonUtils;
import com.gitee.apanlh.web.http.HttpContentType;

import java.util.Map;

/**	
 * 	参数解析器枚举
 * 
 * 	@author Pan
 */
public enum ParamResolverEnum {
	
	GET_RESOLVER			(HttpContentType.GET,  		 			 		SingletonUtils.get(GetResolver.class)),
	JSON_RESOLVER			(HttpContentType.APPLICATION_JSON, 		 		SingletonUtils.get(JsonResolver.class)),
	FORM_RESOLVER			(HttpContentType.APPLICATION_FORM_URLENCODED, 	SingletonUtils.get(FormResolver.class)),
	FORM_DATA_RESOLVER		(HttpContentType.MULTIPART_FORM_DATA, 	 		SingletonUtils.get(FormDataResolver.class)),
	XML_RESOLVER			(HttpContentType.APPLICATION_XML, 		 		SingletonUtils.get(XmlResolver.class)),
	FILE_STREAM_RESOLVER	(HttpContentType.APPLICATION_OCTET_STREAM, 		SingletonUtils.get(FileStreamResolver.class)),
	TEXT_RESOLVER			(HttpContentType.TEXT_PLAIN, 		 			SingletonUtils.get(TextResolver.class));
	
	/** 解析器枚举 */
	private static final Map<HttpContentType, RequestParamResolverStrategy<?>> RESOLVER_MAP = initMap();
	/** 类型 */
	private HttpContentType contentType;
	/** 解析器 */
	private RequestParamResolverStrategy<?> value;
	
	/**	
	 * 	构造函数-加载解析器
	 * 	
	 * 	@author Pan
	 * 	@param 	contentType	 请求类型
	 * 	@param 	value		 策略器
	 */
	ParamResolverEnum(HttpContentType contentType, RequestParamResolverStrategy<?> value) {
		this.contentType = contentType;
		this.value = value;
	}
	
	/**	
	 * 	加载Map
	 * 
	 * 	@author Pan
	 * 	@return	Map
	 */
	private static Map<HttpContentType, RequestParamResolverStrategy<?>> initMap() {
		Map<HttpContentType, RequestParamResolverStrategy<?>> e = MapUtils.newEnumMap(HttpContentType.class);
		e.put(GET_RESOLVER.getContentType(), 		 GET_RESOLVER.getValue());
		e.put(JSON_RESOLVER.getContentType(), 		 JSON_RESOLVER.getValue());
		e.put(FORM_RESOLVER.getContentType(), 		 FORM_RESOLVER.getValue());
		e.put(FORM_DATA_RESOLVER.getContentType(), 	 FORM_DATA_RESOLVER.getValue());
		e.put(XML_RESOLVER.getContentType(), 		 XML_RESOLVER.getValue());
		e.put(FILE_STREAM_RESOLVER.getContentType(), FILE_STREAM_RESOLVER.getValue());
		e.put(TEXT_RESOLVER.getContentType(), 		 TEXT_RESOLVER.getValue());
		return e; 
	}
	
	/**	
	 * 	获取解析器
	 * 	
	 * 	@author Pan
	 * 	@param 	contentType	请求类型
	 * 	@return	RequestParamResolverStrategy
	 */
	public static RequestParamResolverStrategy<?> get(HttpContentType contentType) {
		RequestParamResolverStrategy<?> resolver = RESOLVER_MAP.get(contentType);
		if (resolver == null) {
			throw new ParamParserException("unknown resolve type");
		}
		return resolver;
	}
	
	/**	
	 * 	获取请求类型
	 * 	
	 * 	@author Pan
	 * 	@return	HttpContentType
	 */
	public HttpContentType getContentType() {
		return contentType;
	}

	/**	
	 * 	设置请求类型
	 * 	
	 * 	@author Pan
	 * 	@param 	contentType	请求类型
	 */
	void setContentType(HttpContentType contentType) {
		this.contentType = contentType;
	}

	/**	
	 * 	获取解析器
	 * 	
	 * 	@author Pan
	 * 	@return	RequestParamResolverStrategy
	 */
	public RequestParamResolverStrategy<?> getValue() {
		return value;
	}

	/**	
	 * 	设置解析器
	 * 	
	 * 	@author Pan
	 * 	@param 	value	解析器
	 */
	void setValue(RequestParamResolverStrategy<?> value) {
		this.value = value;
	}
}
