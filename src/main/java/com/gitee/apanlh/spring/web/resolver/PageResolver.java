package com.gitee.apanlh.spring.web.resolver;

import com.gitee.apanlh.web.model.Pager;
import com.gitee.apanlh.web.model.RequestDataType;
import com.gitee.apanlh.web.model.vo.RequestVO;
import jakarta.servlet.http.HttpServletRequest;

/**	
 * 	分页参数解析器
 * 	
 * 	@author Pan
 */
class PageResolver implements RequestParamResolverStrategy<Pager> {
	
	/** KEY */
	private static final String PAGE_KEY = "page";
	private static final String LIMIT_KEY = "limit";
	/** 默认每页总数 */
	private static final int DEF_LIMIT = 10;
	
	/** 接收参数对象 */
	private RequestVO params;
	
	/**
	 * 	默认构造函数
	 * 	@author Pan
	 */
	PageResolver() {
		
	}
	
	/**
	 * 	构造函数-自定义解析对象
	 * 	@author Pan
	 */
	public PageResolver(RequestVO params) {
		this.params = params;
	}
	
	@Override
	public Pager resolve() {
		Pager page = params.getPage();
		parseParam(page);
		return page;
	}
	
	@Override
	public Pager resolve(HttpServletRequest request, RequestVO params, RequestDataType dataType) {
		return null;
	}
	
	/**	
	 * 	解析分页参数
	 * 	
	 * 	@author Pan
	 * 	@param 	pager	分页对象
	 */
	private void parseParam(Pager pager) {
		if (params.getParams() == null) {
			pager.setHasEmpty(true);
			return ;
		}

		Integer page = params.getInt(PAGE_KEY);
		Integer limit = params.getInt(LIMIT_KEY);
        if (page == null) {
        	return ;
        }
        if (limit == null) {
        	limit = DEF_LIMIT;
        }
        pager.setStartRow((page - 1) * limit);
        pager.setEndRow(page * limit);
        pager.setPage(page);
        pager.setLimit(limit);	
        pager.setHasEmpty(false);
	}
}
