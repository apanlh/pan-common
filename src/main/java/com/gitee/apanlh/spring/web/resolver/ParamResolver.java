package com.gitee.apanlh.spring.web.resolver;

import com.gitee.apanlh.web.content.RequestContentTypeContext;
import com.gitee.apanlh.web.http.HttpContentType;
import com.gitee.apanlh.web.model.Pager;
import com.gitee.apanlh.web.model.RequestDataType;
import com.gitee.apanlh.web.model.RequestHeader;
import com.gitee.apanlh.web.model.vo.RequestVO;
import com.gitee.apanlh.web.util.ServletUtils;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 * 	参数解析器
 * 
 * 	@author Pan
 */
class ParamResolver implements RequestParamResolverStrategy<Object> {
	
	/** Request对象 */
	private HttpServletRequest httpRequest;
	/** 实际Request */
	private NativeWebRequest webRequest;
	/** 返回参数 */ 
	private RequestVO params;
	/** 请求类型 */
	private HttpContentType contentType;
	/** 数据类型 */
	private RequestDataType dataType;
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	ParamResolver() {
		super();
	}

	/**		
	 * 	构造函数-初始化参数
	 * 	
	 * 	@author Pan
	 * 	@param	webRequest 	NativeWebRequest对象
	 */
	public ParamResolver(NativeWebRequest webRequest) {
		this.webRequest = webRequest;
		this.httpRequest = getHttpRequest();
		this.contentType = RequestContentTypeContext.getType(httpRequest);
		this.params = new RequestVO(RequestHeader.create(ServletUtils.getHeaders()), RequestDataType.create(), new Pager());
		this.dataType = params.getDataType();
	}

	@Override
	public Object resolve() {
		return ParamResolverEnum.get(contentType).resolve(httpRequest, params, dataType);
	}
	
	@Override
	public Object resolve(HttpServletRequest request, RequestVO params, RequestDataType dataType) {
		return null;
	}
	
	/**	
	 * 	获取HttpServletRequest对象
	 * 	
	 * 	@author Pan
	 * 	@return	HttpServletRequest
	 */
	protected HttpServletRequest getHttpRequest() {
		return webRequest.getNativeRequest(HttpServletRequest.class);
	}
	
	/**	
	 * 	获取MultipartHttpServletRequest对象
	 * 	
	 * 	@author Pan
	 * 	@return	MultipartHttpServletRequest
	 */
	protected MultipartHttpServletRequest getMultipartHttpRequest() {
		return webRequest.getNativeRequest(MultipartHttpServletRequest.class);
	}
}
