package com.gitee.apanlh.spring.web.resolver;

/**
 * 	GET解析器
 * 
 * 	@author Pan
 */
class GetResolver extends FormResolver {
	
	/**
	 * 	默认构造函数
	 * 
	 * 	@author Pan
	 */
	GetResolver() {
		super();
	}
	
}
