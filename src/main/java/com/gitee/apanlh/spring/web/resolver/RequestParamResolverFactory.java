package com.gitee.apanlh.spring.web.resolver;


import com.gitee.apanlh.web.model.vo.RequestVO;
import org.springframework.web.context.request.NativeWebRequest;

/**	
 * 	请求参数解析工厂类
 * 
 * 	@author Pan
 */
public class RequestParamResolverFactory {
	
	/**
	 * 	默认构造函数
	 * 
	 * 	@author Pan
	 */
	private RequestParamResolverFactory() {
		//	不允许外部实例
		super();
	}

	/**	
	 * 	创建解析器
	 * 	
	 * 	@author Pan
	 * 	@param 	webRequest WebRequest接口
	 * 	@return	ParamResolver
	 */
	public static ParamResolver createResolver(NativeWebRequest webRequest) {
		return new ParamResolver(webRequest);
	}

	/**	
	 * 	创建分页解析器
	 * 	
	 * 	@author Pan
	 * 	@param 	requestVO	请求VO对象
	 * 	@return PageResolver
	 */
	public static PageResolver createPageResolver(RequestVO requestVO) {
		return new PageResolver(requestVO);
	}
}
