package com.gitee.apanlh.spring.web.resolver;


import com.gitee.apanlh.annotation.viewresolver.ResponseAuthParam;
import com.gitee.apanlh.web.model.vo.RequestVO;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.core.MethodParameter;

/**	
 * 	RequestAttribute解析器
 * 	<br>用于添加RequestAttribute值
 * 
 * 	@author Pan
 */
class AttributeResolver {
	
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	AttributeResolver() {
		super();
	}
	
	/**	
	 * 	返回参数
	 * 	
	 * 	@author Pan
	 * 	@param 	parameter		MethodParameter对象
	 * 	@param 	httpRequest		HttpServletRequest对象
	 * 	@param 	requestVo		RequestVO对象
	 */
	public static void arguments(MethodParameter parameter, HttpServletRequest httpRequest, RequestVO requestVo) {
		if (parameter.hasMethodAnnotation(ResponseAuthParam.class)) {
			//	默认配置微信
			ResponseAuthResolver.wechatAuthResp(httpRequest, requestVo);
		}
	}
}
