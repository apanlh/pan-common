package com.gitee.apanlh.spring.web.resolver;

import com.gitee.apanlh.annotation.viewresolver.RequestParamVo;
import com.gitee.apanlh.web.model.vo.RequestVO;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

/**
 * 	重写参数解析器
 * 
 * 	@author Pan
 */
public class RequestResolver implements HandlerMethodArgumentResolver {

	/**
	 * 	用于判定是否需要处理该参数分解，返回true为需要，并会去调用下面的方法resolveArgument
	 * 
	 * 	@author Pan 
	 */
	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return parameter.hasParameterAnnotation(RequestParamVo.class);
	}
	
	/**
	 * 	传统请求参数解析
	 * 
	 * 	@author Pan
	 */
	@Override
	public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) {
		//	创建解析器
		ParamResolver paramResolver = RequestParamResolverFactory.createResolver(webRequest);
		Object resolve = paramResolver.resolve();
		
		if (resolve instanceof RequestVO) {
			RequestParamResolverFactory.createPageResolver((RequestVO) resolve).resolve();
			AttributeResolver.arguments(parameter, webRequest.getNativeRequest(HttpServletRequest.class), (RequestVO) resolve);
		}
		return resolve;
	}
}
