package com.gitee.apanlh.spring.web.resolver;

import com.gitee.apanlh.util.reflection.ClassConvertUtils;
import com.gitee.apanlh.web.model.vo.RequestVO;
import jakarta.servlet.http.HttpServletRequest;

import java.util.Map;

/**	
 * 	统一认证返回
 * 
 * 	@author Pan
 */
class ResponseAuthResolver {
	
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private ResponseAuthResolver() {
		//	不允许外部实例
		super();
	}
	
	/**	
	 * 	微信返回openid
	 * 	
	 * 	@author Pan
	 * 	@param 	httpRequest	HttpServletRequest对象
	 * 	@param 	requestVo	请求解析对象
	 */
	public static void wechatAuthResp(HttpServletRequest httpRequest, RequestVO requestVo) {
		Map<String, String> requestMap = ClassConvertUtils.castMapStr(requestVo.getParams());
		
		httpRequest.setAttribute("openid", requestMap.get("openid"));
		httpRequest.setAttribute("token", requestMap.get("token"));
		httpRequest.setAttribute("pubkeyHex", requestMap.get("pubkeyHex"));
	}
}
