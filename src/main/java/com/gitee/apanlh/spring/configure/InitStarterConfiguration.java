package com.gitee.apanlh.spring.configure;

/**	
 * 	启动初始化装配
 * 
 * 	@author Pan
 */
public class InitStarterConfiguration {
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	public InitStarterConfiguration() {
		super();
	}
	
}
