package com.gitee.apanlh.spring.configure;

import org.springframework.stereotype.Component;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;


/**
 * 	WebMVC配置
 * 	
 * 	@author Pan
 */
@Component
public class WebConfiguration implements WebMvcConfigurer {
	
	/**
	 * 	静态资源放行
	 * 	<br>添加静态资源访问({@code registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");})
	 *
	 * 	@author Pan
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		WebMvcConfigurer.super.addResourceHandlers(registry);
	}
	
	/**
	 * 	扫描自定义注解
	 *
	 * 	@author Pan
	 */
	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
		RequestParamConfiguration requestParamConfiguration = new RequestParamConfiguration();
		requestParamConfiguration.addArgumentResolvers(resolvers);
		WebMvcConfigurer.super.addArgumentResolvers(resolvers);
	}
}
