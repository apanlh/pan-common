package com.gitee.apanlh.spring.configure;

import com.gitee.apanlh.spring.web.resolver.RequestResolver;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**	
 * 	请求参数格式化
 * 	<br>需要手动注入至Spring中
 * 	
 * 	@author Pan
 */
class RequestParamConfiguration implements WebMvcConfigurer {
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	public RequestParamConfiguration() {
		super();
	}
	
	/**
	 * 	扫描自定义注解
	 * 	@author Pan
	 */
	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
		resolvers.add(new RequestResolver());
	}
}
