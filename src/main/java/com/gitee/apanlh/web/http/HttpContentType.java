package com.gitee.apanlh.web.http;

import com.gitee.apanlh.util.base.ArrayUtils;
import com.gitee.apanlh.util.base.StringUtils;
import com.gitee.apanlh.util.valid.ValidParam;

/**
 * 	请求类型枚举
 *  <br>列举了常见的 HTTP Content-Type
 * 	@author Pan
 */
public enum HttpContentType {
	
	/** 未知类型 */
	UNKNOWN("unknown"),
	/** 特殊GET类型 */
	GET("get"),
	/** JSON 格式数据 */	
	APPLICATION_JSON("application/json"),
	/** JSON类型 UTF-8 */
	APPLICATION_JSON_UTF8("application/json;charset=UTF-8"),
	/** FORM表单数据，{@code 如：key1=value1&key2=value2} */
	APPLICATION_FORM_URLENCODED("application/x-www-form-urlencoded"),
	/** 文件流 */
	APPLICATION_OCTET_STREAM("application/octet-stream"),
	/** XML 格式数据 */	
	APPLICATION_XML("application/xml"),
	/** 压缩包类型 */
	APPLICATION_ZIP("application/zip"),
	/** javascript类型 */ 
	APPLICATION_JAVASCRIPT("application/javascript"),
	/** form-data类型 多媒体数据，如上传文件等 */
	MULTIPART_FORM_DATA("multipart/form-data"),
	/** JPEG 图片 */
	IMAGE_JPEG("image/jpeg"),
	/** PNG 图片 */
	IMAGE_PNG("image/png"),
	/** GIF 图片 */
	IMAGE_GIF("image/gif"),
	/** 自定义图片格式 */
	IMAGE_FORMAT("image/${format}"),
	/** text文本类型 普通文本数据 */
	TEXT_PLAIN("text/plain"),
	/** text文本类型 HTML格式数据 */
	TEXT_HTML("text/html"),
	/** text文本类型 XML格式数据 */
	TEXT_XML("text/xml");
	
	/** 默认请求类型名称 */
	public static final String CONTENT_TYPE = "Content-Type";
	/** 默认请求类型名称 */
	public static final String CONTENT_TYPE_LINE = "Content-Type: ";
	
	/** 获取类型 */
	private String type;
	
	/**
	 * 	构造函数-自定义类型
	 * 	
	 * 	@author Pan
	 * 	@param 	type	类型
	 */
	HttpContentType(String type) {
		this.type = type;
	}

	/**
	 * 	获取类型
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getType() {
		return type;
	}
	
	/**	
	 * 	自定义格式
	 * 	<br>仅支持存在format标识的枚举类
	 * 	
	 * 	@author Pan
	 * 	@param 	contentType	HTTP请求类型枚举
	 * 	@param 	content		替换内容
	 * 	@return	String
	 */
	public static String format(HttpContentType contentType, String content) {
		return StringUtils.replace(contentType.getType(), "${format}", content);
	}
	
	/**	
	 * 	指定枚举类尾部添加值
	 * 	<br>自动添加分隔符(;)
	 * 	<pre>
	 * 	{@code 如：application/json->application/json;charset=UTF-8}
	 * 	</pre>
	 * 
	 * 	@author Pan
	 * 	@param 	contentType	HTTP请求类型枚举
	 * 	@param 	value		替换内容
	 * 	@return	String
	 */
	public static String append(HttpContentType contentType, String... value) {
		return StringUtils.append(contentType.getType(), "; ", ArrayUtils.join(value, "; "));
	}
	
	/**	
	 * 	前缀指定添加Content-Type:	
	 * 	<br>指定枚举类尾部添加值
	 * 	<br>自动添加分隔符(;)
	 * 	<pre>
	 * 	{@code 如：application/json-> Content-Type: application/json; charset=UTF-8}
	 * 	</pre>
	 * 
	 * 	@author Pan
	 * 	@param 	contentType	HTTP请求类型枚举
	 * 	@param 	value		替换内容
	 * 	@return	String
	 */
	public static String appendContentType(HttpContentType contentType, String... value) {
		if (ValidParam.isEmpty(value)) {
			return StringUtils.append(CONTENT_TYPE_LINE, contentType.getType());
		}
		return StringUtils.append(CONTENT_TYPE_LINE, contentType.getType(), "; ", ArrayUtils.join(value, "; "));
	}
}
