package com.gitee.apanlh.web.http;

import com.gitee.apanlh.util.base.MapUtils;

import java.util.Map;

/**
 * 	HttpRequest方法
 * 
 * 	@author Pan
 */
public enum HttpMethod {
	
	GET, 
	HEAD, 
	POST, 
	PUT, 
	PATCH, 
	DELETE, 
	OPTIONS, 
	TRACE;
	
	/** HTTP请求方法 */
	static final Map<HttpMethod, String> REQUEST_METHOD_MAP = MapUtils.newEnumMap(HttpMethod.class, HttpMethod.values());
	
	/**	
	 * 	获取Request方法名
	 * 	
	 * 	@author Pan
	 * 	@param 	requestMethod	HTTP枚举
	 * 	@return	String
	 */
	public static String getRequestMethodName(HttpMethod requestMethod) {
		return REQUEST_METHOD_MAP.get(requestMethod);
	}
}
