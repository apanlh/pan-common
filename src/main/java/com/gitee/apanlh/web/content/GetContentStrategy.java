package com.gitee.apanlh.web.content;

import com.gitee.apanlh.util.base.StringUtils;
import com.gitee.apanlh.web.http.HttpContentType;
import com.gitee.apanlh.web.http.HttpMethod;
import com.gitee.apanlh.web.util.ServletUtils;

/**	
 * 	GET请求类型
 * 
 * 	@author Pan
 */
class GetContentStrategy implements RequestContentStrategy {

	@Override
	public HttpContentType getContent() {
		return HttpContentType.GET;
	}

	@Override
	public String getContentStr() {
		return HttpContentType.GET.getType();
	}

	@Override
	public String getContentStr(String contentType) {
		//	验证是否为GET类型
		if (StringUtils.compare(ServletUtils.getMethod(), HttpMethod.GET) || contentType == null) {
			return getContentStr();
		}
		return null;
	}
}
