package com.gitee.apanlh.web.content;

import com.gitee.apanlh.web.http.HttpContentType;

/**	
 * 	文件流类型
 * 
 * 	@author Pan
 */
class FileStreamContentStrategy implements RequestContentStrategy {

	@Override
	public HttpContentType getContent() {
		return HttpContentType.APPLICATION_OCTET_STREAM;
	}

	@Override
	public String getContentStr() {
		//	默认为文件流
		return HttpContentType.APPLICATION_OCTET_STREAM.getType();
	}
	
	/**	
	 * 	获取ZIP类型
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getContentZipStr() {
		 return HttpContentType.APPLICATION_ZIP.getType();
	}
	
	/**	
	 * 	获取图片JPEG类型
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getContentImageJpegStr() {
		return HttpContentType.IMAGE_JPEG.getType();
	}
	
	@Override
	public String getContentStr(String contentType) {
		if (getContentStr().equalsIgnoreCase(contentType)) {
			return getContentStr();
		}
		if (getContentZipStr().equalsIgnoreCase(contentType)) {
			return getContentZipStr();
		}
		if (getContentImageJpegStr().equalsIgnoreCase(contentType)) {
			return getContentImageJpegStr();
		}
		return null;
	}
}
