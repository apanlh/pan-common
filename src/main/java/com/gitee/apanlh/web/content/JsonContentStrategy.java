package com.gitee.apanlh.web.content;

import com.gitee.apanlh.web.http.HttpContentType;

/**	
 * 	JSON数据类型
 * 
 * 	@author Pan
 */
class JsonContentStrategy implements RequestContentStrategy {

	@Override
	public HttpContentType getContent() {
		return HttpContentType.APPLICATION_JSON;
	}

	@Override
	public String getContentStr() {
		return HttpContentType.APPLICATION_JSON.getType();
	}
	
	/**	
	 * 	获取JSON-UTF8类型
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getContentJsonUtf8Str() {
		return HttpContentType.APPLICATION_JSON_UTF8.getType();
	}
	
	@Override
	public String getContentStr(String contentType) {
		if (getContentStr().equalsIgnoreCase(contentType) || getContentJsonUtf8Str().equalsIgnoreCase(contentType)) {
			return getContentStr();
		}
		return null;
	}
}
