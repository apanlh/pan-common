package com.gitee.apanlh.web.content;

import com.gitee.apanlh.web.http.HttpContentType;

/**	
 * 	FORM表单类型
 * 
 * 	@author Pan
 */
class FormContentStrategy implements RequestContentStrategy {

	@Override
	public HttpContentType getContent() {
		return HttpContentType.APPLICATION_FORM_URLENCODED;
	}

	@Override
	public String getContentStr() {
		return HttpContentType.APPLICATION_FORM_URLENCODED.getType();
	}

	@Override
	public String getContentStr(String contentType) {
		if (getContentStr().equalsIgnoreCase(contentType)) {
			return getContentStr();
		}
		return null;
	}
}
