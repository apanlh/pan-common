package com.gitee.apanlh.web.content;

import com.gitee.apanlh.web.http.HttpContentType;

/**	
 * 	Request-Content请求头
 * 
 * 	@author Pan
 */
public interface RequestContentStrategy {
	
	/**	
	 * 	获取枚举类型
	 * 
	 * 	@author Pan
	 * 	@return	HttpContentType
	 */
	HttpContentType getContent();
	
	/**	
	 * 	获取默认类型字符串
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	String getContentStr();
	
	/**	
	 * 	获取类型字符串
	 * 	<br>根据类型验证值类型
	 * 	<br>例如TEXT类型分别有HTMl、XML、TEXT、JavaScript等
	 * 	
	 * 	@author Pan
	 * 	@param	contentType	内容类型
	 * 	@return	String
	 */
	String getContentStr(String contentType);
}
