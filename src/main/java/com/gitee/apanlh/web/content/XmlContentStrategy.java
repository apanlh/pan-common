package com.gitee.apanlh.web.content;

import com.gitee.apanlh.web.http.HttpContentType;

/**	
 * 	只包含传输时XML类型非文本
 * 
 * 	@author Pan
 */
class XmlContentStrategy implements RequestContentStrategy {

	@Override
	public HttpContentType getContent() {
		return HttpContentType.APPLICATION_XML;
	}

	@Override
	public String getContentStr() {
		return HttpContentType.APPLICATION_XML.getType();
	}

	@Override
	public String getContentStr(String contentType) {
		if (getContentStr().equalsIgnoreCase(contentType)) {
			return getContentStr();
		}
		return null;
	}
}
