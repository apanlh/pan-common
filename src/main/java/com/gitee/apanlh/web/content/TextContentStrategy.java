package com.gitee.apanlh.web.content;

import com.gitee.apanlh.web.http.HttpContentType;

/**	
 * 	TEXT文本数据类型
 * 	<br>包含文本、HTML、XML类型
 * 
 * 	@author Pan
 */
class TextContentStrategy implements RequestContentStrategy {

	@Override
	public HttpContentType getContent() {
		return HttpContentType.TEXT_PLAIN;
	}
	
	@Override
	public String getContentStr() {
		return HttpContentType.TEXT_PLAIN.getType();
	}
	
	/**	
	 * 	获取text文本类型 HTML数据 
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getContentHtmlStr() {
		return HttpContentType.TEXT_HTML.getType();
	}
	
	/**	
	 * 	获取text文本类型 XML格式数据 
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getContentXmlStr() {
		return HttpContentType.TEXT_XML.getType();
	}
	
	/**	
	 * 	获取脚本javascript类型 
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getContentJavascriptStr() {
		return HttpContentType.APPLICATION_JAVASCRIPT.getType();
	}

	@Override
	public String getContentStr(String contentType) {
		if (getContentStr().equalsIgnoreCase(contentType)) {
			return getContentStr();
		}
		if (getContentHtmlStr().equalsIgnoreCase(contentType)) {
			return getContentHtmlStr();
		}
		if (getContentXmlStr().equalsIgnoreCase(contentType)) {
			return getContentXmlStr();
		}
		if (getContentJavascriptStr().equalsIgnoreCase(contentType)) {
			return getContentJavascriptStr();
		}
		return null;
	}
}
