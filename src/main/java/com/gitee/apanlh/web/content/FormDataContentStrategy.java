package com.gitee.apanlh.web.content;

import com.gitee.apanlh.web.http.HttpContentType;

/**	
 * 	FormData数据类型
 * 
 * 	@author Pan
 */
class FormDataContentStrategy implements RequestContentStrategy {

	@Override
	public HttpContentType getContent() {
		return HttpContentType.MULTIPART_FORM_DATA;
	}

	@Override
	public String getContentStr() {
		return HttpContentType.MULTIPART_FORM_DATA.getType();
	}

	@Override
	public String getContentStr(String contentType) {
		if (contentType == null || !contentType.startsWith(getContentStr())) {
			return null;
		}
		return getContentStr();
	}
}
