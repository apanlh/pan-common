package com.gitee.apanlh.web.config;

import com.gitee.apanlh.util.base.StringUtils;
import jakarta.servlet.http.HttpServletRequest;

/**
 * 	放行策略
 * 	@author Pan
 */
public class UriFilterConfig {

	/** 默认放行的白名单配置 */
	static final String[] DEFAULT_FILTER = {".js", ".css", ".png", ".ttf", ".gif", ".jpg", ".ico", ".woff"};
	static final String[] DEFAULT_WEB_CONFIG = {"/**/*.js", "/**/*.css", "/**/*.png", "/**/*.ttf", "/**/*.gif", "/**/*.jpg", "/**/*.ico", "/**/*.woff"};

	/**
	 * 	构造函数
	 *
	 * 	@author Pan
	 */
	private UriFilterConfig() {
		//	不允许外部实例
		super();
	}

	/**
	 * 	采用默认白名单放行配置
	 * 	重载方法
	 *
	 * 	@author Pan
	 * 	@param  request	HttpServletRequest
	 * 	@return	boolean true为已找到匹配项
	 */
	public static boolean filterUri(HttpServletRequest request) {
		return StringUtils.contains(request.getRequestURI(), DEFAULT_FILTER);
	}

	/**
	 * 	采用默认白名单放行配置
	 * 	值相等模式
	 *
	 * 	@author Pan
	 * 	@param  request		HttpServletRequest
	 * 	@param  filterUrl	过滤字符串
	 * 	@return	boolean		true为已找到匹配项
	 */
	public static boolean filterUri(HttpServletRequest request, String... filterUrl) {
		if (filterUri(request)) {
			return true;
		}
		return StringUtils.contains(filterUrl, StringUtils.cleanSpecial(request.getRequestURI()));
	}

	/**
	 * 	匹配放行URI
	 * 	<br>包含模式
	 * 	<br>例如.html等
	 * 	@author Pan
	 * 	@param 	request	HttpServletRequest
	 * 	@param 	matchPatternUri		url
	 * 	@return	boolean
	 */
	public static boolean filterMatchPatternUri(HttpServletRequest request, String matchPatternUri) {
		return StringUtils.contains(request.getRequestURI(), matchPatternUri);
	}

	/**
	 * 	采用默认白名单放行配置
	 *
	 * 	@author Pan
	 * 	@param  url	路径
	 * 	@return	boolean	true 为已找到匹配项
	 */
	public static boolean filterUri(String url) {
		return StringUtils.contains(url, DEFAULT_FILTER);
	}

	/**
	 * 	第一次过滤采用默认放行配置
	 * 	如果第一次未匹配到放行策略则继续匹配传递的放行策略
	 *
	 * 	@author Pan
	 * 	@param  url	路径
	 * 	@param  filterUrl 过滤url String[] 或 String
	 * 	@return	boolean	true 为已找到匹配项
	 */
	public static boolean filterUri(String url, String... filterUrl) {
		if (filterUri(url)) {
			return true;
		}
		return StringUtils.contains(url, filterUrl);
	}

	/**
	 * 	用于Filter过滤
	 *
	 * 	@author Pan
	 * 	@return	String[]
	 */
	public static String[] getDefaultFilter() {
		return DEFAULT_FILTER;
	}

	/**
	 * 	获取默认SpringMvc WebConfig放行策略
	 *
	 * 	@author Pan
	 * 	@return	String[]
	 */
	public static String[] getDefaultWebConfig() {
		return DEFAULT_WEB_CONFIG;
	}
}
