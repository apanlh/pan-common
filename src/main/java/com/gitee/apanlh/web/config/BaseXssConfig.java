package com.gitee.apanlh.web.config;

import com.gitee.apanlh.util.base.CollUtils;
import com.gitee.apanlh.util.encode.StrEncodeUtils;
import com.gitee.apanlh.util.valid.RegexUtils;

import java.util.List;

/**	
 * 	默认XSS过滤配置
 * 	@author Pan
 */
public class BaseXssConfig {
	
	private static final List<String> XSS_CONFIG = CollUtils.newArrayList(
		"'",
		"\'",
		"?",
		"=",
		"“",
		"\"",
		"/",
		"<",
		">",
		"&lt;",
		"&gt;",
		"&quot;",
		"&#x27;",
		"&#x2F;",
		"&amp;",
		"(",
		"\\(",
		")",
		"\\)",
		"img",
		"src",
		"imgsrc",
		"java",
		"script",
		"prompt",
		"eval",
		"document",
		"write",
		"iframe",
		"window",
		"link",
		"style",
		"NEVER",
		"div",
		"href",
		"http",
		"https",
		"session",
		"cookie",
		"and",
		"or",
		"where",
		"svg",
		"onload"
	);
	
	/**
	 * 	构造函数
	 * 	@author Pan
	 */
	private BaseXssConfig() {
		//	不允许外部实例
		super();
	}
	
	/**	
	 * 	检测是否包含XSS攻击范围
	 * 	
	 * 	@author Pan
	 * 	@param  value	值
	 * 	@return	true为包含XSS攻击
	 */
	public static boolean isXss(String value) {
		if (value == null) {
			return false;
		}
		
		try {
			if (value.startsWith("%")) {
				value = StrEncodeUtils.urlDecode(value);
			}
		} catch (Exception e) {
			return true;
		}

		for (int i = 0, len = XSS_CONFIG.size(); i < len; i++) {
			String config = XSS_CONFIG.get(i);
			//	验证是否为邮箱 
			if (value.contains(config)) {
				return rule(config, value);
			}
		}
		return false;
	}
	
	/**	
	 * 	规则匹配(true为XSS攻击, false为正常)
	 * 	<br>默认匹配规则到就为true
	 * 	
	 * 	@author Pan
	 * 	@param 	xssConfig	xss过滤配置
	 * 	@param 	value		传递值
	 * 	@return	boolean
	 */
	private static boolean rule(String xssConfig, String value) {
		//	此条废弃
		if ("com".equals(xssConfig) || "cn".equals(xssConfig)) {
			return !(RegexUtils.isEmail(value));
		}
		return true;
	}
}
