package com.gitee.apanlh.web.model.dto;

/**	
 * 	服务之间传递-通用、增删改返回影响行数
 * 	#mark 增加说明
 * 
 * 	@author Pan
 */
public class ResultRowsRDTO {
	
	public ResultRowsRDTO() {
		super();
	}
	
	public ResultRowsRDTO(Integer rows) {
		super();
		this.rows = rows;
	}
	
	public ResultRowsRDTO(Integer rows, String flowMark) {
		super();
		this.rows = rows;
		this.flowMark = flowMark;
	}

	/** 影响行数 */ 
	private Integer rows;
	
	/** 自定义流转状态标记 */
	private String flowMark;

	public Integer getRows() {
		return rows;
	}

	public void setRows(Integer rows) {
		this.rows = rows;
	}

	public String getFlowMark() {
		return flowMark;
	}

	public void setFlowMark(String flowMark) {
		this.flowMark = flowMark;
	}
	
	/**	
	 * 	验证增、删、改返回的影响函数
	 * 	
	 * 	@author Pan
	 * 	@return	boolean
	 */
	public boolean hasSuc() {
		if (rows == null) {
			return false;
		}
		return rows > 0;
	}
	
	/**	
	 * 	验证count是否大于0
	 * 	
	 * 	@author Pan
	 * 	@return	boolean
	 */
	public boolean hasZero() {
		if (rows == null) {
			return false;
		}
		return rows == 0;
	}
	
	@Override
	public String toString() {
		return "ResultRowsRDTO [rows=" + rows + ", flowMark=" + flowMark + "]";
	}
}
