package com.gitee.apanlh.web.model;

import java.io.Serializable;

/**
 * 	请求数据类型
 * 
 * 	@author Pan
 */
public class RequestDataType implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/** 请求类型标识 */
	private boolean hasForm;
	private boolean hasJson;
	private boolean hasFile;
	private boolean hasFormData;
	private boolean hasXml;
	private boolean hasText;
	
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private RequestDataType() {
		//	不允许外部实例
		super();
	}
	
	public boolean hasForm() {
		return hasForm;
	}

	public void setHasForm(boolean hasForm) {
		this.hasForm = hasForm;
	}

	public boolean hasJson() {
		return hasJson;
	}

	public void setHasJson(boolean hasJson) {
		this.hasJson = hasJson;
	}

	public boolean hasFile() {
		return hasFile;
	}

	public void setHasFile(boolean hasFile) {
		this.hasFile = hasFile;
	}

	public boolean hasFormData() {
		return hasFormData;
	}

	public void setHasFormData(boolean hasFormData) {
		this.hasFormData = hasFormData;
	}

	public boolean hasXml() {
		return hasXml;
	}

	public void setHasXml(boolean hasXml) {
		this.hasXml = hasXml;
	}

	public boolean hasText() {
		return hasText;
	}

	public void setHasText(boolean hasText) {
		this.hasText = hasText;
	}

	/**	
	 * 	创建请求类型
	 * 	
	 * 	@author Pan
	 * 	@return RequestDataType
	 */
	public static RequestDataType create() {
		return new RequestDataType();
	}
}
