package com.gitee.apanlh.web.model.dto;

import com.gitee.apanlh.util.base.MapUtils;
import com.gitee.apanlh.util.reflection.ClassConvertUtils;

import java.util.List;
import java.util.Map;

/**
 *  用于消费者和提供者之间的数据交互实体类
 *
 *  @author Pan
 */
public class ResultDTO {

    private Map<String, Object> resultData;

    public ResultDTO() {
        super();
    }

    public ResultDTO(Map<String, Object> resultData) {
        this.resultData = resultData;
    }

    public ResultDTO(List<?> list) {
        this.resultData = MapUtils.newHashMap(map -> map.put("list", list), 16);
    }

    public ResultDTO(List<?> list, int page, int limit) {
        this.resultData = MapUtils.newHashMap(map -> {
            map.put("list", list);
            map.put("page", page);
            map.put("limit", limit);
        }, 16);
    }

    public <T> ResultDTO(T bean) {
        this.resultData = ClassConvertUtils.toMap(bean);
    }

    public Map<String, Object> getResultData() {
        return resultData;
    }

    public void setResultData(Map<String, Object> resultData) {
        this.resultData = resultData;
    }

    @Override
    public String toString() {
        return "ResultDTO{" +
                "resultData=" + resultData +
                '}';
    }
}
