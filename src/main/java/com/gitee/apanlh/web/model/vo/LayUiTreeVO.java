package com.gitee.apanlh.web.model.vo;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * 	用于LAY-UI的 树形表单实体
 * 	
 * 	@author Pan
 */
public class LayUiTreeVO {

	/**
	 * 	默认构造函数
	 */
	public LayUiTreeVO() {
		super();
	}

	/**
	 *	构造函数
	 *
	 * 	@param title  标题
	 * 	@param id	  id
	 * 	@param code   编码
	 * 	@param children 子集
	 * 	@param level  等级
	 * 	@param parentCode 父类code
	 * 	@param spread  是否展开
	 * 	@param url  url
	 */
	public LayUiTreeVO(String title, String id, String code, List<LayUiTreeVO> children, Integer level, String parentCode, String spread, String url) {
		super();
		this.title = title;
		this.id = id;
		this.code = code;
		this.children = children;
		this.level = level;
		this.parentCode = parentCode;
		this.spread = spread;
		this.url = url;
	}

	/**
	 *  构造函数
	 *
	 * 	@param title  标题
	 * 	@param id	  id
	 * 	@param code   编码
	 * 	@param children 子集
	 * 	@param level  等级
	 * 	@param parentCode 父类code
	 * 	@param spread  是否展开
	 * 	@param url  url
	 * 	@param icon 图标
	 */
	public LayUiTreeVO(String title, String id, String code, List<LayUiTreeVO> children, Integer level, String parentCode, String spread, String url, String icon) {
		super();
		this.title = title;
		this.id = id;
		this.code = code;
		this.children = children;
		this.level = level;
		this.parentCode = parentCode;
		this.spread = spread;
		this.url = url;
		this.icon = icon;
	}
	
	/** 名称 */
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String title;

    /** id */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String id;

    /** code */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String code;

    /** 子对象 */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<LayUiTreeVO> children;

    /** 等级 */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Integer level;

    /** 父类code */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String parentCode;

    /** 默认是否展开 */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String spread;
    
    /** 地址 */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String url;
    
    /** 图标 */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String icon;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<LayUiTreeVO> getChildren() {
		return children;
	}

	public void setChildren(List<LayUiTreeVO> children) {
		this.children = children;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	public String getSpread() {
		return spread;
	}

	public void setSpread(String spread) {
		this.spread = spread;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
}
