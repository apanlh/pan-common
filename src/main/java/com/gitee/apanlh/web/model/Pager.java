package com.gitee.apanlh.web.model;

import java.io.Serializable;

/**	
 * 	分页对象
 * 	@author Pan
 */
public class Pager implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/** 起始条数 */
	private int startRow;
	/** 结束条数 */
	private int endRow;
	/** 页码 */
	private int page;
	/** 每页条数 */
	private int limit;
	/** 数据总条数 */
	private int total;
	/** 总页码 */
	private int totalPages;
	/** 是否存在分页标识 */
	private boolean hasEmpty = true;
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	public Pager() {
		super();
	}
	
	/**	
	 * 	构造函数-接收页码，当前条数值
	 * 	
	 * 	@author Pan
	 * 	@param 	startRow	起始搜索
	 * 	@param 	endRow		结束搜索
	 * 	@param 	page		当前页码
	 * 	@param 	limit		页码总数
	 */
	public Pager(int startRow, int endRow, int page, int limit) {
		this.startRow = startRow;
		this.endRow = endRow;
		this.page = page;
		this.limit = limit;
	}
	
	public void setHasEmpty(boolean hasEmpty) {
		this.hasEmpty = hasEmpty;
	}

	public int getStartRow() {
		return startRow;
	}

	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}

	public int getEndRow() {
		return endRow;
	}

	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}
	
	public int getTotal() {
		return total;
	}

	public Pager setTotal(int total) {
		this.total = total;
		//	设置总页数
		if (!hasEmpty && totalPages == 0) {
			totalPages = total / limit;
			if (total % limit > 0) {
				totalPages++;
			}
		}
		return this;
	}
	
	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	@Override
	public String toString() {
		return "Pager [startRow=" + startRow + ", endRow=" + endRow + ", page=" + page + ", limit=" + limit + ", total="
				+ total + ", totalPages=" + totalPages + "]";
	}
}
