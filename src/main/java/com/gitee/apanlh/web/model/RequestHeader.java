package com.gitee.apanlh.web.model;

import com.gitee.apanlh.util.base.MapUtils;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**	
 * 	存放Header对象
 * 
 * 	@author Pan
 */
public class RequestHeader implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/** 请求Headers */
	private Map<String, String> headers;
	
	/**	
	 * 	默认构造-初始加载请求头
	 * 
	 * 	@author Pan
	 */
	public RequestHeader() {
		this.headers = MapUtils.newHashMap();
	}
	
	/**	
	 * 	构造函数-自定义请求头
	 * 
	 * 	@author Pan
	 * 	@param 	headers 请求头
	 */
	public RequestHeader(Map<String, String> headers) {
		this.headers = headers;
	}
	
	/**	
	 * 	获取Header值
	 * 	
	 * 	@author Pan
	 * 	@param 	key 键
	 * 	@return String
	 */
	public String get(String key) {
		return this.headers.get(key);
	}
	
	/**	
	 * 	获取所有请求头
	 * 	
	 * 	@author Pan
	 * 	@return Map
	 */
	public final Map<String, String> getHeaders() {
		return this.headers;
	}
	
	/**	
	 * 	转换为List
	 * 	
	 * 	@author Pan
	 * 	@return List
	 */
	public List<String> getValues() {
		return MapUtils.toArrayList(this.headers);
	}
	
	/**	
	 * 	是否存在多个key值
	 * 	<br>与关系
	 * 	
	 * 	@author Pan
	 * 	@param 	keys	一个或多个键
	 * 	@return boolean
	 */
	public boolean containsKeys(String... keys) {
		for (int i = 0; i < keys.length; i++) {
			if (!getHeaders().containsKey(keys[i])) {
				return false;
			}
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "headers = " + headers;
	}

	/**
	 * 	创建实例对象
	 * 	
	 * 	@author Pan
	 * 	@return RequestHeader
	 */
	public static RequestHeader create() {
		return new RequestHeader();
	}
	
	/**
	 * 	创建实例对象
	 * 	
	 * 	@author Pan
	 * 	@param	headers	Map
	 * 	@return RequestHeader
	 */
	public static RequestHeader create(Map<String, String> headers) {
		return new RequestHeader(headers);
	}
}
