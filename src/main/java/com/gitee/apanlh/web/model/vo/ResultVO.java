package com.gitee.apanlh.web.model.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.gitee.apanlh.web.model.Pager;
import com.gitee.apanlh.web.msg.ExcMessage;
import com.gitee.apanlh.web.msg.ResultCode;
import com.gitee.apanlh.web.msg.ResultMsg;

/**	
 * 	通用返回对象
 * 	<br>该类用于封装通用的返回结果，包含返回状态码、消息、数据和分页信息。
 * 	<br>并实现实现了Serializable接口，可进行序列化和反序列化。
  *	
 *	@author Pan
 */
public class ResultVO<T> {
	
	/** 返回状态码 */
	private Integer code;
	/** 消息 */
	private String msg;
	/** 数据 */
	@JsonInclude(value = Include.NON_EMPTY)
	private T data;
	/** 分页数据 */
	@JsonInclude(value=  Include.NON_EMPTY)
	private Pager pager;

	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	public ResultVO() {
		// 不允许外部构造
		super();
	}

	/**
	 * 	构造函数
	 * 	<br>自定义返回状态码、消息。
	 * 	
	 * 	@author Pan
	 *	@param 	code	返回状态码
	 * 	@param 	msg		消息
	 */
	public ResultVO(Integer code, String msg) {
		this.code = code;
		this.msg = msg;
	}
	
	/**
	 * 	构造函数
	 * 	<br>自定义返回状态码、消息、数据。
	 * 	
	 * 	@author Pan
	 *	@param 	code	返回状态码
	 * 	@param 	msg		消息
	 * 	@param 	data 	数据
	 */
	public ResultVO(Integer code, String msg, T data) {
		this.code = code;
		this.msg = msg;
		this.data = data;
	}
	
	/**
	 * 	构造函数
	 * 	<br>自定义返回状态码、消息、数据、分页信息。
	 * 	
	 * 	@author Pan
	 *	@param 	code	返回状态码
	 * 	@param 	msg		消息
	 * 	@param 	data 	数据
	 * 	@param 	pager 	分页信息
	 */
	public ResultVO(Integer code, String msg, T data, Pager pager) {
		this.code = code;
		this.msg = msg;
		this.data = data;
		this.pager = pager;
	}

	/**	
	 * 	获取返回状态码
	 * 	
	 * 	@author Pan
	 * 	@return Integer	返回状态码
	 */
	public Integer getCode() {
		return code;
	}

	/**	
	 * 	设置返回状态码
	 * 	
	 * 	@author Pan
	 * 	@param 	code	返回状态码
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**	
	 * 	获取返回消息
	 * 	
	 * 	@author Pan
	 * 	@return String	返回消息
	 */
	public String getMsg() {
		return msg;
	}

	/**	
	 * 	设置返回消息
	 * 	
	 * 	@author Pan
	 * 	@param 	msg	返回消息
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**	
	 * 	获取返回对象
	 * 	
	 * 	@author Pan
	 * 	@return T	返回对象
	 */
	public T getData() {
		return data;
	}

	/**	
	 * 	设置返回对象
	 * 	
	 * 	@author Pan
	 * 	@param 	data 返回对象
	 */
	public void setData(T data) {
		this.data = data;
	}

	/**	
	 * 	获取返回分页信息
	 * 	
	 * 	@author Pan
	 * 	@return Pager	返回分页信息
	 */
	public Pager getPager() {
		return pager;
	}

	/**	
	 * 	设置分页信息
	 * 	
	 * 	@author Pan
	 * 	@param 	pager 分页信息
	 */
	public void setPager(Pager pager) {
		this.pager = pager;
	}
	
	@Override
	public String toString() {
		return "ResultVO [code=" + code + ", msg=" + msg + ", data=" + data + ", pager=" + pager + "]";
	}
	
	/**	
	 * 	构建-成功对象
	 * 	<br>默认返回成功消息
	 * 	<br>默认返回成功状态
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>		数据类型
	 * 	@return ResultVO
	 */
	public static <T> ResultVO<T> suc() {
		return new ResultVO<>(ResultCode.SUC_CODE, ResultMsg.SUC_MSG);
	}
	
	/**	
	 * 	构建-成功对象
	 * 	<br>自定义返回消息
	 * 	<br>默认返回成功状态
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>		数据类型
	 * 	@param 	msg		返回消息
	 * 	@return ResultVO
	 */
	public static <T> ResultVO<T> suc(String msg) {
		return new ResultVO<>(ResultCode.SUC_CODE, msg);
	}
	
	/**
	 * 	构建-成功对象
	 * 	<br>自定义消息
	 * 	<br>自定义状态
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>		数据类型
	 *	@param 	code	返回状态码
	 * 	@param 	msg		消息
	 * 	@return ResultVO
	 */
	public static <T> ResultVO<T> suc(Integer code, String msg) {
		return new ResultVO<>(code, msg);
	}
	
	/**
	 * 	构建-成功对象
	 * 	<br>自定义消息
	 * 	<br>自定义状态
	 * 	<br>自定义返回对象
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>		数据类型
	 *	@param 	code	返回状态码
	 * 	@param 	msg		消息
	 * 	@param 	data	对象
	 * 	@return ResultVO
	 */
	public static <T> ResultVO<T> suc(Integer code, String msg, T data) {
		return new ResultVO<>(code, msg, data);
	}
	
	/**
	 * 	构建-成功对象
	 * 	<br>自定义消息
	 * 	<br>自定义状态
	 * 	<br>自定义返回对象
	 * 	<br>自定义分页信息
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>		数据类型
	 *	@param 	code	返回状态码
	 * 	@param 	msg		消息
	 * 	@param 	data	对象
	 * 	@param 	pager	分页信息
	 * 	@return ResultVO
	 */
	public static <T> ResultVO<T> suc(Integer code, String msg, T data, Pager pager) {
		return new ResultVO<>(code, msg, data, pager);
	}
	
	/**
	 * 	构建-成功对象
	 * 	<br>默认返回成功消息
	 * 	<br>默认返回成功状态
	 * 	<br>无分页信息
	 * 	<br>自定义返回对象
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>		数据类型
	 * 	@param 	data	对象
	 * 	@return ResultVO
	 */
	public static <T> ResultVO<T> suc(T data) {
		return new ResultVO<>(ResultCode.SUC_CODE, ResultMsg.SUC_MSG, data);
	}
	
	/**
	 * 	构建-成功对象
	 * 	<br>默认返回成功消息
	 * 	<br>默认返回成功状态
	 * 	<br>自定义分页信息
	 * 	<br>自定义返回对象
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>		数据类型
	 * 	@param 	data	对象
	 * 	@param 	pager	分页信息
	 * 	@return ResultVO
	 */
	public static <T> ResultVO<T> suc(T data, Pager pager) {
		return new ResultVO<>(ResultCode.SUC_CODE, ResultMsg.SUC_MSG, data, pager);
	}
	
	/**
	 * 	构建-成功对象
	 * 	<br>默认返回成功状态
	 * 	<br>自定义返回成功消息
	 * 	<br>无分页信息
	 * 	<br>自定义返回对象
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>		数据类型
	 * 	@param 	msg		消息
	 * 	@param 	data	对象
	 * 	@return ResultVO
	 */
	public static <T> ResultVO<T> suc(String msg, T data) {
		return new ResultVO<>(ResultCode.SUC_CODE, msg, data);
	}
	
	/**
	 * 	构建-成功对象
	 * 	<br>默认返回成功状态
	 * 	<br>自定义返回成功消息
	 * 	<br>自定义分页信息
	 * 	<br>自定义返回对象
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>		数据类型
	 * 	@param 	msg		消息
	 * 	@param 	data	对象
	 * 	@param 	pager	分页信息
	 * 	@return ResultVO
	 */
	public static <T> ResultVO<T> suc(String msg, T data, Pager pager) {
		return new ResultVO<>(ResultCode.SUC_CODE, msg, data, pager);
	}
	
	/**	
	 * 	构建-失败对象
	 * 	<br>默认返回失败消息
	 * 	<br>默认返回失败状态
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>		数据类型
	 * 	@return ResultVO
	 */
	public static <T> ResultVO<T> fail() {
		return new ResultVO<>(ResultCode.ERROR_CODE, ResultMsg.ERROR_MSG);
	}
	
	/**	
	 * 	构建-失败对象
	 * 	<br>自定义返回消息
	 * 	<br>默认返回失败状态
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>		数据类型
	 * 	@param 	msg		返回消息
	 * 	@return ResultVO
	 */
	public static <T> ResultVO<T> fail(String msg) {
		return new ResultVO<>(ResultCode.ERROR_CODE, msg);
	}
	
	/**
	 * 	构建-失败对象
	 * 	<br>默认返回失败状态
	 * 	<br>自定义返回失败消息
	 * 	<br>无分页信息
	 * 	<br>自定义返回对象
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>		数据类型
	 * 	@param 	msg		消息
	 * 	@param 	data	对象
	 * 	@return ResultVO
	 */
	public static <T> ResultVO<T> fail(String msg, T data) {
		return new ResultVO<>(ResultCode.ERROR_CODE, msg, data);
	}
	
	/**
	 * 	构建-失败对象
	 * 	<br>默认返回失败消息
	 * 	<br>默认返回失败状态
	 * 	<br>自定义返回对象
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>		数据类型
	 * 	@param 	data	对象
	 * 	@return ResultVO
	 */
	public static <T> ResultVO<T> fail(T data) {
		return new ResultVO<>(ResultCode.ERROR_CODE, ResultMsg.ERROR_MSG, data);
	}
	
	/**
	 * 	构建-失败对象
	 * 	<br>自定义消息
	 * 	<br>自定义状态
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>		数据类型
	 *	@param 	code	返回状态码
	 * 	@param 	msg		消息
	 * 	@return ResultVO
	 */
	public static <T> ResultVO<T> fail(Integer code, String msg) {
		return new ResultVO<>(code, msg);
	}
	
	/**
	 * 	构建-失败对象
	 * 	<br>自定义消息
	 * 	<br>自定义状态
	 * 	<br>自定义返回对象
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>		数据类型
	 *	@param 	code	返回状态码
	 * 	@param 	msg		消息
	 * 	@param 	data	对象
	 * 	@return ResultVO
	 */
	public static <T> ResultVO<T> fail(Integer code, String msg, T data) {
		return new ResultVO<>(code, msg, data);
	}
	
	/**
	 * 	构建-失败对象
	 * 	<br>自定义异常处理状态消息(包含了状态及消息)
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>			数据类型
	 *	@param 	excMessage	异常处理状态枚举
	 * 	@return ResultVO
	 */
	public static <T> ResultVO<T> fail(ExcMessage excMessage) {
		return new ResultVO<>(excMessage.getCode(), excMessage.getCause());
	}
}
