package com.gitee.apanlh.web.filter.xss;

import com.gitee.apanlh.util.base.IteratorUtils;
import com.gitee.apanlh.util.dataformat.JsonUtils;
import com.gitee.apanlh.util.log.Log;
import com.gitee.apanlh.util.reflection.ClassConvertUtils;
import com.gitee.apanlh.util.valid.ValidParam;
import com.gitee.apanlh.web.config.BaseXssConfig;
import com.gitee.apanlh.web.content.RequestContentTypeContext;
import com.gitee.apanlh.web.wrapper.BaseXssRequestWrapper;
import jakarta.servlet.http.HttpServletRequest;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**	
 * 	XSS验证
 * 	@author Pan
 */
public class BaseXssValid {
	
	/**
	 * 	构造函数
	 * 	@author Pan
	 */
	private BaseXssValid() {
		//	不允许外部实例
		super();
	}
	
	/**	
	 * 	检测是否包含XSS
	 * 	
	 * 	@author Pan
	 * 	@param  parameterMap	参数内容
	 * 	@return	boolean
	 */
	public static boolean checkXss(Map<String, Object> parameterMap) {
		Iterator<Entry<String, Object>> iterator = IteratorUtils.entrySet(parameterMap);
		//	过滤XSS
		while (iterator.hasNext()) {
			Entry<String, Object> entry = iterator.next();
			String value = String.valueOf(entry.getValue());
			if (ValidParam.required(value) && BaseXssConfig.isXss(value)) {
				return true;
			}
		}
		return false;
	}
	
	/**	
	 * 	检测是否包含XSS
	 * 	
	 * 	@author Pan
	 * 	@param  request	HttpServletRequest
	 * 	@return	boolean
	 */
	public static boolean checkXss(HttpServletRequest request) {
		//	检测是否JSON类型数据
		if (RequestContentTypeContext.isJson(request)) {
			Map<String, Object> stringObjectMap = JsonUtils.toMap(((BaseXssRequestWrapper) request).getBody());

			Iterator<Entry<String, String>> iterator = IteratorUtils.entrySet(ClassConvertUtils.castMapStr(stringObjectMap));
			//	过滤XSS
			while (iterator.hasNext()) {
				Entry<String, String> entrySet = iterator.next();
				String value = entrySet.getValue();
				if (ValidParam.required(value) && BaseXssConfig.isXss(value)) {
					Log.get().error("出现XSS攻击 访问路径:{},参数名:{}, 参值数:{}", request.getServletPath(), entrySet.getKey(), value);
					return true;
				}
			}
		} else {
			//	普通的参数解析器
			Enumeration<?> paraNames = request.getParameterNames();
			while (paraNames.hasMoreElements()) {
				String fieldName = (String) paraNames.nextElement();
				String fieldValue = request.getParameter(fieldName);
				if (BaseXssConfig.isXss(fieldValue)) {
					Log.get().error("出现XSS攻击 访问路径:{},参数名:{}, 参值数:{}", request.getServletPath(), fieldName, fieldValue);
					return true;
				}
			}
		}
		return false;
	}
	
	/**	
	 * 	Xss解密
	 * 	
	 * 	@author Pan
	 * 	@param 	request	BaseXssRequestWrapper
	 * 	@return	boolean
	 */
	public static boolean checkXssDecrypt(BaseXssRequestWrapper request) {
		//	检测是否JSON类型数据
		if (RequestContentTypeContext.isJson(request)) {

			Iterator<Entry<Object, String>> iterator = IteratorUtils.entrySet(ClassConvertUtils.castMapStr(JsonUtils.toMap(request.getBody())));
			while (iterator.hasNext()) {
				Entry<Object, String> next = iterator.next();
				String value = next.getValue();
				//	过滤XSS
				if (ValidParam.required(value) && BaseXssConfig.isXss(value)) {
					return true;
				}
			}
		} else {
			//	普通的参数解析器
			Enumeration<?> paraNames = request.getParameterNames();
			while (paraNames.hasMoreElements()) {
				String fieldName = (String)paraNames.nextElement();
				String fieldValue = request.getParameter(fieldName);
				if (BaseXssConfig.isXss(fieldValue)) {
					return true;
				}
			}
		}
		return false;
	}
}
