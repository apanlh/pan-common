package com.gitee.apanlh.web.msg;

/**	
 * 	异常处理状态消息
 * 	
 * 	@author Pan
 */
public enum ExcMessage {
	
	EXC_ERROR(10001, "出现错误,请稍后再试!"),
	NULL_POINTER_ERROR(10010, "数据异常!"),
	ILLEGAL_STATE_ERROR(1000, "服务器暂时开小差中,请稍后再试!");
	
	/** 状态码 */
	private Integer code;
	/** 原因 */
	private String cause;
	
	ExcMessage(Integer code, String cause) {
		this.code = code;
		this.cause = cause;
	}

	public Integer getCode() {
		return code;
	}

	public String getCause() {
		return cause;
	}
}
