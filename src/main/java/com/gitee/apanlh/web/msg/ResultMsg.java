package com.gitee.apanlh.web.msg;

/**
 *	通用返回消息
 *
 * 	@author Pan
 */
public class ResultMsg {
	
	public static final String SUC_MSG = "成功";
	public static final String ERROR_MSG = "失败";
			
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private ResultMsg() {
		//	不允许外部实例
		super();
	}
}
