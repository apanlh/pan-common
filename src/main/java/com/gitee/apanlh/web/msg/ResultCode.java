package com.gitee.apanlh.web.msg;

/**
 * 	通用返回状态
 * 
 * 	@author Pan
 */
public class ResultCode {
	
	/** 成功 */
	public static final Integer SUC_CODE = 200;
	/** 失败 */
	public static final Integer ERROR_CODE = 400;
	/** token验证 */
	public static final Integer TOKEN_ERROR_CODE = 401;
	
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private ResultCode() {
		//	不允许外部实例
		super();
	}
}
