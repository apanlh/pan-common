package com.gitee.apanlh.util.base.enums;

/**	
 * 	计算方式枚举
 *
 * 	@author Pan
 */
public enum CalculationTypeEnum {
	
	/** 最大值 */
	MAX,
	/** 最小值 */
	MIN,
	/** 和 */
	SUM,
	;
}
