package com.gitee.apanlh.util.base;

import com.gitee.apanlh.exp.SerializationException;
import com.gitee.apanlh.util.io.IOUtils;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;

/**	
 * 	对象工具类
 *
 * 	@author Pan
 */
public class ObjectUtils {
	
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private ObjectUtils() {
		//	不允许外部实例
		super();
	}
	
	/**	
	 * 	是否对象为空
	 * 	
	 * 	@author Pan
	 * 	@param 	obj		值1
	 * 	@return	boolean
	 */
	public static boolean isNull(Object obj) {
		return obj == null;
	}
	
	/**	
	 * 	对象值比较
	 * 	
	 * 	@author Pan
	 * 	@param 	obj1	值1
	 * 	@param 	obj2	值2
	 * 	@return	boolean
	 */
	public static boolean eq(Object obj1, Object obj2) {
		 return obj1 == obj2 || (obj1 != null && obj1.equals(obj2));
	}
	
	/**
	 * 	对象值比较
	 * 	<br>包含匹配模式(或)
	 * 	<br>传递匹配值数组其中包含匹配值obj1返回true
	 * 
	 * 	@author Pan
	 * 	@param 	obj1	值1
	 * 	@param 	objs	一个或多个值
	 * 	@return	boolean
	 */
	public static boolean eqOr(Object obj1, Object... objs) {
		if (obj1 == null || ArrayUtils.isEmpty(objs)) {
			return false;
		}
		
		for (int i = 0; i < objs.length; i++) {
			if (eq(obj1, objs[i])) {
				return true;
			}
		}
		return false;
	}
	
	/**	
	 * 	类比较
	 * 	
	 * 	@author Pan
	 * 	@param 	clazz1	类1
	 * 	@param 	clazz2	类2
	 * 	@return	boolean
	 */
	public static boolean eq(Class<?> clazz1, Class<?> clazz2) {
		 return clazz1 == clazz2 || (clazz1 != null && clazz1.equals(clazz2));
	}

    /**
     * 	将对象转byte[]
     * 
     * 	@author Pan
     * 	@param 	obj 对象
     * 	@return	byte[]
     */
    public static byte[] toByte(Object obj) {
        ObjectOutputStream oos = null;
        
        try {
        	ByteArrayOutputStream bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            
            oos.writeObject(obj);
            oos.flush();
            return bos.toByteArray();
        } catch (Exception e) {
        	throw new SerializationException(e.getMessage(), e);
        } finally {
        	IOUtils.close(oos);
		}
    }
}
