package com.gitee.apanlh.util.base.builder;

import com.gitee.apanlh.util.base.CollUtils;

import java.util.Collection;
import java.util.List;

/**	
 *	集合Builder
 *	
 * 	@author Pan
 * 	@param  <T>	数据类型
 */
public class CollBuilder<T> {

	/** 集合 */
	private List<T> list;
	
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private CollBuilder() {
		//	不允许外部实例
		super();
	}
	
	/**	
	 * 	构造函数
	 * 	<br>自定义长度
	 * 	
	 * 	@author Pan
	 * 	@param 	size	长度
	 */
	private CollBuilder(int size) {
		this.list = CollUtils.newArrayList(size);
	}
	
	/**
	 * 	建造方法
	 * 	<br>用例:{@code CollBuilder.builder(TestBean.class).build()}
	 * 	
	 * 	@author Pan
	 * 	@param  <T>  数据类型
	 * 	@return	T
	 */
	public static <T> CollBuilder<T> builder() {
		return builder(16);
	}
	
	/**	
	 * 	建造方法
	 * 	<br>自定义长度
	 * 	<br>用例:{@code CollBuilder.builder(TestBean.class).build()}
	 * 	
	 * 	@author Pan
	 * 	@param  <T>      数据类型
	 * 	@param 	size     长度
	 * 	@return	T
	 */
	public static <T> CollBuilder<T> builder(int size) {
		return new CollBuilder<>(size);
	}
	
	/**	
	 * 	建造方法
	 * 	<br>自定义类
	 * 	<br>用例:{@code CollBuilder.builder(TestBean.class).build()}
	 * 	
	 * 	@author Pan
	 * 	@param  <T>      数据类型
	 * 	@param  clazz	类
	 * 	@return	T
	 */
	public static <T> CollBuilder<T> builder(Class<T> clazz) {
		return builder(clazz, 16);
	}
	
	/**	
	 * 	建造方法
	 * 	<br>自定义类
	 * 	<br>自定义长度
	 * 	<br>用例:{@code CollBuilder.builder(TestBean.class).build()}
	 * 	
	 * 	@author Pan
	 * 	@param  <T>     数据类型
	 * 	@param  clazz	类
	 * 	@param 	size     长度
	 * 	@return	T
	 */
	public static <T> CollBuilder<T> builder(Class<T> clazz, int size) {
		return new CollBuilder<T>(size);
	}
	
	/**	
	 * 	添加元素
	 * 	
	 * 	@author Pan
	 * 	@param 	obj		值
	 * 	@return	CollBuilder
	 */
	public CollBuilder<T> add(T obj) {
		this.list.add(obj);
		return this;
	}
	
	/**	
	 * 	根据集合添加元素
	 * 	
	 * 	@author Pan
	 * 	@param 	collection	集合
	 * 	@return	CollBuilder
	 */
	public CollBuilder<T> addAll(Collection<T> collection) {
		this.list.addAll(collection);
		return this;
	}
	
	/**	
	 * 	构建
	 * 	
	 * 	@author Pan
	 * 	@return	List
	 */
	public List<T> build() {
		return this.list;
	}
}
