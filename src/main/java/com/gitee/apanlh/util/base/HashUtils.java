package com.gitee.apanlh.util.base;

/**	
 * 	Hash工具类
 * 	@author Pan
 */
public class HashUtils {
	
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private HashUtils() {
		//	不允许外部实例
		super();
	}
	
	/**	
	 * 	默认JDK String hashCode 算法
	 * 	
	 * 	@author Pan
	 * 	@param  str	字符串
	 * 	@return int
	 */
	public static int hashCode(String str) {
		if (str == null) {
			return 0;
		}
		
		int h = 0;
        for (int i = 0, len = str.length(); i < len; i++) {
            h = 31 * h + (str.charAt(i));
        }
        return h;
	}
	
}
