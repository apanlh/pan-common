package com.gitee.apanlh.util.base.enums;


/**
 * 	排序标识
 * 
 * 	@author Pan
 */
public enum Sort {
	
	/** 升序 */
	ASC,
	/** 倒序 */
	DESC,
	;
	
}
