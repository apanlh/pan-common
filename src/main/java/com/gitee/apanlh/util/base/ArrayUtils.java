package com.gitee.apanlh.util.base;

import com.gitee.apanlh.util.func.FuncFind;
import com.gitee.apanlh.util.reflection.ClassConvertUtils;
import com.gitee.apanlh.util.valid.Assert;

import java.lang.reflect.Array;

/**	
 * 	数组工具类
 * 
 * 	@author Pan
 */
public class ArrayUtils {
	
	/** 默认find返回值 */
	private static final int NO_SUCH_VALUE = -1;
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	private ArrayUtils() {
		// 不允许外部构造
		super();
	}
	
	/**
	 * 	搜寻某个值
	 * 	
	 * 	@author Pan
	 * 	@param 	array		 数组
	 * 	@param 	searchValue	搜索值
	 * 	@return	int
	 */
	public static short find(short[] array, int searchValue) {
		if (isEmpty(array)) {
			return NO_SUCH_VALUE;
		}
		
		for (int i = 0, len = array.length; i < len; i++) {
			short value = array[i];
			if (ObjectUtils.eq(value, searchValue)) {
				return value;
			}
		}
		return NO_SUCH_VALUE;
	}
	
	/**
	 * 	搜寻某个值
	 * 	
	 * 	@author Pan
	 * 	@param 	array		 数组
	 * 	@param 	searchValue	搜索值
	 * 	@return	int
	 */
	public static int find(int[] array, int searchValue) {
		if (isEmpty(array)) {
			return NO_SUCH_VALUE;
		}
		
		for (int i = 0, len = array.length; i < len; i++) {
			int value = array[i];
			if (ObjectUtils.eq(value, searchValue)) {
				return value;
			}
		}
		return NO_SUCH_VALUE;
	}
	
	/**
	 * 	搜寻某个值
	 * 	
	 * 	@author Pan
	 * 	@param 	array		 数组
	 * 	@param 	searchValue	搜索值
	 * 	@return	int
	 */
	public static long find(long[] array, int searchValue) {
		if (isEmpty(array)) {
			return NO_SUCH_VALUE;
		}
		
		for (int i = 0, len = array.length; i < len; i++) {
			long value = array[i];
			if (ObjectUtils.eq(value, searchValue)) {
				return value;
			}
		}
		return NO_SUCH_VALUE;
	}
	
	/**
	 * 	搜索某个值
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>			数据类型
	 * 	@param 	array		 数组
	 * 	@param 	searchValue	搜索值
	 * 	@return	T
	 */
	public static <T> T find(T[] array, T searchValue) {
		if (isEmpty(array)) {
			return null;
		}
		
		for (int i = 0, len = array.length; i < len; i++) {
			T value = array[i];
			if (ObjectUtils.eq(value, searchValue)) {
				return value;
			}
		}
		return null;
	}
	
	/**
	 * 	搜索某个值
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>		数据类型
	 * 	@param 	array	数组
	 * 	@param 	find	搜索函数
	 * 	@return	T
	 */
	public static <T> T find(T[] array, FuncFind<T> find) {
		if (isEmpty(array)) {
			return null;
		}
		
		for (int i = 0, len = array.length; i < len; i++) {
			T t = array[i];
			if (find.accept(t)) {
				return t;
			}
		}
		return null;
	}
	
	/**	
	 * 	返回找到出现的第一个索引
	 * 	
	 * 	@author Pan
	 * 	@param 	array	数组
	 * 	@param 	value	值
	 * 	@return	int
	 */
	public static int indexOf(int[] array, int value) {
		return indexOf(array, value, 0);
	}
	
	/**	
	 * 	返回找到出现的第一个索引
	 * 	
	 * 	@author Pan
	 * 	@param 	array		数值
	 * 	@param 	value		值
	 * 	@param 	startIndex	开始搜索的索引下标
	 * 	@return	int
	 */
	public static int indexOf(int[] array, int value, int startIndex) {
		if (isEmpty(array)) {
			return NO_SUCH_VALUE;
		}
		
		for (int i = startIndex, len = array.length; i < len; i++) {
			if (ObjectUtils.eq(array[i], value)) {
				return i;
			}
		}
		return NO_SUCH_VALUE;
	}
	
	/**	
	 * 	返回找到出现的第一个索引
	 * 
	 * 	@author Pan
	 * 	@param 	<T>			数据类型
	 * 	@param 	array		数组
	 * 	@param 	value		值
	 * 	@return	int
	 */
	public static <T> int indexOf(T[] array, T value) {
		if (isEmpty(array)) {
			return NO_SUCH_VALUE;
		}
		
		for (int i = 0, len = array.length; i < len; i++) {
			if (ObjectUtils.eq(array[i], value)) {
				return i;
			}
		}
		return NO_SUCH_VALUE;
	}
	
	/**	
	 * 	末端开始寻找，返回找到出现的第一个索引
	 * 	
	 * 	@author Pan
	 * 	@param 	array		数组
	 * 	@param 	value		值
	 * 	@return	int
	 */
	public static int lastIndexOf(int[] array, int value) {
		if (isEmpty(array)) {
			return NO_SUCH_VALUE;
		}
		
		for (int i = array.length - 1; i >= 0; i--) {
			if (array[i] == value) {
				return i;
			}
		}
		return NO_SUCH_VALUE;
	}
	
	/**	
	 * 	末端开始寻找，返回找到出现的第一个索引
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>			数据类型
	 * 	@param 	array		数组
	 * 	@param 	value		值
	 * 	@return	int
	 */
	public static <T> int lastIndexOf(T[] array, T value) {
		if (isEmpty(array)) {
			return NO_SUCH_VALUE;
		}
		
		for (int i = array.length - 1; i >= 0; i--) {
			if (ObjectUtils.eq(array[i], value)) {
				return i;
			}
		}
		return NO_SUCH_VALUE;
	}
	
	/**	
	 * 	数组是否为空
	 * 	
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@return	boolean
	 */
	public static boolean isEmpty(byte[] arr) {
		return arr == null || arr.length == 0;
	}
	
	/**	
	 * 	数组是否为空
	 * 	
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@return	boolean
	 */
	public static boolean isEmpty(short[] arr) {
		return arr == null || arr.length == 0;
	}
	
	/**	
	 * 	数组是否为空
	 * 	
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@return	boolean
	 */
	public static boolean isEmpty(int[] arr) {
		return arr == null || arr.length == 0;
	}
	
	/**	
	 * 	数组是否为空
	 * 	
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@return	boolean
	 */
	public static boolean isEmpty(long[] arr) {
		return arr == null || arr.length == 0;
	}
	
	/**	
	 * 	数组是否为空
	 * 	
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@return	boolean
	 */
	public static boolean isEmpty(float[] arr) {
		return arr == null || arr.length == 0;
	}
	
	/**	
	 * 	数组是否为空
	 * 	
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@return	boolean
	 */
	public static boolean isEmpty(double[] arr) {
		return arr == null || arr.length == 0;
	}
	
	/**	
	 * 	数组是否为空
	 * 	
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@return	boolean
	 */
	public static boolean isEmpty(boolean[] arr) {
		return arr == null || arr.length == 0;
	}
	
	/**	
	 * 	包装类型数组是否为空
	 * 	
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@return	boolean
	 */
	public static boolean isEmpty(char[] arr) {
		return arr == null || arr.length == 0;
	}
	
	/**	
	 * 	包装类型数组是否为空
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>		数据类型
	 * 	@param  arr		数组
	 * 	@return	boolean
	 */
	public static <T> boolean isEmpty(T[] arr) {
		return arr == null || arr.length == 0;
	}
	
	/**	
	 * 	字符连接
	 * 	
	 * 	@author Pan
	 * 	@param 	arr		数组
	 * 	@param 	joinCs	连接符
	 * 	@return	String
	 */
	public static String join(int[] arr, CharSequence joinCs) {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0, len = arr.length; i < len; i++) {
			if (i == (len - 1)) {
				sb.append(arr[i]);
				break;
			}
			sb.append(arr[i]).append(joinCs);
		}
		return sb.toString();
	}
	
	/**	
	 * 	字符连接
	 * 	
	 * 	@author Pan
	 * 	@param 	arr		数组
	 * 	@param 	joinCs	连接符
	 * 	@return	String
	 */
	public static String join(short[] arr, CharSequence joinCs) {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0, len = arr.length; i < len; i++) {
			if (i == (len - 1)) {
				sb.append(arr[i]);
				break;
			}
			sb.append(arr[i]).append(joinCs);
		}
		return sb.toString();
	}
	
	/**	
	 * 	字符连接
	 * 	
	 * 	@author Pan
	 * 	@param 	arr		数组
	 * 	@param 	joinCs	连接符
	 * 	@return	String
	 */
	public static String join(long[] arr, CharSequence joinCs) {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0, len = arr.length; i < len; i++) {
			if (i == (len - 1)) {
				sb.append(arr[i]);
				break;
			}
			sb.append(arr[i]).append(joinCs);
		}
		return sb.toString();
	}
	
	/**	
	 * 	字符连接
	 * 	
	 * 	@author Pan
	 * 	@param 	arr		数组
	 * 	@param 	joinCs	连接符
	 * 	@return	String
	 */
	public static String join(float[] arr, CharSequence joinCs) {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0, len = arr.length; i < len; i++) {
			if (i == (len - 1)) {
				sb.append(arr[i]);
				break;
			}
			sb.append(arr[i]).append(joinCs);
		}
		return sb.toString();
	}
	
	/**	
	 * 	字符连接
	 * 	
	 * 	@author Pan
	 * 	@param 	arr		数组
	 * 	@param 	joinCs	连接符
	 * 	@return	String
	 */
	public static String join(double[] arr, CharSequence joinCs) {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0, len = arr.length; i < len; i++) {
			if (i == (len - 1)) {
				sb.append(arr[i]);
				break;
			}
			sb.append(arr[i]).append(joinCs);
		}
		return sb.toString();
	}
	
	/**	
	 * 	字符连接
	 * 	
	 * 	@author Pan
	 * 	@param 	arr		数组
	 * 	@param 	joinCs	连接符
	 * 	@return	String
	 */
	public static String join(byte[] arr, CharSequence joinCs) {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0, len = arr.length; i < len; i++) {
			if (i == (len - 1)) {
				sb.append(arr[i]);
				break;
			}
			sb.append(arr[i]).append(joinCs);
		}
		return sb.toString();
	}
	
	/**	
	 * 	字符连接
	 * 	
	 * 	@author Pan
	 * 	@param 	arr		数组
	 * 	@param 	joinCs	连接符
	 * 	@return	String
	 */
	public static String join(boolean[] arr, CharSequence joinCs) {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0, len = arr.length; i < len; i++) {
			if (i == (len - 1)) {
				sb.append(arr[i]);
				break;
			}
			sb.append(arr[i]).append(joinCs);
		}
		return sb.toString();
	}
	
	/**	
	 * 	字符连接
	 * 	<br>在添加的最后时不会添加连接字符
	 * 	<br>如数组[abcde]  连接符为[,]  结果[a,b,c,d,e]
	 * 
	 * 	@author Pan
	 * 	@param 	<T>			数据类型
	 * 	@param 	arr			数组
	 * 	@param 	joinCs		连接符
	 * 	@return String
	 */
	public static <T> String join(T[] arr, CharSequence joinCs) {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0, len = arr.length; i < len; i++) {
			if (i == (len - 1)) {
				sb.append(arr[i]);
				break;
			}
			sb.append(arr[i]).append(joinCs);
		}
		return sb.toString();
	}
	
	/**	
	 * 	ArrayCopy封装
	 * 	<br>如果长度不够则返回arrLen - copyLength
	 * 	<br>如果长度传递长度超出原数组长度则扩容原数组
	 * 
	 * 	@author Pan
	 * 	@param 	arr			数组
	 * 	@param 	copyLength	扩容长度
	 * 	@return	byte[]
	 */
	public static byte[] copy(byte[] arr, int copyLength) {
		int arrLen = arr.length;
		if (arrLen == copyLength) {
			return arr;
		}
		
		int min = Math.min(arrLen, copyLength);
		byte[] copyArr = new byte[copyLength];
		System.arraycopy(arr, 0, copyArr, 0, min);
		return copyArr;
	}
	
	/**	
	 * 	ArrayCopy封装
	 * 	<br>如果长度不够则返回arrLen - copyLength
	 * 	<br>如果长度传递长度超出原数组长度则扩容原数组
	 * 
	 * 	@author Pan
	 * 	@param 	arr			数组
	 * 	@param 	copyLength	扩容长度
	 * 	@return	short[]
	 */
	public static short[] copy(short[] arr, int copyLength) {
		int arrLen = arr.length;
		if (arrLen == copyLength) {
			return arr;
		}
		
		int min = Math.min(arrLen, copyLength);
		short[] copyArr = new short[copyLength];
		System.arraycopy(arr, 0, copyArr, 0, min);
		return copyArr;
	}
	
	/**	
	 * 	ArrayCopy封装
	 * 	<br>如果长度不够则返回arrLen - copyLength
	 * 	<br>如果长度传递长度超出原数组长度则扩容原数组
	 * 
	 * 	@author Pan
	 * 	@param 	arr			数组
	 * 	@param 	copyLength	扩容长度
	 * 	@return	int[]
	 */
	public static int[] copy(int[] arr, int copyLength) {
		int arrLen = arr.length;
		if (arrLen == copyLength) {
			return arr;
		}
		
		int min = Math.min(arrLen, copyLength);
		int[] copyArr = new int[copyLength];
		System.arraycopy(arr, 0, copyArr, 0, min);
		return copyArr;
	}
	
	/**	
	 * 	ArrayCopy封装
	 * 	<br>如果长度不够则返回arrLen - copyLength
	 * 	<br>如果长度传递长度超出原数组长度则扩容原数组
	 * 
	 * 	@author Pan
	 * 	@param 	arr			数组
	 * 	@param 	copyLength	扩容长度
	 * 	@return	long[]
	 */
	public static long[] copy(long[] arr, int copyLength) {
		int arrLen = arr.length;
		if (arrLen == copyLength) {
			return arr;
		}
		
		int min = Math.min(arrLen, copyLength);
		long[] copyArr = new long[copyLength];
		System.arraycopy(arr, 0, copyArr, 0, min);
		return copyArr;
	}
	
	/**	
	 * 	ArrayCopy封装
	 * 	<br>如果长度不够则返回arrLen - copyLength
	 * 	<br>如果长度传递长度超出原数组长度则扩容原数组
	 * 
	 * 	@author Pan
	 * 	@param 	arr			数组
	 * 	@param 	copyLength	扩容长度
	 * 	@return	float[]
	 */
	public static float[] copy(float[] arr, int copyLength) {
		int arrLen = arr.length;
		if (arrLen == copyLength) {
			return arr;
		}
		
		int min = Math.min(arrLen, copyLength);
		float[] copyArr = new float[copyLength];
		System.arraycopy(arr, 0, copyArr, 0, min);
		return copyArr;
	}
	
	/**
	 * 	ArrayCopy封装
	 * 	<br>如果长度不够则返回arrLen - copyLength
	 * 	<br>如果长度传递长度超出原数组长度则扩容原数组
	 * 
	 * 	@author Pan
	 * 	@param 	arr			数组
	 * 	@param 	copyLength	扩容长度
	 * 	@return	double[]
	 */
	public static double[] copy(double[] arr, int copyLength) {
		int arrLen = arr.length;
		if (arrLen == copyLength) {
			return arr;
		}
		
		int min = Math.min(arrLen, copyLength);
		double[] copyArr = new double[copyLength];
		System.arraycopy(arr, 0, copyArr, 0, min);
		return copyArr;
	}
	
	/**	
	 * 	ArrayCopy封装
	 * 	<br>如果长度不够则返回arrLen - copyLength
	 * 	<br>如果长度传递长度超出原数组长度则扩容原数组
	 * 
	 * 	@author Pan
	 * 	@param 	arr			数组
	 * 	@param 	copyLength	扩容长度
	 * 	@return	boolean[]
	 */
	public static boolean[] copy(boolean[] arr, int copyLength) {
		int arrLen = arr.length;
		if (arrLen == copyLength) {
			return arr;
		}
		
		int min = Math.min(arrLen, copyLength);
		boolean[] copyArr = new boolean[copyLength];
		System.arraycopy(arr, 0, copyArr, 0, min);
		return copyArr;
	}
	
	/**	
	 * 	ArrayCopy封装
	 * 	<br>如果长度不够则返回arrLen - copyLength
	 * 	<br>如果长度传递长度超出原数组长度则扩容原数组
	 * 
	 * 	@author Pan
	 * 	@param 	arr			数组
	 * 	@param 	copyLength	扩容长度
	 * 	@return	Integer[]
	 */
	public static Integer[] copy(Integer[] arr, int copyLength) {
		int arrLen = arr.length;
		if (arrLen == copyLength) {
			return arr;
		}
		
		int min = Math.min(arrLen, copyLength);
		Integer[] copyArr = new Integer[copyLength];
		System.arraycopy(arr, 0, copyArr, 0, min);
		return copyArr;
	}
	
	/**	
	 * 	ArrayCopy封装
	 * 	<br>如果长度不够则返回arrLen - copyLength
	 * 	<br>如果长度传递长度超出原数组长度则扩容原数组
	 * 
	 * 	@author Pan
	 * 	@param 	arr			数组
	 * 	@param 	copyLength	扩容长度
	 * 	@return	char[]
	 */
	public static char[] copy(char[] arr, int copyLength) {
		int arrLen = arr.length;
		if (arrLen == copyLength) {
			return arr;
		}
		
		int min = Math.min(arrLen, copyLength);
		char[] copyArr = new char[copyLength];
		System.arraycopy(arr, 0, copyArr, 0, min);
		return copyArr;
	}
	
	/**	
	 * 	ArrayCopy封装
	 * 	<br>如果长度不够则返回arrLen - copyLength
	 * 	<br>如果长度传递长度超出原数组长度则扩容原数组
	 * 	
	 * 	@author Pan
	 * 	@param 	arr			数组
	 * 	@param 	copyLength	扩容长度
	 * 	@return	String[]
	 */
	public static String[] copy(String[] arr, int copyLength) {
		int arrLen = arr.length;
		if (arrLen == copyLength) {
			return arr;
		}
		
		int min = Math.min(arrLen, copyLength);
		String[] copyArr = new String[copyLength];
		System.arraycopy(arr, 0, copyArr, 0, min);
		return copyArr;
	}
		
	/**	
	 * 	ArrayCopy封装
	 * 	<br>如果长度不够则返回arrLen - copyLength
	 * 	<br>如果长度传递长度超出原数组长度则扩容原数组
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>			数据类型
	 * 	@param 	arr			数组
	 * 	@param 	copyLength	扩容长度
	 * 	@return	T[]
	 */
	@SuppressWarnings("unchecked")
	public static <T> T[] copy(T[] arr, int copyLength) {
		int arrLen = arr.length;
		if (arrLen == copyLength) {
			return arr;
		}
		            
		int min = Math.min(arrLen, copyLength);
		Object[] copyArr = (Object[]) Array.newInstance(arr.getClass().getComponentType(), copyLength);
		System.arraycopy(arr, 0, copyArr, 0, min);
		return (T[]) copyArr;
	}
	
	/**
	 * 	将B数组结果连接至A数组中
	 * 	<br>例如:{@code 数组1(1,2,3) | 数组2(4,5) -> 1,2,3,4,5}
	 * 	<br>不改变原有数组
	 * 
	 * 	@author Pan
	 * 	@param 	arr			      数组
	 * 	@param 	concatContent  连接内容
	 * 	@return	String[]
	 */
	public static String[] concat(String[] arr, String... concatContent) {
		if (arr == null) {
			arr = Empty.arrayString();
		}
		if (isEmpty(concatContent)) {
			return arr;
		}
		
		int arrLen = arr.length;
		int concatContentLen = concatContent.length;
		int copyLen = arrLen + concatContentLen;
		String[] copy = copy(arr, copyLen);
		
		for (int i = arrLen; i < copyLen; i++) {
			copy[i] = concatContent[i - arrLen];
		}
		return copy;
	}
	
	/**
	 * 	将B数组插入A数组中
	 * 	<br>不改变原有数组
	 * 	
	 * 	@author Pan
	 * 	@param 	arr			      数组
	 * 	@param 	concatContent  连接内容
	 * 	@return	String[]
	 */
	public static byte[] concat(byte[] arr, byte... concatContent) {
		if (arr == null) {
			arr = Empty.arrayByte();
		}
		if (isEmpty(concatContent)) {
			return arr;
		}
		
		int arrLen = arr.length;
		int concatContentLen = concatContent.length;
		int copyLen = arrLen + concatContentLen;
		byte[] copy = copy(arr, copyLen);
		
		for (int i = arrLen; i < copyLen; i++) {
			copy[i] = concatContent[i - arrLen];
		}
		return copy;
	}

	/**
	 * 	将B数组插入A数组中
	 * 	<br>不改变原有数组
	 * 	
	 * 	@author Pan
	 * 	@param 	arr			      数组
	 * 	@param 	concatContent  连接内容
	 * 	@return	String[]
	 */
	public static short[] concat(short[] arr, short... concatContent) {
		if (arr == null) {
			arr = Empty.arrayShort();
		}
		if (isEmpty(concatContent)) {
			return arr;
		}
		
		int arrLen = arr.length;
		int concatContentLen = concatContent.length;
		int copyLen = arrLen + concatContentLen;
		short[] copy = copy(arr, copyLen);
		
		for (int i = arrLen; i < copyLen; i++) {
			copy[i] = concatContent[i - arrLen];
		}
		return copy;
	}
	
	/**
	 * 	将B数组插入A数组中
	 * 	<br>不改变原有数组
	 * 	
	 * 	@author Pan
	 * 	@param 	arr			      数组
	 * 	@param 	concatContent  连接内容
	 * 	@return	String[]
	 */
	public static int[] concat(int[] arr, int... concatContent) {
		if (arr == null) {
			arr = Empty.arrayInt();
		}
		if (isEmpty(concatContent)) {
			return arr;
		}
		
		int arrLen = arr.length;
		int concatContentLen = concatContent.length;
		int copyLen = arrLen + concatContentLen;
		int[] copy = copy(arr, copyLen);
		
		for (int i = arrLen; i < copyLen; i++) {
			copy[i] = concatContent[i - arrLen];
		}
		return copy;
	}
	
	/**
	 * 	将B数组插入A数组中
	 * 	<br>不改变原有数组
	 * 	
	 * 	@author Pan
	 * 	@param 	arr			      数组
	 * 	@param 	concatContent  连接内容
	 * 	@return	String[]
	 */
	public static long[] concat(long[] arr, long... concatContent) {
		if (arr == null) {
			arr = Empty.arrayLong();
		}
		if (isEmpty(concatContent)) {
			return arr;
		}
		
		int arrLen = arr.length;
		int concatContentLen = concatContent.length;
		int copyLen = arrLen + concatContentLen;
		long[] copy = copy(arr, copyLen);
		
		for (int i = arrLen; i < copyLen; i++) {
			copy[i] = concatContent[i - arrLen];
		}
		return copy;
	}
	
	/**
	 * 	将B数组插入A数组中
	 * 	<br>不改变原有数组
	 * 	
	 * 	@author Pan
	 * 	@param 	arr			      数组
	 * 	@param 	concatContent  连接内容
	 * 	@return	String[]
	 */
	public static double[] concat(double[] arr, double... concatContent) {
		if (arr == null) {
			arr = Empty.arrayDouble();
		}
		if (isEmpty(concatContent)) {
			return arr;
		}
		
		int arrLen = arr.length;
		int concatContentLen = concatContent.length;
		int copyLen = arrLen + concatContentLen;
		double[] copy = copy(arr, copyLen);
		
		for (int i = arrLen; i < copyLen; i++) {
			copy[i] = concatContent[i - arrLen];
		}
		return copy;
	}
	
	/**
	 * 	将B数组插入A数组中
	 * 	<br>不改变原有数组
	 * 	
	 * 	@author Pan
	 * 	@param 	arr			      数组
	 * 	@param 	concatContent  连接内容
	 * 	@return	String[]
	 */
	public static float[] concat(float[] arr, float... concatContent) {
		if (arr == null) {
			arr = Empty.arrayFloat();
		}
		if (isEmpty(concatContent)) {
			return arr;
		}
		
		int arrLen = arr.length;
		int concatContentLen = concatContent.length;
		int copyLen = arrLen + concatContentLen;
		float[] copy = copy(arr, copyLen);
		
		for (int i = arrLen; i < copyLen; i++) {
			copy[i] = concatContent[i - arrLen];
		}
		return copy;
	}
	
	/**
	 * 	将B数组插入A数组中
	 * 	<br>不改变原有数组
	 * 	
	 * 	@author Pan
	 * 	@param 	arr				数组
	 * 	@param 	concatContent  	连接内容
	 * 	@return	String[]
	 */
	public static boolean[] concat(boolean[] arr, boolean... concatContent) {
		if (arr == null) {
			arr = Empty.arrayBoolean();
		}
		if (isEmpty(concatContent)) {
			return arr;
		}
		
		int arrLen = arr.length;
		int concatContentLen = concatContent.length;
		int copyLen = arrLen + concatContentLen;
		boolean[] copy = copy(arr, copyLen);
		
		for (int i = arrLen; i < copyLen; i++) {
			copy[i] = concatContent[i - arrLen];
		}
		return copy;
	}
	
	/**
	 * 	将B数组插入A数组中
	 * 	<br>不改变原有数组
	 * 	
	 * 	@author Pan
	 * 	@param 	arr				数组
	 * 	@param 	concatContent  	连接内容
	 * 	@return	String[]
	 */
	public static char[] concat(char[] arr, char... concatContent) {
		if (arr == null) {
			arr = Empty.arrayChar();
		}
		if (isEmpty(concatContent)) {
			return arr;
		}
		
		int arrLen = arr.length;
		int concatContentLen = concatContent.length;
		int copyLen = arrLen + concatContentLen;
		char[] copy = copy(arr, copyLen);
		
		for (int i = arrLen; i < copyLen; i++) {
			copy[i] = concatContent[i - arrLen];
		}
		return copy;
	}
	
	/**
	 * 	将B数组插入A数组中
	 * 	<br>不改变原有数组
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>			数据类型
	 * 	@param 	arr				数组
	 * 	@param 	concatContent  	连接内容
	 * 	@return	String[]
	 */
	@SuppressWarnings("unchecked")
	public static <T> T[] concat(T[] arr, T... concatContent) {
		if (isEmpty(concatContent)) {
			return arr;
		}
		if (arr == null) {
			arr = (T[]) Array.newInstance(concatContent[0].getClass(), 0);
		}
		
		int arrLen = arr.length;
		int concatContentLen = concatContent.length;
		int copyLen = arrLen + concatContentLen;
		T[] copy = copy(arr, copyLen);
		
		for (int i = arrLen; i < copyLen; i++) {
			copy[i] = concatContent[i - arrLen];
		}
		return copy;
	}
	
	/**	
	 * 	将元素插入指定下标位置
	 * 	<br>如果下标超出原始数组长度则直接添加在原始数组尾部
	 * 	<br>不会修改原始数组
	 * 	<p> 实例:
	 * 	<br>原始数组a = {1, 2, 3}
	 * 	<br>插入数组b = {4, 5}
	 * 	<br>插入下标index = 1;
	 * 	<br>返回结果c = {1, 4, 5, 2, 3}
	 * 
	 * 	@author Pan
	 * 	@param 	arr			原数组
	 * 	@param 	index		下标位置
	 * 	@param 	elements	插入元素
	 * 	@return	byte[]
	 */
	public static byte[] insert(byte[] arr, int index, byte... elements) {
		if (isEmpty(elements)) {
			return arr;
		}
		if (arr == null) {
			return concat(arr, elements);
		}
		//	修正：超出长度则在最后的位置进行插入
		int arrLen = arr.length;
		if (index > arrLen) {
			return concat(arr, elements);
		}
		
		int copyLen = arrLen + elements.length;
		//	插入起始位置长度
		int newArrayInsertLen = index + elements.length;
		byte[] newArray = new byte[copyLen];
		
		//	还原之前元素
		System.arraycopy(arr, 0, newArray, 0, index);
		//	插入元素
		System.arraycopy(elements, 0, newArray, index, elements.length);
		//	还原尾部最后的元素
		System.arraycopy(arr, index, newArray, newArrayInsertLen, arrLen - index);
		return newArray;
	}
	
	/**	
	 * 	将元素插入指定下标位置
	 * 	<br>如果下标超出原始数组长度则直接添加在原始数组尾部
	 * 	<br>不会修改原始数组
	 * 	<p> 实例:
	 * 	<br>原始数组a = {1, 2, 3}
	 * 	<br>插入数组b = {4, 5}
	 * 	<br>插入下标index = 1;
	 * 	<br>返回结果c = {1, 4, 5, 2, 3}
	 * 
	 * 	@author Pan
	 * 	@param 	arr			原数组
	 * 	@param 	index		下标位置
	 * 	@param 	elements	插入元素
	 * 	@return	short[]
	 */
	public static short[] insert(short[] arr, int index, short... elements) {
		if (isEmpty(elements)) {
			return arr;
		}
		if (arr == null) {
			return concat(arr, elements);
		}
		//	修正：超出长度则在最后的位置进行插入
		int arrLen = arr.length;
		if (index > arrLen) {
			return concat(arr, elements);
		}
		
		int copyLen = arrLen + elements.length;
		//	插入起始位置长度
		int newArrayInsertLen = index + elements.length;
		short[] newArray = new short[copyLen];
		
		//	还原之前元素
		System.arraycopy(arr, 0, newArray, 0, index);
		//	插入元素
		System.arraycopy(elements, 0, newArray, index, elements.length);
		//	还原尾部最后的元素
		System.arraycopy(arr, index, newArray, newArrayInsertLen, arrLen - index);
		return newArray;
	}
	
	/**	
	 * 	将元素插入指定下标位置
	 * 	<br>如果下标超出原始数组长度则直接添加在原始数组尾部
	 * 	<br>不会修改原始数组
	 * 	<p> 实例:
	 * 	<br>原始数组a = {1, 2, 3}
	 * 	<br>插入数组b = {4, 5}
	 * 	<br>插入下标index = 1;
	 * 	<br>返回结果c = {1, 4, 5, 2, 3}
	 * 
	 * 	@author Pan
	 * 	@param 	arr			原数组
	 * 	@param 	index		下标位置
	 * 	@param 	elements	插入元素
	 * 	@return	int[]
	 */
	public static int[] insert(int[] arr, int index, int... elements) {
		if (isEmpty(elements)) {
			return arr;
		}
		if (arr == null) {
			return concat(arr, elements);
		}
		//	修正：超出长度则在最后的位置进行插入
		int arrLen = arr.length;
		if (index > arrLen) {
			return concat(arr, elements);
		}
		
		int copyLen = arrLen + elements.length;
		//	插入起始位置长度
		int newArrayInsertLen = index + elements.length;
		int[] newArray = new int[copyLen];
		
		//	还原之前元素
		System.arraycopy(arr, 0, newArray, 0, index);
		//	插入元素
		System.arraycopy(elements, 0, newArray, index, elements.length);
		//	还原尾部最后的元素
		System.arraycopy(arr, index, newArray, newArrayInsertLen, arrLen - index);
		return newArray;
	}
	
	/**	
	 * 	将元素插入指定下标位置
	 * 	<br>如果下标超出原始数组长度则直接添加在原始数组尾部
	 * 	<br>不会修改原始数组
	 * 	<p> 实例:
	 * 	<br>原始数组a = {1, 2, 3}
	 * 	<br>插入数组b = {4, 5}
	 * 	<br>插入下标index = 1;
	 * 	<br>返回结果c = {1, 4, 5, 2, 3}
	 * 
	 * 	@author Pan
	 * 	@param 	arr			原数组
	 * 	@param 	index		下标位置
	 * 	@param 	elements	插入元素
	 * 	@return	long[]
	 */
	public static long[] insert(long[] arr, int index, long... elements) {
		if (isEmpty(elements)) {
			return arr;
		}
		if (arr == null) {
			return concat(arr, elements);
		}
		//	修正：超出长度则在最后的位置进行插入
		int arrLen = arr.length;
		if (index > arrLen) {
			return concat(arr, elements);
		}
		
		int copyLen = arrLen + elements.length;
		//	插入起始位置长度
		int newArrayInsertLen = index + elements.length;
		long[] newArray = new long[copyLen];
		
		//	还原之前元素
		System.arraycopy(arr, 0, newArray, 0, index);
		//	插入元素
		System.arraycopy(elements, 0, newArray, index, elements.length);
		//	还原尾部最后的元素
		System.arraycopy(arr, index, newArray, newArrayInsertLen, arrLen - index);
		return newArray;
	}
	
	/**	
	 * 	将元素插入指定下标位置
	 * 	<br>如果下标超出原始数组长度则直接添加在原始数组尾部
	 * 	<br>不会修改原始数组
	 * 	<p> 实例:
	 * 	<br>原始数组a = {1, 2, 3}
	 * 	<br>插入数组b = {4, 5}
	 * 	<br>插入下标index = 1;
	 * 	<br>返回结果c = {1, 4, 5, 2, 3}
	 * 
	 * 	@author Pan
	 * 	@param 	arr			原数组
	 * 	@param 	index		下标位置
	 * 	@param 	elements	插入元素
	 * 	@return	float[]
	 */
	public static float[] insert(float[] arr, int index, float... elements) {
		if (isEmpty(elements)) {
			return arr;
		}
		if (arr == null) {
			return concat(arr, elements);
		}
		//	修正：超出长度则在最后的位置进行插入
		int arrLen = arr.length;
		if (index > arrLen) {
			return concat(arr, elements);
		}
		
		int copyLen = arrLen + elements.length;
		//	插入起始位置长度
		int newArrayInsertLen = index + elements.length;
		float[] newArray = new float[copyLen];
		
		//	还原之前元素
		System.arraycopy(arr, 0, newArray, 0, index);
		//	插入元素
		System.arraycopy(elements, 0, newArray, index, elements.length);
		//	还原尾部最后的元素
		System.arraycopy(arr, index, newArray, newArrayInsertLen, arrLen - index);
		return newArray;
	}
	
	/**	
	 * 	将元素插入指定下标位置
	 * 	<br>如果下标超出原始数组长度则直接添加在原始数组尾部
	 * 	<br>不会修改原始数组
	 * 	<p> 实例:
	 * 	<br>原始数组a = {1, 2, 3}
	 * 	<br>插入数组b = {4, 5}
	 * 	<br>插入下标index = 1;
	 * 	<br>返回结果c = {1, 4, 5, 2, 3}
	 * 
	 * 	@author Pan
	 * 	@param 	arr			原数组
	 * 	@param 	index		下标位置
	 * 	@param 	elements	插入元素
	 * 	@return	double[]
	 */
	public static double[] insert(double[] arr, int index, double... elements) {
		if (isEmpty(elements)) {
			return arr;
		}
		if (arr == null) {
			return concat(arr, elements);
		}
		//	修正：超出长度则在最后的位置进行插入
		int arrLen = arr.length;
		if (index > arrLen) {
			return concat(arr, elements);
		}
		
		int copyLen = arrLen + elements.length;
		//	插入起始位置长度
		int newArrayInsertLen = index + elements.length;
		double[] newArray = new double[copyLen];
		
		//	还原之前元素
		System.arraycopy(arr, 0, newArray, 0, index);
		//	插入元素
		System.arraycopy(elements, 0, newArray, index, elements.length);
		//	还原尾部最后的元素
		System.arraycopy(arr, index, newArray, newArrayInsertLen, arrLen - index);
		return newArray;
	}
	
	/**	
	 * 	将元素插入指定下标位置
	 * 	<br>如果下标超出原始数组长度则直接添加在原始数组尾部
	 * 	<br>不会修改原始数组
	 * 	<p> 实例:
	 * 	<br>原始数组a = {1, 2, 3}
	 * 	<br>插入数组b = {4, 5}
	 * 	<br>插入下标index = 1;
	 * 	<br>返回结果c = {1, 4, 5, 2, 3}
	 * 
	 * 	@author Pan
	 * 	@param 	arr			原数组
	 * 	@param 	index		下标位置
	 * 	@param 	elements	插入元素
	 * 	@return	boolean[]
	 */
	public static boolean[] insert(boolean[] arr, int index, boolean... elements) {
		if (isEmpty(elements)) {
			return arr;
		}
		if (arr == null) {
			return concat(arr, elements);
		}
		//	修正：超出长度则在最后的位置进行插入
		int arrLen = arr.length;
		if (index > arrLen) {
			return concat(arr, elements);
		}
		
		int copyLen = arrLen + elements.length;
		//	插入起始位置长度
		int newArrayInsertLen = index + elements.length;
		boolean[] newArray = new boolean[copyLen];
		
		//	还原之前元素
		System.arraycopy(arr, 0, newArray, 0, index);
		//	插入元素
		System.arraycopy(elements, 0, newArray, index, elements.length);
		//	还原尾部最后的元素
		System.arraycopy(arr, index, newArray, newArrayInsertLen, arrLen - index);
		return newArray;
	}
	
	/**	
	 * 	将元素插入指定下标位置
	 * 	<br>如果下标超出原始数组长度则直接添加在原始数组尾部
	 * 	<br>不会修改原始数组
	 * 	<p> 实例:
	 * 	<br>原始数组a = {1, 2, 3}
	 * 	<br>插入数组b = {4, 5}
	 * 	<br>插入下标index = 1;
	 * 	<br>返回结果c = {1, 4, 5, 2, 3}
	 * 
	 * 	@author Pan
	 * 	@param 	arr			原数组
	 * 	@param 	index		下标位置
	 * 	@param 	elements	插入元素
	 * 	@return	char[]
	 */
	public static char[] insert(char[] arr, int index, char... elements) {
		if (isEmpty(elements)) {
			return arr;
		}
		if (arr == null) {
			return concat(arr, elements);
		}
		//	修正：超出长度则在最后的位置进行插入
		int arrLen = arr.length;
		if (index > arrLen) {
			return concat(arr, elements);
		}
		
		int copyLen = arrLen + elements.length;
		//	插入起始位置长度
		int newArrayInsertLen = index + elements.length;
		char[] newArray = new char[copyLen];
		
		//	还原之前元素
		System.arraycopy(arr, 0, newArray, 0, index);
		//	插入元素
		System.arraycopy(elements, 0, newArray, index, elements.length);
		//	还原尾部最后的元素
		System.arraycopy(arr, index, newArray, newArrayInsertLen, arrLen - index);
		return newArray;
	}
	
	/**	
	 * 	short数组比较
     * 	<br>对比两者short[]是否一致
     * 	<br>两个数组均为为null返回false
     * 
     * 	@author Pan
     * 	@param 	a1	来源short[]
     * 	@param 	a2	目标short[]
     * 	@return	boolean
     */
    public static boolean eq(short[] a1, short[] a2) {
		if (a1 == null || a2 == null) {
			return false;
		}
    	if (a1.length != a2.length) {
    		return false;
    	}
		if (a1 == a2) {
			return true;
		}
		
		for (int i = 0, len = a1.length; i < len; i++) {
			if (a1[i] != a2[i]) {
				return false;
			}
		}
		return true;
    }

	/**	
	 * 	int数组比较
     * 	<br>对比两者int[]是否一致
     * 	<br>两个数组均为为null返回false
     * 
     * 	@author Pan
     * 	@param 	a1	来源int[]
     * 	@param 	a2	目标int[]
     * 	@return	boolean
     */
    public static boolean eq(int[] a1, int[] a2) {
		if (a1 == null || a2 == null) {
			return false;
		}
    	if (a1.length != a2.length) {
    		return false;
    	}
		if (a1 == a2) {
			return true;
		}
		
		for (int i = 0, len = a1.length; i < len; i++) {
			if (a1[i] != a2[i]) {
				return false;
			}
		}
		return true;
    }

	/**	
	 * 	long数组比较
     * 	<br>对比两者long[]是否一致
     * 	<br>两个数组均为为null返回false
     * 
     * 	@author Pan
     * 	@param 	a1	来源long[]
     * 	@param 	a2	目标long[]
     * 	@return	boolean
     */
    public static boolean eq(long[] a1, long[] a2) {
		if (a1 == null || a2 == null) {
			return false;
		}
    	if (a1.length != a2.length) {
    		return false;
    	}
		if (a1 == a2) {
			return true;
		}
		
		for (int i = 0, len = a1.length; i < len; i++) {
			if (a1[i] != a2[i]) {
				return false;
			}
		}
		return true;
    }
    
	/**	
	 * 	float数组比较
     * 	<br>对比两者float[]是否一致
     * 	<br>两个数组均为为null返回false
     * 
     * 	@author Pan
     * 	@param 	a1	来源float[]
     * 	@param 	a2	目标float[]
     * 	@return	boolean
     */
    public static boolean eq(float[] a1, float[] a2) {
		if (a1 == null || a2 == null) {
			return false;
		}
    	if (a1.length != a2.length) {
    		return false;
    	}
		if (a1 == a2) {
			return true;
		}
		
		for (int i = 0, len = a1.length; i < len; i++) {
			if (a1[i] != a2[i]) {
				return false;
			}
		}
		return true;
    }
    
	/**	
	 * 	double数组比较
     * 	<br>对比两者double[]是否一致
     * 	<br>两个数组均为为null返回false
     * 
     * 	@author Pan
     * 	@param 	a1	来源double[]
     * 	@param 	a2	目标double[]
     * 	@return	boolean
     */
    public static boolean eq(double[] a1, double[] a2) {
		if (a1 == null || a2 == null) {
			return false;
		}
    	if (a1.length != a2.length) {
    		return false;
    	}
		if (a1 == a2) {
			return true;
		}
		
		for (int i = 0, len = a1.length; i < len; i++) {
			if (a1[i] != a2[i]) {
				return false;
			}
		}
		return true;
    }

	/**	
	 * 	char数组比较
     * 	<br>对比两者char[]是否一致
     * 	<br>两个数组均为为null返回false
     * 
     * 	@author Pan
     * 	@param 	a1	来源char[]
     * 	@param 	a2	目标char[]
     * 	@return	boolean
     */
    public static boolean eq(char[] a1, char[] a2) {
		if (a1 == null || a2 == null) {
			return false;
		}
    	if (a1.length != a2.length) {
    		return false;
    	}
		if (a1 == a2) {
			return true;
		}
		
		for (int i = 0, len = a1.length; i < len; i++) {
			if (a1[i] != a2[i]) {
				return false;
			}
		}
		return true;
    }

	/**	
	 * 	boolean数组比较
     * 	<br>对比两者boolean[]是否一致
     * 	<br>两个数组均为为null返回false
     * 
     * 	@author Pan
     * 	@param 	a1	来源boolean[]
     * 	@param 	a2	目标boolean[]
     * 	@return	boolean
     */
    public static boolean eq(boolean[] a1, boolean[] a2) {
		if (a1 == null || a2 == null) {
			return false;
		}
    	if (a1.length != a2.length) {
    		return false;
    	}
		if (a1 == a2) {
			return true;
		}
		
		for (int i = 0, len = a1.length; i < len; i++) {
			if (a1[i] != a2[i]) {
				return false;
			}
		}
		return true;
    }
    
	/**	
	 * 	字节数组比较
     * 	<br>对比两者byte[]是否一致
     * 	<br>两个数组均为为null返回false
     * 
     * 	@author Pan
     * 	@param 	b1	来源byte[]
     * 	@param 	b2	目标byte[]
     * 	@return	boolean
     */
    public static boolean eq(byte[] b1, byte[] b2) {
		if (b1 == null || b2 == null) {
			return false;
		}
    	if (b1.length != b2.length) {
    		return false;
    	}
		if (b1 == b2) {
			return true;
		}
		
		for (int i = 0, len = b1.length; i < len; i++) {
			if (b1[i] != b2[i]) {
				return false;
			}
		}
		return true;
    }
    
    /**	
     * 	对象数组比较
     * 	<br>两个数组均为为null返回false
     * 
     * 	@author Pan
	 * @param 	<T>	  	数据类型
     * 	@param 	o1		数组1
     * 	@param 	o2		数组2
     * 	@return	boolean
     */
    public static <T> boolean eq(T[] o1, T[] o2) {
    	if (o1 == null || o2 == null) {
    		return false;
    	}
        if (o2.length != o1.length) {
            return false;
    	}
        if (o1 == o2) {
        	return true;
        }
        
        for (int i = 0; i < o1.length; i++) {
            if (!ObjectUtils.eq(o1[i], o2[i])) {
            	return false;
            }
        }
        return true;
    }
    
	/** 		
	 * 	String数组转换成String并且带分割符号(默认逗号)
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@return	String
	 */
	public static String toString(String[] arr) {
		if (arr == null) {
			return "null";
		}
		
		int builderLen = 0; 
		int arrLen = arr.length;
		
		for (int i = 0; i < arrLen; i++) {
			int strLen = arr[i].length();
			if (arrLen > 1) {
				builderLen += (strLen) + 1;
			} else {
				builderLen += strLen;
			}
		}
		
		StringBuilder strBuilder = new StringBuilder(builderLen);
		for (int i = 0; i < arrLen; i++) {
			if (i == (arrLen - 1)) {
				strBuilder.append(arr[i]);
			} else {
				strBuilder.append(arr[i]).append(",");
			}
		}
		return strBuilder.toString();
	} 
	
	/**
	 * 	数组toString方法
	 * 	
	 * 	@author Pan
	 * 	@param 	arr		数组
	 * 	@return	String
	 */
	public static String toString(byte[] arr) {
		if (arr == null) {
			return "null";
		}

        int maxLen = arr.length - 1;
        if (maxLen == -1) {
        	return "[]";
        }

        StringBuilder b = new StringBuilder().append('[');
        for (int i = 0; ; i++) {
            b.append(String.valueOf(arr[i]));
            if (i == maxLen) {
            	break;
            }
            b.append(",");
        }
        return b.append(']').toString();
	}
	
	/**
	 * 	数组toString方法
	 * 	
	 * 	@author Pan
	 * 	@param 	arr		数组
	 * 	@return String
	 */
	public static String toString(short[] arr) {
		if (arr == null) {
			return "null";
		}

        int maxLen = arr.length - 1;
        if (maxLen == -1) {
        	return "[]";
        }

        StringBuilder b = new StringBuilder().append('[');
        for (int i = 0; ; i++) {
            b.append(String.valueOf(arr[i]));
            if (i == maxLen) {
            	break;
            }
            b.append(",");
        }
        return b.append(']').toString();
	}
	
	/**
	 * 	数组toString方法
	 * 	
	 * 	@author Pan
	 * 	@param 	arr		数组
	 * 	@return String
	 */
	public static String toString(int[] arr) {
		if (arr == null) {
			return "null";
		}

        int maxLen = arr.length - 1;
        if (maxLen == -1) {
        	return "[]";
        }

        StringBuilder b = new StringBuilder().append('[');
        for (int i = 0; ; i++) {
            b.append(arr[i]);
            if (i == maxLen) {
            	break;
            }
            b.append(",");
        }
        return b.append(']').toString();
	}
	
	/**
	 * 	数组toString方法
	 * 	
	 * 	@author Pan
	 * 	@param 	arr		数组
	 * 	@return String
	 */
	public static String toString(long[] arr) {
		if (arr == null) {
			return "null";
		}

        int maxLen = arr.length - 1;
        if (maxLen == -1) {
        	return "[]";
        }

        StringBuilder b = new StringBuilder().append('[');
        for (int i = 0; ; i++) {
            b.append(arr[i]);
            if (i == maxLen) {
            	break;
            }
            b.append(",");
        }
        return b.append(']').toString();
	}
	
	/**
	 * 	数组toString方法
	 * 	
	 * 	@author Pan
	 * 	@param 	arr		数组
	 * 	@return String
	 */
	public static String toString(float[] arr) {
		if (arr == null) {
			return "null";
		}

        int maxLen = arr.length - 1;
        if (maxLen == -1) {
        	return "[]";
        }

        StringBuilder b = new StringBuilder().append('[');
        for (int i = 0; ; i++) {
			b.append(arr[i]);
            if (i == maxLen) {
            	break;
            }
            b.append(",");
        }
        return b.append(']').toString();
	}
	
	/**
	 * 	数组toString方法
	 * 	
	 * 	@author Pan
	 * 	@param 	arr		数组
	 * 	@return String
	 */
	public static String toString(double[] arr) {
		if (arr == null) {
			return "null";
		}

        int maxLen = arr.length - 1;
        if (maxLen == -1) {
        	return "[]";
        }

        StringBuilder b = new StringBuilder().append('[');
        for (int i = 0; ; i++) {
			b.append(arr[i]);
            if (i == maxLen) {
            	break;
            }
            b.append(",");
        }
        return b.append(']').toString();
	}
	
	/**
	 * 	数组toString方法
	 * 	
	 * 	@author Pan
	 * 	@param 	arr		数组
	 * 	@return String
	 */
	public static String toString(boolean[] arr) {
		if (arr == null) {
			return "null";
		}

        int maxLen = arr.length - 1;
        if (maxLen == -1) {
        	return "[]";
        }

        StringBuilder b = new StringBuilder().append('[');
        for (int i = 0; ; i++) {
			b.append(arr[i]);
            if (i == maxLen) {
            	break;
            }
            b.append(",");
        }
        return b.append(']').toString();
	}
	
	/**
	 * 	数组toString方法
	 * 	
	 * 	@author Pan
	 * 	@param 	arr		数组
	 * 	@return String
	 */
	public static String toString(char[] arr) {
		if (arr == null) {
			return "null";
		}

        int maxLen = arr.length - 1;
        if (maxLen == -1) {
        	return "[]";
        }

        StringBuilder b = new StringBuilder().append('[');
        for (int i = 0; ; i++) {
			b.append(arr[i]);
            if (i == maxLen) {
            	break;
            }
            b.append(",");
        }
        return b.append(']').toString();
	}
	
	/**
	 * 	数组toString方法
	 * 	
	 * 	@author Pan
	 * 	@param 	arr		数组
	 * 	@return String
	 */
	public static String toString(Object[] arr) {
        if (arr == null) {
        	return "null";
        }

        int maxLen = arr.length - 1;
        if (maxLen == -1) {
        	return "[]";
        }

        StringBuilder b = new StringBuilder().append('[');
        for (int i = 0; ; i++) {
			b.append(arr[i]);
            if (i == maxLen) {
            	break;
            }
            b.append(",");
        }
        return b.append(']').toString();
    }
	
	/**	
	 * 	String数组转换int数组
	 * 	
	 * 	@author Pan
	 * 	@param	array		数组
	 * 	@return	int[]
	 */
	public static int[] toInt(String[] array) {
		Assert.isNotNull(array);
		
		int[] intArray = new int[array.length];
		for (int i = 0; i < array.length; i++) {
			intArray[i] = ClassConvertUtils.toInt(array[i]);
		}
		return intArray;
	}
	
	/**	
	 * 	String数组转换Integer数组
	 * 	
	 * 	@author Pan
	 * 	@param	array		数组
	 * 	@return	Integer[]
	 */
	public static Integer[] toIntWrapper(String[] array) {
		Assert.isNotNull(array);
		
		Integer[] intArray = new Integer[array.length];
		for (int i = 0; i < array.length; i++) {
			intArray[i] = ClassConvertUtils.toInt(array[i]);
		}
		return intArray;
	}
	
	/**	
	 * 	String数组转换boolean数组
	 * 	
	 * 	@author Pan
	 * 	@param	array		数组
	 * 	@return	boolean[]
	 */
	public static boolean[] toBoolean(String[] array) {
		Assert.isNotNull(array);
		
		boolean[] booleanArray = new boolean[array.length];
		for (int i = 0; i < array.length; i++) {
			booleanArray[i] = ClassConvertUtils.toBoolean(array[i]);
		}
		return booleanArray;
	}
	
	/**	
	 * 	String数组转换Integer数组
	 * 	
	 * 	@author Pan
	 * 	@param	array		数组
	 * 	@return	Boolean[]
	 */
	public static Boolean[] toBooleanWrapper(String[] array) {
		Assert.isNotNull(array);
		
		Boolean[] booleanArray = new Boolean[array.length];
		for (int i = 0; i < array.length; i++) {
			booleanArray[i] = ClassConvertUtils.toBoolean(array[i]);
		}
		return booleanArray;
	}
	
	/**	
	 * 	String数组转换long数组
	 * 	
	 * 	@author Pan
	 * 	@param	array		数组
	 * 	@return	long[]
	 */
	public static long[] toLong(String[] array) {
		Assert.isNotNull(array);
		
		long[] longArray = new long[array.length];
		for (int i = 0; i < array.length; i++) {
			longArray[i] = ClassConvertUtils.toInt(array[i]);
		}
		return longArray;
	}
	
	/**	
	 * 	String数组转换Long数组
	 * 	
	 * 	@author Pan
	 * 	@param	array		数组
	 * 	@return	Long[]
	 */
	public static Long[] toLongWrapper(String[] array) {
		Assert.isNotNull(array);
		
		Long[] longArray = new Long[array.length];
		for (int i = 0; i < array.length; i++) {
			longArray[i] = ClassConvertUtils.toLong(array[i]);
		}
		return longArray;
	}
	
	/**	
	 * 	二分查找
	 * 	
	 * 	@author Pan
	 * 	@param 	array		数组
	 * 	@param 	value		搜索值
	 * 	@return	int
	 */
	public static int binarySearch(short[] array, int value) {
	    //	定义初始最小、最大索引
	    int start = 0;
	    int end = array.length - 1;
	    
	    //	确保不会出现重复查找，越界
	    while (start <= end) {
	        //	计算出中间索引值
	        int middle = (end + start) >>> 1;//防止溢出
	        if (value == array[middle]) {
	            return middle;
	        //	判断下限
	        } else if (value < array[middle]) {
	            end = middle - 1;
	        //	判断上限
	        } else {
	            start = middle + 1;
	        }
	    }
	    //若没有，则返回-1
	    return NO_SUCH_VALUE;
	}
	
	/**	
	 * 	二分查找
	 * 	
	 * 	@author Pan
	 * 	@param 	array		数组
	 * 	@param 	value		搜索值
	 * 	@return	int
	 */
	public static int binarySearch(int[] array, int value) {
	    int start = 0;
	    int end = array.length - 1;
	    
	    while (start <= end) {
	        int middle = (end + start) >>> 1;
	        if (value == array[middle]) {
	            return middle;
	        } else if (value < array[middle]) {
	            end = middle - 1;
	        } else {
	            start = middle + 1;
	        }
	    }
	    return NO_SUCH_VALUE;
	}
	
	/**	
	 * 	二分查找
	 * 	
	 * 	@author Pan
	 * 	@param 	array		数组
	 * 	@param 	value		搜索值
	 * 	@return	long
	 */
	public static long binarySearch(long[] array, long value) {
	    int start = 0;
	    int end = array.length - 1;
	    
	    while (start <= end) {
	        int middle = (end + start) >>> 1;
	        if (value == array[middle]) {
	            return middle;
	        } else if (value < array[middle]) {
	            end = middle - 1;
	        } else {
	            start = middle + 1;
	        }
	    }
	    return NO_SUCH_VALUE;
	}
}
