package com.gitee.apanlh.util.base;

import com.gitee.apanlh.exp.CompareException;
import com.gitee.apanlh.util.base.builder.CollBuilder;
import com.gitee.apanlh.util.reflection.ClassUtils;
import com.gitee.apanlh.util.reflection.ReflectionUtils;
import com.gitee.apanlh.util.valid.ValidParam;

import java.lang.reflect.Field;
import java.util.Comparator;
import java.util.List;

/**	
 * 	集合对比器
 * 
 * 	@author Pan
 */
class CollComparator {
	
	/** 基础对比器 */
	static final Comparator<Object> BASIC_COMPARATOR = BigDecimalUtils::compare;

	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private CollComparator() {
		//	不允许外部实例
		super();
	}
	
	/**	
	 * 	设置比较器对象
	 * 	
	 * 	@author Pan
	 * 	@param 	o1		对象1
	 * 	@param 	o2		对象2
	 *	@return	List
	 */
	static <T> List<Object> setCompareList(T o1, T o2) {
		return CollBuilder.builder().add(o1).add(o2).build();
	}
	
	/**	
	 * 	获取比较器属性值
	 * 
	 * 	@author Pan
	 * 	@param 	listObject	集合对象
	 * 	@param 	fieldName	字段名
	 * 	@return	List
	 */
	static <T> List<String> getCompareObj(List<T> listObject, String fieldName) {
		try {
			int objSize = listObject.size();
			List<String> list = CollUtils.newArrayList(objSize);
			
			Field field = ReflectionUtils.getField(ClassUtils.getClass(listObject), fieldName);
			
			for (int i = 0; i < objSize; i++) {
				String val = String.valueOf(ReflectionUtils.getFieldValue(field, listObject.get(i)));
				if (!ValidParam.isEmpty(val)) {
					list.add(val);
				}
			}
			//	最大对象参数
			int parameterLength = 2;
			return parameterLength != list.size() ? null : list;
		} catch (Exception e) {
			throw new CompareException(e.getMessage(), e);
		}
	}
}
