package com.gitee.apanlh.util.base;

import com.gitee.apanlh.util.valid.ValidParam;

/**	
 * 	中文格式化
 * 	
 * 	@author Pan
 */
public class ChineseFormatUtils {
	
	/** 繁体中文形式 */
	private static final String[] NUMBER_DIGITS = {"零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖"};
	/**	繁体中文单位 */
	private static final String[] NUMBER_UNITS = {"", "拾", "佰", "仟"};
	
	private ChineseFormatUtils() {
		super();
	}
	
	/**
	 * 阿拉伯数字转换成中文,小数点后四舍五入保留两位. 使用于整数、小数的转换.
	 *	
	 * @param 	amount  数字
	 * @return 	String	中文
	 */
	public static String format(double amount) {
		final String[] numArray = NUMBER_DIGITS;

		if (amount > 99999999999999.99 || amount < -99999999999999.99) {
			throw new IllegalArgumentException("number support only: (-99999999999999.99 ~ 99999999999999.99)！");
		}

		boolean negative = false;
		if (amount < 0) {
			negative = true;
			amount = -amount;
		}

		long temp = Math.round(amount * 100);
		int numFen = (int) (temp % 10);
		temp = temp / 10;
		int numJiao = (int) (temp % 10);
		temp = temp / 10;

		//将数字以万为单位分为多份
		int[] parts = new int[20];
		int numParts = 0;
		for (int i = 0; temp != 0; i++) {
			int part = (int) (temp % 10000);
			parts[i] = part;
			numParts++;
			temp = temp / 10000;
		}

		// 标志“万”下面一级是不是 0
		boolean beforeWanIsZero = true;

		StringBuilder chineseStr = new StringBuilder();
		for (int i = 0; i < numParts; i++) {
			String partChinese = toChinese(parts[i]);
			if (i % 2 == 0) {
				beforeWanIsZero = ValidParam.isEmpty(partChinese);
			}

			if (i != 0) {
				if (i % 2 == 0) {
					chineseStr.insert(0, "亿");
				} else {
					if ("".equals(partChinese) && !beforeWanIsZero) {
						// 如果“万”对应的 part 为 0，而“万”下面一级不为 0，则不加“万”，而加“零”
						chineseStr.insert(0, "零");
					} else {
						if (parts[i - 1] < 1000 && parts[i - 1] > 0) {
							// 如果"万"的部分不为 0, 而"万"前面的部分小于 1000 大于 0， 则万后面应该跟“零”
							chineseStr.insert(0, "零");
						}
						if (parts[i] > 0) {
							// 如果"万"的部分不为 0 则增加万
							chineseStr.insert(0, "万");
						}
					}
				}
			}
			chineseStr.insert(0, partChinese);
		}

		// 整数部分为 0, 则表达为"零"
		if ("".equals(chineseStr.toString())) {
			chineseStr = new StringBuilder(numArray[0]);
		}
		//负数
		if (negative) { // 整数部分不为 0
			chineseStr.insert(0, "负");
		}

		// 小数部分
		if (numFen != 0 || numJiao != 0) {
			if (numFen == 0) {
				chineseStr.append("元").append(numArray[numJiao]).append("角");
			} else { // “分”数不为 0
				if (numJiao == 0) {
					chineseStr.append("元零").append(numArray[numFen]).append("分");
				} else {
					chineseStr.append("元").append(numArray[numJiao]).append("角").append(numArray[numFen]).append("分");
				}
			}
		} else {
			//无小数部分的金额结尾
			chineseStr.append("元整");
		}
		return chineseStr.toString();
	}

	/**
	 * 数字字符转中文，非数字字符原样返回
	 *
	 * @param 	c                数字字符
	 * @return 	中文字符
	 */
	public static String numberCharToChinese(char c) {
		int index = c - 48;
		if (index < 0 || index >= NUMBER_DIGITS.length) {
			return String.valueOf(c);
		}
		return NUMBER_DIGITS[index];
	}

	/**
	 * 把一个 0~9999 之间的整数转换为汉字的字符串，如果是 0 则返回 ""
	 *
	 * @param 	amountPart       数字部分
	 * @return 转换后的汉字
	 */
	private static String toChinese(int amountPart) {
		int temp = amountPart;

		StringBuilder chineseStr = new StringBuilder();
		boolean lastIsZero = true; // 在从低位往高位循环时，记录上一位数字是不是 0
		for (int i = 0; temp > 0; i++) {
			int digit = temp % 10;
			if (digit == 0) { // 取到的数字为 0
				if (!lastIsZero) {
					// 前一个数字不是 0，则在当前汉字串前加“零”字;
					chineseStr.insert(0, "零");
				}
				lastIsZero = true;
			} else { 
				// 取到的数字不是 0
				chineseStr.insert(0, NUMBER_DIGITS[digit].concat(NUMBER_UNITS[i]));
				lastIsZero = false;
			}
			temp = temp / 10;
		}
		return chineseStr.toString();
	}
}
