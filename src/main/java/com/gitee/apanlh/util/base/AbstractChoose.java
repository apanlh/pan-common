package com.gitee.apanlh.util.base;

import com.gitee.apanlh.exp.CallRuntimeException;
import com.gitee.apanlh.util.func.FuncCall;
import com.gitee.apanlh.util.func.FuncExecute;

/**	
 * 	抽象类-用于条件选择使用
 * 	
 * 	@author Pan
 */
public abstract class AbstractChoose<V> {
	
	/** 条件 */
	private boolean condition;
	/** 值 */
	private V value;
	
	/**
	 * 	默认构造函数
	 * 
	 * 	@author Pan
	 */
	AbstractChoose() {
		super();
	}
	
	/**	
	 * 	构造函数-初始化条件对象
	 * 	<br>验证条件结果为true返回Value
	 * 	
	 * 	@author Pan
	 * 	@param 	condition	条件
	 * 	@param 	value		对象
	 */
	AbstractChoose(boolean condition, V value) {
		condition(condition, value);
	}
	
	/**	
	 * 	构造-初始化条件对象
	 * 	<br>验证条件结果为Ture返回Value
	 * 	<br>将不会初始化结果状态
	 * 	
	 * 	@author Pan
	 * 	@param 	condition	条件
	 * 	@param 	func		回调对象
	 */
	AbstractChoose(boolean condition, FuncCall<V> func) {
		if (!condition) {
			return ;
		}
		try {
			condition(true, func.call());
		} catch (Exception e) {
			throw new CallRuntimeException(e.getMessage(), e);
		}
	}

	/**
	 * 	构造-初始化条件对象
	 * 	<br>验证条件结果为Ture返回Value
	 * 	<br>将不会初始化结果状态
	 *
	 * 	@author Pan
	 * 	@param 	condition	条件
	 * 	@param 	func		回调对象
	 */
	AbstractChoose(boolean condition, FuncExecute func) {
		if (!condition) {
			return ;
		}
		try {
			this.condition = true;
			func.execute();
		} catch (Exception e) {
			throw new CallRuntimeException(e.getMessage(), e);
		}
	}

	/**
	 * 条件验证
	 *
	 * @param condition 条件
	 * @param value     数据
	 * @author Pan
	 */
	private void condition(boolean condition, V value) {
		if (setCondition(condition)) {
			setValue(value);
		}
	}

	/**	
	 * 	获取条件结果
	 * 	
	 * 	@author Pan
	 * 	@return	boolean
	 */
	boolean getCondition() {
		return this.condition;
	}
	
	/**	
	 * 	获取返回第一次出现true的结果值
	 * 	<br>如果没有将获取else值
	 * 	
	 * 	@author Pan
	 * 	@return	V
	 */
	V getValue() {
		return this.value;
	}
	
	/**	
	 * 	设置条件结果
	 * 	
	 * 	@author Pan
	 * 	@param 	condition	条件
	 * 	@return	boolean
	 */
	boolean setCondition(boolean condition) {
		this.condition = condition;
		return this.condition;
	}
	
	/**	
	 * 	设置值
	 * 	
	 * 	@author Pan
	 * 	@param 	value	值数据
	 */
	void setValue(V value) {
		this.value = value;
	}
}
