package com.gitee.apanlh.util.base.builder;

import com.gitee.apanlh.util.base.CollUtils;
import com.gitee.apanlh.util.base.MapUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**	
 * 	MapBuilder
 * 	<pre>
 * 	调用用例:
 * 	{@code
 *  1.Map<String, Object> build = new MapBuilder.Builder<String, Object>()
 *		.put("1", "2")
 *	.build();
 *	2.Map<String, Object> build2 = MapBuilder.<String, Object>builder()
 *		.put("1", 2)
 *	.build();
 *	3.Map<String, TestBean> build3 = new MapBuilder.Builder<String, TestBean>()
 *		.put("1", new TestBean("张三"))
 *	.build();
 *	}
 *	</pre>
 *
 * 	@author Pan
 */
public class MapBuilder<K, V> {
	
	/** 值 */
	private Map<K, V> map;
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	public MapBuilder() {
		this(16);
	}
	
	/**	
	 * 	构造函数
	 * 	<br>自定义长度
	 * 	
	 * 	@author Pan
	 * 	@param 	size	长度
	 */
	public MapBuilder(int size) {
		this(new HashMap<>(size));
	}
	
	/**	
	 * 	构造函数
	 * 	<br>自定义Map
	 * 	
	 * 	@author Pan
	 * 	@param 	map		Map
	 */
	public MapBuilder(Map<K, V> map) {
		this.map = map;
	}

	/**
	 * 	添加键值
	 * 
	 * 	@author Pan
	 * 	@param 	key		键
	 * 	@param 	value	值
	 * 	@return	MapBuilder
	 */
	public MapBuilder<K, V> put(K key, V value) {
		this.map.put(key, value);
		return this;
	}
	
	/**	
	 * 	根据Map添加
	 * 	
	 * 	@author Pan
	 * 	@param 	map		Map
	 * 	@return	MapBuilder
	 */
	public MapBuilder<K, V> putAll(Map<K, V> map) {
		this.map.putAll(map);
		return this;
	}
	
	/**	
	 * 	清空Map
	 * 	
	 * 	@author Pan
	 * 	@return	MapBuilder
	 */
	public MapBuilder<K, V> clear() {
		if (!MapUtils.isEmpty(this.map)) {
			this.map.clear();
		}
		return this;
	}
	
	/**	
	 * 	构建
	 * 	
	 * 	@author Pan
	 * 	@return	Map
	 */
	public Map<K, V> build() {
		return this.map;
	}
	
	/**	
	 * 	构建EntrySet
	 * 	
	 * 	@author Pan
	 * 	@return	Set
	 */
	public Set<Entry<K, V>> buildEntrySet() {
		return this.map.entrySet();
	}
	
	/**	
	 * 	构建KeySet
	 * 	
	 * 	@author Pan
	 * 	@return	Set
	 */
	public Set<K> buildKeySet() {
		return this.map.keySet();
	}
	
	/**	
	 * 	构建ArrayList
	 * 	
	 * 	@author Pan
	 * 	@return	List
	 */
	public List<V> buildList() {
		return CollUtils.newArrayList(this.map.values());
	}
	
	/**	
	 * 	建造类
	 * 	
	 * 	@author Pan
	 */
	public static class Builder<K, V> extends MapBuilder<K, V> {

		/**
		 * 	默认构造
		 * 	
		 * 	@author Pan
		 */
		public Builder() {
			super();
		}
		
		/**	
		 * 	构造-自定义长度
		 * 	
		 * 	@author Pan
		 * 	@param 	size	长度
		 */
		public Builder(int size) {
			super(size);
		}
		
		/**	
		 * 	构造-自定义map
		 * 
		 * 	@author Pan
		 * 	@param 	map		Map
		 */
		public Builder(Map<K, V> map) {
			super(map);
		}
	}
}
