package com.gitee.apanlh.util.base;

/**	
 * 	操作运算符枚举
 * 	
 * 	@author Pan
 */
public enum OperatorEnum {
	
	PLUS("+"),
	MINUS("-"),
	MULTIPLY("*"),
	DIVISION("/"),
	MODULE("%"),
	;
	
	/** 操作运算符名称 */
	private String operatorName;
	
	/**
	 * 	构造函数-运算符
	 * 	
	 * 	@author Pan
	 * 	@param 	operatorName	操作运算符名称
	 */
	OperatorEnum(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getOperatorName() {
		return operatorName;
	}
}
