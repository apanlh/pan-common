package com.gitee.apanlh.util.base;

import com.gitee.apanlh.util.cache.local.Cache;
import com.gitee.apanlh.util.cache.local.CacheUtils;

import java.lang.reflect.Array;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;


/**	
 * 	用于返回空属性
 * 
 * 	@author Pan
 */
public class Empty {
	
	
	/** String */
	private static final String 						STRING_EMPTY 			= "";
	private static final String			 				STRING_NULL 			= "null";
	/** Array */
	private static final String[] 						ARRAY_STRING 			= new String	[0];
	private static final byte[] 						ARRAY_BYTE 				= new byte		[0];
	private static final short[]				    	ARRAY_SHORT     		= new short		[0];
	private static final int[] 							ARRAY_INT 				= new int		[0];
	private static final long[] 						ARRAY_LONG 				= new long		[0];
	private static final float[] 						ARRAY_FLOAT 			= new float		[0];
	private static final double[] 						ARRAY_DOUBLE 			= new double	[0];
	private static final boolean[] 						ARRAY_BOOLEAN 			= new boolean	[0];
	private static final char[] 						ARRAY_CHAR 				= new char		[0];
	private static final Byte[] 						ARRAY_BYTE_WRAPPER 		= new Byte		[0];
	private static final Short[]				    	ARRAY_SHORT_WRAPPER     = new Short		[0];
	private static final Integer[] 						ARRAY_INT_WRAPPER 		= new Integer	[0];
	private static final Long[] 						ARRAY_LONG_WRAPPER 		= new Long		[0];
	private static final Float[] 						ARRAY_FLOAT_WRAPPER 	= new Float		[0];
	private static final Double[] 						ARRAY_DOUBLE_WRAPPER 	= new Double	[0];
	private static final Boolean[] 						ARRAY_BOOLEAN_WRAPPER 	= new Boolean	[0];
	private static final Character[] 					ARRAY_CHARACTER_WRAPPER = new Character	[0];
	private static final Cache<Class<?>, Object[]> ARRAY_CACHE 			= CacheUtils.cache(64);
	private static final Class<?>[]                     ARRAY_CLASS 		    = new Class		[0];
	
	/** List */
	private static final List<Object> 					LIST 					= Collections.emptyList();
	
	/** Set */
	private static final Set<Object> 					SET 					= Collections.emptySet();
	private static final SortedSet<Object> 				SORTED_SET 				= Collections.emptySortedSet();
	private static final NavigableSet<Object> 			NAVIGABLE_SET 			= Collections.emptyNavigableSet();
	
	/** Map */
	private static final Map<Object, Object> 			MAP 					= Collections.emptyMap();
	private static final SortedMap<Object, Object> 		SORTED_MAP 				= Collections.emptySortedMap();
	private static final NavigableMap<Object, Object> 	NAVIGABLE_MAP 			= Collections.emptyNavigableMap();
	
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private Empty() {
		//	不允许外部实例
		super();
	}
	
	/**	
	 * 	String-返回空串
	 * 
	 * 	@author Pan
	 * 	@return String
	 */
	public static String str() {
		return STRING_EMPTY;
	}
	
	/**	
	 * 	String-返回null字符串
	 * 
	 * 	@author Pan
	 * 	@return String
	 */
	public static String strNull() {
		return STRING_NULL;
	}
	
	/**	
	 * 	String-返回空数组
	 * 	<br>不可改变
	 * 
	 * 	@author Pan
	 * 	@return String[]
	 */
	public static String[] arrayString() {
		return ARRAY_STRING;
	}
	
	/**	
	 * 	byte-返回空数组
	 * 	<br>不可改变
	 * 
	 * 	@author Pan
	 * 	@return byte[]
	 */
	public static byte[] arrayByte() {
		return ARRAY_BYTE;
	}
	
	/**	
	 * 	short-返回空数组
	 * 	<br>不可改变
	 * 
	 * 	@author Pan
	 * 	@return short[]
	 */
	public static short[] arrayShort() {
		return ARRAY_SHORT;
	}
	
	/**	
	 * 	int-返回空数组
	 * 	<br>不可改变
	 * 
	 * 	@author Pan
	 * 	@return int[]
	 */
	public static int[] arrayInt() {
		return ARRAY_INT;
	}
	
	/**	
	 * 	long-返回空数组
	 * 	<br>不可改变
	 * 
	 * 	@author Pan
	 * 	@return long[]
	 */
	public static long[] arrayLong() {
		return ARRAY_LONG;
	}
	
	/**	
	 * 	float-返回空数组
	 * 	<br>不可改变
	 * 
	 * 	@author Pan
	 * 	@return float[]
	 */
	public static float[] arrayFloat() {
		return ARRAY_FLOAT;
	}
	
	/**	
	 * 	double-返回空数组
	 * 	<br>不可改变
	 * 
	 * 	@author Pan
	 * 	@return double[]
	 */
	public static double[] arrayDouble() {
		return ARRAY_DOUBLE;
	}
	
	/**	
	 * 	boolean-返回空数组
	 * 	<br>不可改变
	 * 
	 * 	@author Pan
	 * 	@return boolean[]
	 */
	public static boolean[] arrayBoolean() {
		return ARRAY_BOOLEAN;
	}
	
	/**	
	 * 	char-返回空数组
	 * 	<br>不可改变
	 * 
	 * 	@author Pan
	 * 	@return char[]
	 */
	public static char[] arrayChar() {
		return ARRAY_CHAR;
	}
	
	/**	
	 * 	Byte-返回空数组
	 * 	<br>不可改变
	 * 	<br>包装类型
	 * 
	 * 	@author Pan
	 * 	@return Byte[]
	 */
	public static Byte[] arrayByteWrapper() {
		return ARRAY_BYTE_WRAPPER;
	}
	
	/**	
	 * 	Short-返回空数组
	 * 	<br>不可改变
	 * 	<br>包装类型
	 * 
	 * 	@author Pan
	 * 	@return Short[]
	 */
	public static Short[] arrayShortWrapper() {
		return ARRAY_SHORT_WRAPPER;
	}
	
	/**	
	 * 	Integer-返回空数组
	 * 	<br>不可改变
	 * 	<br>包装类型
	 * 
	 * 	@author Pan
	 * 	@return Integer[]
	 */
	public static Integer[] arrayIntWrapper() {
		return ARRAY_INT_WRAPPER;
	}
	
	/**	
	 * 	Long-返回空数组
	 * 	<br>不可改变
	 * 	<br>包装类型
	 * 
	 * 	@author Pan
	 * 	@return Long[]
	 */
	public static Long[] arrayLongWrapper() {
		return ARRAY_LONG_WRAPPER;
	}
	
	/**	
	 * 	Float-返回空数组
	 * 	<br>不可改变
	 * 	<br>包装类型
	 * 
	 * 	@author Pan
	 * 	@return Float[]
	 */
	public static Float[] arrayFloatWrapper() {
		return ARRAY_FLOAT_WRAPPER;
	}
	
	/**	
	 * 	Double-返回空数组
	 * 	<br>不可改变
	 * 	<br>包装类型
	 * 
	 * 	@author Pan
	 * 	@return Double[]
	 */
	public static Double[] arrayDoubleWrapper() {
		return ARRAY_DOUBLE_WRAPPER;
	}
	
	/**	
	 * 	Boolean-返回空数组
	 * 	<br>不可改变
	 * 	<br>包装类型
	 * 
	 * 	@author Pan
	 * 	@return Boolean[]
	 */
	public static Boolean[] arrayBooleanWrapper() {
		return ARRAY_BOOLEAN_WRAPPER;
	}
	
	/**	
	 * 	Character-返回空数组
	 * 	<br>不可改变
	 * 	<br>包装类型
	 * 
	 * 	@author Pan
	 * 	@return Character[]
	 */
	public static Character[] arrayCharacterWrapper() {
		return ARRAY_CHARACTER_WRAPPER;
	}
	
	/**	
	 * 	对象-返回对象
	 * 	<br>不可改变
	 * 
	 * 	@author Pan
	 * 	@param 	<T>	  数据类型
	 * 	@param 	clazz  类
	 * 	@return T[]
	 */
	@SuppressWarnings("unchecked")
	public static <T> T[] arrayObject(Class<T> clazz) {
		return (T[]) ARRAY_CACHE.get(clazz, () -> ARRAY_CACHE.put(clazz, (T[]) Array.newInstance(clazz, 0)));
	}
	

	/**	
	 * 	类-返回类
	 * 	<br>不可改变
	 * 
	 * 	@author Pan
	 * 	@return Class[]
	 */
	public static Class<?>[] arrayClass() {
		return ARRAY_CLASS;
	}
	
	/**	
	 * 	List-返回空集合
	 * 	<br>不可改变
	 * 	<br>不可添加
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>	  数据类型
	 * 	@return List
	 */
	@SuppressWarnings("unchecked")
	public static <T> List<T> list() {
		return (List<T>) LIST;
	}
	
	/**	
	 * 	Set-返回空集合
	 * 	<br>不可改变
	 * 	<br>不可添加
	 * 	
	 * 	@author Pan
	 * 	@param <T>	  数据类型
	 * 	@return Set
	 */
	@SuppressWarnings("unchecked")
	public static <T> Set<T> set() {
		return (Set<T>) SET;
	}

	/**	
	 * 	SortedSet-返回空集合
	 * 	<br>不可改变
	 * 	<br>不可添加
	 * 	
	 * 	@author Pan
	 * 	@param <T>	  数据类型
	 * 	@return SortedSet
	 */
	@SuppressWarnings("unchecked")
	public static <T> SortedSet<T> sortedSet() {
		return (SortedSet<T>) SORTED_SET;
	}
	
	/**	
	 * 	NavigableSet-返回空集合
	 * 	<br>不可改变
	 * 	<br>不可添加
	 * 	
	 * 	@author Pan
	 * 	@param <T>	  数据类型
	 * 	@return NavigableSet
	 */
	@SuppressWarnings("unchecked")
	public static <T> NavigableSet<T> navigableSet() {
		return (NavigableSet<T>) NAVIGABLE_SET;
	}
	
	/**	
	 * 	Map-返回空
	 * 	<br>不可改变
	 * 	<br>不可添加
	 * 	
	 * 	@author Pan
	 *  @param  <K>     键类型
	 *  @param  <V>     值类型
	 * 	@return Map
	 */
	@SuppressWarnings("unchecked")
	public static <K, V> Map<K, V> map() {
		return (Map<K, V>) MAP;
	}
	
	/**	
	 * 	SortedMap-返回空
	 * 	<br>不可改变
	 * 	<br>不可添加
	 * 	
	 * 	@author Pan
	 *  @param  <K>     键类型
	 *  @param  <V>     值类型
	 * 	@return SortedMap
	 */
	@SuppressWarnings("unchecked")
	public static <K, V> SortedMap<K, V> sortedMap() {
		return (SortedMap<K, V>) SORTED_MAP;
	}
	
	/**	
	 * 	NavigableMap-返回空
	 * 	<br>不可改变
	 * 	<br>不可添加
	 * 	
	 * 	@author Pan
	 *  @param  <K>     键类型
	 *  @param  <V>     值类型
	 * 	@return NavigableMap
	 */
	@SuppressWarnings("unchecked")
	public static <K, V> NavigableMap<K, V> navigableMap() {
		return (NavigableMap<K, V>) NAVIGABLE_MAP;
	}
}
