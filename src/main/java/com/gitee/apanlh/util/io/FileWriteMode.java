package com.gitee.apanlh.util.io;

/**	
 * 	文件写入模式
 * 	
 * 	@author Pan
 */
public enum FileWriteMode {
	
	/** 零拷贝 */
	ZERO_COPY,
	/** 内存映射 */
	M_MAP,
	;
}
