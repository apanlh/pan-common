package com.gitee.apanlh.util.io;

import com.gitee.apanlh.util.base.Empty;

import java.util.Iterator;

/**
 * 	IO流读取分割器
 * 	<br>用于分段读取时指定读取缓冲大小，并且每次返回相对应字节数
 * 	#mark 待补充
 *
 * 	@author Pan
 */
public class ReadSplitter implements Iterator<byte[]> {
	
	@Override
	public boolean hasNext() {
		return false;
	}
	
	/**	
	 * 	读取方法
	 * 	<br>自定义长度
	 * 	
	 * 	@author Pan
	 * 	@return	byte[]	返回指定长度字节
	 */	
	@Override
	public byte[] next() {
		return Empty.arrayByte();
	}
	
}
