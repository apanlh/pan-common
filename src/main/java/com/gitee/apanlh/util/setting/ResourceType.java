package com.gitee.apanlh.util.setting;

/**
 * 	资源加载类型包含(前缀、文件协议、特殊文件分隔符)
 * 	
 * 	@author Pan
 */
public enum ResourceType {
	
	/** 前缀匹配资源 classpath: */
	PREFIX_CLASSPATH("classpath:"),
	/** 前缀匹配资源 classes */
	PREFIX_CLASSES("classes/"),
	/** 前缀匹配资源 file: */
	PREFIX_FILE("file:"),
	/** 前缀匹配资源 jar: */
	PREFIX_JAR("jar:"),
	/** 前缀匹配资源 war: */
	PREFIX_WAR("war:"),

	/** 资源协议 file */
	PROTOCOL_FILE("file"),
	/** 资源协议 jar */
	PROTOCOL_JAR("jar"),
	/** 资源协议 war */
	PROTOCOL_WAR("war"),
	/** 资源协议 zip */
	PROTOCOL_ZIP("zip"),
	/** 资源协议 wsjar */
	PROTOCOL_WSJAR("wsjar"),
	/** 资源协议 vfszip */
	PROTOCOL_VFSZIP("vfszip"),
	/** 资源协议 vfsfile */
	PROTOCOL_VFSFILE("vfsfile"),
	/** 资源协议 vfs */
	PROTOCOL_VFS("vfs"),
	
	/** 特殊资源文件分隔符 */
	SEPARATOR_JAR("!/"),
	SEPARATOR_WAR("*/");
	
	/** 值 */
	private String value;

	/**	
	 * 	构造函数-初始化值
	 * 	
	 * 	@author Pan
	 * 	@param 	value	值
	 */
	ResourceType(String value) {
		this.value = value;
	}

	/**	
	 * 	获取值
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getValue() {
		return value;
	}
		
	/**	
	 * 	资源地址以某个前缀进行匹配
	 * 	<br>限定4种前缀匹配
	 * 	
	 * 	@author Pan
	 * 	@param 	path			地址
	 * 	@param 	resourceType	资源类型枚举z
	 * 	@return	boolean
	 */
	public static boolean isPrefix(String path, ResourceType resourceType) {
		if (path == null) {
			return false;
		}
		return path.startsWith(resourceType.getValue());
	}
}
