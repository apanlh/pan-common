package com.gitee.apanlh.util.setting;

import com.gitee.apanlh.util.valid.ValidParam;

import java.util.Map;

/**	
 * 	读取配置文件相关工具类
 * 
 * 	@author Pan
 */
public class PropertiesUtils {
	
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private PropertiesUtils() {
		//	不允许外部实例
		super();
	}
	
	/**	
	 * 	读取配置文件
	 * 	<br>资源地址格式:"classpath:", "classes\", "jar:", "war:", "file"
	 * 	<br>classPath:/a.properties
	 * 	<br>classes\abc.properties(如果不填写默认也会找classes下)
	 * 	<br>默认读取classes下
	 *  <br>默认UTF-8字符集编码
	 *  	
	 * 	@author Pan
	 * 	@param  resourceLocation	资源路径
	 * 	@return	Map
	 */
	public static Map<String, String> read(String resourceLocation) {
		return ConfigLoader.create(ResourceUtils.getUrl(null, resourceLocation)).loadFromProperties();
	}
	
	/**	
	 * 	读取配置文件
	 * 	<br>资源地址格式:"classpath:", "classes\", "jar:", "war:", "file"
	 * 	<br>classPath:/a.properties
	 * 	<br>classes\abc.properties(如果不填写默认也会找classes下)
	 * 	<br>默认读取classes下
	 *  <br>自定义字符集编码
	 *  	
	 * 	@author Pan
	 * 	@param  resourceLocation	资源路径
	 * 	@param 	charset				字符集编码
	 * 	@return	Map
	 */
	public static Map<String, String> read(String resourceLocation, String charset) {
		return read(null, resourceLocation, charset);
	}
	
	/**	
	 * 	指定类下进行读取配置文件
	 * 	<br>在指定类下进行读取配置文件，比如{@code A.class, a.properties -> com/pan/test/a.properties位置进行读取配置文件}
	 * 	<br>默认UTF-8字符集编码
	 * 
	 * 	@author Pan
	 * 	@param  clazz				类
	 * 	@param  resourceLocation	资源路径
	 * 	@return	Map
	 */
	public static Map<String, String> read(Class<?> clazz, String resourceLocation) {
		return ConfigLoader.create(ResourceUtils.getUrl(clazz, resourceLocation)).loadFromProperties();
	}
	
	/**	
	 * 	指定类下进行读取配置文件
	 * 	<br>在指定类下进行读取配置文件， {@code 比如A.class, a.properties -> com/pan/test/a.properties位置进行读取配置文件}
	 * 	<br>自定义字符集编码
	 * 
	 * 	@author Pan
	 * 	@param  clazz				类
	 * 	@param  resourceLocation	资源路径
	 * 	@param 	charset				字符集编码
	 * 	@return	Map
	 */
	public static Map<String, String> read(Class<?> clazz, String resourceLocation, String charset) {
		return ConfigLoader.create(ResourceUtils.getUrl(clazz, resourceLocation), charset).loadFromProperties();
	}
	
	/**	
	 * 	读取配置文件
	 * 	<br>资源地址格式:"classpath:", "classes\", "jar:", "war:", "file"
	 * 	<br>classPath:/a.properties
	 * 	<br>classes\abc.properties(如果不填写默认也会找classes下)
	 * 	<br>默认读取classes下
	 * 	
	 * 	@author Pan
	 * 	@param  resourceLocation	资源路径
	 * 	@param 	key					键
	 * 	@return	Map
	 */
	public static String readSingleValue(String resourceLocation, String key) {
		Map<String, String> loadFromProperties = read(resourceLocation);
		return ValidParam.isEmpty(loadFromProperties) ? null : loadFromProperties.get(key);
	}
}
