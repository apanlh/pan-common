package com.gitee.apanlh.util.setting;

import com.gitee.apanlh.util.base.MapUtils;
import com.gitee.apanlh.util.valid.ValidParam;

import java.io.BufferedReader;
import java.util.Map;

/**
 * 	普通文件键值对解析类
 * 	<br>properties等:"、"=键值对解析
 * 	
 * 	@author Pan
 */
public class PropertiesParse extends ConfigParse<String, String> {
	
	/**
	 * 	默认构造函数
	 * 	<br>创建HashMap
	 * 	
	 * 	@author Pan
	 */
	public PropertiesParse() {
		super(MapUtils.newHashMap());
	}
	
	@Override
	public void parse(String readLine) {
		if (ValidParam.isEmpty(readLine)) {
			return ;
		}
		Map<String, String> resolveMap = super.getResolveMap();
		int length = readLine.length();
		
		for (int i = 0; i < length; i++) {
			char c = readLine.charAt(i);
			
			if ('=' == c || ':' == c) {
				String key = readLine.substring(0, i).trim();
				String value = readLine.substring(i + 1, length).trim();
				resolveMap.put(key, value);
				break;
			}
		}
	}

	@Override
	public void parse(BufferedReader reader) {
		// do nothing
	}
}
