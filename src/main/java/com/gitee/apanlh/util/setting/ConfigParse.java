package com.gitee.apanlh.util.setting;

import java.io.BufferedReader;
import java.net.URL;
import java.util.Map;

/**	
 * 	抽象类
 * 	文件资源解析
 * 	
 * 	@author Pan
 */
public abstract class ConfigParse<K, V> {
	
	/** 解析后Map */
	private Map<K, V> resolveMap;
	
	/**
	 * 	默认构造方法
	 * 	
	 * 	@author Pan
	 */
	ConfigParse() {
		super();
	}
	
	/**
	 * 	默认构造方法-自定义初始化Map
	 * 	
	 * 	@author Pan
	 * 	@param 	map		Map
	 */
	ConfigParse(Map<K, V> map) {
		this.resolveMap = map;
	}
	
	/**
	 * 	返回解析读取到的行内容
	 * 	<br>键值对形式
	 * 	
	 * 	@author Pan
	 * 	@param 	readLine	读取行
	 */
	public abstract void parse(String readLine);
	
	/**
	 * 	根据读取器解析
	 * 	<br>键值对形式
	 * 	
	 * 	@author Pan
	 * 	@param 	reader	读取器
	 */
	public abstract void parse(BufferedReader reader);
	
	
	/**	
	 * 	根据URL资源进行加载
	 * 	
	 * 	@author Pan
	 * 	@param 	url		URL资源
	 * 	@param 	charset	字符集编码
	 * 	@return	BufferedReader
	 */
	public BufferedReader load(URL url, String charset) {
		return ResourceUtils.getReader(url, charset);
	}
	
	/**
	 * 	设置解析Map
	 * 	
	 * 	@author Pan
	 * 	@param 	map	Map
	 * 	@return	Map
	 */
	public Map<K, V> setResolveMap(Map<K, V> map) {
		this.resolveMap = map;
		return this.resolveMap;
	}
	
	/**
	 * 	获取解析后的Map
	 * 	
	 * 	@author Pan
	 * 	@return	Map
	 */
	public Map<K, V> getResolveMap() {
		return this.resolveMap;
	}
}
