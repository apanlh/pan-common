package com.gitee.apanlh.util.setting;

import java.io.BufferedReader;

/**	
 * 	用于配置解析器加载时拦截
 *
 * 	@author Pan
 */
interface ConfigHandler {
	
	/**
	 * 	此方法在加载在load之后执行
	 * 	<br>获取Reader之后执行此函数
	 * 		
	 * 	@author Pan
	 * 	@param	reader	读取器
	 */
	void handler(BufferedReader	reader);
}
