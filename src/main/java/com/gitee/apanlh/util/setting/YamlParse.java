package com.gitee.apanlh.util.setting;

import org.yaml.snakeyaml.Yaml;

import java.io.BufferedReader;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 	YML文件解析类
 * 	<br>针对YML文件进行解析
 * 
 * 	@author Pan
 */
public class YamlParse extends ConfigParse<String, LinkedHashMap<String, Object>> {
	
	/**
	 * 	默认构造函数
	 * 	<br>不创建map
	 * 
	 * 	@author Pan
	 */
	public YamlParse() {
		super();
	}
	
	@Override
	public void parse(String readLine) {
		//	do nothing		
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void parse(BufferedReader reader) {
		//	设置解析Map
		super.setResolveMap(new Yaml().loadAs(reader, Map.class));
	}
}
