package com.gitee.apanlh.util.image;


/**
 * 	图形验证码
 *	JDK17-21以不支持
 * 	@author Pan
 */
@Deprecated
public class ImageCodeUtils {
	
//	static {
//		CheckImport.library(CheckLibrary.CAPTCHA);
//	}
//
//	/**
//	 * 	默认构造函数
//	 * 	@author Pan
//	 */
//	private ImageCodeUtils() {
//		super();
//	}
//
//	/**
//	 *	 获取4位数 验证码  包含数字和英文
//	 *
//	 *	@author Pan
//	 * 	@param  request		HttpServletRequest
//	 * 	@param  response	HttpServletResponse
//	 */
//	public static void outImg(HttpServletRequest request, HttpServletResponse response) {
//		try {
//			CaptchaUtil.out(request, response);
//		} catch (IOException e) {
//			throw new RuntimeIoException(e.getMessage(), e);
//		}
//	}
//
//	/**
//	 * 	获取运算验证码 (两位数算数)
//	 *
//	 * 	@author Pan
//	 * 	@param  response	HttpServletResponse
//	 * 	@return 返回运算结果集
//	 */
//	public static String outNumberImg(HttpServletResponse response) {
//		// 设置请求头为输出图片类型
//    	response.setHeader("Pragma", "No-cache");
//    	response.setHeader("Cache-Control", "no-cache");
//    	response.setContentType("image/png");
//    	response.setDateHeader("Expires", 0);
//
//		ArithmeticCaptcha captcha = new ArithmeticCaptcha(130, 48);
//		// 几位数运算，默认是两位
//	    captcha.setLen(2);
//	    // 获取运算的公式：3+2=?
//	    captcha.getArithmeticString();
//	    // 获取运算的结果：
//	    String text = captcha.text();
//		try {
//			captcha.out(response.getOutputStream());
//		} catch (IOException e) {
//			throw new RuntimeIoException(e.getMessage(), e);
//		}
//	    return text;
//	}
//
//	/**
//	 *	 获取 动态gif图验证码  包含中英文
//	 *
//	 * 	@author Pan
//	 * 	@param  request		HttpServletResponse
//	 * 	@param  response	HttpServletResponse
//	 */
//	public static void outGifImg(HttpServletRequest request, HttpServletResponse response) {
//		// 设置请求头为输出图片类型
//    	response.setHeader("Pragma", "No-cache");
//    	response.setHeader("Cache-Control", "no-cache");
//    	response.setContentType("image/gif");
//    	response.setDateHeader("Expires", 0);
//
//        // 使用gif验证码  设置宽、高、验证位数
//        GifCaptcha gifCaptcha = new GifCaptcha(130,48,5);
//		try {
//        	CaptchaUtil.out(gifCaptcha, request, response);
//		} catch (IOException e) {
//			throw new RuntimeIoException(e.getMessage(), e);
//		}
//	}
//
//	/**
//	 *	获取4位数 验证码  包含数字和英文
//	 *	<br> 转换base64
//	 *
//	 * 	@author Pan
//	 * 	@return	ImageCodeObject
//	 */
//	public static ImageCodeObject outBase64Img() {
//		return outBase64Img(4);
//	}
//
//	/**
//	 * 	获取4位数 验证码  包含数字和英文
//	 *	<br>转换base64
//	 *
//	 * 	@author Pan
//	 * 	@param 	codeLen		验证码长度
//	 * 	@return	ImageCodeObject
//	 */
//	public static ImageCodeObject outBase64Img(int codeLen) {
//		return outBase64Img(130, 48, codeLen);
//	}
//
//	/**
//	 * 	获取4位数 验证码  包含数字和英文
//	 *	<br>转换base64
//	 *
//	 * 	@author Pan
//	 * 	@param 	width		宽度
//	 * 	@param 	height		高度
//	 * 	@param 	codeLen		验证码长度
//	 * 	@return	ImageCodeObject
//	 */
//	public static ImageCodeObject outBase64Img(int width, int height, int codeLen) {
//		SpecCaptcha captcha = new SpecCaptcha(width, height, codeLen);
//		return new ImageCodeObject(captcha.toBase64(), captcha.text());
//	}
}
