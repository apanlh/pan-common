package com.gitee.apanlh.util.image;

import com.wf.captcha.SpecCaptcha;

/**	
 * 	生成验证码图片
 * 	@author Pan
 */
public class ImageCodeObject {
	
	/** 图片base64输出 */
	private String base64;
	/** 验证码答案 */
	private String result;
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	public ImageCodeObject() {
		super();
	}
	
	/**	
	 *	初始化验证码-构造函数
	 *	 	
	 * 	@author Pan
	 * 	@param 	result	验证码答案
	 */
	public ImageCodeObject(String result) {
		super();
		this.result = result;
	}
	
	/**	
	 * 	初始化图片及答案构造函数
	 * 	
	 * 	@author Pan
	 * 	@param 	base64	base64图片
	 * 	@param 	result	验证码答案
	 */
	public ImageCodeObject(String base64, String result) {
		super();
		this.base64 = base64;
		this.result = result;
	}
	
	/**	
	 * 	初始化图形验证码构造函数
	 * 	
	 * 	@author Pan
	 * 	@param 	captcha	 图形验证码对象
	 */
	public ImageCodeObject(SpecCaptcha captcha) {
		super();
		this.base64 = captcha.toBase64();
		this.result = captcha.text();
	}

	public String getBase64() {
		return base64;
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
}
