package com.gitee.apanlh.util.random;

import java.util.concurrent.ThreadLocalRandom;

/**
 * 	创建随机姓名
 * 
 * 	@author Pan
 */
public class RandomNameUtils {

	/**
	 * 	构造函数
	 *
	 * 	@author Pan
	 */
	private RandomNameUtils() {
		//	不允许外部实例
		super();
	}

	/** 
	 * 	邱和丘一致 
	 * <br>百家姓单姓 
	 * */
	static final String[] SURNAME = {
		"赵","钱","孙","李","周","吴","郑","王","冯","陈","褚","卫","蒋","沈","韩","杨","朱","秦","尤","许","何","吕","施","张",
		"孔","曹","严","华","金","魏","陶","姜","戚","谢","邹","喻","柏","水","窦","章","云","苏","潘","葛","奚","范","彭","郎",
        "鲁","韦","昌","马","苗","凤","花","方","俞","任","袁","柳","鲍","史","唐","费","廉","岑","薛","雷","贺","倪","汤","滕",
        "殷","罗","毕","郝","邬","安","常","乐","于","时","傅","皮","卞","齐","康","伍","余","元","卜","顾","孟","平","黄","和",
        "穆","萧","尹","姚","邵","湛","汪","祁","毛","禹","狄","米","贝","明","臧","计","伏","成","戴","谈","宋","茅","庞","熊",
        "纪","舒","屈","项","祝","董","梁","杜","阮","蓝","闵","席","季","麻","强","贾","路","娄","危","江","童","颜","郭","梅",
        "盛","林","刁","钟","徐","邱","骆","高","夏","蔡","田","樊","胡","凌","霍","虞","万","支","柯","昝","管","卢","莫","经",
        "房","裘","缪","干","解","应","宗","丁","宣","贲","邓","郁","单","杭","洪","包","诸","左","石","崔","吉","钮","龚","程",
        "嵇","邢","滑","裴","陆","荣","翁","荀","羊","于","惠","甄","家","封","芮","羿","储","靳","汲","邴","糜","松","井","段",
        "富","巫","乌","焦","巴","弓","牧","隗","山","谷","车","侯","宓","蓬","全","郗","班","仰","秋","仲","伊","宫","宁","仇",
        "栾","暴","甘","钭","厉","祖","武","符","刘","景","詹","束","龙","叶","幸","司","韶","郜","黎","蓟","印","宿","白","怀",
        "蒲","邰","从","鄂","索","咸","籍","赖","卓","蔺","屠","蒙","池","乔","阴","郁","胥","能","苍","双","闻","莘","党","翟",
        "谭","贡","劳","姬","申","扶","堵","冉","宰","郦","雍","璩","桑","桂","濮","牛","寿","通","边","扈","燕","冀","浦","尚",
        "农","温","别","庄","晏","柴","瞿","阎","充","慕","连","茹","习","宦","艾","鱼","容","向","古","易","慎","戈","廖","庾",
        "终","暨","居","衡","步","都","耿","满","弘","匡","国","文","寇","广","禄","阙","东","欧","殳","沃","利","蔚","越","隆",
        "师","巩","厍","聂","勾","敖","融","冷","訾","辛","阚","那","简","饶","空","曾","毋","沙","乜","养","鞠","须","丰","巢",
        "关","蒯","相","查","后","荆","红","游","郏","竺","权","逯","盖","益","桓","公"
    };
	/** 比较常见的姓 */
	static final String[] OFTEN_SURNAME = {
		"赵","刘","裴","严","魏","吴","方","袁","张","周","蒋","沈","宋","蔡","何","徐","陈","叶","邱","孙","李","谢","黄","方",
		"李","吴","王","潘","林","杨","卢","雷","许", "范"
	};
	/** 男性简单的名字 */
	static final String MAN_SIMPLE_NAME = "伟刚勇毅俊锋峰强军平保东文辉力明永健世广立志义兴良海山仁波宁贵福生龙元全国胜学祥才发武新利清敏飞彬富顺信子杰涛昌成康星光天达安岩中茂进林有坚和彪博诚先敬震振壮会思群豪心邦承乐绍功松善厚庆磊民友裕河哲江超浩亮政谦亨奇固之轮翰朗伯宏言若鸣朋斌梁栋维启克伦翔旭鹏泽晨辰士以建家致树炎德行时泰盛雄琛钧冠策腾楠榕风航弘";
	static final int MAX_MAN_LEN = MAN_SIMPLE_NAME.length();
	/** 女性简单的名字 */
	static final String WOMAN_SIMPLE_NAME = "玥汐姿妙冉茵珆妃姞清霏觅惜幂菱彤依丝夏萱碧若晴钰涵蕾昕凌双白芊盈秀娟英华慧巧美娜静淑惠珠翠雅芝玉萍红娥玲芬芳燕彩春菊兰凤洁梅琳素云莲真环雪荣爱妹霞香月莺媛艳瑞凡佳嘉琼勤珍贞莉桂娣叶璧璐娅琦晶妍茜秋珊莎锦黛青倩婷姣婉娴瑾颖露瑶怡婵雁蓓纨仪荷丹蓉眉君琴蕊薇菁梦岚苑婕馨瑗琰韵融园艺咏卿聪澜纯毓悦昭冰爽琬茗羽希宁欣飘育滢馥筠柔竹霭凝晓欢霄枫芸菲寒伊亚宜可姬舒影荔枝思丽音";
	static final int MAX_WOMAN_LEN = WOMAN_SIMPLE_NAME.length();
	/** 百家姓 复姓 */
	static final String[] SURNAMES = {
		"万俟","司马","上官","欧阳","夏侯",
		"诸葛","闻人","东方","赫连","皇甫",
		"尉迟","公羊","澹台","公冶","宗政",
        "濮阳","淳于","单于","太叔","申屠",
        "公孙","仲孙","轩辕","令狐","钟离",
        "宇文","长孙","慕容","司徒","司空"
    };

	/**	
	 * 	生成名字
	 * 	<br>常用姓
	 * 	
	 * 	@author Pan
	 * 	@param 	appendSex  true在尾部添加性别标识
	 * 	@return	String
	 */
	public static String generateName(boolean appendSex) {
		ThreadLocalRandom current = ThreadLocalRandom.current();
		
		//	百家姓 单姓所有
		//String surname = gn.SURNAME[current.nextInt(gn.SURNAME.length) - 1];
		//	常用姓
		String surname = OFTEN_SURNAME[current.nextInt(OFTEN_SURNAME.length)];
		//  改为Builder
		StringBuilder name = new StringBuilder(surname);
		
		//	三个字的名字
		int generateNumber = 1;
		if (current.nextInt(2) == 1) {
			generateNumber = 2;
		}
		
		if (generateSexInt() == 0) {
			for (int i = 1; i <= generateNumber; i++) {
				name.append(WOMAN_SIMPLE_NAME.charAt(current.nextInt(MAX_WOMAN_LEN)));
			}
			if (appendSex) {
				name.append("(女)");
			}
		} else {
			for (int i = 1; i <= generateNumber; i++) {
				name.append(MAN_SIMPLE_NAME.charAt(current.nextInt(MAX_MAN_LEN)));
			}
			if (appendSex) {
				name.append("(男)");
			}
		}
		return name.toString();
	}
	
	public static String generateSex() {
		return generateSexInt() == 0 ? "女" : "男";
	}
	
	public static Integer generateSexInt() {
		return ThreadLocalRandom.current().nextInt(2) == 0 ? 0 : 1;
	}
	
	/**		
	 * 	生成年龄默认最大年龄为80
	 * 	
	 * 	@author Pan
	 * 	@return	Integer
	 */
	public static Integer generateAge() {
		return ThreadLocalRandom.current().nextInt(80);
	}
	
	/**	
	 * 	生成年龄
	 * 	
	 * 	@author Pan
	 * 	@param  maxAge 最大上限值
	 * 	@return	Integer
	 */
	public static Integer generateAge(int maxAge) {
		return ThreadLocalRandom.current().nextInt(maxAge);
	}
}
