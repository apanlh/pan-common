package com.gitee.apanlh.util.tree;

/**	
 * 	生成数结构命名
 * 	@author Pan
 */
public class TreeConstant {
	
	/** 树子集命名 */
	public static final String CHILDREN_TREE_NAME = "children";
	
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private TreeConstant() {
		//	不允许外部实例
		super();
	}
}
