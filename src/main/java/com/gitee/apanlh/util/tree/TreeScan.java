package com.gitee.apanlh.util.tree;

import com.gitee.apanlh.annotation.tree.Tree;
import com.gitee.apanlh.annotation.tree.TreeAlias;
import com.gitee.apanlh.annotation.tree.TreeId;
import com.gitee.apanlh.annotation.tree.TreeParentId;
import com.gitee.apanlh.util.reflection.ReflectionUtils;
import com.gitee.apanlh.util.valid.ValidParam;

import java.lang.reflect.Field;

/**	
 * 	树结构扫描
 * 	@author Pan
 */
public class TreeScan {
	
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private TreeScan() {
		//	不允许外部实例
		super();
	}
	
	/**	
	 * 	扫描Class注解
	 * 	
	 * 	@author Pan
	 *  @param  <T>      数据类型
	 * 	@param 	clazz	 类
	 * 	@return	boolean
	 */
	public static <T> boolean getTreeAnnotation(Class<T> clazz) {
		return ReflectionUtils.scanClassAnnotation(clazz, Tree.class);
	}

	/**	
	 * 	扫描字段关联树Id注解
	 * 	
	 * 	@author Pan
	 * 	@param  <T>     数据类型
	 * 	@param 	clazz	类
	 * 	@return boolean	
	 */
	public static <T> boolean getTreeIdAnnotation(Class<T> clazz) {
		if (ReflectionUtils.scanFieldAnnotation(clazz, TreeId.class)) {
			return ReflectionUtils.scanFieldAnnotation(clazz, TreeParentId.class);
		}
		return false;
	} 
	
	/**	
	 * 	扫描获取别名
	 * 	
	 * 	@author Pan
	 * 	@param 	field	字段
	 * 	@return	String
	 */
	public static String getAlias(Field field) {
		TreeAlias treeAlias = (TreeAlias) ReflectionUtils.getFieldAnnotation(field, TreeAlias.class);
		if (treeAlias == null) {
			return field.getName();
		}
		String alias = treeAlias.alias();
		if (ValidParam.isEmpty(alias)) {
			return field.getName();
		}
		return alias;
	}
}
