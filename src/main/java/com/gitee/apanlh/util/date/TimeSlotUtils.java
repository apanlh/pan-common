package com.gitee.apanlh.util.date;

import com.gitee.apanlh.util.base.StringUtils;
import com.gitee.apanlh.util.base.builder.CollBuilder;

import java.time.LocalTime;
import java.util.List;

/**	
 * 	时间段工具类
 * 	
 * 	@author Pan
 */
public class TimeSlotUtils {
	
	/** 时间段范围 */
	static final int ZERO_MIN 	= 0; 
	static final int FIRST_MIN 	= 1;
	static final int LAST_MIN 	= 59;
	
	/** 时间段简称 */
	static final String FORENOON 					= "上午";
	static final int 	FORENOON_HOURS 				= 6;
	static final int 	LAST_FORENOON_HOURS 		= 10;
	
	static final String NOON 						= "中午";
	static final int 	NOON_HOURS 					= 11;
	static final int 	LAST_NOON_HOURS 			= 12;
	
	static final String AFTERNOON 					= "下午";
	static final int 	AFTERNOON_HOURS 			= 13;
	static final int 	LAST_AFTERNOON_HOURS 		= 15;
	
	static final String EVENING 					= "傍晚";
	static final int 	EVENING_HOURS 				= 16;
	static final int 	LAST_EVENING_HOURS 			= 17;
	
	static final String NIGHT 						= "晚上";
	static final int 	NIGHT_HOURS 				= 18;
	static final int 	LAST_NIGHT_HOURS = 23;
	
	static final String AFTER_MIDNIGHT 				= "凌晨";
	static final int 	AFTER_MIDNIGHT_HOURS 		= 0;
	static final int 	LAST_AFTER_MIDNIGHT_HOURS 	= 5;
	
	/** 默认时间段集合 */
	private static final List<TimeSlot> TIME_SLOTS = CollBuilder.builder(TimeSlot.class)
		.add(TimeSlot.of(LocalTime.of(FORENOON_HOURS, 		ZERO_MIN), 	LocalTime.of(LAST_FORENOON_HOURS, 		LAST_MIN), 	FORENOON))
		.add(TimeSlot.of(LocalTime.of(NOON_HOURS, 			ZERO_MIN), 	LocalTime.of(LAST_NOON_HOURS, 			LAST_MIN), 	NOON))
		.add(TimeSlot.of(LocalTime.of(AFTERNOON_HOURS, 		ZERO_MIN), 	LocalTime.of(LAST_AFTERNOON_HOURS, 		LAST_MIN), 	AFTERNOON))
		.add(TimeSlot.of(LocalTime.of(EVENING_HOURS, 		ZERO_MIN), 	LocalTime.of(LAST_EVENING_HOURS, 		LAST_MIN), 	EVENING))
		.add(TimeSlot.of(LocalTime.of(NIGHT_HOURS, 			ZERO_MIN), 	LocalTime.of(LAST_NIGHT_HOURS, 		LAST_MIN), 	NIGHT))
		.add(TimeSlot.of(LocalTime.of(AFTER_MIDNIGHT_HOURS, ZERO_MIN), 	LocalTime.of(LAST_AFTER_MIDNIGHT_HOURS, LAST_MIN), 	AFTER_MIDNIGHT))
	.build();

	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private TimeSlotUtils() {
		//	不允许外部实例
		super();
	}
	
	/**	
	 * 	获取当前时间段
	 * 	<br>波浪线分割(6:00~9:00)
	 * 
	 * 	@author Pan
	 * 	@return	String
	 */
	public static String getNow() {
        LocalTime now = LocalTime.now();
        for (int i = 0; i < TIME_SLOTS.size(); i++) {
        	TimeSlot timeSlot = TIME_SLOTS.get(i);
        	if (timeSlot.isWithin(now)) {
        		return StringUtils.createBuilder(timeSlot.getStartTime()).append("~").append(timeSlot.getEndTime()).toString();
        	}
		}
        return null;
    }
	
	/**	
	 * 	获取当前时间段名称
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public static String getNowName() {
        LocalTime now = LocalTime.now();
        for (int i = 0; i < TIME_SLOTS.size(); i++) {
        	TimeSlot timeSlot = TIME_SLOTS.get(i);
        	if (timeSlot.isWithin(now)) {
        		return timeSlot.getName();
        	}
		}
        return null;
    }
}
