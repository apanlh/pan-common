package com.gitee.apanlh.util.date;

import java.time.LocalTime;

/**	
 * 	时间段
 * 	
 * 	@author Pan
 */
public class TimeSlot {
	
	/** 开始时间 */
	private LocalTime start;
	/** 结束时间 */
	private LocalTime end;
	/** 自定义时间段名称 */
    private String name;
    /** 起始时间段 */
    private String startTime;
    /** 结束时间段*/
    private String endTime;
    
	/**
	 * 	构造函数，自定义开始时间以及结束时间
	 * 
	 * 	@author Pan
	 * 	@param 	start		开始时间
	 * 	@param 	end			结束时间
	 */
	public TimeSlot(LocalTime start, LocalTime end) {
		this(start, end, null);
	}
	
	/**
	 * 	构造函数，自定义开始时间以及结束时间
	 * 	<br>自定义时间段名称
	 * 
	 * 	@author Pan
	 * 	@param 	start		开始时间
	 * 	@param 	end			结束时间
	 * 	@param 	name		自定义时间段名称 
	 */
    public TimeSlot(LocalTime start, LocalTime end, String name) {
        this.start = start;
        this.end = end;
        this.name = name;
        this.startTime = start.toString();
        this.endTime = end.toString();
    }

	/**
	 * 	指定时间，是否在某时间段之内
	 * 	
	 * 	@author Pan
	 * 	@param 	time		传递时间
	 * 	@return	boolean
	 */
	public boolean isWithin(LocalTime time) {
		return time.compareTo(start) >= 0 && time.compareTo(end) <= 0;
	}

    /**	
     * 	获取自定义时间段名称
     * 	
     * 	@author Pan
     * 	@return	String
     */
    public String getName() {
        return name;
    }
    
    /**	
     * 	获取-开始时间
     * 	
     * 	@author Pan
     * 	@return	String
     */
    public String getStartTime() {
		return startTime;
	}
    
    /**	
     * 	获取-结束时间
     * 	
     * 	@author Pan
     * 	@return	String
     */
	public String getEndTime() {
		return endTime;
	}
	
    /**	
     * 	设置-开始时间
     * 	
     * 	@author Pan
     * 	@param	startTime	开始时间
     */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	
    /**	
     * 	设置-结束时间
     * 	
     * 	@author Pan
     * 	@param	endTime	结束时间
     */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	/**
     * 	构建
     * 	
     * 	@author Pan
     * 	@param 	start		开始时间
     * 	@param 	end			结束时间
     * 	@return	TimeSlot
     */
    public static TimeSlot of(LocalTime start, LocalTime end) {
    	return new TimeSlot(start, end);
    }
    
    /**
     * 	构建
     * 	
     * 	@author Pan
     * 	@param 	start		开始时间
     * 	@param 	end			结束时间
     * 	@param 	name		自定义时间段名称
     * 	@return	TimeSlot
     */
    public static TimeSlot of(LocalTime start, LocalTime end, String name) {
    	return new TimeSlot(start, end, name);
    }
}
