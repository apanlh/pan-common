package com.gitee.apanlh.util.date;

import com.gitee.apanlh.util.base.StringUtils;
import com.gitee.apanlh.util.date.format.DateTimeFormat;
import com.gitee.apanlh.util.valid.Assert;

import java.util.List;

/**	
 * 	星座工具类
 * 	
 * 	@author Pan
 */
public class ZodiacUtils {
	
	/** 十二星座 */
	private static final String[] CHINESE_ZODIAC = {"水瓶座", "双鱼座", "白羊座", "金牛座", "双子座", "巨蟹座", "狮子座", "处女座", "天秤座", "天蝎座", "射手座", "摩羯座"};
	private static final String[] ENGLISH_ZODIAC = {"Aquarius", "Pisces", "Aries", "Taurus", "Gemini", "Cancer", "Leo", "Virgo", "Libra", "Scorpio", "Sagittarius", "Capricorn"};
	private static final String[] ZODIAC_IMAGE = {"♒", "♓", "♈", "♉", "♊", "♋", "♌", "♍", "♎", "♏", "♐", "♑"};
	/** 十二星座时间范围 */
	private static final String[] ZODIAC_RANGE = {"01-20~02-18", "02-19~03-20", "03-21~04-19", "04-20~05-20", "05-21~06-21", "06-22~07-22", "07-23~08-22", "08-23~09-22", "09-23~10-23", "10-24~11-22", "11-23~12-21", "12-22~01-19"};
	
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private ZodiacUtils() {
		//	不允许外部实例
		super();
	}
	
	 /**	
     * 	获取英文星座
     * 	
     * 	@author Pan
     * 	@return String
     */
    public static String getZodiac() {
    	return ENGLISH_ZODIAC[calculateZodiacDigit()];
    }
    
    /**	
     * 	获取英文星座
     * 	
     * 	@param 	birthday	出生日期(yyyy-MM-dd)
     * 	@author Pan
     * 	@return String
     */
    public static String getZodiac(String birthday) {
    	return ENGLISH_ZODIAC[calculateZodiacDigit(birthday)];
    }
    
    /**	
     * 	获取星座图标
     * 	
     * 	@author Pan
     * 	@return String
     */
    public static String getZodiacImage() {
    	return ZODIAC_IMAGE[calculateZodiacDigit()];
    }
    
    /**	
     * 	获取星座图标
     * 	
     * 	@author Pan
     * 	@param 	birthday	出生日期(yyyy-MM-dd)
     * 	@return String
     */
    public static String getZodiacImage(String birthday) {
    	return ZODIAC_IMAGE[calculateZodiacDigit(birthday)];
    }
    
    /**	
     * 	获取中文星座
     * 	
     * 	@author Pan
     * 	@return String
     */
    public static String getChineseZodiac() {
    	return CHINESE_ZODIAC[calculateZodiacDigit()];
    }
    
    /**	
     * 	获取中文星座
     * 	
     * 	@author Pan
     * 	@param 	birthday	出生日期(yyyy-MM-dd)
     * 	@return String
     */
    public static String getChineseZodiac(String birthday) {
    	return CHINESE_ZODIAC[calculateZodiacDigit(birthday)];
    }
    
	 /**	
     * 	计算当前时间对应的星座
     * 	
     * 	@author Pan
     * 	@return String
     */
    private static int calculateZodiacDigit() {
    	return calculateZodiacDigit(DateUtils.currentTime(DateTimeFormat.DATE_FULL));
    }
    
    /**	
     * 	计算当前时间对应的星座
     * 	
     * 	@author Pan
     * 	@param 	birthday	出生日期(yyyy-MM-dd)
     * 	@return int
     */
    private static int calculateZodiacDigit(String birthday) {
    	Assert.isNotEmpty(birthday);
    	
    	String yearTime = birthday.substring(0, 5);
    	
    	for (int i = 0, len = ZODIAC_RANGE.length; i < len; i++) {
    		List<String> split = StringUtils.splitToList(ZODIAC_RANGE[i], "~");
    		
    		String startRange = yearTime.concat(split.get(0));
    		String endRange = yearTime.concat(split.get(1));
    		
    		if (DateUtils.dateCompareToByRange(startRange, endRange, birthday)) {
    			return i;
    		}
    	}
    	return 0;
    }
}
