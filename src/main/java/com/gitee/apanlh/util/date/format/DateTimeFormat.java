package com.gitee.apanlh.util.date.format;

import com.gitee.apanlh.util.cache.local.Cache;
import com.gitee.apanlh.util.cache.local.CacheUtils;

import java.time.format.DateTimeFormatter;

/**	
 * 	DateTimeFormat类
 * 
 * 	@author Pan
 */
public class DateTimeFormat {
	
	/** 年月日时分秒相关格式*/
	public static final String DATE_TIME_FULL 									= "yyyy-MM-dd HH:mm:ss";
	public static final String DATE_TIME_FULL_WITH_MILLIS 						= "yyyy-MM-dd HH:mm:ss:SSS";
	public static final String DATE_TIME_NO_SEPARATORS 							= "yyyyMMddHHmmss";
	public static final String DATE_TIME_FULL_ALTERNATIVE_SEPARATOR 			= "yyyy/MM/dd HH:mm:ss";
	public static final String DATE_TIME_FULL_ALTERNATIVE_SEPARATOR_WITH_MILLIS = "yyyy/MM/dd HH:mm:ss:SSS";
	
	/** 年月日相关格式 */
	public static final String DATE_FULL 										= "yyyy-MM-dd";
	public static final String DATE_MONTH_DAY 									= "MM-dd";
	public static final String DATE_FULL_ALTERNATIVE_SEPARATOR 					= "yyyy/MM/dd";
	public static final String DATE_MONTH_DAY_ALTERNATIVE_SEPARATOR 			= "MM/dd";
	public static final String DATE_FULL_NO_SEPARATORS							= "yyyyMMdd";
	public static final String DATE_MONTH_DAY_NO_SEPARATORS 					= "MMdd";
	public static final String DATE_DAY_ONLY 									= "dd";

	/** 时分秒相关格式 */
	public static final String TIME_FULL 										= "HH:mm:ss";
	public static final String TIME_MINUTES_SECONDS 							= "mm:ss";
	public static final String TIME_FULL_NO_SEPARATORS 							= "HHmmss";
	public static final String TIME_MINUTES_SECONDS_NO_SEPARATORS 				= "mmss";
	public static final String TIME_SECONDS_ONLY 								= "ss";
	
	protected static final Cache<String, DateTimeFormatter> DATE_FORMAT_CACHE = CacheUtils.cache(newMap -> {
		newMap.put(DATE_TIME_FULL, 							DateTimeFormatter.ofPattern(DATE_TIME_FULL));
		newMap.put(DATE_TIME_FULL_WITH_MILLIS, 				DateTimeFormatter.ofPattern(DATE_TIME_FULL_WITH_MILLIS));
		newMap.put(DATE_TIME_NO_SEPARATORS, 				DateTimeFormatter.ofPattern(DATE_TIME_NO_SEPARATORS));
		newMap.put(DATE_TIME_FULL_ALTERNATIVE_SEPARATOR, 	DateTimeFormatter.ofPattern(DATE_TIME_FULL_ALTERNATIVE_SEPARATOR));
			
		newMap.put(DATE_FULL,								DateTimeFormatter.ofPattern(DATE_FULL));
		newMap.put(DATE_MONTH_DAY, 	   						DateTimeFormatter.ofPattern(DATE_MONTH_DAY));
		newMap.put(DATE_FULL_ALTERNATIVE_SEPARATOR, 		DateTimeFormatter.ofPattern(DATE_FULL_ALTERNATIVE_SEPARATOR));
		newMap.put(DATE_MONTH_DAY_ALTERNATIVE_SEPARATOR, 	DateTimeFormatter.ofPattern(DATE_MONTH_DAY_ALTERNATIVE_SEPARATOR));
		newMap.put(DATE_FULL_NO_SEPARATORS,   				DateTimeFormatter.ofPattern(DATE_FULL_NO_SEPARATORS));
		newMap.put(DATE_MONTH_DAY_NO_SEPARATORS, 	   		DateTimeFormatter.ofPattern(DATE_MONTH_DAY_NO_SEPARATORS));
		newMap.put(DATE_DAY_ONLY, 		   					DateTimeFormatter.ofPattern(DATE_DAY_ONLY));
			
		newMap.put(TIME_FULL,   							DateTimeFormatter.ofPattern(TIME_FULL));
		newMap.put(TIME_MINUTES_SECONDS, 	   				DateTimeFormatter.ofPattern(TIME_MINUTES_SECONDS));
		newMap.put(TIME_FULL_NO_SEPARATORS,   				DateTimeFormatter.ofPattern(TIME_FULL_NO_SEPARATORS));
		newMap.put(TIME_MINUTES_SECONDS_NO_SEPARATORS, 	   	DateTimeFormatter.ofPattern(TIME_MINUTES_SECONDS_NO_SEPARATORS));
		newMap.put(TIME_SECONDS_ONLY, 		   				DateTimeFormatter.ofPattern(TIME_SECONDS_ONLY));
	});
	
	/**
	 * 	构造函数
	 * 	@author Pan
	 */
	private DateTimeFormat() {
		//	不允许外部实例
		super();
	}
	
    /**	
     * 	获取DateTimeFormat
     * 	
     * 	@author Pan
     * 	@param 	format	格式字符
     * 	@return	DateTimeFormatter
     */
    public static DateTimeFormatter getFormat(String format) {
    	return DATE_FORMAT_CACHE.get(format, () -> DateTimeFormatter.ofPattern(format));
    }
}
