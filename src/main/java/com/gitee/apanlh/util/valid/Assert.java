package com.gitee.apanlh.util.valid;

import com.gitee.apanlh.exp.AssertArgumentException;
import com.gitee.apanlh.util.base.ObjectUtils;
import com.gitee.apanlh.util.base.StringUtils;
import com.gitee.apanlh.util.func.AssertCall;

import java.util.Collection;
import java.util.Map;

/**	
 * 	断言工具类
 * 	如果为true	则程序继续执行。
 *	如果为false	则程序抛出异常，并终止执行。(默认抛出AssertArgumentException异常)
 *
 * 	@author Pan
 */
public class Assert {
	
	private static final String IS_TRUE_TIPS 			= "error argument [state is false]";
	private static final String IS_FALSE_TIPS			= "error argument [state is true]";
	static final String IS_EMPTY_TIPS 					= "error argument [is not empty]";
	private static final String IS_NULL_TIPS 			= "error argument [is not null]";
	private static final String IS_NOT_NULL_TIPS 		= "error argument [is null]";
	private static final String IS_EQ_TIPS 				= "error argument [is not equals]";
	private static final String IS_NOT_EQ_TIPS 			= "error argument [is equals]";
	private static final String IS_NOT_NUMBER_TIPS 		= "error argument [is numeric]";
	/** -------------------------------------------------------------------------- */
	private static final String IS_EMPTY_TYPE_TIPS 		= "param type[{}] error argument [is not empty]";
	private static final String IS_NOT_EMPTY_TYPE_TIPS 	= "param type[{}] error argument [is empty]";
	static final String IS_NULL_TYPE_TIPS 				= "param type[{}] error argument [is not null]";
	static final String IS_NOT_NULL_TYPE_TIPS 			= "param type[{}] error argument [is null]";
	
	/**
	 * 	默认构造函数
	 * 
	 * 	@author Pan
	 */
	private Assert() {
		//	不允许外部实例
		super();
	}
	
	/**	
	 * 	字符-验证为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param 	str		字符串
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isEmpty(String str) throws AssertArgumentException {
		isEmpty(str, () -> {
			throw new AssertArgumentException(format(IS_EMPTY_TYPE_TIPS, "String"));
		});
	}

	/**	
	 * 	字符-验证为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param <Exp>	异常
	 * 	@param 	str		字符串
	 * 	@param 	e       异常
	 * 	@throws Exp     抛出异常     抛出异常
	 */
	public static <Exp extends Throwable> void isEmptyThrows(String str, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(str), false, e);
	}
	
	/**	
	 * 	字符-验证为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	str		字符串
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp   	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmpty(String str, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(str), false, call);
	}
	
	/**	
	 * 	数组-验证为空
	 *  <br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isEmpty(String[] arr) throws AssertArgumentException {
		isEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_EMPTY_TYPE_TIPS, " String[] "));
		});
	}
	
	/**	
	 * 	字符-验证为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param <Exp>	异常
	 * 	@param 	arr		字符串数组
	 * 	@param 	e       异常
	 * 	@throws Exp     抛出异常     抛出异常
	 */
	public static <Exp extends Throwable> void isEmptyThrows(String[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), false, e);
	}
	
	/**	
	 * 	数组-验证为空
	 *
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 异常时抛
	 */
	public static <Exp extends Throwable> void isEmpty(String[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), false, call);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isEmpty(byte[] arr) throws AssertArgumentException {
		isEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_EMPTY_TYPE_TIPS, " byte[] "));
		});
	}
	
	/**	
	 * 	字符-验证为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param <Exp>	异常
	 * 	@param 	arr		字符串
	 * 	@param 	e       异常
	 * 	@throws Exp     抛出异常
	 */
	public static <Exp extends Throwable> void isEmptyThrows(byte[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), false, e);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 异常时抛
	 */
	public static <Exp extends Throwable> void isEmpty(byte[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), false, call);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isEmpty(short[] arr) throws AssertArgumentException {
		isEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_EMPTY_TYPE_TIPS, " short[] "));
		});
	}
	
	/**	
	 * 	字符-验证为空
	 * 	<br>结果为false返回自定义异常
	 *
	 * 	@author Pan
	 * 	@param <Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp     抛出异常
	 */
	public static <Exp extends Throwable> void isEmptyThrows(short[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), false, e);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmpty(short[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), false, call);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isEmpty(int[] arr) throws AssertArgumentException {
		isEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_EMPTY_TYPE_TIPS, " int[] "));
		});
	}
	
	/**	
	 * 	字符-验证为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param <Exp>	异常
	 * 	@param 	arr		字符串
	 * 	@param 	e       异常
	 * 	@throws Exp     抛出异常
	 */
	public static <Exp extends Throwable> void isEmptyThrows(int[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), false, e);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmpty(int[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), false, call);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isEmpty(long[] arr) throws AssertArgumentException {
		isEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_EMPTY_TYPE_TIPS, " long[] "));
		});
	}
	
	/**	
	 * 	字符-验证为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param <Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp     抛出异常
	 */
	public static <Exp extends Throwable> void isEmptyThrows(long[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), false, e);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmpty(long[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), false, call);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isEmpty(float[] arr) throws AssertArgumentException {
		isEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_EMPTY_TYPE_TIPS, " float[] "));
		});
	}
	
	/**	
	 * 	字符-验证为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param <Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp     抛出异常
	 */
	public static <Exp extends Throwable> void isEmptyThrows(float[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), false, e);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmpty(float[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), false, call);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isEmpty(double[] arr) throws AssertArgumentException {
		isEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_EMPTY_TYPE_TIPS, " double[] "));
		});
	}
	
	/**	
	 * 	字符-验证为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param <Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp     抛出异常
	 */
	public static <Exp extends Throwable> void isEmptyThrows(double[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), false, e);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmpty(double[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), false, call);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isEmpty(boolean[] arr) throws AssertArgumentException {
		isEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_EMPTY_TYPE_TIPS, " boolean[] "));
		});
	}
	
	/**	
	 * 	字符-验证为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param <Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp     抛出异常
	 */
	public static <Exp extends Throwable> void isEmptyThrows(boolean[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), false, e);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmpty(boolean[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), false, call);
	}
	
	/**	
	 * 	包装类型-验证为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isEmpty(char[] arr) throws AssertArgumentException {
		isEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_EMPTY_TYPE_TIPS, " char[] "));
		});
	}
	
	/**	
	 * 	字符-验证为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param <Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp     抛出异常
	 */
	public static <Exp extends Throwable> void isEmptyThrows(char[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), false, e);
	}
	
	/**	
	 * 	包装类型-验证为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmpty(char[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), false, call);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isEmpty(Byte[] arr) throws AssertArgumentException {
		isEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_EMPTY_TYPE_TIPS, " Byte[] "));
		});
	}
	
	/**	
	 * 	字符-验证为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param <Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp     抛出异常
	 */
	public static <Exp extends Throwable> void isEmptyThrows(Byte[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), false, e);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmpty(Byte[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), false, call);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isEmpty(Short[] arr) throws AssertArgumentException {
		isEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_EMPTY_TYPE_TIPS, " Short[] "));
		});
	}
	
	/**	
	 * 	字符-验证为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param <Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp     抛出异常
	 */
	public static <Exp extends Throwable> void isEmptyThrows(Short[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), false, e);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmpty(Short[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), false, call);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isEmpty(Integer[] arr) throws AssertArgumentException {
		isEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_EMPTY_TYPE_TIPS, " Integer[] "));
		});
	}
	
	/**	
	 * 	字符-验证为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param <Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmptyThrows(Integer[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), false, e);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmpty(Integer[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), false, call);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isEmpty(Long[] arr) throws AssertArgumentException {
		isEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_EMPTY_TYPE_TIPS, " Long[] "));
		});
	}
	
	/**	
	 * 	字符-验证为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param <Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmptyThrows(Long[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), false, e);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmpty(Long[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), false, call);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isEmpty(Float[] arr) throws AssertArgumentException {
		isEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_EMPTY_TYPE_TIPS, " Float[] "));
		});
	}
	
	/**	
	 * 	字符-验证为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param <Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmptyThrows(Float[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), false, e);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmpty(Float[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), false, call);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isEmpty(Double[] arr) throws AssertArgumentException {
		isEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_EMPTY_TYPE_TIPS, " Double[] "));
		});
	}
	
	/**	
	 * 	字符-验证为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param <Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmptyThrows(Double[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), false, e);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmpty(Double[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), false, call);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isEmpty(Boolean[] arr) throws AssertArgumentException {
		isEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_EMPTY_TYPE_TIPS, " Boolean[] "));
		});
	}
	
	/**	
	 * 	字符-验证为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param <Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmptyThrows(Boolean[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), false, e);
	}
	
	/**	
	 * 	数组-验证为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmpty(Boolean[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), false, call);
	}
	
	/**	
	 * 	包装类型-验证为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isEmpty(Character[] arr) throws AssertArgumentException {
		isEmpty(arr,  () -> {
			throw new AssertArgumentException(format(IS_EMPTY_TYPE_TIPS, " Character[] "));
		});
	}
	
	/**	
	 * 	字符-验证为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param <Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmptyThrows(Character[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), false, e);
	}
	
	/**	
	 * 	包装类型-验证为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmpty(Character[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), false, call);
	}
	
	/**	
	 * 	包装类型-验证为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 *  @param  <T>     数据类型
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static <T> void isEmpty(T[] arr) throws AssertArgumentException {
		isEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_EMPTY_TYPE_TIPS, " Object[] "));
		});
	}
	
	/**	
	 * 	字符-验证为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 *  @param  <T>     数据类型
	 * 	@param <Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <T, Exp extends Throwable> void isEmptyThrows(T[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), false, e);
	}
	
	/**	
	 * 	包装类型-验证为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 *  @param  <T>     数据类型
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <T, Exp extends Throwable> void isEmpty(T[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), false, call);
	}
	
	/**	
	 * 	Collection-验证为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 *  @param  <E>     数据类型
	 * 	@param  collection		集合
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static <E> void isEmpty(Collection<E> collection) throws AssertArgumentException {
		isEmpty(collection, () -> {
			throw new AssertArgumentException(format(IS_EMPTY_TYPE_TIPS, "Collection"));
		});
	}
	
	/**	
	 * 	字符-验证为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 *  @param  <E>     数据类型
	 * 	@param 	collection	集合
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <E, Exp extends Throwable> void isEmptyThrows(Collection<E> collection, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(collection), false, e);
	}
	
	/**	
	 * 	Collection-验证为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 *  @param  <E>     数据类型
	 * 	@param  collection	集合
	 * 	@param 	call		执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <E, Exp extends Throwable> void isEmpty(Collection<E> collection, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(collection), false, call);
	}
	
	/**	
	 * 	Map-验证为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 *  @param  <K>     键类型
	 *  @param  <V>     值类型
	 * 	@param  map		Map
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static <K, V> void isEmpty(Map<K, V> map) throws AssertArgumentException {
		isEmpty(map, () -> {
			throw new AssertArgumentException(format(IS_EMPTY_TYPE_TIPS, "Map"));
		});
	}
	
	/**	
	 * 	字符-验证为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 *  @param  <K>     键类型
	 *  @param  <V>     值类型
	 * 	@param 	<Exp>	异常
	 * 	@param 	map		map
	 * 	@param 	e       异常
	 * 	@throws Exp     抛出异常     抛出异常
	 */
	public static <K, V, Exp extends Throwable> void isEmptyThrows(Map<K, V> map, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(map), false, e);
	}
	
	/**	
	 * 	Map-验证为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 *  @param  <K>     键类型
	 *  @param  <V>     值类型
	 * 	@param  map		Map
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <K, V, Exp extends Throwable> void isEmpty(Map<K, V> map, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(map), false, call);
	}
	
	/**	
	 * 	对象-验证为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param 	object	对象
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isEmptyBean(Object object) throws AssertArgumentException {
		isEmptyBean(object, () -> {
			throw new AssertArgumentException(format(IS_EMPTY_TYPE_TIPS, "Object"));
		});
	}
	
	/**	
	 * 	对象-验证为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 * 	@param 	object	对象
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmptyBeanThrows(Object object, Exp e) throws Exp {
		throwEx(ValidParam.isEmptyBean(object), false, e);
	}
	
	/**
	 * 	对象-验证为空
	 * 	<br>自定义返回异常
	 * 	
	 * 	@author Pan
	 * 	@param 	object	对象
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEmptyBean(Object object, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmptyBean(object), false, call);
	}
	
	/**	
	 * 	对象-验证不为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param 	object	对象
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotEmptyBean(Object object) throws AssertArgumentException {
		isNotEmptyBean(object, () -> {
			throw new AssertArgumentException(format(IS_NOT_EMPTY_TYPE_TIPS, "Object"));
		});
	}
	
	/**	
	 * 	对象-验证不为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 * 	@param 	object	对象
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmptyBeanThrows(Object object, Exp e) throws Exp {
		throwEx(ValidParam.isEmptyBean(object), true, e);
	}
	
	/**
	 * 	对象-验证不为空
	 * 	<br>自定义返回异常
	 * 	
	 * 	@author Pan
	 * 	@param 	object	对象
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmptyBean(Object object, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmptyBean(object), true, call);
	}
	
	/**	
	 * 	Collection-验证不为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 *  @param  <E>      数据类型
	 * 	@param  collection		集合
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static <E> void isNotEmpty(Collection<E> collection) throws AssertArgumentException {
		isNotEmpty(collection, () -> {
			throw new AssertArgumentException(format(IS_NOT_EMPTY_TYPE_TIPS, "Collection"));
		});
	}
	
	/**	
	 * 	验证不为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  <E>      数据类型
	 * 	@param 	<Exp>	异常
	 * 	@param 	collection 集合
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <E, Exp extends Throwable> void isNotEmptyThrows(Collection<E> collection, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(collection), true, e);
	}
	
	/**	
	 * 	Collection-验证不为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 *  @param  <E>      数据类型
	 * 	@param  collection	集合
	 * 	@param 	call		执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <E, Exp extends Throwable> void isNotEmpty(Collection<E> collection, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(collection), true, call);
	}
	
	/**	
	 * 	Map-验证不为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 *  @param  <K>     键类型
	 *  @param  <V>     值类型
	 * 	@param  map		Map
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static <K, V> void isNotEmpty(Map<K, V> map) throws AssertArgumentException {
		isNotEmpty(map, () -> {
			throw new AssertArgumentException(format(IS_NOT_EMPTY_TYPE_TIPS, "Map"));
		});
	}
	
	/**	
	 * 	验证不为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 *  @param  <K>     键类型
	 *  @param  <V>     值类型
	 * 	@param 	<Exp>	异常
	 * 	@param 	map		map
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <K, V, Exp extends Throwable> void isNotEmptyThrows(Map<K, V> map, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(map), true, e);
	}
	
	/**	
	 * 	Map-验证不为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 *  @param  <K>     键类型
	 *  @param  <V>     值类型
	 * 	@param  map		Map
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <K, V, Exp extends Throwable> void isNotEmpty(Map<K, V> map, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(map), true, call);
	}

	/**	
	 * 	字符-验证不为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param 	str		字符串
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotEmpty(String str) throws AssertArgumentException {
		isNotEmpty(str, () -> {
			throw new AssertArgumentException(format(IS_NOT_EMPTY_TYPE_TIPS, "String"));
		});
	}
	
	/**	
	 * 	验证不为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 * 	@param 	str		字符串
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmptyThrows(String str, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(str), true, e);
	}
	
	/**	
	 * 	字符-验证不为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 	<br>自定义消息
	 * 	
	 * 	@author Pan
	 * 	@param 	str			字符串
	 * 	@param 	message		自定义消息
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotEmpty(String str, String message) throws AssertArgumentException {
		isNotEmpty(str, () -> {
			throw new AssertArgumentException(message);
		});
	}
	
	/**	
	 * 	字符-验证不为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	str		字符串
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmpty(String str, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(str), true, call);
	}
	
	/**	
	 * 	数组-验证不为空
	 *  <br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotEmpty(String[] arr) throws AssertArgumentException {
		isNotEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_NOT_EMPTY_TYPE_TIPS, " String[] "));
		});
	}
	
	/**	
	 * 	验证不为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmptyThrows(String[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), true, e);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>返回AssertArgumentException异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmpty(String[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), true, call);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotEmpty(byte[] arr) throws AssertArgumentException {
		isNotEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_NOT_EMPTY_TYPE_TIPS, " byte[] "));
		});
	}
	
	/**	
	 * 	验证不为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmptyThrows(byte[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), true, e);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmpty(byte[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), true, call);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotEmpty(short[] arr) throws AssertArgumentException {
		isNotEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_NOT_EMPTY_TYPE_TIPS, " short[] "));
		});
	}
	
	/**	
	 * 	验证不为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmptyThrows(short[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), true, e);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmpty(short[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), true, call);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotEmpty(int[] arr) throws AssertArgumentException {
		isNotEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_NOT_EMPTY_TYPE_TIPS, " int[] "));
		});
	}
	
	/**	
	 * 	验证不为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmptyThrows(int[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), true, e);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmpty(int[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), true, call);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotEmpty(long[] arr) throws AssertArgumentException {
		isNotEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_NOT_EMPTY_TYPE_TIPS, " long[] "));
		});
	}
	
	/**	
	 * 	验证不为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmptyThrows(long[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), true, e);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmpty(long[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), true, call);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotEmpty(float[] arr) throws AssertArgumentException {
		isNotEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_NOT_EMPTY_TYPE_TIPS, " float[] "));
		});
	}
	
	/**	
	 * 	验证不为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmptyThrows(float[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), true, e);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmpty(float[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), true, call);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotEmpty(double[] arr) throws AssertArgumentException {
		isNotEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_NOT_EMPTY_TYPE_TIPS, " double[] "));
		});
	}
	
	/**	
	 * 	验证不为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmptyThrows(double[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), true, e);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmpty(double[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), true, call);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotEmpty(boolean[] arr) throws AssertArgumentException {
		isNotEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_NOT_EMPTY_TYPE_TIPS, " boolean[] "));
		});
	}
	
	/**	
	 * 	验证不为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmptyThrows(boolean[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), true, e);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmpty(boolean[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), true, call);
	}
	
	/**	
	 * 	包装类型-验证不为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotEmpty(char[] arr) throws AssertArgumentException {
		isNotEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_NOT_EMPTY_TYPE_TIPS, " char[] "));
		});
	}
	
	/**	
	 * 	验证不为空
	 * 	<br>结果为false返回自定义异常
	 *
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmptyThrows(char[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), true, e);
	}
	
	/**	
	 * 	包装类型-验证不为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmpty(char[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), true, call);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotEmpty(Byte[] arr) throws AssertArgumentException {
		isNotEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_NOT_EMPTY_TYPE_TIPS, " Byte[] "));
		});
	}
	
	/**	
	 * 	验证不为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmptyThrows(Byte[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), true, e);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmpty(Byte[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), true, call);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotEmpty(Short[] arr) throws AssertArgumentException {
		isNotEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_NOT_EMPTY_TYPE_TIPS, " Short[] "));
		});
	}
	
	/**	
	 * 	验证不为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmptyThrows(Short[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), true, e);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmpty(Short[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), true, call);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 *  @throws AssertArgumentException 抛出异常
	 */
	public static void isNotEmpty(Integer[] arr) throws AssertArgumentException {
		isNotEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_NOT_EMPTY_TYPE_TIPS, " Integer[] "));
		});
	}
	
	/**	
	 * 	验证不为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmptyThrows(Integer[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), true, e);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmpty(Integer[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), true, call);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotEmpty(Long[] arr) throws AssertArgumentException {
		isNotEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_NOT_EMPTY_TYPE_TIPS, " Long[] "));
		});
	}
	
	/**	
	 * 	验证不为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmptyThrows(Long[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), true, e);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmpty(Long[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), true, call);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotEmpty(Float[] arr) throws AssertArgumentException {
		isNotEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_NOT_EMPTY_TYPE_TIPS, " Float[] "));
		});
	}
	
	/**	
	 * 	验证不为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmptyThrows(Float[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), true, e);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmpty(Float[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), true, call);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotEmpty(Double[] arr) throws AssertArgumentException {
		isNotEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_NOT_EMPTY_TYPE_TIPS, " Double[] "));
		});
	}
	
	/**	
	 * 	验证不为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmptyThrows(Double[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), true, e);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmpty(Double[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), true, call);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotEmpty(Boolean[] arr) throws AssertArgumentException {
		isNotEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_NOT_EMPTY_TYPE_TIPS, " Boolean[] "));
		});
	}
	
	/**	
	 * 	验证不为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmptyThrows(Boolean[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), true, e);
	}
	
	/**	
	 * 	数组-验证不为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmpty(Boolean[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), true, call);
	}
	
	/**	
	 * 	包装类型-验证不为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotEmpty(Character[] arr) throws AssertArgumentException {
		isNotEmpty(arr,  () -> {
			throw new AssertArgumentException(format(IS_NOT_EMPTY_TYPE_TIPS, " Character[] "));
		});
	}
	
	/**	
	 * 	验证不为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmptyThrows(Character[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), true, e);
	}
	
	/**	
	 * 	包装类型-验证不为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEmpty(Character[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), true, call);
	}
	
	/**	
	 * 	包装类型-验证不为空
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 *  @param  <T>     数据类型
	 * 	@param  arr		数组
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static <T> void isNotEmpty(T[] arr) throws AssertArgumentException {
		isNotEmpty(arr, () -> {
			throw new AssertArgumentException(format(IS_NOT_EMPTY_TYPE_TIPS, " Object[] "));
		});
	}
	
	/**	
	 * 	验证不为空
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 *  @param  <T>     数据类型
	 * 	@param 	<Exp>	异常
	 * 	@param 	arr		数组
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <T, Exp extends Throwable> void isNotEmptyThrows(T[] arr, Exp e) throws Exp {
		throwEx(ValidParam.isEmpty(arr), true, e);
	}
	
	/**	
	 * 	包装类型-验证不为空
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 *  @param  <T>      数据类型
	 * 	@param  arr		数组
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <T, Exp extends Throwable> void isNotEmpty(T[] arr, AssertCall<Exp> call) throws Exp {
		throwExCall(ValidParam.isEmpty(arr), true, call);
	}

	/**	
	 * 	对象-验证为null
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param 	object	对象
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNull(Object object) throws AssertArgumentException {
		isNull(object, () -> {
			throw new AssertArgumentException(format(IS_NULL_TIPS));
		});
	}
	
	/**	
	 * 	对象-验证为null
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 * 	@param 	object	对象
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNullThrows(Object object, Exp e) throws Exp {
		throwEx(object == null, false, e);
	}
	
	/**
	 * 	对象-验证为null
	 * 	<br>自定义返回异常
	 * 	
	 * 	@author Pan
	 * 	@param 	object	对象
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNull(Object object, AssertCall<Exp> call) throws Exp {
		throwExCall(object == null, false, call);
	}
	
	/**	
	 * 	对象-验证不为null
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param 	object	对象
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotNull(Object object) throws AssertArgumentException {
		isNotNull(object, () -> {
			throw new AssertArgumentException(format(IS_NOT_NULL_TIPS));
		});
	}
	
	/**	
	 * 	对象-验证不为null
	 * 	<br>如果为false返回{@link AssertArgumentException}异常
	 * 	<br>自定义返回消息
	 * 	
	 * 	@author Pan
	 * 	@param 	object	对象
	 * 	@param 	message	自定义消息
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotNull(Object object, String message) throws AssertArgumentException {
		isNotNull(object, () -> {
			throw new AssertArgumentException(message);
		});
	}
	
	/**	
	 * 	对象-验证不为null
	 * 	<br>结果为false返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	<Exp>	异常
	 * 	@param 	object	对象
	 * 	@param 	e       异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotNullThrows(Object object, Exp e) throws Exp {
		throwEx(object == null,  true, e);
	}
	
	/**
	 * 	对象-验证不为null
	 * 	<br>自定义返回异常
	 * 	
	 * 	@author Pan
	 * 	@param 	object	对象
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotNull(Object object, AssertCall<Exp> call) throws Exp {
		throwExCall(object == null, true, call);
	}
	
	/**	
	 * 	对象-两者不相等时触发异常
	 * 	<br>返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param 	o1		对象1
	 * 	@param 	o2		对象2
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isEq(Object o1, Object o2) throws AssertArgumentException {
		isEq(o1, o2, () -> {
			throw new AssertArgumentException(IS_EQ_TIPS);
		});
	}
	
	/**	
	 * 	对象-两者不相等时触发异常
	 * 	<br>自定义异常
	 * 	
	 * 	@author Pan
	 * 	@param 	o1		对象1
	 * 	@param 	o2		对象2
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEq(Object o1, Object o2, AssertCall<Exp> call) throws Exp {
		throwExCall(ObjectUtils.eq(o1, o2), false, call);
	}

	/**	
	 * 	对象-两者不相等时触发异常
	 * 	<br>返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param 	str1		字符串1
	 * 	@param 	str2		字符串2
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isEq(String str1, String str2) throws AssertArgumentException {
		isEq(str1, str2, () -> {
			throw new AssertArgumentException(IS_EQ_TIPS);
		});
	}
	
	/**	
	 * 	字符串-两者不相等时触发异常
	 * 	<br>自定义异常
	 * 	
	 * 	@author Pan
	 * 	@param 	str1		字符串1
	 * 	@param 	str2		字符串2
	 * 	@param 	call		执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEq(String str1, String str2, AssertCall<Exp> call) throws Exp {
		//	验证为Null情况
		if (str1 == null && str2 == null) {
			throwExCall(true, true, call);
			return ;
		}
		throwExCall(StringUtils.eq(str1, str2), false, call);
	}
	
	/**	
	 * 	字节数组-两者不相等时触发异常
	 * 	<br>返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param 	b1		字节数组1
	 * 	@param 	b2		字节数组2
	 *  @throws AssertArgumentException 抛出异常
	 */
	public static void isEq(byte[] b1, byte[] b2) throws AssertArgumentException {
		isEq(b1, b2, () -> {
			throw new AssertArgumentException(IS_EQ_TIPS);
		});
	}
	
	/**	
	 * 	字节数组-两者不相等时触发异常
	 * 	<br>自定义异常
	 * 	
	 * 	@author Pan
	 * 	@param 	b1		字节数组1
	 * 	@param 	b2		字节数组2
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isEq(byte[] b1, byte[] b2, AssertCall<Exp> call) throws Exp {
		throwExCall(ObjectUtils.eq(b1, b2), false, call); 
	}
	
	/**	
	 * 	对象-两者相等时触发异常
	 * 	<br>返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param 	o1		对象1
	 * 	@param 	o2		对象2
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotEq(Object o1, Object o2) throws AssertArgumentException {
		isNotEq(o1, o2, () -> {
			throw new AssertArgumentException(IS_NOT_EQ_TIPS);
		});
	}
	
	/**	
	 * 	对象-两者相等时触发异常
	 * 	<br>自定义异常
	 * 	
	 * 	@author Pan
	 * 	@param 	o1		对象1
	 * 	@param 	o2		对象2
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEq(Object o1, Object o2, AssertCall<Exp> call) throws Exp {
		throwExCall(ObjectUtils.eq(o1, o2), true, call); 
	}
	
	
	/**	
	 * 	字符串-两者相等时触发异常
	 * 	<br>返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param 	str1		字符串1
	 * 	@param 	str2		字符串2
	 *  @throws AssertArgumentException 抛出异常
	 */
	public static void isNotEq(String str1, String str2) throws AssertArgumentException {
		isNotEq(str1, str2, () -> {
			throw new AssertArgumentException(IS_NOT_EQ_TIPS);
		});
	}
	
	/**	
	 * 	字符串-两者相等时触发异常
	 * 	<br>自定义异常
	 * 	
	 * 	@author Pan
	 * 	@param 	str1		字符串1
	 * 	@param 	str2		字符串2
	 * 	@param 	call		执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEq(String str1, String str2, AssertCall<Exp> call) throws Exp {
		//	验证为Null情况
		if (str1 == null && str2 == null) {
			return ;
		}
		throwExCall(StringUtils.eq(str1, str2), true, call); 
	}

	/**	
	 * 	字节数组-两者相等时触发异常
	 * 	<br>返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param 	b1		字节数组1
	 * 	@param 	b2		字节数组2
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotEq(byte[] b1, byte[] b2) throws AssertArgumentException {
		isNotEq(b1, b2, () -> {
			throw new AssertArgumentException(IS_NOT_EQ_TIPS);
		});
	}
	
	/**	
	 * 	字节数组-两者相等时触发异常
	 * 	<br>自定义异常
	 * 	
	 * 	@author Pan
	 * 	@param 	b1		字节数组1
	 * 	@param 	b2		字节数组2
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotEq(byte[] b1, byte[] b2, AssertCall<Exp> call) throws Exp {
		throwExCall(ObjectUtils.eq(b1, b2), true, call); 
	}
	
	/**	
	 * 	字符串-不为数字抛出异常
	 * 	<br>返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param 	str		字符串
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNumber(String str) throws AssertArgumentException {
		isNumber(str, () -> {
			throw new AssertArgumentException(IS_NOT_NUMBER_TIPS);
		});
	}
	
	/**	
	 * 	字符串-不为数字抛出异常
	 * 	<br>自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	str		字符串
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNumber(String str, AssertCall<Exp> call) throws Exp {
		throwExCall(RegexUtils.isNumeric(str), false, call); 
	}
	
	/**	
	 * 	字符串-为数字抛出异常
	 * 	<br>返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param 	str		字符串
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isNotNumber(String str) throws AssertArgumentException {
		isNotNumber(str, () -> {
			throw new AssertArgumentException(IS_NOT_NUMBER_TIPS);
		});
	}
	
	/**	
	 * 	字符串-为数字抛出异常
	 * 	<br>自定义异常
	 * 
	 * 	@author Pan
	 * 	@param 	str		字符串
	 * 	@param 	call	执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isNotNumber(String str, AssertCall<Exp> call) throws Exp {
		throwExCall(RegexUtils.isNumeric(str), true, call); 
	}

	/**	
	 * 	结果为false触发异常
	 * 	<br>返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param 	state		状态
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isTrue(boolean state) throws AssertArgumentException {
		isTrue(state, () -> {
			throw new AssertArgumentException(IS_TRUE_TIPS);
		});
	}
	
	/**	
	 * 	结果为false触发异常
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  <Exp>       异常
	 * 	@param 	state		状态
	 * 	@param 	e			异常
	 * 	@throws Exp     	抛出异常
	 */
	public static <Exp extends Throwable> void isTrueThrows(boolean state, Exp e) throws Exp {
		throwEx(state, false, e);
	}
	
	/**	
	 * 	结果为false触发异常
	 * 	<br>返回{@link AssertArgumentException}异常
	 * 	<br>自定义消息
	 * 	
	 * 	@author Pan
	 * 	@param 	state		状态
	 * 	@param  message 	自定义消息
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isTrue(boolean state, String message) throws AssertArgumentException {
		isTrue(state, () -> {
			throw new AssertArgumentException(message);
		});
	}
	
	/**	
	 * 	结果为false触发异常
	 * 	<br>自定义异常
	 * 	
	 * 	@author Pan
	 * 	@param 	state		状态
	 * 	@param 	call		执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isTrue(boolean state, AssertCall<Exp> call) throws Exp {
		throwExCall(state, false, call); 
	}
	
	/**	
	 * 	结果为true触发异常
	 * 	<br>返回{@link AssertArgumentException}异常
	 * 
	 * 	@author Pan
	 * 	@param 	state		状态
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isFalse(boolean state) throws AssertArgumentException {
		isFalse(state, () -> {
			throw new AssertArgumentException(IS_FALSE_TIPS);
		});
	}
	
	/**	
	 * 	结果为true触发异常
	 * 	<br>返回自定义异常
	 * 
	 * 	@author Pan
	 * 	@param  <Exp>       异常
	 * 	@param 	state		状态
	 * 	@param 	e			异常
	 * 	@throws Exp     	抛出异常
	 */
	public static <Exp extends Throwable> void isFalseThrows(boolean state, Exp e) throws Exp {
		throwEx(state, true, e);
	}
	
	/**	
	 * 	结果为true触发异常
	 * 	<br>返回{@link AssertArgumentException}异常
	 * 	<br>自定义消息
	 * 	
	 * 	@author Pan
	 * 	@param 	state		状态
	 * 	@param  message 	自定义消息
	 * 	@throws AssertArgumentException 抛出异常
	 */
	public static void isFalse(boolean state, String message) throws AssertArgumentException {
		isFalse(state, () -> {
			throw new AssertArgumentException(message);
		});
	}
	
	/**	
	 * 	结果为true触发异常
	 * 	<br>自定义异常
	 * 	
	 * 	@author Pan
	 * 	@param 	state		状态
	 * 	@param 	call		执行断言异常
	 * 	@param 	<Exp>	异常
	 * 	@throws Exp 	异常时抛出
	 */
	public static <Exp extends Throwable> void isFalse(boolean state, AssertCall<Exp> call) throws Exp {
		throwExCall(state, true, call); 
	}
	
	/**		
	 * 	出现异常时返回特定参数
	 * 	
	 * 	@author Pan
	 * 	@param  <Exp>       异常
	 * 	@param 	state		状态标识
	 * 	@param 	statusFlow	状态流程(如果值为true时将传递状态变更为true时触发异常)
	 * 	@param 	e			执行断言异常
	 * 	@throws	Exp			异常
	 */
	private static <Exp extends Throwable> void throwEx(boolean state, boolean statusFlow, Exp e) throws Exp {
		if (statusFlow) {
			if (state) {
				throw e;
			}
		} else {
			if (!state) {
				throw e;
			}
		}
	}
	
	/**		
	 * 	出现异常时返回特定参数
	 * 	
	 * 	@author Pan
	 * 	@param 	state		状态标识
	 * 	@param 	statusFlow	状态流程(如果值为true时将传递状态变更为true时触发异常)
	 * 	@param 	call		执行断言异常
	 * 	@throws	Exp			异常
	 */
	private static <Exp extends Throwable> void throwExCall(boolean state, boolean statusFlow, AssertCall<Exp> call) throws Exp {
		if (statusFlow) {
			if (state) {
				throw call.throwEx();
			}
		} else {
			if (!state) {
				throw call.throwEx();
			}
		}
	}
	
	/**	
	 * 	格式化文本
	 * 	
	 * 	@author Pan
	 * 	@param 	tips		提示信息
	 * 	@param 	argument	参数
	 * 	@return	String
	 */
	private static String format(String tips, Object... argument) {
		return StringUtils.format(tips, argument);
	}
}
