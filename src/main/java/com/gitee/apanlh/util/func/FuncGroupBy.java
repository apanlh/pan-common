package com.gitee.apanlh.util.func;

/**
 * 	分组函数
 * 
 * 	@author 	Pan
 * 	@version 1.1
 */
@FunctionalInterface
public interface FuncGroupBy<T, K> {
	
	/**
	 * 	分组
	 * 
	 * 	@author Pan
	 * 	@param 	t 对象
	 * 	@return K
	 */
	K groupBy(T t);
}
