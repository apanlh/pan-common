package com.gitee.apanlh.util.func;

/**
 * 	Find方法
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncFind<T> {
	
	/**	
	 * 	搜索条件为true才执行
	 * 	
	 * 	@author Pan
	 * 	@param 	t 对象
	 * 	@return	boolean
	 */
	boolean accept(T t);
}
