package com.gitee.apanlh.util.func;

import java.util.Collection;

/**
 * 	用于函数执行exec方法	
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncCollectionExecute<E> {

	/**	
	 * 	执行函数
	 * 
	 * 	@author Pan
	 * 	@param 	newList		本身List
	 */
	void execute(Collection<E> newList);
}
