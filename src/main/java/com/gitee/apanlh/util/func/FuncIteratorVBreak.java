package com.gitee.apanlh.util.func;

import java.util.Iterator;

/**	
 * 	迭代器Value
 * 
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncIteratorVBreak<V> {
	
	/**		
	 * 	返回value
	 * 	
	 * 	@author Pan
	 * 	@param 	value		值
	 * 	@param 	iterator	迭代器
	 * 	@return true则继续 false则终止
	 */
	boolean next(V value, Iterator<V> iterator);
}
