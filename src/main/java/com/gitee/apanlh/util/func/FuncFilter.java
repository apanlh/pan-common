package com.gitee.apanlh.util.func;


/**	
 * 	List/Map Filter
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncFilter<T> {

	/**	
	 * 	搜索条件为true才执行
	 * 	
	 * 	@author Pan
	 * 	@param 	t 对象
	 * 	@return	boolean
	 */
	boolean accept(T t);
}
