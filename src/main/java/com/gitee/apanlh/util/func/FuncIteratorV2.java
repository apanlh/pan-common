package com.gitee.apanlh.util.func;

import java.util.Iterator;

/**	
 * 	迭代器Value
 * 	<br>包含迭代器
 * 	
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncIteratorV2<V> {
	
	/**		
	 * 	返回value
	 * 	
	 * 	@author Pan
	 * 	@param 	value		值
	 * 	@param 	iterator	迭代器
	 */
	void next(V value, Iterator<V> iterator);
}
