package com.gitee.apanlh.util.func;

import java.util.Iterator;

/**	
 * 	迭代器Element
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncIteratorE<E> {
	
	/**		
	 * 	返回元素
	 * 	
	 * 	@author Pan
	 * 	@param 	e			元素		
	 * 	@param 	iterator	迭代器		
	 */
	void next(E e, Iterator<E> iterator);
}
