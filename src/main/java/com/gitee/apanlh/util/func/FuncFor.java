package com.gitee.apanlh.util.func;

/**
 * 	数组遍历
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncFor<T> {
	
	/**	
	 * 	返回数组值
	 * 	
	 * 	@author Pan
	 * 	@param 	t 对象
	 */
	void get(T t);
}
