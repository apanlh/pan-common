package com.gitee.apanlh.util.func;

/**	
 * 	用于数据流获取单个值
 * 
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncGet<T, R> {
	
	/**
	 * 	数据流获取
	 * 
	 * 	@author Pan
	 * 	@param  t   对象
	 * 	@return R
	 */
	R get(T t);
}
