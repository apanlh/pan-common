package com.gitee.apanlh.util.func;

/**
 * 	锁接口
 * 	
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncLock {
	
	/**
	 * 	锁
	 * 	
	 * 	@author Pan
	 */
	void lock();
}
