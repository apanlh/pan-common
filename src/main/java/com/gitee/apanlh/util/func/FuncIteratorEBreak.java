package com.gitee.apanlh.util.func;

import java.util.Iterator;

/**	
 * 	迭代器Element
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncIteratorEBreak<E> {
	
	/**		
	 * 	返回元素
	 * 	
	 * 	@author Pan
	 * 	@param 	e	元素		
	 * 	@param 	iterator	迭代器		
	 * 	@return true则继续 false则终止
	 */
	boolean next(E e, Iterator<E> iterator);
}
