package com.gitee.apanlh.util.func;

/**
 * 	循环遍历函数
 *
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncLoop<T> {
	
	/**	
	 * 	返回对象
	 * 	
	 * 	@author Pan
	 * 	@param 	t 对象
	 * 	@return boolean true则继续 false则终止
	 */
	boolean get(T t);
}
