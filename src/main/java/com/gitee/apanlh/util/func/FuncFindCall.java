package com.gitee.apanlh.util.func;

/**
 * 	FindCall方法
 * 	
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncFindCall<T> {
	
	/**	
	 * 	搜索返回
	 * 	
	 * 	@author Pan
	 * 	@param 	t 对象
	 * 	@return	T
	 */
	T accept(T t);
}
