package com.gitee.apanlh.util.func;

/**	
 * 	迭代器K,V无迭代器
 * 	
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncIteratorBreak2<K, V> {
	
	/**		
	 * 	返回Key Value
	 * 	
	 * 	@author Pan
	 * 	@param 	key		getKey()
	 * 	@param 	value	getValue()
	 * 	@return true则继续 false则终止
	 */
	boolean next(K key, V value);
}
