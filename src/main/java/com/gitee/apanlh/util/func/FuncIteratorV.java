package com.gitee.apanlh.util.func;

/**	
 * 	迭代器Value
 * 	<br>不包含迭代器
 * 	
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncIteratorV<V> {
	
	/**		
	 * 	返回value
	 * 	
	 * 	@author Pan
	 * 	@param 	value		值
	 */
	void next(V value);
}
