package com.gitee.apanlh.util.func;

/**
 * 	循环遍历函数
 *	<br>增加返回当前索引
 *
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncLoopIndex<T> {
	
	/**	
	 * 	返回对象
	 * 	
	 * 	@author Pan
	 * 	@param 	t 对象
	 * 	@param 	index 返回当前索引
	 * 	@return true停止循环
	 */
	boolean get(T t, int index);
}
