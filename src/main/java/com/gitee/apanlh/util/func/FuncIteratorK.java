package com.gitee.apanlh.util.func;

import java.util.Iterator;

/**	
 * 	迭代器Key
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncIteratorK<K> {
	
	/**		
	 * 	返回Key
	 * 	
	 * 	@author Pan
	 * 	@param 	key		getKey()
	 * 	@param 	iterator	迭代器
	 */
	void next(K key, Iterator<K> iterator);
}
