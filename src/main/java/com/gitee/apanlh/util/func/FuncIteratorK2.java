package com.gitee.apanlh.util.func;

/**	
 * 	KEY迭代(不包含迭代器)
 * 	
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncIteratorK2<K> {
	
	/**		
	 * 	返回Key
	 * 	
	 * 	@author Pan
	 * 	@param 	key	键
	 */
	void next(K key);
}
