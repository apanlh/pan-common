package com.gitee.apanlh.util.func;

import java.util.List;

/**	
 * 	数据分页
 * 	
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncPage<T> {
	
	/**	
	 * 	返回已经分卷过后的集合
	 * 	
	 * 	@author Pan
	 * 	@param 	list 集合
	 */
	void part(List<T> list);
}
