package com.gitee.apanlh.util.func;

import java.util.Iterator;
import java.util.Map.Entry;

/**	
 * 	迭代器K,V
 * 	
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncIterator<K, V> {
	
	/**		
	 * 	返回Key Value
	 * 	
	 * 	@author Pan
	 * 	@param 	key			getKey()
	 * 	@param 	value		getValue()
	 * 	@param 	iterator	迭代器
	 */
	void next(K key, V value, Iterator<Entry<K, V>> iterator);
}
