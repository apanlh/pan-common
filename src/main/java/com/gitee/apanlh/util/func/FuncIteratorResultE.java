package com.gitee.apanlh.util.func;

import java.util.Iterator;

/**	
 * 	迭代器Element
 * 	<br>可返回结果
 * 	
 * 	@author Pan
 */
public interface FuncIteratorResultE<E, R> {
	
	/**		
	 * 	返回元素
	 * 	
	 * 	@author Pan
	 * 	@param 	e			元素		
	 * 	@param 	iterator	迭代器	
	 * 	@return	boolean		true则继续迭代, false 则停止	
	 */
	boolean next(E e, Iterator<E> iterator);

	/**	
	 * 	自定义返回结果
	 * 	
	 * 	@author Pan
	 * 	@return	R
	 */
	 R call();
}
