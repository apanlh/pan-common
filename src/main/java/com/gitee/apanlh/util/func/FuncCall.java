package com.gitee.apanlh.util.func;

/**
 * 	函数回调
 * 		
 * 	@author Pan
 * 	@param <R> 返回对象
 */
@FunctionalInterface
public interface FuncCall<R> {
	
	/**	
	 * 	回调函数
	 * 	
	 * 	@author Pan
	 * 	@return	R
	 * 	@throws Exception  异常抛出
	 */
	R call() throws Exception;
}
