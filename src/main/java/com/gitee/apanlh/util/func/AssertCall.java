package com.gitee.apanlh.util.func;

/**
 * 	断言回调
 * 
 * 	@author Pan
 */
public interface AssertCall<Exp> {
	
	/**	
	 * 	断言抛出异常
	 * 	
	 * 	@author Pan
	 * 	@return	Exp
	 */
	Exp throwEx();
}
