package com.gitee.apanlh.util.func;

import java.util.Iterator;

/**	
 * 	迭代器Key
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncIteratorKBreak<K> {
	
	/**		
	 * 	返回Key
	 * 	
	 * 	@author Pan
	 * 	@param 	key		 getKey()
	 * 	@param 	iterator 迭代器
	 * 	@return boolean true则继续 false则终止
	 */
	boolean next(K key, Iterator<K> iterator);
}
