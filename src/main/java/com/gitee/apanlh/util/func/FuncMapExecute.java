package com.gitee.apanlh.util.func;

import com.gitee.apanlh.exp.InitLoadException;

import java.util.Map;

/**
 * 	用于函数执行exec方法	
 * 	
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncMapExecute<K, V> {

	/**	
	 * 	执行函数
	 * 
	 * 	@author Pan
	 * 	@param 	newMap	本身Map
	 * 	@throws InitLoadException 初始化异常抛出
	 */
	void execute(Map<K, V> newMap) throws InitLoadException;
}
