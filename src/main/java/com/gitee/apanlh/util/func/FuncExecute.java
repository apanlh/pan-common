package com.gitee.apanlh.util.func;

/**	
 * 	执行函数
 * 	
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncExecute {
	
	/**
	 * 	执行
	 * 	
	 * 	@author Pan
	 */
	void execute();
}
