package com.gitee.apanlh.util.func;

/**
 * 	锁接口
 * 	<br>将有返回值
 * 	
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncLockResult<V> {
	
	/**
	 * 	锁
	 * 	
	 * 	@author Pan
	 * 	@return V
	 */
	V lock();
}
