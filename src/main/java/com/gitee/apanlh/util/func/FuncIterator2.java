package com.gitee.apanlh.util.func;

/**	
 * 	迭代器K,V无迭代器
 * 	@author Pan
 */
@FunctionalInterface
public interface FuncIterator2<K, V> {
	
	/**		
	 * 	返回Key Value
	 * 	
	 * 	@author Pan
	 * 	@param 	key		getKey()
	 * 	@param 	value	getValue()
	 */
	void next(K key, V value);
}
