package com.gitee.apanlh.util.sensitive;

/**	
 * 	脱敏项的枚举类型
 * 
 * 	@author Pan
 */
public enum SensitiveItemEnum {
	/** 用户编号 */
	USER_NO, 
	/** 用户姓名 */
	USER_NAME,
	/** 用户地址 */
	USER_ADDRESS, 
	/** 用户手机 */
	USER_PHONE,
	;
}
