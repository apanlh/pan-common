package com.gitee.apanlh.util.unit;

/**	
 * 	时间单位
 * 	#mark 待补充
 * 	@author Pan
 */
public enum TimeUnit {
	
	/** 毫秒 */
	MILLISECONDS("ms"),
	/** 秒 */
	SECONDS("s"),
	/** 分钟 */
	MIN("m"),
	/** 小时 */
	HOURS("h"),
	/** 天 */
	DAYS("D"),
	/** 星期 */
	WEEK("W"),
	/** 月 */
	MONTH("M"),
	/** 季度 */
	QUARTERLY("Q"),
	/** 年 */
	YEAR("Y");
	
	/** 时间单位 */
	private String unit;
	
	TimeUnit(String unit) {
		this.unit = unit;
	}

	public String getUnit() {
		return unit;
	}
}
