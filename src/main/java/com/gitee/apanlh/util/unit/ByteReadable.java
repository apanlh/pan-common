package com.gitee.apanlh.util.unit;

import com.gitee.apanlh.util.base.BigDecimalUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**		
 *	可读转换器
 *	
 * 	@author Pan
 */
public class ByteReadable {
	
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private ByteReadable() {
		//	不允许外部实例
		super();
	}
	
	/**
	 * 	byte转为便于阅读的单位
	 * 	
	 * 	@author Pan
	 * 	@param 	bytesSize	字节长度
	 * 	@return	String
	 */
	public static String convert(String bytesSize) {
		return convert(new BigDecimal(bytesSize));
	}
	
	/**
	 * 	byte转为便于阅读的单位
	 * 	
	 * 	@author Pan
	 * 	@param 	bytesSize	字节长度
	 * 	@return	String
	 */
	public static String convert(int bytesSize) {
		return convert(new BigDecimal(bytesSize));
	}
	
	/**
	 * 	byte转为便于阅读的单位
	 * 	
	 * 	@author Pan
	 * 	@param 	bytesSize	字节长度
	 * 	@return	String
	 */
	public static String convert(long bytesSize) {
		return convert(new BigDecimal(bytesSize));
	}
	
	/**	
	 * 
	 * 	byte转为便于阅读的单位
	 * 	<br>1kb = 1024(b)
	 * 	<br>1Mb = 1,048,576(b)
	 * 	<br>1Gb = 1,073,741,824(b)
	 * 	<br>1Tb = 1,099,511,627,776(b)
	 * 	<br>1Pb = 1125899906842624(b)
	 * 	
	 * 	@author Pan
	 * 	@param  size	长度
	 * 	@return	String
	 */
	public static String convert(BigDecimal size) {
		long val = size.longValue();
		if (val < DataSize.B1) {
			return size.toString().concat(" bytes");
		} else if (val < DataSize.B2) {
			return BigDecimalUtils.division(size, DataSize.B1, 1, RoundingMode.HALF_UP).toString().concat(" KB");
		} else if (val < DataSize.B3) {
			return BigDecimalUtils.division(size, DataSize.B2, 1, RoundingMode.HALF_UP).toString().concat(" MB");
		} else {
			return BigDecimalUtils.division(size, DataSize.B3, 1, RoundingMode.HALF_UP).toString().concat(" GB");
		}
	}
}
