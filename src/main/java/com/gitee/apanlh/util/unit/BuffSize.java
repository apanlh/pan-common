package com.gitee.apanlh.util.unit;

/**	
 * 	缓冲长度
 *
 * 	@author Pan
 */
public class BuffSize {
	
	public static final int SIZE_4B 			= 0x4;
	public static final int SIZE_8B 			= 0x8;
	public static final int SIZE_16B 			= 0x10;
	public static final int SIZE_32B 			= 0x20;
	public static final int SIZE_64B 			= 0x40;
	public static final int SIZE_128B 			= 0x80;
	public static final int SIZE_256B 			= 0x100;
	public static final int SIZE_512B 			= 0x200;
	public static final int SIZE_1K 			= 0x400;
	public static final int SIZE_2K 			= 0x800;
	public static final int SIZE_4K 			= 0x1000;
	public static final int SIZE_8K 			= 0x2000;
	public static final int SIZE_16K 			= 0x4000;
	public static final int SIZE_32K 			= 0x8000;
	public static final int SIZE_64K 			= 0x10000;
	public static final int SIZE_128K 			= 0x20000;
	public static final int SIZE_256K 			= 0x40000;
	public static final int SIZE_512K 			= 0x80000;
	public static final int SIZE_1M 			= 0x100000;
	public static final int SIZE_2M 			= 0x200000;
	public static final int SIZE_5M 			= 0x500000;
	public static final int SIZE_10M 			= 0xa00000;
	
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private BuffSize() {
		//	不允许外部实例
		super();
	}
	
	/**	
	 * 	指定一个缓冲长度byte[]
	 * 	<br>用于一些IO的操作
	 * 	
	 * 	@author Pan
	 * 	@param 	bufferSize	缓冲长度
	 * 	@return	byte[]
	 */
	public static byte[] newBufferByte(int bufferSize) {
		return new byte[bufferSize];
	}
	
	/**	
	 * 	指定一个缓冲长度char[]
	 * 	<br>用于一些IO的操作
	 * 	
	 * 	@author Pan
	 * 	@param 	bufferSize	缓冲长度
	 * 	@return	char[]
	 */
	public static char[] newBufferChar(int bufferSize) {
		return new char[bufferSize];
	}
}
