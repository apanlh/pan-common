package com.gitee.apanlh.util.thread;

import com.gitee.apanlh.exp.ThreadSleepException;

/**	
 * 	线程睡眠
 * 	
 * 	@author Pan
 */
public class Sleep {
	
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private Sleep() {
		//	不允许外部实例
		super();
	}
	
	/**	
	 * 	线程睡眠(毫秒)
	 * 	
	 * 	@author Pan
	 * 	@param 	time	毫秒
	 */
	public static void mills(long time) {
		if (time <= 0l) {
			return ;
		}

		try {
			Thread.sleep(time);
		} catch (Exception e) {
			Thread.currentThread().interrupt();
			throw new ThreadSleepException(e.getMessage(), e);
		}
	}
	
	/**	
	 * 	线程睡眠(毫秒)
	 * 	
	 * 	@author Pan
	 * 	@param 	time	毫秒
	 * 	@param 	nanos	纳秒
	 */
	public static void mills(long time, int nanos) {
		if (time <= 0l || nanos <= 0) {
			return ;
		}

		try {
			Thread.sleep(time, nanos);
		} catch (Exception e) {
			Thread.currentThread().interrupt();
			throw new ThreadSleepException(e.getMessage(), e);
		}
	}
	
	/**	
	 * 	线程睡眠(秒)
	 * 	
	 * 	@author Pan
	 * 	@param 	time 秒
	 */
	public static void seconds(long time) {
		if (time <= 0l) {
			return ;
		}
		
		try {
			Thread.sleep(time * 1000L);
		} catch (Exception e) {
			Thread.currentThread().interrupt();
			throw new ThreadSleepException(e.getMessage(), e);
		}
	}
}
