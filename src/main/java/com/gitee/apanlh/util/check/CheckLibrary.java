package com.gitee.apanlh.util.check;

import com.gitee.apanlh.exp.NotFoundException;
import com.gitee.apanlh.util.base.StringUtils;

/**
 * 	该类用于映射第三方库名与加载第三方类
 *	 	
 * 	@author Pan
 */
public enum CheckLibrary {
	
	JWT("Java JWT", "com.auth0.jwt.interfaces.Claim", "3.10.3"),
	POI("Apache POI", "org.apache.poi.ss.usermodel.Workbook", "5.2.2"),
	BOUNCY_CASTLE("Bouncy Castle Provider", "java.security.cert.CertPathChecker", "1.68~1.76"),
	SPRING_WEB("Spring Web", "org.springframework.http.ReactiveHttpInputMessage", "5.1.3.RELEASE"),
	SPRING_CORE("Spring Core", "org.springframework.cglib.core.Converter", "5.1.3.RELEASE"),
	SPRING_REDIS("Spring Redis", "org.springframework.data.redis.serializer.RedisSerializationContext", "2.1.3.RELEASE"),
	DOM4J("Dom4j", "org.w3c.dom.Document", "2.1.4"),
	XSTREAM("XStream", "com.thoughtworks.xstream.converters.javabean.JavaBeanProvider", "1.4.20"),
	ZIP_LZ4("LZ4", "net.jpountz.lz4.LZ4UnknownSizeDecompressor", "1.8.0"),
	ZIP_SNAPPY("Snappy", "org.xerial.snappy.pool.BufferPool", "1.1.8.4"),
	ZIP_BROTLI("Brotli4j ", "com.aayushatharva.brotli4j.common.annotations.Internal", "1.8.0"),
	QRCODE("ZXing Core", "java.beans.PropertyEditor", "3.5.1"),
	CAPTCHA("EasyCaptcha", "com.wf.captcha.utils.CaptchaUtil", "1.6.2"),
	DRUID("Druid", "com.alibaba.druid.support.ibatis.SpringIbatisBeanTypeAutoProxyCreatorMBean", "1.2.16"),
	FASTJSON("Fastjson", "com.alibaba.fastjson.serializer.LabelFilter", "1.2.83"),
	IP2REGION("Ip2Region", "org.lionsoul.ip2region.xdb.Header", "2.7.0"),
	PDF("Apache PDFBox", "org.apache.pdfbox.pdmodel.fixup.PDDocumentFixup", "2.0.27"),
	APACHE_NET("Apache Commons Net", "org.apache.commons.net.ftp.Configurable", "3.9.0"),
	THREAD_AFFINITY("Thread Affinity", "net.openhft.affinity.CpuLayout", "3.23.2");
	
	/** 依赖库名 */
	private String libraryName;
	/** 加载类名 */
	private String loadClassName;
	/** 加载版本 */
	private String loadVersion;
	
	/**	
	 * 	构造函数
	 * 	<br>设置库名
	 * 	<br>设置加载类名
	 * 	<br>设置当前工具类版本
	 * 	
	 * 	@author Pan
	 * 	@param 	libraryName		依赖库名
	 * 	@param 	loadClassName	加载类名
	 */
	CheckLibrary(String libraryName, String loadClassName, String loadVersion) {
		this.libraryName = libraryName;
		this.loadClassName = loadClassName;
		this.loadVersion = loadVersion;
	}
	
	/**		
	 * 	获取依赖库名
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getLibraryName() {
		return libraryName;
	}

	/**		
	 * 	获取加载类名
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getLoadClassName() {
		return loadClassName;
	}

	/**		
	 * 	获取加载版本
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getLoadVersion() {
		return loadVersion;
	}

	/**
	 * 	加载依赖库
	 * 	
	 * 	@author Pan
	 * 	@throws NotFoundException	如果不存在某第三方库抛出此异常
	 */
	public void load() {
		try {
			Class.forName(this.loadClassName);
		} catch (Exception e) {
			throw new NotFoundException(StringUtils.format("library name [{}] need version [{}] does not exist please check pom.xml", libraryName, this.loadVersion));
		}
	}
}
