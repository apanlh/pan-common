package com.gitee.apanlh.util.check;

import com.gitee.apanlh.exp.NotFoundException;
import com.gitee.apanlh.util.base.CollUtils;
import com.gitee.apanlh.util.base.IteratorUtils;

import java.util.List;

/**
 * 	此工具类用于验证外部导入包
 * 	<br>验证是否加载了某个第三方依赖库
 * 	
 * 	@author Pan
 */
public class CheckImport {
	
	/**
	 * 	默认构造函数
	 * 
	 * 	@author Pan
	 */
	private CheckImport() {
		//	不允许外部实例
		super();
	}
	
	/**
	 * 	检测是否库资源是否存在
	 * 	
	 * 	@author Pan
	 * 	@param	checkLibrary		检测库资源枚举
	 * 	@throws NotFoundException	如果不存在某第三方库抛出此异常
	 */
	public static void library(CheckLibrary checkLibrary) {
		checkLibrary.load();
	}
	
	/**	
	 * 	检测需要使用完全该工具类的功能所缺失的依赖包
	 * 	<br>注意是需要完全使用工具类的所有功能所缺失的依赖包
	 * 	<br>如果只是选择使用部分功能无需关注
	 * 	
	 * 	@author Pan
	 * 	@return	List	缺失依赖包
	 */
	public static List<String> checkMissingLibraryAll() {
		return CollUtils.newArrayList(newList -> 
			IteratorUtils.array(CheckLibrary.values(), t -> {
				try {
					t.load();
				} catch (Exception e) {
					newList.add(t.getLibraryName());
				}
			})
		);
	}
}
