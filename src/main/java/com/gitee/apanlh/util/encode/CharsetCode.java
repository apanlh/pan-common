package com.gitee.apanlh.util.encode;

import com.gitee.apanlh.util.base.CollUtils;
import com.gitee.apanlh.util.base.Eq;
import com.gitee.apanlh.util.valid.ValidParam;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * 	字符集编码
 * 	
 * 	@author Pan
 */
public class CharsetCode {
	
	/** 虚拟机默认字符集编码 */
	public static final Charset CHARSET_DEFAULT     = Charset.defaultCharset(); 
	public static final String  DEFAULT     		= CHARSET_DEFAULT.name();
	 
	/**	获取当前所支持的字符集 */
	private static final Map<String, Charset> ALL_CHARSET 			= Charset.availableCharsets();
	private static final List<String> 		  ALL_CHARSET_STRING 	= Collections.unmodifiableList(CollUtils.newArrayList(ALL_CHARSET.keySet()));
	private static final List<Charset> 		  ALL_CHARSET_CHARSET 	= Collections.unmodifiableList(CollUtils.newArrayList(ALL_CHARSET.values()));
	/** --------------------------------------------------------------------------------- */
	public static final String US_ASCII 	= "US-ASCII";
    public static final String ISO_8859_1 	= "ISO-8859-1";
    public static final String GBK 			= "GBK";
    public static final String UTF_8 		= "UTF-8";
    public static final String UTF_16BE 	= "UTF-16BE";
    public static final String UTF_16LE 	= "UTF-16LE";
    public static final String UTF_16 		= "UTF-16";
    /** --------------------------------------------------------------------------------- */
    public static final Charset CHARSET_US_ASCII 	= StandardCharsets.US_ASCII;
    public static final Charset CHARSET_ISO_8859_1 	= StandardCharsets.ISO_8859_1;
    public static final Charset CHARSET_GBK 		= Charset.forName(GBK);
    public static final Charset CHARSET_UTF_8 		= StandardCharsets.UTF_8;
    public static final Charset CHARSET_UTF_16BE 	= StandardCharsets.UTF_16BE;
    public static final Charset CHARSET_UTF_16LE 	= StandardCharsets.UTF_16LE;
    public static final Charset CHARSET_UTF_16 		= StandardCharsets.UTF_16;
    
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private CharsetCode() {
		//	不允许外部实例
		super();
	}
	
	/**	
	 * 	根据Charset类型字符集获取String类型字符集
	 * <br>如果获取字符集编码为空返回指定字符集编码
	 * 
	 * 	@author Pan
	 * 	@param 	charset			指定字符集
	 * 	@param 	defaultCharset	默认返回字符集
	 * 	@return	Charset
	 */
	public static String getString(Charset charset, Charset defaultCharset) {
		if (charset == null) {
			return defaultCharset.toString();
		}
		
		for (int index = 0; index < ALL_CHARSET_CHARSET.size(); index++) {
			if (Eq.object(ALL_CHARSET_CHARSET.get(index), charset)) {
				return ALL_CHARSET_STRING.get(index);
			}
		}
		return defaultCharset.toString();
	}
	
	 /**		
     * 	获取当前系统默认字符集
     * 	<br>返回Charset类型
     * 	
     * 	@author Pan
     * 	@return	Charset
     */
	public static Charset getDefaultToCharset() {
		return CHARSET_DEFAULT;
	}
	
	 /**		
     * 	获取当前系统默认字符集
     * 	<br>返回String类型
     * 	
     * 	@author Pan
     * 	@return	String
     */
	public static String getDefaultToString() {
		return DEFAULT;
	}
	
	/**	
	 * 	根据String类型字符集获取Charset类型字符集
	 *  <br>如果获取字符集编码为空返回指定字符集编码
	 *  
	 * 	@author Pan
	 * 	@param 	charset	字符集
	 * 	@param 	defaultCharset	默认返回指定字符集编码
	 * 	@return	Charset
	 */
	public static Charset getCharset(String charset, Charset defaultCharset) {
		if (ValidParam.isEmpty(charset)) {
			return defaultCharset;
		}
		Charset charset2 = ALL_CHARSET.get(charset);
		if (charset2 == null) {
			return defaultCharset;
		}
		return charset2;
	}
	
    /**		
     * 	获取当前所支持的字符集
     * 	<br>返回Charset类型
     * 	
     * 	@author Pan
     * 	@return	List
     */
    public static List<Charset> getAllToCharset() {
    	return ALL_CHARSET_CHARSET;
    }
    
    /**		
     * 	获取当前所支持的字符集
     * 	<br>返回String类型
     * 	
     * 	@author Pan
     * 	@return	List
     */
    public static List<String> getAllToString() {
    	return ALL_CHARSET_STRING;
    }
}
