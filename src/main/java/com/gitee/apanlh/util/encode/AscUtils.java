package com.gitee.apanlh.util.encode;

import com.gitee.apanlh.util.base.Empty;

/**	
 * 	ASC编码
 * 	
 * 	@author Pan
 */
public class AscUtils {

	/**
	 * 	默认构造函数
	 * 	@author Pan
	 */
	private AscUtils() {
		//	不允许实例化
		super();
	}

	/**
	 * 	转换ASC
	 *
	 * 	@author Pan
	 * 	@param 	content	内容
	 * 	@return	byte[]
	 */
	public static byte[] toAsc(byte[] content) {
		byte[] bs = new byte[content.length];
		for (int i = 0; i < content.length; i++) {
			bs[i] = (byte) toAsc(content[i]);
		}
		return bs;
	}
	
	/**
	 * 		
	 * 	@author Pan
	 * 	@param 	b	字节
	 * 	@return	int
	 */
	private static int toAsc(byte b) {
		return b;
	}
	
	/**	
	 * 	
	 * 	@author Pan
	 * 	@param 	content	内容
	 * 	@return	byte[]
	 */
	public static byte[] toAsc(String content) {
		if (content == null) {
			return Empty.arrayByte();
		}
		return toAsc(content.getBytes());
	}
	
	/**	
	 * 	
	 * 	@author Pan
	 * 	@param 	asc	asc内容
	 * 	@return	String
	 */
	public static String ascToStr(byte[] asc) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < asc.length; i++) {
			sb.append(asc[i]);
		}
		return sb.toString();
	}
}
