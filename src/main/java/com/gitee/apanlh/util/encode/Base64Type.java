package com.gitee.apanlh.util.encode;

import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

/**
 * 	Base64编码类型枚举
 * 	
 * 	@author Pan
 */
public enum Base64Type {
	
	/** 
	 * 	Base64编码的标准方式，包括字符集、填充字符以及编码和解码的算法。
	 *  <br>使用的字符集包括大写字母（A-Z）、小写字母（a-z）、数字（0-9）、加号（+）和斜杠（/），
	 *  <br>并使用等号（=）作为填充字符 
	 */
	RFC4648() {
		@Override
		public Encoder getEncoder() { return Base64.getEncoder();}
		@Override
		public Decoder getDecoder() {return Base64.getDecoder();}
	},
	
	/**
	 *  RFC4648_URLSAFE使用了不同的字符集(一般用于URL编码及解码)
	 *  <br>将加号（+）和斜杠（/）替换为连字符（-）和下划线（_）
	 *  <br>不使用等号（=）作为填充字符。(按照字符串实际位数进行解码，最后若为1位，则不补全，不会出现 == 或者 = )
	 *  <br>避免在URL中引起歧义或被特殊字符转义。
	 */
	RFC4648_URLSAFE() {
		@Override
		public Encoder getEncoder() { return Base64.getUrlEncoder();}
		@Override
		public Decoder getDecoder() {return Base64.getUrlDecoder();}
	},
	
	/**
	 * 	与RFC4648基本一致，但在填充字符方面有所不同。
	 * 	<br>RFC2045使用等号（=）作为填充字符，而RFC4648使用零长度的序列作为填充字符
	 * 	<br>超过76个字符则换行
	 */
	RFC2045() {
		@Override
		public Encoder getEncoder() { return Base64.getMimeEncoder();}
		@Override
		public Decoder getDecoder() {return Base64.getMimeDecoder();}
	};
	
    /**	
     * 	获取编码器
     * 	
     * 	@author Pan
     * 	@return	Encoder
     */
    public Encoder getEncoder() {
    	throw new AbstractMethodError("not found encode abstract method");
    }
    
    /**
     * 	获取解码器
     * 	
     * 	@author Pan
     * 	@return	Decoder
     */
    public Decoder getDecoder() {
		throw new AbstractMethodError("not found decode abstract method");
    }
}
