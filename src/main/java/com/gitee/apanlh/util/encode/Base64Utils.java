package com.gitee.apanlh.util.encode;

/**
 * 	此工具类封装了Java标准库中的Base64相关功能，使其更易于使用。
 * 	<br>Base64编码是一种将二进制数据转换为可打印字符的编码方式，常用于数据传输和存储。
 * 	<br>支持的编码类型包括标准编码方式(RFC4648)、URL安全编码方式(RFC4648_URLSAFE)、MIME编码方式(RFC2045)。
 * 	
 * 	@author Pan
 */
public class Base64Utils {
	
    /**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private Base64Utils() {
		//	不允许外部实例
		super();
	}
	
	/**	
	 * 	进行Base64编码
	 * 	<br>默认使用标准Base64编码类型(RFC4648)
	 * 	
	 * 	@author Pan
	 * 	@param  content	要编码的字节数组
	 * 	@return	byte[]	编码后的字节数组
	 */
	public static byte[] encode(byte[] content) {
		return Base64Type.RFC4648.getEncoder().encode(content);
	}
	
	/**	
	 * 	进行Base64编码
	 * 	<br>自定义选择Base64编码类型
	 * 	
	 * 	@author Pan
	 * 	@param  content		要编码的字节数组
	 * 	@param  base64Type	编码类型
	 * 	@return	byte[]		编码后的字节数组
	 */
	public static byte[] encode(byte[] content, Base64Type base64Type) {
		return base64Type.getEncoder().encode(content);
	}
	
	/**	
	 * 	进行Base64编码
	 * 	<br>默认使用标准Base64编码类型(RFC4648)
	 * 	
	 * 	@author Pan
	 * 	@param  content	要编码的字符串
	 * 	@return	byte[]	编码后的字节数组
	 */
	public static byte[] encode(String content) {
		return encode(content.getBytes());
	}
	
	/**	
	 * 	进行Base64编码
	 * 	<br>自定义选择Base64编码类型
	 * 	
	 * 	@author Pan
	 * 	@param  content		要编码的字节数组
	 * 	@param  base64Type	编码类型
	 * 	@return	byte[]		编码后的字节数组
	 */
	public static byte[] encode(String content, Base64Type base64Type) {
		return encode(content.getBytes(), base64Type);
	}

	/**	
	 * 	进行Base64编码
	 * 	<br>默认使用标准Base64编码类型(RFC4648)
	 * 	
	 * 	@author Pan
	 * 	@param  content	要编码的字节数组
	 * 	@return	String	编码后的字符串
	 */
	public static String encodeToStr(byte[] content) {
		return encodeToStr(content, Base64Type.RFC4648);
	}
	
	/**	
	 * 	进行Base64编码
	 * 	<br>自定义选择Base64编码类型
	 * 	
	 * 	@author Pan
	 * 	@param  content		要编码的字节数组
	 * 	@param  base64Type	编码类型
	 * 	@return	String		编码后的字符串
	 */
	public static String encodeToStr(byte[] content, Base64Type base64Type) {
		return StrEncodeUtils.utf8EncodeToStr(base64Type.getEncoder().encode(content));
	}
	
	/**	
	 * 	进行Base64编码
	 * 	<br>默认使用标准Base64编码类型(RFC4648)
	 * 	
	 * 	@author Pan
	 * 	@param  content	要编码的字符串
	 * 	@return	String	编码后的字符串
	 */
	public static String encodeToStr(String content) {
		return encodeToStr(content.getBytes());
	}
	
	/**	
	 * 	进行Base64编码
	 * 	<br>自定义选择Base64编码类型
	 * 	
	 * 	@author Pan
	 * 	@param  content		要编码的字符串
	 * 	@param  base64Type	编码类型
	 * 	@return	String		编码后的字符串
	 */
	public static String encodeToStr(String content, Base64Type base64Type) {
		return encodeToStr(content.getBytes(), base64Type);
	}
	
	/**	
	 * 	进行Base64解码
	 * 	<br>默认使用标准Base64解码类型(RFC4648)
	 * 	
	 * 	@author Pan
	 * 	@param  content	要解码的字节数组
	 * 	@return	byte[]	解码后的字节数组
	 */
	public static byte[] decode(byte[] content) {
		return Base64Type.RFC4648.getDecoder().decode(content);
	}
	
	/**	
	 * 	进行Base64解码
	 * 	<br>自定义选择Base64解码类型
	 * 
	 * 	@author Pan
	 * 	@param  content		要解码的字节数组
	 * 	@param  base64Type	解码类型
	 * 	@return	byte[]
	 */
	public static byte[] decode(byte[] content, Base64Type base64Type) {
		return base64Type.getDecoder().decode(content);
	}
	
	/**	
	 * 	进行Base64解码
	 * 	<br>默认使用标准Base64解码类型(RFC4648)
	 * 	
	 * 	@author Pan
	 * 	@param  content	要解码的字符串
	 * 	@return	byte[]	解码后的字节数组
	 */
	public static byte[] decode(String content) {
		return decode(content.getBytes());
	}
	
	/**	
	 * 	进行Base64解码
	 * 	<br>自定义选择Base64解码类型
	 * 
	 * 	@author Pan
	 * 	@param  content		要解码的字符串
	 * 	@param  base64Type	解码类型
	 * 	@return	byte[]
	 */
	public static byte[] decode(String content, Base64Type base64Type) {
		return decode(content.getBytes(), base64Type);
	}

	/**	
	 * 	进行Base64解码
	 * 	<br>默认使用标准Base64解码类型(RFC4648)
	 * 	
	 * 	@author Pan
	 * 	@param  content	要解码的字节数组
	 * 	@return	String	解码后的字节数组
	 */
	public static String decodeToStr(byte[] content) {
		return decodeToStr(content, Base64Type.RFC4648);
	}
	
	/**	
	 * 	进行Base64解码
	 * 	<br>自定义选择Base64解码类型
	 * 
	 * 	@author Pan
	 * 	@param  content		要解码的字节数组
	 * 	@param  base64Type	解码类型
	 * 	@return	String
	 */
	public static String decodeToStr(byte[] content, Base64Type base64Type) {
		return StrEncodeUtils.utf8EncodeToStr(base64Type.getDecoder().decode(content));
	}	
	
	/**	
	 * 	进行Base64解码
	 * 	<br>默认使用标准Base64解码类型(RFC4648)
	 * 	
	 * 	@author Pan
	 * 	@param  content	要解码的字符串
	 * 	@return	String	解码后的字节数组
	 */
	public static String decodeToStr(String content) {
		return decodeToStr(content.getBytes());
	}
	
	/**	
	 * 	进行Base64解码
	 * 	<br>自定义选择Base64解码类型
	 * 
	 * 	@author Pan
	 * 	@param  content		要解码的字符串
	 * 	@param  base64Type	解码类型
	 * 	@return	String
	 */
	public static String decodeToStr(String content, Base64Type base64Type) {
		return decodeToStr(content.getBytes(), base64Type);
	}
}
