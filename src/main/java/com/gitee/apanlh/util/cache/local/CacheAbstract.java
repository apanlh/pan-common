package com.gitee.apanlh.util.cache.local;

import com.gitee.apanlh.util.base.Empty;
import com.gitee.apanlh.util.base.MapUtils;
import com.gitee.apanlh.util.cache.local.api.CacheClear;
import com.gitee.apanlh.util.cache.local.api.CacheProvider;
import com.gitee.apanlh.util.thread.LockExecutor;
import com.gitee.apanlh.util.valid.ValidParam;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * 	缓存抽象类
 * 	<br>用于非Cache类使用的抽象类，Cache类单独实现方法，用于作为基本本地缓存
 * 	<br>使用重入锁
 * 	
 * 	@author Pan
 */
public abstract class CacheAbstract<K, V> extends LockExecutor implements CacheProvider, CacheClear {
	
	/** 缓存 */
	private Map<K, V> cache;
	
	/**	
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	CacheAbstract() {
		super();
		this.cache = MapUtils.newHashMap();
	}
	
	/**	
	 * 	构造函数-初始化map
	 * 	
	 * 	@author Pan
	 * 	@param 	map	Map
	 */
	CacheAbstract(Map<K, V> map) {
		super();
		this.cache = map;
	}
	
	/**	
	 * 	获取值
	 * 	<br>执行加锁
	 * 	
	 * 	@author Pan
	 * 	@param 	key	键
	 * 	@return	V
	 */
	public V get(K key) {
		if (key == null) {
			return null;
		}
		
		return lock(() -> {
			if (hasUnmodifiableMap()) {
				return getHandler(key, null);
			}
			return getHandler(key, cache.get(key));
		});
	}
	
	/**	
	 * 	获取值
	 * 	<br>无锁
	 * 	
	 * 	@author Pan
	 * 	@param 	key	键
	 * 	@return	V
	 */
	public V getUnlocked(K key) {
		if (key == null) {
			return null;
		}
		if (hasUnmodifiableMap()) {
			return getHandler(key, null);
		}
		return getHandler(key, cache.get(key));
	}
	
	/**
	 * 	设置值
	 * 	<br>执行加锁
	 * 	
	 * 	@author Pan
	 * 	@param 	key		键
	 * 	@param 	value	值
	 * 	@return	V
	 */
	public V put(K key, V value) {
		if (value == null) {
			return null;
		}
		return lock(() -> {
			V putHandler = putHandler(key, value);
			if (ValidParam.isNotNull(putHandler)) {
				cache.put(key, putHandler);
			}
			return value;
		});
	}
	
	/**
	 * 	设置值
	 * 	<br>无锁
	 *
	 * 	@author Pan
	 * 	@param 	key		键
	 * 	@param 	value	值
	 * 	@return	V
	 */
	public V putUnlocked(K key, V value) {
		if (value == null) {
			return null;
		}
		V putHandler = putHandler(key, value);
		if (ValidParam.isNotNull(putHandler)) {
			cache.put(key, putHandler);
		}
		return value;
	}
	
	/**	
	 * 	获取大小
	 * 	
	 * 	@author Pan
	 * 	@return	int
	 */	
	public int size() {
		return this.cache.size();
	}
	
	/**
	 * 	移除
	 * 	<br>执行加锁
	 * 	
	 * 	@author Pan
	 * 	@param 	key	键
	 * 	@return	V
	 */
	public V remove(K key) {
		return lock(() -> cache.remove(key));
	}

	/**
	 * 	只有在指定键当前映射到指定值时，才删除指定键的条目。
	 * 	<br>执行加锁
	 * 	
	 * 	@author Pan
	 * 	@param 	key		键
	 * 	@param 	value	值
	 * 	@return	V
	 */
	public boolean remove(K key, V value) {
		return lock(() -> cache.remove(key, value));
	}
	
	/**
	 * 	移除
	 * 	<br>无锁
	 *
	 * 	@author Pan
	 * 	@param 	key	键
	 * 	@return	V
	 */
	public V removeUnlocked(K key) {
		return cache.remove(key);
	}
	
	/**
	 * 	只有在指定键当前映射到指定值时，才删除指定键的条目。
	 * 	<br>无锁
	 * 	
	 * 	@author Pan
	 * 	@param 	key		键
	 * 	@param 	value	值
	 * 	@return	V
	 */
	public boolean removeUnlocked(K key, V value) {
		return cache.remove(key, value);
	}
	
	/**
	 * 	将Map清空
	 * 	<br>执行加锁
	 * 	
	 * 	@author Pan
	 */
	public void clear() {
		lock(() -> this.cache.clear());
	}
	
	/**
	 * 	将Map清空
	 * 	<br>未加锁
	 * 	
	 * 	@author Pan
	 */
	public void clearUnlock() {
		this.cache.clear();
	}
	
	/**	
	 * 	获取本身Map对象
	 * 	
	 * 	@author Pan
	 * 	@return	Map
	 */
	public Map<K, V> getMap() {
		return this.cache;
	}
	
	/**
	 * 	获取命中率
	 * 	
	 *	@author Pan
	 *	@throws AbstractMethodError 如果子类未实现此方法则抛出
	 */
	public int getHit() {
		throw new AbstractMethodError("not found getHit method");
	}
	
	/**	
	 * 	是否为空
	 * 	
	 * 	@author Pan
	 * 	@return	boolean
	 */
	public boolean isEmpty() {
		return ValidParam.isEmpty(cache);
	}

	/**	
	 * 	包含某键
	 * 	<br>执行加锁
	 * 	
	 * 	@author Pan
	 * 	@param 	key	键
	 * 	@return	boolean
	 */
	public boolean containsKey(K key) {
		return lock(() -> cache.containsKey(key));
	}

	/**	
	 * 	包含某键
	 * 	<br>未加锁
	 * 	
	 * 	@author Pan
	 * 	@param 	key	键
	 * 	@return	boolean
	 */
	public boolean containsKeyUnlocked(K key) {
		return cache.containsKey(key);
	}
	
	/**	
	 * 	包含某值
	 * 	<br>执行加锁
	 * 	
	 * 	@author Pan
	 * 	@param 	value	键
	 * 	@return	boolean
	 */
	public boolean containsValue(V value) {
		return lock(() -> cache.containsValue(value));
	}
	
	/**	
	 * 	包含某值
	 * 	<br>未加锁
	 * 	
	 * 	@author Pan
	 * 	@param 	value	键
	 * 	@return	boolean
	 */
	public boolean containsValueUnlocked(V value) {
		return cache.containsValue(value);
	}
	
	/**
	 * 	实现清理Map方法
	 * 	
	 *	@author Pan
	 *	@throws AbstractMethodError 如果子类未实现此方法则抛出
	 */
	public void clearCache() {
		throw new AbstractMethodError("not found clear method");
	}

	/**
	 * 	获取entrySet
	 * 	
	 * 	@author Pan
	 * 	@return	Set
	 */
	public Set<Entry<K, V>> entrySet() {
		return cache.entrySet();
	}
	
	/**
	 * 	获取keySet
	 * 	
	 * 	@author Pan
	 * 	@return	Set
	 */
	public Set<K> keySet() {
		return cache.keySet();
	}
	
	/**
	 * 	获取集合
	 * 	
	 * 	@author Pan
	 * 	@return	Collection
	 */
	public Collection<V> values() {
		return cache.values();
	}
	
	/**
	 * 	执行get方法
	 * 	<br>默认执行一次get方法并将获取到的值返回给value参数
	 * 	
	 * 	@author Pan
	 * 	@param 	key		键
	 * 	@param 	value	返回值
	 * 	@return	V		最终返回值
	 */
	abstract V getHandler(K key, V value);
	
	/**
	 * 	执行put方法
	 * 	<br>最终根据返回值并执行put方法
	 * 	
	 * 	@author Pan
	 * 	@param 	key		键
	 * 	@param 	value	返回值
	 * 	@return	V		最终返回值
	 */
	abstract V putHandler(K key, V value);
	
	@Override
	public boolean equals(Object o) {
		return super.equals(o);
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	/**	
	 * 	是否为不可变Map
	 * 	
	 * 	@author Pan
	 * 	@return boolean
	 */
	boolean hasUnmodifiableMap() {
		return Empty.map().equals(cache);
	}
}
