package com.gitee.apanlh.util.cache.redis.api;

import com.gitee.apanlh.util.cache.redis.RedisAutoAppendKey;

/**	
 * 	HyperLogLog结构
 * 	
 * 	#mark 完善
 * 	@author Pan
 */
public class RedisHyperLogLog extends AbstractRedis {

	public RedisHyperLogLog(RedisAutoAppendKey redisAutoKey) {
		super(redisAutoKey);
	}
	
//	private HyperLogLogOperations<String, Object> executor;
//	
//	@Override
//	public void executorConfigure() {
//		executor = getRedisTemplate().opsForHyperLogLog();
//	}
}
