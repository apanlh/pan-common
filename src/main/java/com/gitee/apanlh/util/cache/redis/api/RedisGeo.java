package com.gitee.apanlh.util.cache.redis.api;

import com.gitee.apanlh.util.cache.redis.RedisAutoAppendKey;

/**	
 * 	Geo结构
 * 	<br>用于地理空间相关
 * 	#mark 完善
 * 	@author Pan
 */
public class RedisGeo extends AbstractRedis {

	public RedisGeo(RedisAutoAppendKey redisAutoKey) {
		super(redisAutoKey);
	}

//	private GeoOperations<String, Object> executor;
//	
//	@Override
//	public void executorConfigure() {
//		this.executor = getRedisTemplate().opsForGeo();
//	}
}
