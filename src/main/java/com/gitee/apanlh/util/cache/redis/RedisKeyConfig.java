package com.gitee.apanlh.util.cache.redis;

/**	
 * 	RedisKey配置
 * 
 * 	@author Pan
 */
public class RedisKeyConfig {
	
	/** 默认分隔符风格 */
	private static final String DEFAULT_SEPARATOR = ":";
	
	/**	分隔符 */
	private String separator;
	
	/**
	 * 	默认-构造函数
	 * 	<br>:符号风格
	 * 
	 * 	@author Pan
	 */
	public RedisKeyConfig() {
		this(DEFAULT_SEPARATOR);
	}
	
	/**	
	 * 	自定义分隔符
	 * 	
	 * 	@author Pan
	 * 	@param  separator 自定义分割符号
	 */
	public RedisKeyConfig(String separator) {
		this.separator = separator;
	}

	/**	
	 * 	获取分隔符风格
	 * 	
	 * 	@author Pan
	 * 	@return String
	 */
	public String getSeparator() {
		return separator;
	}

	/**
	 * 	设置分隔符风格
	 * 	
	 * 	@author Pan
	 * 	@param 	separator	自定义分隔符
	 */
	public void setSeparator(String separator) {
		this.separator = separator;
	}
}
