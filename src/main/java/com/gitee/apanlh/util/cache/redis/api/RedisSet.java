package com.gitee.apanlh.util.cache.redis.api;

import com.gitee.apanlh.util.cache.redis.RedisAutoAppendKey;
import org.springframework.data.redis.core.SetOperations;

/**	
 * 	Set结构
 * 	
 * 	#mark 完善
 * 	@author Pan
 */
public class RedisSet extends AbstractRedis {

	public RedisSet(RedisAutoAppendKey redisAutoKey) {
		super(redisAutoKey);
	}
	
	private SetOperations<String, Object> executor;

	@Override
	public void executorConfigure() {
		this.executor = getRedisTemplate().opsForSet();
	}
}
