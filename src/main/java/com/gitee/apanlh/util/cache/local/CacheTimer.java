package com.gitee.apanlh.util.cache.local;

import com.gitee.apanlh.util.base.IteratorUtils;
import com.gitee.apanlh.util.base.MapUtils;
import com.gitee.apanlh.util.cache.local.api.CacheAutoClear;
import com.gitee.apanlh.util.func.FuncExecute;
import com.gitee.apanlh.util.valid.Assert;

/**	
 * 	本地时间缓存
 * 	<br>可自定义时间
 * 	<br>自定义回收打印日志输出
 *  <br>注意：如果超出最大缓存大小将不会在缓存（0将不限制）
 *  
 * 	@author Pan
 */
public class CacheTimer<T> implements CacheAutoClear {
	
	/** 缓存 */
	private Cache<String, CacheObject<T>> cache;
    /** 最大大小 */
    private int maxSize;
    
    /**	
     * 	默认构造函数
     * 	<br> 无上限缓存大小
     * 	<br> 初始化长度256	
     * 	<br> 每隔3秒检测缓存池
     * 	<br> 不输出缓存池信息
     * 
     * 	@author Pan
     */
    public CacheTimer() {
    	this(0, 256, 3000L, false);
    }

    /**	
     * 	构造函数
     * 	<br>每隔3秒检测缓存池
     * 	<br>不输出缓存池信息
     * 
     * 	@author Pan
     * 	@param 	maxPoolSize		0将不限制，最大缓存池长度
     * 	@param 	initMapSize		初始化长度
     */
    public CacheTimer(int maxPoolSize, int initMapSize) {
    	this(maxPoolSize, initMapSize, 3000L, false);
    }
    
    /**	
     * 	构造函数
     * 	<br>默认初始长度256
     * 	<br>每隔三秒检测缓存池
     * 	<br>不输出缓存池信息
     * 	
     * 	@author Pan
     * 	@param 	maxPoolSize		0将不限制，最大缓存池长度
     * 	@param 	period			删除机制延迟频率
     */
    public CacheTimer(int maxPoolSize, long period) {
    	this(maxPoolSize, 256, 3000L, false);
    }
    
    /**	
     * 	构造函数
     * 	<br>自定义最大长度
     * 	<br>默认初始长度256
     * 	<br>每隔三秒检测缓存池
     * 	<br>自定义输出缓存池信息
     * 	
     * 	@author Pan
     * 	@param 	maxPoolSize		0将不限制，最大缓存池长度
     * 	@param 	printLog		是否打印缓存池信息
     */
    public CacheTimer(int maxPoolSize, boolean printLog) {
    	this(maxPoolSize, 256, 3000L, printLog);
    }
    
    /**	
     * 	构造函数
     * 	<br>默认初始长度256
     * 	<br>每隔三秒检测缓存池
     * 	
     * 	@author Pan
     * 	@param 	maxPoolSize		0将不限制，最大缓存池长度
     * 	@param 	period			删除机制延迟频率
     * 	@param 	printLog		是否打印缓存池信息
     */
    public CacheTimer(int maxPoolSize, long period, boolean printLog) {
    	this(maxPoolSize, 256, 3000L, printLog);
    }
    
    /**	
     * 	构造函数
     * 	<br>不输出缓存池信息
     * 	
     * 	@author Pan
     * 	@param 	maxPoolSize		0将不限制，最大缓存池长度
     * 	@param 	initMapSize		初始化长度
     * 	@param 	period			删除机制延迟频率
     */
    public CacheTimer(int maxPoolSize, int initMapSize, long period) {
    	this(maxPoolSize, initMapSize, period, false);
    }
    
    /**	
     * 	自定义构造函数
     * 	
     * 	@author Pan
     * 	@param 	maxPoolSize		0将不限制，最大缓存池长度
     * 	@param 	initMapSize		初始化长度
     * 	@param 	period			删除机制延迟频率
     * 	@param 	printLog		是否打印缓存池信息
     */
    public CacheTimer(int maxPoolSize, int initMapSize, long period, boolean printLog) {
    	cache = CacheUtils.cache(MapUtils.newConcurrentHashMap(16)) ;
    	this.maxSize = maxPoolSize;
    	CacheClearTask.start(cache, period, printLog, autoClearCache());
    }

	/**	
	 * 	存放缓存数据
	 * 	默认不过期时间
	 * 	
	 * 	@author Pan
	 * 	@param  key		  	键
	 * 	@param  value 	   	值
	 * 	@return	T
	 */
	public T put(String key, T value) {
		return put(key, value, ExpireTime.DEFAULT.getTime());
	}

	/**	
	 * 	存放缓存并设置有效期 单位(秒)
	 * 
	 * 	@author Pan
	 * 	@param  key		  	键
	 * 	@param  value 	   	值
	 * 	@param  expireTime 	过期时间单位(秒)
	 * 	@return T
	 */
	public T put(String key, T value, long expireTime) {
		if (value == null || (maxSize > 0 && size() >= maxSize)) {
			return value;
		}
		Assert.isFalse(expireTime < 0L, "expire time must not be less than zero");
		
		if (expireTime == ExpireTime.DEFAULT.getTime()) {
			return cache.put(key, new CacheObject<>(value, ExpireTime.DEFAULT.getTime())).getData();
		}
		//	 存放到期时间
		return cache.put(key, new CacheObject<>(value, System.currentTimeMillis() + (expireTime * 1000L))).getData();
	}
	
	/**	
	 * 	存放缓存并设置有效期 单位(秒)
	 * 
	 * 	@author Pan
	 * 	@param  key		  	键
	 * 	@param  value 	   	值
	 * 	@param  expireTime 	过期时间单位(秒)
	 * 	@return T
	 */
	public T put(String key, T value, ExpireTime expireTime) {
		return put(key, value, expireTime.getTime());
	}

	/**
	 * 	获取缓存数据
	 * 
	 * 	@author Pan
	 * 	@param  key		键
	 * 	@return	T
	 */
	public T get(String key) {
		if (key == null) {
			return null;
		}
		CacheObject<T> cacheMapObject = cache.get(key);
		if (cacheMapObject == null) {
			return null;
		}
		
		if (checkExpireTime(cacheMapObject)) {
			cache.remove(key);
			return null;
		}
		return cacheMapObject.getData();
	}
	
	/**	
	 * 	获取缓存
	 * 	<br>指定类型
	 * 	
	 * 	@author Pan
	 * 	@param 	<R>		返回类型
	 * 	@param  key		键
	 * 	@param  clazz	类
	 * 	@return	R
	 */
	@SuppressWarnings("unchecked")
	public <R> R get(String key, Class<R> clazz) {
		T t = get(key);
		if (t == null) {
			return null;
		}
		return (R) t;
	}

	/**
	 * 	检测失效时间(false为未过期 true为过期)
	 * 	
	 * 	@author Pan
	 * 	@param  cacheMapObject		缓存对象
	 * 	@return	boolean
	 */
	private boolean checkExpireTime(CacheObject<T> cacheMapObject) {
		long expireTime = cacheMapObject.getExpire();
		if (expireTime == ExpireTime.DEFAULT.getTime()) {
			return false;
		}
		// 如果当前时间 > 设置的过期时间则认定Key失效
		return System.currentTimeMillis() > expireTime;
	}

	/**	
	 * 	从这个映射中移除键(及其对应的值)。如果键不在映射中，此方法不执行任何操作。
	 * 
	 *  @author Pan
	 * 	@param  key	键
	 */
	public void remove(String key) {
		if (get(key) != null) {
			cache.remove(key);
		}
	}
	
	/**
	 * 	获取缓存长度
	 *
	 * 	@author Pan
	 * 	@return	int
	 */
	public int size() {
		return cache.size();
	}

	/**
	 * 	执行清理策略
	 * 	
	 *	@author Pan
	 */	
	@Override
	public FuncExecute autoClearCache() {
		return () -> IteratorUtils.values(cache.getMap(), (value, iterator) -> {
			if (checkExpireTime(value)) {
				iterator.remove();
			}
		});
	}

	@Override
	public String toString() {
		return "CacheTimer[" + cache + "]";
	}
	
	/**
	 * 	缓存时间枚举
	 * 
	 *	@author Pan
	 */
	public enum ExpireTime {

		/** 默认时效(不过期)T */
		DEFAULT(0L),
		/** 一秒钟(单位秒) */
		ONE_SECONDS(1L),
		/** 一分钟(单位秒) */
		ONE_MINUTES(60L),
		/** 一小时(单位秒)*/
		ONE_HOURS(3600L),
		/** 一天(单位秒)*/
		ONE_DAY(86400L);
	    
		/** 过期时间  */
		private long time;
		
		/**
		 * 	构造函数
		 * 
		 * 	@author Pan
		 */
		ExpireTime(long time) {
			this.time = time;
		}

		/**
		 * 	获取时间
		 *
		 * 	@author Pan
		 *  @return long
		 */
		public long getTime() {
			return time;
		}
	}
}
