package com.gitee.apanlh.util.cache.local.api;

/**	
 * 	清理缓存接口
 * 	
 * 	@author Pan
 */
public interface CacheClear {
	
	/**	
	 * 	清理策略
	 * 	
	 * 	@author Pan
	 */
	void clearCache();
}
