package com.gitee.apanlh.util.cache.redis.api;

import com.gitee.apanlh.exp.RedisException;
import com.gitee.apanlh.util.cache.redis.RedisAutoAppendKey;
import com.gitee.apanlh.util.reflection.ClassConvertUtils;
import com.gitee.apanlh.util.valid.Assert;
import org.springframework.data.redis.core.HashOperations;

import java.util.List;
import java.util.Map;

/**	
 * 	Hash数据类型
 * 
 * 	@author Pan
 */
public class RedisHash extends AbstractRedis {
	
	private HashOperations<String, Object, Object> executor;
	
	/**	
	 * 	构造函数-自定义键
	 * 	
	 * 	@author Pan
	 * 	@param 	redisAutoKey	key对象
	 */
	public RedisHash(RedisAutoAppendKey redisAutoKey) {
		super(redisAutoKey);
	}
	
    /**
     * 	hashSet方法(multiSet)
     * 	<br>将Bean转换成Map存储
     * 	
     * 	@author Pan
     * 	@param 	<T>			数据类型
     * 	@param  key			键
     * 	@param  value 		值
     * 	@return	boolean
     */
    public <T> boolean set(String key, T value) {
    	try {
	    	executor.putAll(getRedisAutoKey().appendKey(key), ClassConvertUtils.toMap(value));
	    	return true;
    	} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
    }
    
    /**
     * 	hashSet方法
     * 	
     * 	@author Pan
     * 	@param  key			键
     * 	@param  mapKey		键
     * 	@param  value 		值
     * 	@return	boolean		true 为成功
     */
    public boolean set(String key, Object mapKey, Object value) {
    	try {
    		executor.put(getRedisAutoKey().appendKey(key), mapKey, value);
    		return true;
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
    }
    
    /**
     * 	hashSet方法
     * 	<br>设置过期时间  单位为秒
     * 	
     * 	@author Pan
     * 	@param  key 		键
     * 	@param  mapKey 		Map键
     * 	@param  value 		值
     * 	@param  time 		秒
     * 	@return	boolean
     */
    public boolean set(String key, Object mapKey, Object value, long time) {
    	try {
    		executor.put(getRedisAutoKey().appendKey(key), mapKey, value);
    		return expire(key, time);
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
    }
   
    /**	
     * 	hashSet	批量添加(multiSet)
     * 	@author Pan
     * 	@param  key			键		
     * 	@param  params		map 对应多个键值
     * 	@return	boolean		true成功 false 失败
     */
    public boolean set(String key, Map<Object, Object> params) {
    	try {
	    	executor.putAll(getRedisAutoKey().appendKey(key), params);
	    	return true;
    	} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
    }
    
    /**	
     * 	hashSet	批量添加(multiSet)
     * 	<br>设置过期有效期	单位为秒
     * 	
     * 	@author Pan
     * 	@param  key		键
     * 	@param  params	map 对应多个键值
     * 	@param  time 	时间单位 为秒
     * 	@return	boolean	true成功 false 失败
     */
    public boolean setForSeconds(String key, Map<Object, Object> params, long time) {
    	try {
    		executor.putAll(getRedisAutoKey().appendKey(key), params);
    		return expire(key, time);
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
    }
    
    /**	
     * 	如果变量值存在,可以在变量中可以添加不存在的的键值对
     * 	<br>如果变量不存在,则新增一个变量,同时将键值对添加到该变量。
     * 	<br>如果键值存在则返回false
     * 	
     * 	@author Pan
     * 	@param  key		键
     * 	@param  mapKey	Map键
     * 	@param  value	值
     * 	@return	boolean	true为添加成功 false为	找到相同的key
     */
    public boolean setIfAbsent(String key, Object mapKey, Object value) {
    	try {
    		return executor.putIfAbsent(getRedisAutoKey().appendKey(key), mapKey, value);
    	} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
    }
    
    /**	
     * 	判断map中是否有指定的HashKey。
     * 	
     * 	@author Pan
     * 	@param  key		键
     * 	@param  hashKey	值
     * 	@return	boolean	true包含指定key false为不包含指定的key
     */
    public boolean existKey(String key, Object hashKey) {
    	try {
    		return executor.hasKey(getRedisAutoKey().appendKey(key), hashKey);
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
    }
    
    /**	
     * 	根据mapKey获取一个value值
     * 	<br>没有则返回null	
     * 
     * 	@author Pan
     * 	@param  key		键
     * 	@param  hashKey	Map键
     * 	@return	Object
     */
    public Object get(String key, Object hashKey) {
    	try {
    		return executor.get(getRedisAutoKey().appendKey(key), hashKey);
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
    }
    
    /**	
     *	以集合的方式
     *	<br>获取一个或多个key中的值。	
     * 	
     * 	@author Pan
     * 	@param  key			键
     * 	@param  hashKeysList 以集合的方式存放key值
     * 	@return	List
     */
    public List<Object> get(String key, List<Object> hashKeysList) {
    	try {
    		return executor.multiGet(getRedisAutoKey().appendKey(key), hashKeysList);
    	} catch (Exception e) {
 			throw new RedisException(e.getMessage(), e);
 		}
    }
    
    /**	
     * 	获取map所有key
     * 	
     * 	@author Pan
     * 	@param  key		键
     *	@return	Map
     */
    public Map<Object, Object> getKeys(String key) {
    	try {
    		return executor.entries(getRedisAutoKey().appendKey(key));
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
    }
    
    /**
     * 	删除多个key
     * 	
     * 	@author Pan
     * 	@param  key			键
     * 	@param  hashKeys	Object数组包含字符串或整形类型
     * 	@return	Long 		返回的是删除条数
     */
    public Long del(String key, Object... hashKeys) {
    	try {
    		return executor.delete(getRedisAutoKey().appendKey(key), hashKeys);
    	} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
    }
   
    /**	
     * 	以集合类型删除Hash里面的Key
     * 	<br>删除key一个或多个key
     * 	<br>返回的是删除条数
     * 	
     * 	@author Pan
     * 	@param 	<T>			数据类型
     * 	@param  key			键
     * 	@param  hashKeys	集合类型
     * 	@return	Long		返回的是删除条数
     */
    public <T> Long del(String key, List<T> hashKeys) {
    	Assert.isNotEmpty(hashKeys);
    	try {
    		return executor.delete(getRedisAutoKey().appendKey(key), hashKeys.toArray());
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
    }
    
    /**	
     * 	以Object[]
     * 	<br>删除多个key
     * 	<br>返回删除后hashMap剩余的值以 key Val形式返回
     * 	
     * 	@author Pan
     * 	@param  key			键
     * 	@param  hashKeys	object数组包含字符串或整形类型
     * 	@return	Map
     */
    public Map<Object, Object> delToKeys(String key, Object... hashKeys) {
    	try {
    		executor.delete(getRedisAutoKey().appendKey(key), hashKeys);
    		return getKeys(key);
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
    }
    
    /**	
     * 	获取map中 key共有多少个
     * 	
     * 	@author Pan
     * 	@param  key		键
     * 	@return	Long	返回的是共有条数
     */
    public Long size(String key) {
    	try {
    		return executor.size(getRedisAutoKey().appendKey(key));
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
    }
    
    /**	
     * 	根据key返回value的长度
     * 	
     * 	@author Pan
     * 	@param  key			键
     * 	@param  hashKey		Map键
     * 	@return	Long
     */
    public Long sizeOfValue(String key, String hashKey) {
    	try {
    		return executor.lengthOfValue(getRedisAutoKey().appendKey(key), hashKey);
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
    }
    
    /**	
     * 	递增
     * 	<br>整数计数器
     * 	<br>如果key不存在则创建key 并赋值输入的计数值
     * 	
     * 	@author Pan
     * 	@param  key	键
     * 	@param  mapKey		Map键
     * 	@param  delta		递增因子
     * 	@return	Long
     */
    public Long increment(String key, Object mapKey, long delta) {
    	try {
    		return executor.increment(getRedisAutoKey().appendKey(key), mapKey, delta);
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
    }
    
    /**	
     * 	递增1
     * 	<br>整数 计数器
     * 	<br>递增因子默认为1
     * 	
     * 	@author Pan
     * 	@param  key			键
     * 	@param  mapKey		Map键
     * 	@return	Long
     */
    public Long increment(String key, Object mapKey) {
    	try {
    		return executor.increment(getRedisAutoKey().appendKey(key), mapKey, 1);
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
    }
    
    /**	
     * 	递减
     * 	<br>整数 计数器
     * 	<br>如果key不存在则创建key 并赋值输入的计数值
     * 	
     * 	@author Pan
     * 	@param  key			键
     * 	@param  mapKey		Map键
     * 	@param  delta 		递增因子
     * 	@return	Long
     */
    public Long decrement(String key, Object mapKey, long delta) {
    	try {
    		return executor.increment(getRedisAutoKey().appendKey(key), mapKey, delta);
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
    }
    
    /**	
     * 	递减1
     * 	<br>整数计数器
     * 	
     * 	@param  key		键
     * 	@param  mapKey	Map键
     * 	@return	Long
     */
    public Long decrement(String key, Object mapKey) {
    	try {
    		return executor.increment(getRedisAutoKey().appendKey(key), mapKey, -1);
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
    }
    
    /**	
     * 	浮点小数计数器
     * 	<br>如果key不存在则创建key 并赋值输入的计数值
     * 	
     * 	@author Pan
     * 	@param  key		键
     * 	@param  mapKey	Map键
     * 	@param  delta	递增因子(浮点数)
     * 	@return	Long
     */
    public Double floatIncrement(String key, Object mapKey, Double delta) {
    	try {
    		return executor.increment(getRedisAutoKey().appendKey(key), mapKey, delta);
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
    }
    
    /**	
     * 	浮点小数计数器
     * 	<br>如果key不存在则创建key 并赋值输入的计数值
     * 	
     * 	@author Pan
     * 	@param  key		键
     * 	@param  mapKey	Map键
     * 	@param  delta	递增因子(浮点数)	
     * 	@return	Double
     */
    public Double floatDecrement(String key, Object mapKey, Double delta) {
    	try {
    		return executor.increment(getRedisAutoKey().appendKey(key), mapKey, -delta);
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
    }
    
	@Override
	public void executorConfigure() {
		this.executor = getRedisTemplate().opsForHash();
	}
}
