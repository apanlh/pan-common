package com.gitee.apanlh.util.cache.redis.api;

import com.gitee.apanlh.util.cache.redis.RedisAutoAppendKey;
import org.springframework.data.redis.connection.RedisStringCommands;
import org.springframework.data.redis.core.RedisCallback;

/**	
 * 	BigMap结构
 * 	#mark
 * 	@author Pan
 */
public class RedisBigMap extends AbstractRedis {

	public RedisBigMap(RedisAutoAppendKey redisAutoKey) {
		super(redisAutoKey);
	}

	public Boolean setBit(String key, Integer index, Boolean tag) {
		return getRedisTemplate().execute((RedisCallback<Boolean>) con -> con.setBit(getRedisAutoKey().appendKey(key).getBytes(), index, tag));
	}

	public Boolean getBit(String key, Integer index) {
		return getRedisTemplate().execute((RedisCallback<Boolean>) con -> con.getBit(getRedisAutoKey().appendKey(key).getBytes(), index));
	}

	/**
	 * 	统计bitmap中，value为1的个数，
	 * 	<br>适用于统计网站的每日活跃用户数等类似的场景
	 *
	 *	@author Pan
	 * 	@param 	key	 键
	 * 	@return	Long
	 */
	public Long bitCount(String key) {
		return getRedisTemplate().execute((RedisCallback<Long>) con -> con.bitCount(getRedisAutoKey().appendKey(key).getBytes()));
	}

	public Long bitCount(String key, int start, int end) {
		return getRedisTemplate().execute((RedisCallback<Long>) con -> con.bitCount(getRedisAutoKey().appendKey(key).getBytes(), start, end));
	}

	public Long bitOp(RedisStringCommands.BitOperation op, String key, String... desKey) {
		byte[][] bytes = new byte[desKey.length][];
		for (int i = 0; i < desKey.length; i++) {
			bytes[i] = getRedisAutoKey().appendKey(key).getBytes();
		}
		return getRedisTemplate().execute((RedisCallback<Long>) con -> con.bitOp(op, getRedisAutoKey().appendKey(key).getBytes(), bytes));
	}

	@Override
	public void executorConfigure() {
		//	空实现
	}
}
