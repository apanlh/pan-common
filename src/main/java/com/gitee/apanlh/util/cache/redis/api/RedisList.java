package com.gitee.apanlh.util.cache.redis.api;

import com.gitee.apanlh.exp.RedisException;
import com.gitee.apanlh.util.cache.redis.RedisAutoAppendKey;
import com.gitee.apanlh.util.valid.Assert;
import org.springframework.data.redis.connection.RedisListCommands;
import org.springframework.data.redis.connection.RedisListCommands.Position;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisCallback;

import java.util.List;

/**
 * 	List结构
 * 	@author Pan
 */
public class RedisList extends AbstractRedis {

	public RedisList(RedisAutoAppendKey redisAutoKey) {
		super(redisAutoKey);
	}

	private ListOperations<String, Object> executor;

	/**
	 * 	根据指定索引获取列表中的元素
	 * 	
	 * 	@author Pan
	 * 	@param 	key			键
	 * 	@param 	index		索引
	 * 	@return Object
	 */
	public Object get(String key, long index) {
		return get(key, index, Object.class);
	}
	
	/**
	 * 	根据指定索引获取列表中的元素
	 * 	<br>自定义转换
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>			数据类型
	 * 	@param 	key			键
	 * 	@param 	index		索引
	 * 	@param 	clazz		类
	 * 	@return	T
	 */
	@SuppressWarnings("unchecked")
	public <T> T get(String key, long index, Class<T> clazz) {
		Object value = executor.index(key, index);
		if (value == null) {
			return null;
		}
		return (T) value;
	}
	
	/**
	 * 	获取列表中首个元素
	 * 	
	 * 	@author Pan
	 * 	@param 	key			键
	 * 	@return Object
	 */
	public Object getFirst(String key) {
		return get(key, 0, Object.class);
	}
	
	/**
	 * 	获取列表中首个元素
	 * 	<br>自定义转换
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>			数据类型
	 * 	@param 	key			键
	 * 	@param 	clazz		类
	 * 	@return T
	 */
	public <T> T getFirst(String key, Class<T> clazz) {
		return get(key, 0, clazz);
	}
	
	/**
	 * 	获取列表中最后一个元素
	 * 	
	 * 	@author Pan
	 * 	@param 	key			键
	 * 	@return	Object
	 */
	public Object getLast(String key) {
		return getLast(key, Object.class);
	}
	
	/**
	 * 	获取列表中最后一个元素
	 * 	<br>自定义转换
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>			数据类型
	 * 	@param 	key			键
	 * 	@param 	clazz		类
	 * 	@return	T
	 */
	public <T> T getLast(String key, Class<T> clazz) {
		try {
			Long index = size(key);
			if (index == null || index == 0L) {
				return null;
			}
			return get(key, index - 1, clazz);
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
	}
	
	/**	
	 * 	在列表中从左插入如果键不存在则自动创建
	 * 	<br>如果要存放Bean直接传入即可
	 * 	
	 * 	@author Pan
	 * 	@param 	key			键
	 * 	@param 	value		值
	 * 	@return	返回列表长度
	 */
	public Long leftPush(String key, Object value) {
		Assert.isNotNull(value);
		try {
			return executor.leftPush(getRedisAutoKey().appendKey(key), value);
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
	}
	
	/**	
	 * 	在列表中从左插入如果键不存在则自动创建
	 * 	<br>批量插入
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>			数据类型
	 * 	@param 	key			键
	 * 	@param 	value		值
	 * 	@return	Long 		返回列表长度
	 */
	public <T> Long leftPushAll(String key, List<T> value) {
		try {
			return executor.leftPushAll(getRedisAutoKey().appendKey(key), value);
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
	}
	
	/**	
	 * 	在列表中从左插入如果键不存在则自动创建
	 * 	<br>批量插入
	 * 	
	 * 	@author Pan
	 * 	@param 	key			键
	 * 	@param 	value		值
	 * 	@return	返回列表长度
	 */
	public Long leftPushAll(String key, Object... value) {
		try {
			return executor.leftPushAll(getRedisAutoKey().appendKey(key), value);
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
	}
	
	/**	
	 * 	从左弹出首位元素，
	 * 	<br>弹出元素后列表中删除该元素
	 * 	<br>如果键不存在则返回null
	 * 	
	 * 	@author Pan
	 * 	@param 	key	键
	 * 	@return 返回弹出元素
	 */
	public Object leftPop(String key) {
		return leftPop(key, Object.class);
	}
	
	/**	
	 * 	从左弹出首位元素，
	 * 	<br>弹出元素后列表中删除该元素
	 * 	<br>如果键不存在则返回null
	 * 	<br>自定义返回类型
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>		数据类型
	 * 	@param 	key		键
	 * 	@param 	clazz	类
	 * 	@return T 返回弹出元素
	 */
	@SuppressWarnings("unchecked")
	public <T> T leftPop(String key, Class<T> clazz) {
		try {
			return (T) executor.leftPop(getRedisAutoKey().appendKey(key));
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
	}
	
	/**	
	 * 	在列表中从右插入如果键不存在则自动创建
	 * 	
	 * 	@author Pan
	 * 	@param 	key			键
	 * 	@param 	value		值
	 * 	@return	返回列表长度
	 */
	public Long rightPush(String key, Object value) {
		try {
			return executor.rightPush(getRedisAutoKey().appendKey(key), value);
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
	}
	
	/**	
	 * 	在列表中从右插入如果键不存在则自动创建
	 * 	<br>批量插入
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>			数据类型
	 * 	@param 	key			键
	 * 	@param 	value		值
	 * 	@return	Long 		返回列表长度
	 */
	public <T> Long rightPushAll(String key, List<T> value) {
		try {
			return executor.rightPushAll(getRedisAutoKey().appendKey(key), value);
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
	}
	
	/**	
	 * 	在列表中从右插入如果键不存在则自动创建
	 * 	<br>批量插入
	 * 	
	 * 	@author Pan
	 * 	@param 	key			键
	 * 	@param 	value		值
	 * 	@return	返回列表长度
	 */
	public Long rightPushAll(String key, Object... value) {
		try {
			return executor.rightPushAll(getRedisAutoKey().appendKey(key), value);
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
	}
	
	/**	
	 * 	从右弹出首位元素
	 * 	<br>弹出元素后列表中删除该元素
	 * 	<br>如果键不存在则返回null
	 * 	
	 * 	@author Pan
	 * 	@param 	key	键
	 * 	@return 返回弹出元素	
	 */
	public Object rightPop(String key) {
		return rightPop(key, Object.class);
	}
	
	/**	
	 * 	从右弹出首位元素
	 * 	<br>弹出元素后列表中删除该元素
	 * 	<br>如果键不存在则返回null
	 * 	<br>自定义返回类型
	 * 	
	 * 	@author Pan
	 * 	@param 	<T>		数据类型
	 * 	@param 	key		键
	 * 	@param 	clazz	类
	 * 	@return 返回弹出元素
	 */
	@SuppressWarnings("unchecked")
	public <T> T rightPop(String key, Class<T> clazz) {
		try {
			return (T) executor.rightPop(getRedisAutoKey().appendKey(key));
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
	}
	/**	
	 * 	在列表中里搜索到的value后,再左端或右端插入值
	 * 	<br>从左插入O(1)
	 * 	<br>最坏情况O(N)
	 * 	<br>未找到位置返回-1或者pivot的值或0值
	 * 
	 * 	
	 * 	@author Pan
	 * 	@param 	key			键
	 * 	@param 	position	插入位置
	 * 	@param 	pivot		搜索插入的value
	 * 	@param 	value		插入值
	 * 	@return	返回列表长度
	 */
	private Long insert(byte[] key, Position position, byte[] pivot, byte[] value) {
		try {
			return executor.getOperations().execute((RedisCallback<Long>) connection -> {
				RedisListCommands listCommands = connection.listCommands();
				return listCommands.lInsert(key, position, pivot, value);
			});
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
	}

	/**	
	 * 	在列表中里搜索到的value后,在value左端插入值
	 * 	<br>未找到位置返回-1或者pivot的值或0值
	 * 	
	 * 	@author Pan
	 * 	@param 	key				键
	 * 	@param 	pivot			搜索列表中的value
	 * 	@param 	value			插入值
	 * 	@return	返回列表长度
	 */
	public Long insertAfter(String key, Object pivot, Object value) {
		Assert.isNotNull(pivot);
		Assert.isNotNull(value);
		return insert(getRedisAutoKey().appendKey(key).getBytes(), Position.AFTER, pivot.toString().getBytes(), value.toString().getBytes());
	}
	
	/**	
	 * 	在列表中里搜索到的value后,在value右端插入值
	 * 	<br>未找到位置返回-1或者pivot的值或0值
	 * 	
	 * 	@author Pan
	 * 	@param 	key				键
	 * 	@param 	searchValue		搜索列表中的value
	 * 	@param 	insertValue		插入值
	 * 	@return	返回列表长度
	 */
	public Long insertBefore(String key, Object searchValue, Object insertValue) {
		Assert.isNotNull(searchValue);
		Assert.isNotNull(insertValue);
		return insert(getRedisAutoKey().appendKey(key).getBytes(), Position.BEFORE, searchValue.toString().getBytes(), insertValue.toString().getBytes());
	}
	
	/**	
	 * 	获取列表中的长度
	 * 	
	 * 	@author Pan
	 * 	@param 	key		键
	 * 	@return	返回列表长度
	 */
	public Long size(String key) {
		try {
			return executor.size(getRedisAutoKey().appendKey(key));
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
	}
	
	@Override
	public void executorConfigure() {
		this.executor = getRedisTemplate().opsForList();
	}
}
