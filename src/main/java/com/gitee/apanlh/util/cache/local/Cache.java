package com.gitee.apanlh.util.cache.local;

import com.gitee.apanlh.exp.CallRuntimeException;
import com.gitee.apanlh.util.base.MapUtils;
import com.gitee.apanlh.util.base.StringUtils;
import com.gitee.apanlh.util.func.FuncCall;
import com.gitee.apanlh.util.func.FuncMapExecute;
import com.gitee.apanlh.util.reflection.ClassTypeUtils;
import com.gitee.apanlh.util.reflection.ClassUtils;
import com.gitee.apanlh.util.thread.LockExecutor;
import com.gitee.apanlh.util.valid.Assert;
import com.gitee.apanlh.util.valid.ValidParam;

import java.util.AbstractMap;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.BiFunction;
import java.util.function.Function;

/**	
 * 	本地缓存
 * 	<br>默认为弱引用缓存(WeakHashMap)
 * 	<br>默认对并发做了处理
 * 
 * 	@author Pan
 */
public class Cache<K, V> extends AbstractMap<K, V> implements Map<K, V> {
	
	/** 缓存 */
	private final Map<K, V> cacheMap;
	/** 获取Map类型 */
	private String cacheMapClassName;
	/** 锁执行器 */
	private LockExecutor lock = LockExecutor.createReadWriteLock();
	/** 是否为ConcurrentHashMap */
	private boolean hasConcurrentHashMap;
	
	/**
	 * 	构造函数-默认使用弱引用缓存(WeakHashMap)
	 * 	<br>默认64长度
	 * 	
	 * 	@author Pan
	 */
	public Cache() {
		this(64);
	}
	
	/**	
	 * 	构造函数-默认使用弱引用缓存(WeakHashMap)
	 * 	<br>自定义长度
	 * 
	 * 	@author Pan
	 * 	@param 	initSize	初始化长度
	 */
	public Cache(int initSize) {
		this(MapUtils.newWeakHashMap(initSize));
	}
	
	/**
	 * 	构造函数-自定义Map类型
	 *	<br>区分对ConcurrentHashMap做特殊处理
	 *	
	 *	@author Pan
	 *  @param 	initMap 自定义Map类型
	 */
	public Cache(Map<K, V> initMap) {
		Assert.isNotNull(initMap);

		this.cacheMap = initMap;
		this.cacheMapClassName = ClassUtils.getSimpleName(initMap);
		
		if (ClassTypeUtils.isConcurrentHashMap(this.cacheMap)) {
			this.hasConcurrentHashMap = true;
		}
	}
	
	/**
	 * 	构造函数-自定义初始化加载map
	 *	<br>默认使用弱引用缓存(WeakHashMap)实现自动清理
	 *
	 *	@author Pan
	 *  @param 	funcMapExecute 函数加载执行Map
	 */
	public Cache(FuncMapExecute<K, V> funcMapExecute) {
		this(MapUtils.newWeakHashMap(16));
		funcMapExecute.execute(this.cacheMap);
	}
	
	/**	
	 * 	获取缓存
	 * 	
	 * 	@author Pan
	 * 	@param 	key	键
	 * 	@return	V
	 */
	@Override
	public V get(Object key) {	
		if (hasConcurrentHashMap) {
			return cacheMap.get(key);
		}
		return lock.readLock(() -> cacheMap.get(key));
	}
	
	/**
	 * 	重入读写锁
	 * 	<br>对ConcurrentHashMap优化处理
	 * 
	 * 	@author Pan
	 * 	@param 	key		键
	 * 	@param 	value	回调接口
	 * 	@return	V
	 */
	public V get(K key, FuncCall<V> value) {
		if (this.hasConcurrentHashMap) {
			computeIfAbsent(key, k -> {
				try {
					return value.call();
				} catch (Exception e) {
					throw new CallRuntimeException(e.getMessage(), e);
				}
			});
		}
		
		V v = cacheMap.get(key);
		if (ValidParam.isNull(v) && ValidParam.isNotNull(value)) {
			ReentrantReadWriteLock.WriteLock writeLock = lock.getWriteLock();
			writeLock.lock();
			v = cacheMap.get(key);
			try {
				if (v == null) {
					V call = value.call();
					cacheMap.put(key, call);
					return call;
				}
			} catch (Exception e) {
				throw new CallRuntimeException(e.getMessage(), e);
			} finally {
				writeLock.unlock();
			}
		}
		return v;
	}
	
	/**	
	 * 	存入缓存
	 * 	
	 * 	@author Pan
	 * 	@param 	key		键
	 * 	@param 	value	值
	 * 	@return	V
	 */
	@Override
	public V put(K key, V value) {
		if (hasConcurrentHashMap) {
			computeIfAbsent(key, k -> value);
		}
		lock.writeLock(() -> cacheMap.put(key, value));
		return value;
	}

	/**
	 * 	悲观锁
	 * 	<br>双重检查
	 * 	<br>基本用于需要读写一致情况下使用
	 * 	<br>例如：单例等
	 * 	
	 * 	@author Pan
	 * 	@param 	key		键
	 * 	@param 	value	值
	 * 	@return	V
	 */
	public V doubleCheck(K key, FuncCall<V> value) {
		if (this.hasConcurrentHashMap) {
			computeIfAbsent(key, k -> {
				try {
					return value.call();
				} catch (Exception e) {
					throw new CallRuntimeException(e.getMessage(), e);
				}
			});
		}
		
		V v = cacheMap.get(key);
		if (ValidParam.isNull(v) && ValidParam.isNotNull(value)) {
			synchronized (cacheMap) {
				v = cacheMap.get(key);
				if (v == null) {
					try {
						V call = value.call();
						cacheMap.put(key, call);
						return call;
					} catch (Exception e) {
						throw new CallRuntimeException(e.getMessage(), e);
					}
				}
			}
		}
		return v;
	}
	
	@Override
	public int size() {
		if (hasConcurrentHashMap) {
			return (int) ((ConcurrentHashMap<K, V>) this.cacheMap).mappingCount();
		}
		return lock.readLock(cacheMap::size);
	}
	
	@Override
	public V remove(Object key) {
		return lock.writeLock(() -> cacheMap.remove(key));
	}
	
	@Override
	public void clear() {
		lock.writeLock(cacheMap::clear);
	}
	
	/**	
	 * 	获取本身Map对象
	 * 	
	 * 	@author Pan
	 * 	@return	Map
	 */
	public Map<K, V> getMap() {
		return this.cacheMap;
	}

	@Override
	public boolean isEmpty() {
		return ValidParam.isEmpty(cacheMap);
	}

	@Override
	public boolean containsKey(Object key) {
		if (hasConcurrentHashMap) {
			return cacheMap.containsKey(key);
		}
		return lock.readLock(() -> cacheMap.containsKey(key));
	}

	@Override
	public boolean containsValue(Object value) {
		if (hasConcurrentHashMap) {
			return cacheMap.containsValue(value);
		}
		return lock.readLock(() -> cacheMap.containsValue(value));
	}
	
	@Override
	public V compute(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
		if (hasConcurrentHashMap) {
			return cacheMap.compute(key, remappingFunction);
		}
		return lock.writeLock(() -> cacheMap.compute(key, remappingFunction));
	}
	
	@Override
	public V computeIfAbsent(K key, Function<? super K, ? extends V> mappingFunction) {
		if (hasConcurrentHashMap) {
			V v = cacheMap.get(key);
			if (v == null) {
				return cacheMap.computeIfAbsent(key, mappingFunction);
			}
			return v;
		}
		return lock.writeLock(() -> cacheMap.computeIfAbsent(key, mappingFunction));
	}
	
	@Override
	public V computeIfPresent(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
		if (hasConcurrentHashMap) {
			return cacheMap.computeIfPresent(key, remappingFunction);
		}
		return lock.writeLock(() -> cacheMap.computeIfPresent(key, remappingFunction));
	}
	
	@Override
	public Set<Entry<K, V>> entrySet() {
		return cacheMap.entrySet();
	}
	
	@Override
	public Set<K> keySet() {
		return cacheMap.keySet();
	}
	
	@Override
	public Collection<V> values() {
		return cacheMap.values();
	}
	
	@Override
	public String toString() {
		return StringUtils.format("type=[{}], CacheMap=[{}]", this.cacheMapClassName, this.cacheMap);
	}
	
	@Override
	public boolean equals(Object o) {
		return cacheMap.equals(o);
	}
	
	@Override
	public int hashCode() {
		return cacheMap.hashCode();
	}
}
