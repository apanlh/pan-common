package com.gitee.apanlh.util.cache.local;

import com.gitee.apanlh.util.base.IteratorUtils;

import java.util.LinkedHashMap;
import java.util.Map;


/**	
 * 	CacheLRU
 * 	<br>淘汰最久未使用
 * 	
 * 	@author Pan
 */
public class CacheLru<K, V> extends CacheAbstract<K, V> {
	
	/**	最大容量 */
	private int capacity;
	/** 尾端节点 */
	private K endNodeK;
	/** 缓存 */
	private Map<K, V> map;
	/** 命中次数 */
	private int hit;
	
	/**
	 * 	默认构造函数
	 * 	<br>默认256最大容量
	 * 	
	 * 	@author Pan
	 */
	public CacheLru() {
		this(256);
	}
	
	/**	
	 * 	构造函数-自定义容量
	 * 	
	 * 	@author Pan
	 * 	@param 	capacity	容量
	 */
	public CacheLru(int capacity) {
		super(new LinkedHashMap<K, V>(capacity, 0.75f, true));
		this.capacity = capacity;
		this.map = getMap();
	}

	@Override
	V getHandler(K key, V value) {
		if (value == null) {
			return value;
		}
		// 命中
		hit++;
		return value;
	}

	@Override
	V putHandler(K key, V value) {
		if (this.endNodeK == key) {
			return value;
		}
		clearCache();
		this.endNodeK = key;
		return value;
	}
	
	@Override
	public String toString() {
		return getMap().toString();
	}	
	
	@Override
	public boolean equals(Object o) {
		return super.equals(o);
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	@Override
	public int getHit() {
		return this.hit;
	}
	
	@Override
	public void clearCache() {
		//	移除队列队头
		if (size() == this.capacity) {
			IteratorUtils.values(this.map, (v, iterator) -> iterator.remove());
		}
	}
}
