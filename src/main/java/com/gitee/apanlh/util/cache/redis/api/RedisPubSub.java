package com.gitee.apanlh.util.cache.redis.api;

import com.gitee.apanlh.util.cache.redis.RedisAutoAppendKey;

/**	
 * 	Pub/Sub结构
 * 
 * 	#mark 完善
 * 	@author Pan
 */
public class RedisPubSub extends AbstractRedis {

	public RedisPubSub(RedisAutoAppendKey redisAutoKey) {
		super(redisAutoKey);
	}
	
//	private Object a = null;
//	
//	@Override
//	public void executorConfigure() {
//		getRedisTemplate();
//	}
}
