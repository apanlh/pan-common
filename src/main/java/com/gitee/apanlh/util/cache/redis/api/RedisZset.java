package com.gitee.apanlh.util.cache.redis.api;

import com.gitee.apanlh.util.cache.redis.RedisAutoAppendKey;
import org.springframework.data.redis.core.ZSetOperations;

/**	
 * 	ZSet结构
 * 	
 * 	#mark 完善
 * 	@author Pan
 */
public class RedisZset extends AbstractRedis {

	public RedisZset(RedisAutoAppendKey redisAutoKey) {
		super(redisAutoKey);
	}
	
	private ZSetOperations<String, Object> executor;

	@Override
	public void executorConfigure() {
		this.executor = getRedisTemplate().opsForZSet();
	}
}
