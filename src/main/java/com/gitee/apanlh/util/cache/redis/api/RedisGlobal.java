package com.gitee.apanlh.util.cache.redis.api;

import com.gitee.apanlh.util.cache.redis.RedisAutoAppendKey;

/**	
 * 	拥有Redis全类型方法
 * 	<br>不限制类型
 * 	<br>不自动添加key
 *
 * 	#mark 完善
 * 	@author Pan
 */
public class RedisGlobal extends AbstractRedis {

	RedisGlobal(RedisAutoAppendKey redisAutoKey) {
		super(redisAutoKey);
	}
	
	//	private Object a = null;
}
