package com.gitee.apanlh.util.cache.redis.api;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**	
 * 	Redis基础Api
 * 	@author Pan
 */
public interface RedisApi {

	/**	
     * 	获取当前连接的Redis时间
     * 	
     * 	@author Pan
     * 	@return	Long
     */
	Long getRedisServerTime();
	
	/**
     * 	指定一个key的缓存失效时间 (秒)
     * 	<br>时间不可为0
     * 	
     * 	@author Pan
     * 	@param  key		键
     * 	@param  time	过期时间
     * 	@return boolean	true成功
     */
	boolean expire(String key, long time);

	/**
	 * 	指定一个key的缓存失效时间 (自定义单位)
	 * 	<br>自定义时间
	 * 	
	 * 	@author Pan
	 * 	@param 	key		键
	 * 	@param 	time	过期时间
	 * 	@param 	unit	时间单位
	 * 	@return boolean	true成功
	 */
	boolean expire(String key, long time, TimeUnit unit);
	
	 /**
     * 	指定一个key的缓存失效时间 (小时)
     * 	<br>时间不可为0
     * 
     * 	@author Pan
     * 	@param  key		键
     * 	@param  time	过期时间	
     *	@return boolean	true成功
     */
	boolean expireForHours(String key, long time);
	
	 /**
     *	指定一个key的缓存失效时间 (Date对象)
     *
     * 	@author Pan
     * 	@param  key		键
     * 	@param  date	过期时间
     * 	@return boolean	true成功
     */
	boolean expireForDate(String key, Date date);
	
	 /**
     * 	查找键是否存在
     * 
     * 	@author Pan
     * 	@param  key		键
     * 	@return boolean true成功 
     */
	boolean exist(String key);
	
	/**
     * 	查找多个键是否同时存在({@code &}与条件)
     * 
     * 	@author Pan
     * 	@param  keys	一个或多个键
     * 	@return boolean true存在
     */
	boolean exists(String... keys);
	
	/**
	 * 	查找多个键某一个是否存在(@code |}或条件)
	 * 
	 * 	@author Pan
	 * 	@param  keys	一个或多个键
	 * 	@return	boolean
	 */
	boolean existsOr(String... keys);
	
	 /**
     * 	删除一个key
     * 
     * 	@author Pan
     * 	@param  key		键
     * 	@return boolean true成功
     */
	boolean delKey(String key);

	 /**
     * 	删除多个键
     * 
     * 	@author Pan
     * 	@param  keys	键
     * 	@return Long	返回删除的总条数
     */
	Long delKeys(String... keys);
	
	/**
     * 	删除多个键
     * 
     * 	@author Pan
     * 	@param  keys 	键
     * 	@return Long	返回删除的总条数
     */
	Long delKeys(List<String> keys);
	
	/**
     * 	扫描匹配的键值	
     * 
     * 	@author Pan
     * 	@param  pattern	查询的key
     * 	@param  limit	分页条数
     * 	@return	Set
     */
	Set<String> scan(String pattern, long limit);
	
	/**	
	 * 	返回此Key的数据类型
	 * 	<br>string、list、set、zset和hash等
	 * 	
	 * 	@author Pan
	 * 	@param 	key		键
	 * 	@return	String
	 */
	String type(String key);
}
