package com.gitee.apanlh.util.cache.redis;

import com.gitee.apanlh.exp.LoadErrorException;
import com.gitee.apanlh.spring.BeanContextUtils;
import com.gitee.apanlh.util.base.IteratorUtils;
import com.gitee.apanlh.util.base.MapUtils;
import com.gitee.apanlh.util.base.StringUtils;
import com.gitee.apanlh.util.cache.local.Cache;
import com.gitee.apanlh.util.cache.local.CacheUtils;
import com.gitee.apanlh.util.cache.redis.api.AbstractRedis;
import com.gitee.apanlh.util.id.IdUtils;
import com.gitee.apanlh.util.reflection.ClassConvertUtils;
import com.gitee.apanlh.util.thread.StackUtils;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Map;

/**	
 * 	RedisKey加载器
 * 
 * 	@author Pan
 */
public class RedisKeyLoad {
	
	static {
		RedisLoadTask.createInitBeansTask();
	}
	
	/** 存放加载对象 */
	static final Cache<String, RedisKeyBuilder<? extends AbstractRedis>> LOAD_BEANS = CacheUtils.cache(MapUtils.newConcurrentHashMap(32));
	/**	 初始化加载对象 */
	static Map<String, RedisKeyBuilder<? extends AbstractRedis>> initLoadBeans = MapUtils.newConcurrentHashMap(16);
	
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private RedisKeyLoad() {
		//	不允许外部实例
		super();
	}
	
	/**
	 * 	加载RedisTemplate
	 * 	
	 * 	@author Pan
	 * 	@param  <V>     值类型
	 * 	@param 	redisKeyBuilder	Redis构建对象
	 * 	@return RedisKeyBuilder
	 */
	public static <V extends AbstractRedis> RedisKeyBuilder<V> load(RedisKeyBuilder<V> redisKeyBuilder) {
		//	验证是否重复加载key
		checkKeyExist(redisKeyBuilder);
		//	保存加载对象以及初始化对象
		putInitLoadBean(redisKeyBuilder);
		return putLoadBean(redisKeyBuilder);
	}
	
	/**	
	 * 	添加已经加载完成的Redis对象
	 * 	
	 * 	@author Pan
	 * 	@param 	<V>					Redis数据类型
	 * 	@param 	redisKeyBuilder		RedisKey对象
	 * 	@return RedisKeyBuilder
	 */
	static <V extends AbstractRedis> RedisKeyBuilder<V> putLoadBean(RedisKeyBuilder<V> redisKeyBuilder) {
		LOAD_BEANS.put(redisKeyBuilder.getKey(), redisKeyBuilder);
		return redisKeyBuilder;
	}
	
	/**	
	 * 	添加初始化加载Redis对象
	 * 	
	 * 	@author Pan
	 * 	@param 	<V>					Redis数据类型
	 * 	@param 	redisKeyBuilder		RedisKey对象
	 * 	@return RedisKeyBuilder
	 */
	static <V extends AbstractRedis> RedisKeyBuilder<V> putInitLoadBean(RedisKeyBuilder<V> redisKeyBuilder) {
		initLoadBeans.put(getKey(redisKeyBuilder.getKey()), redisKeyBuilder);
		return redisKeyBuilder;
	}
	
	/**
	 * 	获取初始化键
	 * 	
	 * 	@author Pan
	 * 	@param 	initKey	初始化key键
	 * 	@return	String
	 */
	private static String getKey(String initKey) {
		StackTraceElement stackTraceElement = StackUtils.getStackTraceByKeyword(".<clinit>");
		
		if (stackTraceElement == null) {
			return IdUtils.generateIdStr();
		}
		return new StringBuilder()
			.append(stackTraceElement.getClassName())
			.append("[")
			.append(initKey)
			.append("]")
		.toString();
	}
	
	/**	
	 * 	获取RedisTemplateBean
	 * 	
	 * 	@author Pan
	 * 	@return	RedisTemplate
	 */
	static RedisTemplate<String, Object> getRedisTemplateBean() {
		if (BeanContextUtils.getApplicationContext() == null) {
			return null;
		}
		return ClassConvertUtils.cast(BeanContextUtils.getBean("redisTemplate"));
	}
	
	/**
	 * 	检测是否重复加载相同key
	 * 	
	 * 	@author Pan
	 * 	@param 	redisKeyBuilder	Key对象
	 */
	static <V extends AbstractRedis> void checkKeyExist(RedisKeyBuilder<V> redisKeyBuilder) {
		if (LOAD_BEANS.get(redisKeyBuilder.getKey()) != null) {
			throw new LoadErrorException(StringUtils.format("redis key [{}] is exist", redisKeyBuilder.getKey()));
		}
	}

	/**	
	 * 	获取所有初始化加载Redis对象
	 * 
	 * 	@author Pan
	 * 	@return	Map
	 */
	static Map<String, RedisKeyBuilder<? extends AbstractRedis>> getInitLoadBeans() {
		return initLoadBeans;
	}

	/**	
	 * 	获取所有加载Redis对象
	 * 	
	 * 	@author Pan
	 * 	@return	Cache
	 */
	public static Cache<String, RedisKeyBuilder<? extends AbstractRedis>> getLoadBeans() {
		return LOAD_BEANS;
	}
	
	/**
	 * 	清除所有初始化加载Bean
	 * 	
	 * 	@author Pan
	 */
	static void clearInitLoadBeans() {
		IteratorUtils.entrySet(initLoadBeans, (k, v, iterator) -> iterator.remove());
		initLoadBeans = null;
	}
}
