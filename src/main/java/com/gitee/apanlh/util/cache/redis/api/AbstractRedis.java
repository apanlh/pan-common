package com.gitee.apanlh.util.cache.redis.api;

import com.gitee.apanlh.exp.RedisException;
import com.gitee.apanlh.util.base.StringUtils;
import com.gitee.apanlh.util.cache.redis.RedisAutoAppendKey;
import com.gitee.apanlh.util.io.IOUtils;
import com.gitee.apanlh.util.reflection.ClassUtils;
import com.gitee.apanlh.util.valid.Assert;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**	
 * 	抽象Redis数据类型
 * 	<br>集成基础API
 * 
 * 	@author Pan
 */
public abstract class AbstractRedis implements RedisApi {
	
	/** 自动装配Key */
	private RedisAutoAppendKey redisAutoKey;
	/** 客户端 */
	private RedisTemplate<String, Object> redisTemplate;
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	AbstractRedis() {
		super();
	}
	
	/**	
	 * 	构造函数-加载装配
	 * 	
	 * 	@author Pan
	 * 	@param 	redisAutoKey	装配Key对象
	 */
	AbstractRedis(RedisAutoAppendKey redisAutoKey) {
		this.redisAutoKey = redisAutoKey;
	}
	
	/**	
	 * 	获取自动装配Key
	 * 	
	 * 	@author Pan
	 * 	@return	RedisAutoAppendKey
	 */
	RedisAutoAppendKey getRedisAutoKey() {
		return redisAutoKey;
	}
	
	/**	
	 * 	获取Redis客户端
	 * 	
	 * 	@author Pan
	 * 	@return	RedisTemplate
	 */
	public RedisTemplate<String, Object> getRedisTemplate() {
		return redisTemplate;
	}

	/**
	 * 	获取最终构造的键
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getBuildKey() {
		return redisAutoKey.getKey();
	}
	
	@Override
	public Long getRedisServerTime() {
		return redisTemplate.execute((RedisCallback<Long>) t -> t.time());
	}

	@Override
	public boolean expire(String key, long time) {
		return expire(key, time, TimeUnit.SECONDS);
	}

	@Override
	public boolean expire(String key, long time, TimeUnit unit) {
		Assert.isFalse(time <= 0L);
		
		try {
			Boolean expire = redisTemplate.expire(redisAutoKey.appendKey(key), time, unit);
			if (expire == null) {
				return false;
			}
			return expire;
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
	}
	
	@Override
	public boolean expireForHours(String key, long time) {
		return expire(key, time, TimeUnit.HOURS);
	}

	@Override
	public boolean expireForDate(String key, Date date) {
		try {
			Boolean expire = redisTemplate.expireAt(redisAutoKey.appendKey(key), date);
			if (expire == null) {
				return false;
			}
			return expire;
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
	}
	
	@Override
	public boolean exist(String key) {
		try {
			Boolean aBoolean = redisTemplate.hasKey(redisAutoKey.appendKey(key));
			if (aBoolean == null) {
				return false;
			}
			return aBoolean;
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
	}

	@Override
	public boolean exists(String... keys) {
		Assert.isNotEmpty(keys);
		
		try {
    		for (int i = 0, len = keys.length; i < len; i++) {
    			Boolean hasKey = redisTemplate.hasKey(redisAutoKey.appendKey(keys[i]));
    			if (hasKey == null || !hasKey) {
    				return false;
    			}
    		}
    		return true;
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
	}
	
	@Override
	public boolean existsOr(String... keys) {
		Assert.isNotEmpty(keys);
		
		try {
    		for (int i = 0, len = keys.length; i < len; i++) {
    			Boolean hasKey = redisTemplate.hasKey(redisAutoKey.appendKey(keys[i]));
				if (hasKey == null) {
					continue;
				}
    			if (hasKey) {
    				return true;
    			}
    		}
    		return false;
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
	}
	
	@Override
	public boolean delKey(String key) {
		try {
			Boolean aBoolean = redisTemplate.delete(redisAutoKey.appendKey(key));
			if (aBoolean == null) {
				return false;
			}
			return aBoolean;
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
	}

	@Override
	public Long delKeys(String... keys) {
		try {
			return redisTemplate.delete(redisAutoKey.appendKeys(keys));
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
    	}
	}

	@Override
	public Long delKeys(List<String> keys) {
		try {
    		return redisTemplate.delete(redisAutoKey.appendKeys(keys));
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
	}

	@Override
	public Set<String> scan(String pattern, long limit) {
		try {
    		return redisTemplate.execute((RedisCallback<Set<String>>) connection -> {
    			Set<String> keys = new HashSet<>();
    			Cursor<byte[]> cursor = null;
    			try {
    				cursor = connection.scan(ScanOptions.scanOptions().match(redisAutoKey.appendKey(pattern)).count(limit).build());
    				while (cursor.hasNext()) {
    					keys.add(new String(cursor.next()));
    				}
    			} catch (Exception e) {
    				throw new RedisException(StringUtils.format("scanCursor error, {}", e.getMessage()) , e);
    			}  finally {
    				IOUtils.close(cursor);
    			}
				return keys;
    		});
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
	}
	
	@Override
	public String type(String key) {
		try {
			return redisTemplate.execute((RedisCallback<String>) connection -> connection.type(redisAutoKey.appendKey(key).getBytes()).name());
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
	}
	
	/**	
	 * 	设置RedisTemplate
	 * 	
	 * 	@author Pan
	 * 	@param 	redisTemplate	RedisTemplate
	 */
	public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
		this.redisTemplate = redisTemplate;
	}
	
	/**
	 * 	获取Redis类型名称
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */	
	public String getRedisTypeName() {
		String simpleName = ClassUtils.getSimpleName(this);
		return simpleName == null ? "unknown" : simpleName;
	}
	
	/**
	 * 	用于装配配置
	 * 
	 * 	@author Pan
	 */
	public void executorConfigure() {
		//	空实现
	}
}
