package com.gitee.apanlh.util.cache.redis;

import com.gitee.apanlh.util.base.CollUtils;
import com.gitee.apanlh.util.base.IteratorUtils;
import com.gitee.apanlh.util.base.MapUtils;
import com.gitee.apanlh.util.base.StringUtils;
import com.gitee.apanlh.util.cache.redis.api.AbstractRedis;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**	
 * 	自动填充Key值
 * 
 * 	@author Pan
 */
public class RedisAutoAppendKey {
	
	/** RedisKey对象 */
	private RedisKeyBuilder<? extends AbstractRedis> redisKey;
	
	/**	
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 * 	@param 	redisKey	RedisKey对象
	 */
	public RedisAutoAppendKey(RedisKeyBuilder<? extends AbstractRedis> redisKey) {
		this.redisKey = redisKey;
	}
	
	/**	
	 * 	验证是否是RedisKey构造
	 * 	<br>保留原有方法
	 * 	
	 * 	@author Pan
	 * 	@return boolean
	 */
	public boolean isRedisKey() {
		return this.redisKey != null || getKey() != null;
	}
	
	/**	
	 * 	添加子类RedisKey的内容
	 * 	<br> a:b: 这是RedisKey中的Keys代表多层级,a:b:这里是Key值
	 * 	
	 * 	@author Pan
	 * 	@param 	key	例如a:b:这里是Key值
	 * 	@return String
	 */
	public String appendKey(String key) {
		if (isRedisKey()) {
			return StringUtils.append(getKey(), key, getKey().length());
		}
		return key;
	}
	
	/**	
	 * 	添加子类RedisKey的内容Map类型
	 * 	
	 * 	@author Pan
	 * 	@param 	keys	键值对
	 * 	@return Map
	 */
	public Map<String, Object> appendKeys(Map<String, Object> keys) {
		if (isRedisKey()) {
			return MapUtils.newHashMap(newMap -> IteratorUtils.entrySet(newMap, (k, v) -> newMap.put(appendKey(k), v)));
		}
		return keys;
	}
	
	/**	
	 * 	添加子类RedisKey的内容集合类型
	 * 	
	 * 	@author Pan
	 * 	@param 	keys	集合键
	 * 	@return List
	 */
	public List<String> appendKeys(List<String> keys) {
		if (isRedisKey()) {
			return CollUtils.newArrayList(newList -> IteratorUtils.array(keys, t -> newList.add(appendKey(t))));
		}
		return keys;
	}
	
	/**	
	 * 	添加子类RedisKey的内容数组类型
	 * 	
	 * 	@author Pan
	 * 	@param 	keys	数组键
	 * 	@return List
	 */
	public List<String> appendKeys(String... keys) {
		return appendKeys(Arrays.asList(keys));
	}
	
	/**
	 * 	获取最终构造的键
	 * 	
	 * 	@author Pan
	 * 	@return String
	 */
	public String getKey() {
		return redisKey.getKey();
	}
}
