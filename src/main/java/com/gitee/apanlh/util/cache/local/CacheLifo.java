package com.gitee.apanlh.util.cache.local;

import com.gitee.apanlh.util.base.CollUtils;
import com.gitee.apanlh.util.base.Empty;
import com.gitee.apanlh.util.base.ObjectUtils;
import com.gitee.apanlh.util.thread.LockExecutor;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;

/**
 * 	LIFO缓存(栈)
 * 	
 * 	@author Pan
 */
public class CacheLifo<E> extends LockExecutor {
	
	private Deque<E> stack;
	/** 最大容量 */
	private int capacity;
	/** 是否限制栈容量 */
	private boolean limitless;
	
	/**
	 * 	构造函数
	 * 	<br>默认无限容量
	 * 	
	 * 	@author Pan
	 */
	public CacheLifo() {
		this(0, false);
	}
	
	/**
	 * 	构造函数
	 * 	<br>自定义容量限制
	 * 	
	 * 	@author Pan
	 * 	@param 	capacity	容量
	 */
	public CacheLifo(int capacity) {
		this(capacity, true);
	}
	
	/**	
	 * 	构造函数
	 * 	<br>包含边界及容量限制
	 * 	
	 * 	@author Pan
	 * 	@param 	capacity		最大容量
	 * 	@param 	limitless		true为边界限制
	 */
	private CacheLifo(int capacity, boolean limitless) {
		super();
		this.stack = new ArrayDeque<>();
		this.capacity = capacity;
		this.limitless = limitless;
	}
	
	/**
	 * 	压栈
	 *
	 * 	@author Pan
	 * 	@param 	e	元素
	 * 	@return E
	 * 	@throws IndexOutOfBoundsException 如果超出最大容量则抛出
	 */
	public E push(E e) {
		if (e == null) {
			return e;
		}
		
		return lock(() -> {
			if (this.limitless && isFull()) {
				throw new IndexOutOfBoundsException("stack is full");
			}
			this.stack.push(e);
			return e;
		});
	}
	
	/**
	 * 	返回栈顶元素不出栈
	 * 	
	 * 	@author Pan
	 * 	@return E
	 */
	public E peek() {
		return lock(() -> isEmpty() ? null : this.stack.peek());
	}
	
	/**
	 * 	出栈
	 * 	
	 * 	@author Pan
	 * 	@return	E
	 */
	public E pop() {
		return lock(() -> isEmpty() ? null : this.stack.pop());
	}
	
	/**
	 * 	栈中所有元素出栈
	 * 	
	 * 	@author Pan
	 * 	@return	List
	 */
	public List<E> popAll() {
		return lock(() -> {
			if (isEmpty()) {
				return Empty.list();
			}
			return CollUtils.newArrayList(list -> {
				for (int i = 0, len = size(); i < len; i++) {
					list.add(this.stack.pop());
				}
			});
		});
	}
	
	/**	
	 * 	获取栈中的某个值
	 * 	
	 * 	@author Pan
	 * 	@param 	e	元素
	 * 	@return	E
	 */
	public E get(E e) {
		if (e == null) {
			return null;
		}
		
		return lock(() -> {
			if (isEmpty()) {
				return null;
			}
			
			Iterator<E> iterator = this.stack.iterator();
			while (iterator.hasNext()) {
				E e2 = iterator.next();
				if (ObjectUtils.eq(e2, e)) {
					return e2;
				}
			}
			return null;
		});
	}
	
	/**	
	 * 	获取栈底
	 * 	
	 * 	@author Pan
	 * 	@return	E
	 */
	public E getFirstElement() {
		return lock(() -> isEmpty() ? null : this.stack.getFirst());
	}
	
	/**	
	 * 	获取栈顶
	 * 	
	 * 	@author Pan
	 * 	@return	E
	 */
	public E getLastElement() {
		return lock(() -> isEmpty() ? null : this.stack.getLast());
	}
	
	/**	
	 * 	获取栈长度
	 * 	
	 * 	@author Pan
	 * 	@return	int
	 */
	public int size() {
		return this.stack.size();
	}
	
	/**
	 * 	移除某个元素
	 * 	
	 * 	@author Pan
	 * 	@param 	e	元素
	 * 	@return	boolean
	 */
	public boolean remove(E e) {
		if (e == null || isEmpty()) {
			return false;
		}
		return lock(() -> this.stack.remove(e));
	}
	
	/**	
	 * 	栈是否为空
	 * 	
	 * 	@author Pan
	 * 	@return	boolean
	 */
	public boolean isEmpty() {
		return this.stack.isEmpty();
	}
	
	/**
	 * 	当前栈是否被占满
	 * 	
	 * 	@author Pan
	 * 	@return	boolean
	 */
	public boolean isFull() {
		return this.stack.size() == this.capacity;
	}
	
	@Override
	public String toString() {
		return this.stack.toString();
	}
}
