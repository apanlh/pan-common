package com.gitee.apanlh.util.cache.redis;

/**
 * 	Redis数据类型(支持5.0以上)
 * 	@author Pan
 */
public enum RedisDataType {
	
	STRING,				/** 字符串 **/
	HASH,				/** hash **/
	LIST,				/** 链表 **/
	SET,				/** 集合 **/
	ZSET,				/** 有序集合	sorted set **/
	HYPER_LOG_LOG,		/** 特殊结构 **/
	GEO,				/** 经纬度 **/
	BIGMAP,				/** 位图 **/
	PUB,				/** 发布(发送消息) **/
	SUB,				/** 订阅(接收消息) **/
	SCRIPT,				/** 脚本 **/
	STREAM;				/** 流(Redis5.0以上兼容) **/
	
	RedisDataType() {
		
	}
}
