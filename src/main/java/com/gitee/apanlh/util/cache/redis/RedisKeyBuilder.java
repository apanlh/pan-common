package com.gitee.apanlh.util.cache.redis;

import com.gitee.apanlh.util.cache.redis.api.AbstractRedis;
import com.gitee.apanlh.util.cache.redis.api.RedisBigMap;
import com.gitee.apanlh.util.cache.redis.api.RedisGeo;
import com.gitee.apanlh.util.cache.redis.api.RedisHash;
import com.gitee.apanlh.util.cache.redis.api.RedisHyperLogLog;
import com.gitee.apanlh.util.cache.redis.api.RedisList;
import com.gitee.apanlh.util.cache.redis.api.RedisPubSub;
import com.gitee.apanlh.util.cache.redis.api.RedisScript;
import com.gitee.apanlh.util.cache.redis.api.RedisSet;
import com.gitee.apanlh.util.cache.redis.api.RedisStream;
import com.gitee.apanlh.util.cache.redis.api.RedisString;
import com.gitee.apanlh.util.cache.redis.api.RedisZset;
import com.gitee.apanlh.util.valid.Assert;

import java.util.List;

/**	
 * 	RedisKey构建
 * 
 * 	@author Pan
 *  @param  <V>     值类型
 */
public class RedisKeyBuilder<V extends AbstractRedis> {
	
	/** 是否加载成功标识 */
	private boolean 				initStatus;
	/** Redis	层级Keys */
	private String 					key;
	private StringBuilder 			keyBuilder;
	/** 存放RedisData */
	private AbstractRedis redisData;
	/** Redis键配置 */
	private RedisKeyConfig 			keyConfig;
	
	/**
	 * 	默认-构造函数
	 * 
	 * 	@author Pan
	 */
	private RedisKeyBuilder() {
		this.keyBuilder = new StringBuilder(16);
		this.keyConfig = new RedisKeyConfig();
	}
	
	/**	
	 * 	构建-延迟执行加载Redis
	 * 	
	 * 	@author Pan
	 * 	@param 	redisData	Redis数据对象
	 * 	@return AbstractRedis
	 */
	private AbstractRedis build(AbstractRedis redisData) {
		return this.build(redisData, true);
	}
	
	/**	
	 * 	构建-加载Redis
	 * 	<br>自定义初始化
	 * 	
	 * 	@author Pan
	 * 	@param 	redisData	Redis数据对象
	 * 	@param 	hasInit		true延迟化加载
	 * 	@return	AbstractRedis
	 */
	private AbstractRedis build(AbstractRedis redisData, boolean hasInit) {
		withSetKey();
		
		//	如果在建造在方法体内则不加载RedisTemplate
		if (!getInitStatus() && hasInit) {
			RedisKeyLoad.load(this);
		}
		this.redisData = redisData;
		return this.redisData;
	}
	
	/**	
	 * 	构建String类型
	 * 	<br>默认:符号风格
	 * 
	 * 	@author Pan
	 * 	@return RedisString
	 */
	public RedisString buildString() {
		return (RedisString) build(new RedisString(withAutoAppendKey()));
	}
	
	/**	
	 * 	构建Hash类型
	 * 	<br>默认:符号风格
	 * 
	 * 	@author Pan
	 * 	@return RedisHash
	 */
	public RedisHash buildHash() {
		return (RedisHash) build(new RedisHash(withAutoAppendKey()));
	}
	
	/**	
	 * 	构建ZSet类型
	 * 	<br>默认:符号风格
	 * 
	 * 	@author Pan
	 * 	@return RedisZset
	 */
	public RedisZset buildZset() {
		return (RedisZset) build(new RedisZset(withAutoAppendKey()));
	}
	
	/**	
	 * 	构建BigMap类型
	 * 	<br>默认:符号风格
	 * 
	 * 	@author Pan
	 * 	@return RedisBigMap
	 */
	public RedisBigMap buildBigMap() {
		return (RedisBigMap) build(new RedisBigMap(withAutoAppendKey()));
	}
	
	/**	
	 * 	构建Geo类型
	 * 	<br>默认:符号风格
	 * 
	 * 	@author Pan
	 * 	@return RedisGeo
	 */
	public RedisGeo buildGeo() {
		return (RedisGeo) build(new RedisGeo(withAutoAppendKey()));
	}
	
	/**	
	 * 	构建HyperLogLog类型
	 * 	<br>默认:符号风格
	 * 
	 * 	@author Pan
	 * 	@return RedisHyperLogLog
	 */
	public RedisHyperLogLog buildHyperLogLog() {
		return (RedisHyperLogLog) build(new RedisHyperLogLog(withAutoAppendKey()));
	}
	
	/**	
	 * 	构建List类型
	 * 	<br>默认:符号风格
	 * 
	 * 	@author Pan
	 * 	@return RedisList
	 */
	public RedisList buildList() {
		return (RedisList) build(new RedisList(withAutoAppendKey()));
	}
	
	/**	
	 * 	构建PubSub类型
	 * 	<br>默认:符号风格
	 * 
	 * 	@author Pan
	 * 	@return RedisPubSub
	 */
	public RedisPubSub buildPubSub() {
		return (RedisPubSub) build(new RedisPubSub(withAutoAppendKey()));
	}
	
	/**	
	 * 	构建Script类型
	 * 	<br>默认:符号风格
	 * 
	 * 	@author Pan
	 * 	@return RedisScript
	 */
	public RedisScript buildScript() {
		return (RedisScript) build(new RedisScript(withAutoAppendKey()));
	}
	
	/**	
	 * 	构建Set类型
	 * 	<br>默认:符号风格
	 * 
	 * 	@author Pan
	 * 	@return RedisSet
	 */
	public RedisSet buildSet() {
		return (RedisSet) build(new RedisSet(withAutoAppendKey()));
	}
	
	/**	
	 * 	构建Stream类型
	 * 	<br>默认:符号风格
	 * 
	 * 	@author Pan
	 * 	@return RedisStream
	 */
	public RedisStream buildStream() {
		return (RedisStream) build(new RedisStream(withAutoAppendKey()));
	}
	
	/**	
	 * 	填充-自动填充Key值
	 * 	
	 * 	@author Pan
	 * 	@return RedisAutoAppendKey
	 */
	private RedisAutoAppendKey withAutoAppendKey() {
		return new RedisAutoAppendKey(this);
	}
	
	/**	
	 * 	添加自定义分隔符
	 * 	<br>默认规则[:]符号
	 * 	
	 * 	@author Pan
	 * 	@param 	keyConfig	RedisKey配置
	 * 	@return RedisKeyBuilder
	 */
	public RedisKeyBuilder<V> withKeyConfig(RedisKeyConfig keyConfig) {
		this.keyConfig = keyConfig;
		return this;
	}
	
	/**	
	 * 	添加内容
	 * 	
	 * 	@author Pan
	 * 	@param 	key	键
	 * 	@return RedisKeyBuilder
	 */
	private RedisKeyBuilder<V> withKey(String key) {
		this.keyBuilder.append(key);
		return this;
	}
	
	/**
	 * 	添加键
	 * 
	 * 	@author Pan
	 * 	@param 	keys	一个或多个键
	 * 	@return RedisKeyBuilder
	 */
	public RedisKeyBuilder<V> withKeys(String... keys) {
		Assert.isNotEmpty(keys);
		
		for (int i = 0, len = keys.length; i < len; i++) {
			withKey(keys[i]);
			withKey(this.keyConfig.getSeparator());
		}
		return this;
	}
	
	/**
	 * 	添加键
	 * 
	 * 	@author Pan
	 * 	@param 	keys	键
	 * 	@return RedisKeyBuilder
	 */
	public RedisKeyBuilder<V> withKeys(List<String> keys) {
		Assert.isNotNull(keys);
		
		for (int i = 0, len = keys.size(); i < len; i++) {
			withKey(keys.get(i));
			withKey(this.keyConfig.getSeparator());
		}
		return this;
	}
	
	/**
	 * 	构建时设置最终键值
	 * 	
	 * 	@author Pan
	 */
	private void withSetKey() {
		this.key = this.keyBuilder.toString();
	}
	
	/**	
	 * 	获取key
	 * 	
	 * 	@author Pan
	 * 	@return String
	 */
	public String getKey() {
		return this.key;
	}
	
	
	/**	
	 * 	是否初始化完成
	 * 	
	 * 	@author Pan
	 * 	@return boolean
	 */
	public boolean getInitStatus() {
		return initStatus;
	}

	/**	
	 * 	设置初始化值
	 * 	
	 * 	@author Pan
	 * 	@param 	initStatus 初始化状态
	 */
	public void setInitStatus(boolean initStatus) {
		this.initStatus = initStatus;
	}

	/**	
	 * 	获取Redis数据类型
	 * 	
	 * 	@author Pan
	 * 	@return AbstractRedis
	 */
	public AbstractRedis getRedisData() {
		return redisData;
	}

	/**	
	 * 	设置Redis数据类型
	 * 	
	 * 	@author Pan
	 * 	@param 	redisData Redis数据类型
	 */
	public void setRedisData(AbstractRedis redisData) {
		this.redisData = redisData;
	}

	/**	
	 * 	构造
	 * 	
	 * 	@author Pan
	 * 	@param  <V>     值类型
	 * 	@return RedisKeyBuilder
	 */
	public static <V extends AbstractRedis> RedisKeyBuilder<V> builder() {
		return new RedisKeyBuilder<>();
	}
}
