package com.gitee.apanlh.util.cache.local;

import com.gitee.apanlh.util.func.FuncMapExecute;

import java.util.Map;

/**	
 * 	整合Cache类
 * 	<br>其中包含基础cache、弱引用、FIFO、LFU、LIFO、LRU、LRU-K、LRK2等缓存
 * 	
 * 	@author Pan
 */
public class CacheUtils {
	
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private CacheUtils() {
		//	不允许外部实例
		super();
	}
	
	/**	
	 * 	默认WeakMap类型缓存
	 * 	<br>默认初始化长度64
	 * 	
	 * 	@author Pan
	 * 	@param 	<K>	键
	 * 	@param 	<V>	值
	 * 	@return	Cache
	 */
	public static <K, V> Cache<K, V> cache() {
		return new Cache<>();
	}
	
	/**
	 * 	默认WeakMap类型缓存
	 * 		
	 * 	@author Pan
	 * 	@param 	<K>	键
	 * 	@param 	<V>	值
	 * 	@param 	initCapacity 初始化长度
	 * 	@return	Cache
	 */
	public static <K, V> Cache<K, V> cache(int initCapacity) {
		return new Cache<>(initCapacity);
	}
	
	/**
	 * 	默认WeakMap类型缓存
	 * 		
	 * 	@author Pan
	 * 	@param 	<K>	键
	 * 	@param 	<V>	值
	 * 	@param 	funcMapExecute 初始化函数
	 * 	@return	Cache
	 */
	public static <K, V> Cache<K, V> cache(FuncMapExecute<K, V> funcMapExecute) {
		return new Cache<>(funcMapExecute);
	}
	
	/**		
	 * 	自定义缓存
	 * 	<br>默认内部方法实现数据一致性及锁
	 * 	
	 * 	@author Pan
	 * 	@param 	<K>	键
	 * 	@param 	<V>	值
	 * 	@param 	map		map
	 * 	@return	Cache
	 */
	public static <K, V> Cache<K, V> cache(Map<K, V> map) {
		return new Cache<>(map);
	}
	
	/**
	 * 	FIFO类型缓存
	 * 	<br>默认256最大容量
	 * 		
	 * 	@author Pan
	 * 	@param 	<K>	键
	 * 	@param 	<V>	值
	 * 	@return	CacheFifo
	 */
	public static <K, V> CacheFifo<K, V> fifo() {
		return new CacheFifo<>();
	}
	
	/**
	 * 	FIFO类型缓存
	 * 	<br>自定义最大容量
	 * 		
	 * 	@author Pan
	 * 	@param 	<K>	键
	 * 	@param 	<V>	值
	 * 	@param 	capacity 最大容量
	 * 	@return	CacheFifo
	 */
	public static <K, V> CacheFifo<K, V> fifo(int capacity) {
		return new CacheFifo<>(capacity);
	}
	
	/**
	 * 	LIFO类型缓存
	 * 	<br>默认无限容量
	 * 		
	 * 	@author Pan
	 * 	@param 	<E>	元素
	 * 	@return	CacheLifo
	 */
	public static <E> CacheLifo<E> lifo() {
		return new CacheLifo<>();
	}
	
	/**
	 * 	LIFO类型缓存
	 * 	<br>自定义最大容量
	 * 		
	 * 	@author Pan
	 * 	@param 	<E>	元素
	 * 	@param 	capacity 最大容量
	 * 	@return	CacheLifo
	 */
	public static <E> CacheLifo<E> lifo(int capacity) {
		return new CacheLifo<>(capacity);
	}
	
	/**
	 * 	LFU类型缓存
	 * 	<br>默认256最大容量
	 * 		
	 * 	@author Pan
	 * 	@param 	<K>	键
	 * 	@param 	<V>	值
	 * 	@return	CacheLfu
	 */
	public static <K, V> CacheLfu<K, V> lfu() {
		return new CacheLfu<>();
	}
	
	/**
	 * 	LFU类型缓存
	 * 	<br>自定义最大容量
	 * 		
	 * 	@author Pan
	 * 	@param 	<K>	键
	 * 	@param 	<V>	值
	 * 	@param 	capacity 最大容量
	 * 	@return	CacheLfu
	 */
	public static <K, V> CacheLfu<K, V> lfu(int capacity) {
		return new CacheLfu<>(capacity);
	}
	
	/**
	 * 	LRU类型缓存
	 * 	<br>默认256最大容量
	 * 		
	 * 	@author Pan
	 * 	@param 	<K>	键
	 * 	@param 	<V>	值
	 * 	@return	CacheLru
	 */
	public static <K, V> CacheLru<K, V> lru() {
		return new CacheLru<>();
	}
	
	/**
	 * 	LRU类型缓存
	 * 	<br>自定义最大容量
	 * 		
	 * 	@author Pan
	 * 	@param 	<K>	键
	 * 	@param 	<V>	值
	 * 	@param 	capacity 最大容量
	 * 	@return	CacheFifo
	 */
	public static <K, V> CacheLru<K, V> lru(int capacity) {
		return new CacheLru<>(capacity);
	}
	
	/**
	 * 	LRU-K类型缓存
	 * 	<br>默认历史访问列表最大256容量 
	 *	<br>默认LRU缓存列表最大128容量 
	 *	<br>默认K值为2次
	 * 		
	 * 	@author Pan
	 * 	@param 	<K>	键
	 * 	@param 	<V>	值
	 * 	@return	CacheLru
	 */
	public static <K, V> CacheLruK<K, V> lruk() {
		return new CacheLruK<>();
	}
	
	/**
	 * 	LRU-K类型缓存
	 * 	<br>自定义最大容量 
	 * 	<br>默认K值2次
	 * 		
	 * 	@author Pan
	 * 	@param 	<K>	键
	 * 	@param 	<V>	值
	 * 	@param 	capacity	最大容量
	 * 	@return	CacheFifo
	 */
	public static <K, V> CacheLruK<K, V> lruk(int capacity) {
		return new CacheLruK<>(capacity, capacity);
	}
	
	/**
	 * 	LRU-K类型缓存
	 * 	<br>自定义历史访问列表最大容量 
	 *	<br>自定义LRU缓存列表最大容量 
	 *	<br>自定义K值
	 * 		
	 * 	@author Pan
	 * 	@param 	<K>	键
	 * 	@param 	<V>	值
	 * 	@param 	historyCapacity 历史访问最大容量
	 * 	@param 	lruCapacity 	LRU最大容量
	 * 	@param 	k			 	K值
	 * 	@return	CacheFifo
	 */
	public static <K, V> CacheLruK<K, V> lruk(int historyCapacity, int lruCapacity, int k) {
		return new CacheLruK<>(historyCapacity, lruCapacity, k);
	}
	
	/**
	 * 	LRU-2类型缓存
	 * <br>默认256最大容量
	 * 		
	 * 	@author Pan
	 * 	@param 	<K>	键
	 * 	@param 	<V>	值
	 * 	@return	CacheLru
	 */
	public static <K, V> CacheLruTwoQ<K, V> lru2() {
		return new CacheLruTwoQ<>();
	}
	
	/**
	 * 	LRU-2类型缓存
	 * 	<br>自定义最大容量
	 * 		
	 * 	@author Pan
	 * 	@param 	<K>	键
	 * 	@param 	<V>	值
	 * 	@param 	capacity	最大容量
	 * 	@return	CacheFifo
	 */
	public static <K, V> CacheLruTwoQ<K, V> lru2(int capacity) {
		return new CacheLruTwoQ<>(capacity, capacity);
	}
	
	/**
	 * 	LRU-2类型缓存
	 * 	<br>a1最大容量 
	 *	<br>a2最大容量 
	 * 		
	 * 	@author Pan
	 * 	@param 	<K>	键
	 * 	@param 	<V>	值
	 * 	@param 	a1Capacity  历史访问最大容量
	 * 	@param 	a2Capacity 	LRU最大容量
	 * 	@return	CacheFifo
	 */
	public static <K, V> CacheLruTwoQ<K, V> lru2(int a1Capacity, int a2Capacity) {
		return new CacheLruTwoQ<>(a1Capacity, a2Capacity);
	}
}
