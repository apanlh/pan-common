package com.gitee.apanlh.util.cache.local.api;

import com.gitee.apanlh.util.func.FuncExecute;

/**	
 * 	自动清理缓存接口
 * 	
 * 	@author Pan
 */
@FunctionalInterface
public interface CacheAutoClear {
	
	/**	
	 * 	返回一个接口类型，用于执行清理策略
	 * 	
	 * 	@author Pan
	 * 	@return	FuncExecute
	 */
	FuncExecute autoClearCache();
}
