package com.gitee.apanlh.util.cache.redis;

import com.gitee.apanlh.exp.LoadErrorException;
import com.gitee.apanlh.util.base.IteratorUtils;
import com.gitee.apanlh.util.base.StringUtils;
import com.gitee.apanlh.util.cache.redis.api.AbstractRedis;
import com.gitee.apanlh.util.log.Log;
import com.gitee.apanlh.util.valid.ValidParam;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 	Redis加载初始化任务
 * 	#mark 增加Redis检验机制
 * 	
 * 	@author Pan
 */
public class RedisLoadTask {
	
	/** 初始化等待次数 */
	private static int initWaitCount = 0;
	/** 初始化次数验证---是否初始化完成标识 */
	private static int initCount = 0;
	/** 初始化次数最大次数*/
	private static int initMaxCount = 10;
	
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private RedisLoadTask() {
		//	不允许外部实例
		super();
	}
	
	/**
	 * 	初始化任务
	 *
	 * 	@author Pan
	 */
	static void createInitBeansTask() {
		Timer timer = new Timer("redisBean");
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				loadBean(timer);
			}
		}, 0L, 1000L);
	}
	
	/**	
	 * 	加载Bean
	 * 	
	 * 	@author Pan
	 * 	@param 	timer	定时器对象
	 */
	private static void loadBean(Timer timer) {
		RedisTemplate<String, Object> redisTemplateBean = RedisKeyLoad.getRedisTemplateBean();
		Map<String, RedisKeyBuilder<? extends AbstractRedis>> initLoadBeans = RedisKeyLoad.getInitLoadBeans();

		if (initCount > initMaxCount) {
			//	二次检查
			if (ValidParam.isEmpty(initLoadBeans)) {
				clearInitBeans(timer);
				return ;
			}

			timer.cancel();
			timer.purge();
			throw new LoadErrorException("please check redis cause : redis init fail or redisTemplate is null");
		}

		if (ValidParam.isNotNull(redisTemplateBean) && ValidParam.isNotEmpty(initLoadBeans)) {
			IteratorUtils.entrySet(initLoadBeans, (stackClassName, redisKey, iterator) -> {
				if (!redisKey.getInitStatus()) {
					//	初始化
					AbstractRedis redisData = redisKey.getRedisData();
					redisKey.setInitStatus(true);
					redisData.setRedisTemplate(redisTemplateBean);
					redisData.executorConfigure();

					//	保存RedisKey对象
					RedisKeyLoad.putLoadBean(redisKey);

					iterator.remove();
					String typeName = StringUtils.removePrefix(redisData.getRedisTypeName(), "Redis");
					Log.get().info("{}--------type[{}]--------bean[{}]", stackClassName, typeName, redisData.getRedisTemplate());
				}
			});

			//	等待后续加载
			initWaitCount++;
			if (initWaitCount == 3 && initLoadBeans.size() == 0) {
				clearInitBeans(timer);
			}
		}
		initCount ++;
	}

	/**
	 *  清理初始化BeanMap，并停止定时器
	 *
	 * 	@author Pan
	 *  @param  timer 定时器
	 */
	private static void clearInitBeans(Timer timer) {
		RedisKeyLoad.clearInitLoadBeans();
		timer.cancel();
		timer.purge();
		Log.get().info("redisBean init suc");
	}
}
