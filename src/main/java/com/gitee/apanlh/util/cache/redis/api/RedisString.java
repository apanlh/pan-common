package com.gitee.apanlh.util.cache.redis.api;

import com.gitee.apanlh.exp.RedisException;
import com.gitee.apanlh.util.cache.redis.RedisAutoAppendKey;
import org.springframework.data.redis.core.ValueOperations;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**	
 * 	String结构
 * 
 * 	@author Pan
 */
public class RedisString extends AbstractRedis {
	
	private ValueOperations<String, Object> executor;
	
	/**	
	 * 	构造函数-基于抽象Redis数据
	 * 	
	 * 	@author Pan
	 * 	@param 	redisAutoKey		自动装配Key
	 */
	public RedisString(RedisAutoAppendKey redisAutoKey) {
		super(redisAutoKey);
	}
	
	/**
     * 	将值放入缓存
     * 
     * 	@author Pan
     * 	@param 	<V>			数据类型
     * 	@param  key   		键
     * 	@param  value 		值
     * 	@return boolean		true成功	false失败
     */
    public <V> boolean set(String key, V value) {
    	try {
    		executor.set(getRedisAutoKey().appendKey(key), value);
    		return true;
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
    }
    
    /**	
     * 	将值放入缓存  并设置失效期 单位（秒）
     * 
     * 	@author Pan
     * 	@param 	<V>			数据类型
     * 	@param  key   		键
     * 	@param  value 		值
     * 	@param  time 		秒
     * 	@return boolean		true成功	false失败
     */
    public <V> boolean setForSeconds(String key, V value, long time) {
    	try {
    		executor.set(getRedisAutoKey().appendKey(key), value, time ,TimeUnit.SECONDS);
    		return true;
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
    }
    
    /**
     * 	将值放入缓存  并设置失效期 单位（分钟）
     *
     * 	@author Pan
     * 	@param  key   		键
     * 	@param  value 		值
     * 	@param  time 		分钟
     * 	@return boolean		true成功	false失败
     */
    public boolean setForMinutes(String key, Object value, long time) {
    	try {
    		executor.set(getRedisAutoKey().appendKey(key), value, time, TimeUnit.MINUTES);
    		return true;
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
    }
    
    /**
     * 	将值放入缓存  并设置失效期 单位（小时）
     *
     * 	@author Pan
     * 	@param  key   		键
     * 	@param  value 		值
     * 	@param  time 		小时
     * 	@return boolean		true成功	false失败
     */
    public boolean setForHours(String key, Object value, long time) {
    	try {
    		executor.set(getRedisAutoKey().appendKey(key), value, time, TimeUnit.HOURS);
    		return true;
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
    }
    
    /**
     * 	将值放入缓存  并设置失效期 单位（天）
     *
     * 	@author Pan
     * 	@param  key   		键
     * 	@param  value 		值
     * 	@param  time 		天
     * 	@return boolean		true成功	false失败
     */
    public boolean setForDay(String key, Object value, long time) {
    	try {
    		executor.set(getRedisAutoKey().appendKey(key), value, time, TimeUnit.DAYS);
    		return true;
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
    }
    
    /**
     * 	批量set
     * 
     * 	@author Pan
     * 	@param  keys		键值对
     * 	@return boolean		true成功	false失败
     */
	public boolean setKeys(Map<String, Object> keys) {
    	try {
    		executor.multiSet(getRedisAutoKey().appendKeys(keys));
			return true;
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
    }
	
    /**
     * 	如果key不存在则新增,存在则不改变已经有的值。
     * 
     * 	@author Pan
     * 	@param  key			键
     * 	@param  value		值
     * 	@return boolean		true成功	false失败
     */
    public boolean setIfAbsent(String key, Object value) {
    	try {
    		Boolean setIfAbsent = executor.setIfAbsent(getRedisAutoKey().appendKey(key), value);
    		if (setIfAbsent == null) {
    			return false;
    		}
    		return setIfAbsent;
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
    }
    
    /**
     *	批量 setIfAbsents(key不存在则新增,存在则不改变已经有的值)。
     *
     * 	@author Pan
     * 	@param  keys		键
     * 	@return boolean		true成功	false失败
     */
    public boolean setIfAbsents(Map<String, Object> keys) {
    	try {
    		Boolean multiSetIfAbsent = executor.multiSetIfAbsent(getRedisAutoKey().appendKeys(keys));
    		if (multiSetIfAbsent == null) {
    			return false;
    		}
    		return multiSetIfAbsent;
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
    }
    
	/**
     * 	根据key获取值
     * 
     * 	@author Pan
     * 	@param  key		键
     * 	@return Object
     */
    public Object get(String key) {
    	try {
    		return executor.get(getRedisAutoKey().appendKey(key));
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
	}
    
    /**
     * 	根据key获取值
     * 	<br>如果有存放{@code @type} 则返回自定义实体类
     * 
     * 	@author Pan
     * 	@param 	<T>		数据类型
     * 	@param  key		键
     * 	@param  clazz	返回的Class
     * 	@return T
     */
    @SuppressWarnings("unchecked")
	public <T> T get(String key, Class<T> clazz) {
    	try {
    		return (T) executor.get(getRedisAutoKey().appendKey(key));
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
    }
    
    /**
     * 	查找多个key 返回对象
     * 
     * 	@author Pan
     * 	@param  keys	List键
     * 	@return	List
     */
	public List<Object> getKeys(List<String> keys) {
    	try {
    		return executor.multiGet(getRedisAutoKey().appendKeys(keys));
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
    }
    
    /**
     * 	查找多个key 返回对象
     * 
     * 	@author Pan
	 * 	@param  <T>      数据类型
     * 	@param  keys	 List键
     * 	@param 	clazz	 返回类型
     * 	@return	List
     */
    @SuppressWarnings("unchecked")
	public <T> List<T> getKeys(List<String> keys, Class<T> clazz) {
    	try {
    		return (List<T>) executor.multiGet(getRedisAutoKey().appendKeys(keys));
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
    }
    
    /**
     * 	获取一个key里的字符串长度
     * 
     * 	@author Pan
     * 	@param  key		键
     * 	@return Long 	返回长度	
     */
	public Long size(String key) {
		try {
			return executor.size(getRedisAutoKey().appendKey(key));
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
	}
	
    /**	
     * 	计数（整型）
     * 	<br>目前的key的value值减少1，如果此key不存在先创建 初始化为0并且数值减去1 此key即为-1
     * 	<br>返回当前的数值
     * 
     * 	@author Pan
     * 	@param  key	 键
     * 	@return	Long 返回值
     */
    public Long decrement(String key) {
    	try {
    		return executor.decrement(getRedisAutoKey().appendKey(key));
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
    }
    
    /**	
     * 	计数（整型） 选择数值减少的范围
     * 	<br>目前的key的value值减少1，如果此key不存在先创建 初始化为0并且数值减去1 此key即为-1
     * 
     * 	@author Pan
     * 	@param  key		键
     * 	@param  delta	整性范围	
     * 	@return	Long 	返回redis中的值
     */	
    public Long decrement(String key, long delta) {
    	try {
    		return executor.decrement(getRedisAutoKey().appendKey(key), delta);
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
    }
    
    /**	
     * 	此key的value 数值+1，
     * 	<br>此key的value只能存整型否则报错 
     * 	<br>如果不存在此key会先创建 默认先初始化0然后+1
     * 
     * 	@author Pan
     * 	@param  key			键
     * 	@return	Long 		返回redis中的值
     */
    public Long increment(String key) {
    	try {
			return executor.increment(getRedisAutoKey().appendKey(key));
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
    }
    
    /**	
     * 	计数（整型） 选择数值增加的范围
     * 	<br>此key的value只能存整型否则报错 如果不存在此key会先创建 
     * 
     * 	@author Pan
     * 	@param  key		键
     * 	@param  delta	范围
     * 	@return Long 返回redis中的值
     */
    public Long increment(String key, long delta) {
    	try {
    		return executor.increment(getRedisAutoKey().appendKey(key), delta);
		} catch (Exception e) {
			throw new RedisException(e.getMessage(), e);
		}
    }
    
    /**	
     * 	计数 （小数）
     * 	<br>此key的value只能存小数 
     * 	<br>如果不存在此key会先创建
     * 	<br>返回值为当前的数值
     * 
     * 	@author Pan
     * 	@param  key		键
     * 	@param  delta	范围
     * 	@return Double 	返回redis中的值
     */
    public Double increment(String key, double delta) {
    	try {
    		return executor.increment(getRedisAutoKey().appendKey(key), delta);
    	} catch (Exception e) {
    		throw new RedisException(e.getMessage(), e);
    	}
    }
    
    @Override
    public void executorConfigure() {
    	this.executor = getRedisTemplate().opsForValue();
    }
}
