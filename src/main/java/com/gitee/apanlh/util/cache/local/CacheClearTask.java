package com.gitee.apanlh.util.cache.local;

import com.gitee.apanlh.util.func.FuncExecute;
import com.gitee.apanlh.util.log.Log;
import com.gitee.apanlh.util.sys.time.RunTimer;

import java.util.Timer;
import java.util.TimerTask;

/**	
 * 	时间缓存任务清理
 * 	
 * 	@author Pan
 */
public class CacheClearTask<K, V> {
	
	/** 缓存Map */
	private Cache<K, V> cacheMap;
	/** 延迟 */
	private long delay = 0L;
    /** 频率 */
    private long period;
    /** 日志开关 */
    private boolean printLog;
    
	/**
	 * 	默认构造函数
	 * 
	 * 	@author Pan
	 */
	CacheClearTask() {
		//	不允许外部实例
		super();
	}
	
	/**
	 * 	构造函数
	 * 	<br>自定义缓存
	 * 
	 * 	@author Pan
	 *  @param 	cacheMap		缓存
	 *  @param 	period			删除机制延迟频率
     * 	@param 	printLog		是否打印缓存池信息
	 */
	CacheClearTask(Cache<K, V> cacheMap, long period, boolean printLog) {
		this.cacheMap = cacheMap;
		this.period = period;
		this.printLog = printLog;
	}
	
    /**	
     * 	单线程操作
     * 	检查过期Key并自动销毁
     *
	 * 	@author Pan
	 * 	@param 	func	执行函数
	 */
	void start(FuncExecute func) {
		RunTimer runTimer = RunTimer.create("clear cache");
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				if (cacheMap.size() == 0) {
					if (printLog) {
						Log.get().info("no cache data");
					}
					return ;
				}
				
				if (printLog) {
					runTimer.clearTimer();
					runTimer.startTime();
				}
				
				//	执行策略
				func.execute();
				
				if (printLog) {
					runTimer.stopTime();
					Log.get().info("cache size[{}], clear time[{}]", cacheMap.size(), runTimer.printTime());
				}
			}
		}, delay, period);
	}

	/**
	 * 	构建自动清理
	 * 	
	 * 	@author Pan
	 * 	@param 	<K>				键
	 * 	@param 	<V>				值
	 *  @param 	cacheMap		缓存
	 *  @param 	period			删除机制延迟频率
     * 	@param 	printLog		是否打印缓存池信息
	 * 	@param 	func			执行函数
	 */
	public static <K, V> void start(Cache<K, V> cacheMap, long period, boolean printLog, FuncExecute func) {
		new CacheClearTask<>(cacheMap, period, printLog).start(func);
	}
}
