package com.gitee.apanlh.util.cache.local;

import com.gitee.apanlh.util.base.Empty;
import com.gitee.apanlh.util.base.ObjectUtils;

/**	
 * 	CacheLRU-K
 * 	<br>淘汰最久未使用
 *  <br>解决LRU算法的缓存污染
 *  	
 * 	@author Pan
 *  @param  <K>     建类型
 *  @param  <V>     值类型
 */
public class CacheLruK<K, V> extends CacheAbstract<K, V> {
	
	/** FIFO队列 */
	private CacheFifo<K, CacheObject<V>> history;
	/**	LRU队列 */
	private CacheLru<K, CacheObject<V>> lru;
	/** 缓存K阈值 */
	private int k;
	/** 命中次数 */
	private int hit;
	
	/**
	 * 	默认构造函数
	 * 	<br>历史访问列表256最大容量
	 * 	<br>LRU缓存列表128最大容量
	 * 	<br>K值为2次
	 * 	
	 * 	@author Pan
	 */	
	public CacheLruK() {
		this(256, 128, 2);
	}
	
	/**	
	 * 	默认构造函数
	 * 	<br>LRU缓存列表容量与历史访问列表一致
	 * 	<br>默认K值为2次
	 * 
	 * 	@author Pan
	 * 	@param 	capacity	最大容量
	 */
	public CacheLruK(int capacity) {
		this(capacity, capacity, 2);
	}
	
	/**	
	 * 	构造函数
	 * 	<br>自定义历史列表容量及LRU列表容量
	 * 
	 * 	@author Pan
	 * 	@param 	historyCapacity	历史访问列表最大容量		
	 * 	@param 	lruCapacity	LRU缓存列表最大容量
	 */
	public CacheLruK(int historyCapacity, int lruCapacity) {
		this(historyCapacity, lruCapacity, 2);
	}
	
	/**	
	 * 	构造函数
	 * 	<br>自定义历史列表最大容量、LRU列表最大容量、K值
	 * 
	 * 	@author Pan
	 * 	@param 	historyCapacity	历史访问列表最大容量			
	 * 	@param 	lruCapacity	LRU缓存列表最大容量
	 * 	@param 	k				K次数
	 */
	public CacheLruK(int historyCapacity, int lruCapacity, int k) {
		super(Empty.map());
		this.k = k;
		this.history = new CacheFifo<>(historyCapacity);
		this.lru = new CacheLru<>(lruCapacity);
	}

	@Override
	V putHandler(K key, V value) {
		CacheObject<V> cacheObject = this.lru.getUnlocked(key);
		if (cacheObject == null) {
			cacheObject = new CacheObject<>(value);
			//	存放历史访问记录
			this.history.putUnlocked(key, cacheObject);
		} else {
			//	避免缓存值不一致情况
			if (!ObjectUtils.eq(value, cacheObject.getData())) {
				cacheObject.setData(value);
			}
			//	添加K值
			cacheObject.setCount();
		}
		return null;
	}
	
	@Override
	V getHandler(K key, V value) {
		//	优先检索lru缓存中的数据
		V v = getLru(key);
		return v == null ? getHistory(key) : v;
	}
	
	/**	
	 * 	获取历史队列缓存数据
	 * 	
	 * 	@author Pan
	 * 	@param 	key	键
	 * 	@return	V
	 */
	V getHistory(K key) {
		CacheObject<V> cacheHistoryObject = this.history.getUnlocked(key);
		//	避免在历史队列中已被淘汰的数据出现空指针
		if (cacheHistoryObject == null) {
			return null;
		}
		this.hit++;
		//	添加访问次数
		cacheHistoryObject.setCount();
		// 检查是否达到K阈值、默认队列已实现淘汰机制
		if (cacheHistoryObject.getCount() == this.k) {
			this.lru.putUnlocked(key, cacheHistoryObject);
			this.history.removeUnlocked(key);
		}
		return cacheHistoryObject.getData();
	}
	
	/**	
	 * 	获取LRU缓存数据
	 * 	
	 * 	@author Pan
	 * 	@param 	key	键
	 * 	@return	V
	 */
	V getLru(K key) {
		//	优先检索lru缓存中的数据
		CacheObject<V> lruCacheObject = this.lru.getUnlocked(key);
		if (null == lruCacheObject) {
			return null;
		}
		this.hit++;
		lruCacheObject.setCount();
		return lruCacheObject.getData();
	}
	
	@Override
	public String toString() {
		return "LRU-K[history=" + this.history + ", LRU=" + this.lru + "]";
	}
	
	@Override
	public int size() {
		return getHistorySize() + getLruSize();
	}
	
	/**
	 * 	清理历史缓存及LRU缓存
	 * 	<br>两者缓存都会清空
	 * 
	 * 	@author Pan
	 */
	@Override
	public void clear() {
		lock(this::clearUnlock);
	}

	@Override
	public void clearUnlock() {
		history.clearUnlock();
		lru.clearUnlock();
	}
	
	/**
	 * 	获取历史缓存长度
	 * 	
	 * 	@author Pan
	 * 	@return	int
	 */
	public int getHistorySize() {
		return history.size();
	}
	
	/**	
	 * 	获取LRU缓存长度
	 * 	
	 * 	@author Pan
	 * 	@return	int
	 */
	public int getLruSize() {
		return lru.size();
	}
	
	/**	
	 * 	获取历史缓存
	 * 	
	 * 	@author Pan
	 * 	@return	CacheFifo
	 */
	public CacheFifo<K, CacheObject<V>> getHistory() {
		return history;
	}
	
	/**	
	 * 	获取LRU缓存
	 * 	
	 * 	@author Pan
	 * 	@return	CacheLru
	 */
	public CacheLru<K, CacheObject<V>> getLru() {
		return lru;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		CacheLruK<?, ?> cacheLruK = (CacheLruK<?, ?>) o;

		if (!history.equals(cacheLruK.history)) return false;
		return lru.equals(cacheLruK.lru);
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + history.hashCode();
		result = 31 * result + lru.hashCode();
		return result;
	}
}
