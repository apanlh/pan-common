package com.gitee.apanlh.util.cache.local;

import com.gitee.apanlh.util.base.MapUtils;

import java.util.Iterator;

/**	
 * 	FIFO缓存
 * 	<br>先进先出
 * 	
 * 	@author Pan
 */
public class CacheFifo<K, V> extends CacheAbstract<K, V> {
	
	/**	最大容量 */
	private int capacity;
	/** 命中次数 */
	private int hit;
	
	/**
	 * 	构造函数
	 * 	<br>默认256最大容量
	 * 
	 * 	@author Pan
	 */
	public CacheFifo() {
		this(256);
	}
	
	/**
	 *  构造函数
	 *  <br>自定义容量
	 *  
	 * 	@author Pan
	 * 	@param 	capacity 容量
	 */
	public CacheFifo(int capacity) {
		super(MapUtils.newLinkedHashMap(capacity));
		this.capacity = capacity;
	}
	
	@Override
	V putHandler(K key, V value) {
		//	避免重复key时,不重新向队头移动
		if (containsKeyUnlocked(key)) {
			removeUnlocked(key);
			return value;
		}
		clearCache();
		return value;
	}
	
	@Override
	V getHandler(K key, V value) {
		if (value == null) {
			return value;
		}
		
		this.hit++;
		return value;
	}
	
	@Override
	public void clearCache() {
		//	超出容量移除队头
		if (size() == this.capacity) {
			Iterator<V> iterator = values().iterator();
			iterator.next();
			iterator.remove();
		}
	}
	
	@Override
	public String toString() {
		return getMap().toString();
	}	
	
	@Override
	public int getHit() {
		return this.hit;
	}
	
	@Override
	public boolean equals(Object o) {
		return super.equals(o);
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
