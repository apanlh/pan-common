package com.gitee.apanlh.util.cache.local.api;

/**	
 * 	缓存额外基础接口
 * 	
 * 	@author Pan
 */
public interface CacheProvider {
	
	/**	
	 * 	获取命中次数
	 * 	
	 * 	@author Pan
	 * 	@return	int
	 */
	int getHit();
	
//	/** 是否满缓存池 */
//	boolean hasFull();
}
