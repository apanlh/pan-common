package com.gitee.apanlh.util.print;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.gitee.apanlh.exp.ParseException;
import com.gitee.apanlh.util.dataformat.JsonUtils;

/**
 * 	打印输出格式化
 * 
 * 	@author Pan
 */
public class PrintFormatUtils {
	
	/**
	 * 构造函数
	 * 
	 * @author Pan
	 */
	private PrintFormatUtils() {
		// 不允许外部实例
		super();
	}

	/**
	 * 	格式化JSON
	 * 	<br>需要Fastjson库
	 * 	
	 * 	@author Pan
	 * 	@param 	jsonString	JSON字符串
	 * 	@return String
	 */
	 public static String formatJson(String jsonString) {
        boolean hasJson = JsonUtils.hasJson(jsonString);
        if (!hasJson) {
        	throw new ParseException("string is not in json format");
        }
        return JSON.toJSONString(JSON.parseObject(jsonString), SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue, SerializerFeature.WriteDateUseDateFormat);
    }
	 
}
