package com.gitee.apanlh.util.algorithm.digest;

import com.gitee.apanlh.util.check.CheckImport;
import com.gitee.apanlh.util.check.CheckLibrary;
import com.gitee.apanlh.util.valid.Assert;
import org.bouncycastle.crypto.digests.SM3Digest;

/**	
 * 	SM3摘要(国密算法)
 * 	<br>采用BouncyCastle开源库实现
 * 	
 * 	@author Pan
 */
public class SM3 extends CustomDigestAbstract {
	
	static {
		CheckImport.library(CheckLibrary.BOUNCY_CASTLE);
	}
	
	/** SM3摘要 */
	private SM3Digest digest;
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	public SM3() {
		super();
		this.digest = new SM3Digest();
	}
	
	@Override
	public byte[] hash(byte[] content) {
		Assert.isNotEmpty(content);
		
		digest.update(content, 0, content.length);
		byte[] bytes = new byte[digest.getDigestSize()];
		digest.doFinal(bytes, 0);
		return bytes;
	}
	
	/**	
	 * 	构建SM3摘要
	 * 	
	 * 	@return	SM3
	 */
	public static SM3 create() {
		return new SM3();
	}
}
