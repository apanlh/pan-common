package com.gitee.apanlh.util.algorithm.digest;

/**	
 * 	消息摘要类型
 * 	
 * 	@author Pan
 */
public enum DigestType {
	
	MD5			("MD5"),
	/** SHA-2 */
	SHA_1		("SHA-1"),
	SHA_224		("SHA-224"),
	SHA_256		("SHA-256"),
	SHA_384		("SHA-384"),
	SHA_512		("SHA-512"),
	/** SHA-224 */
	SHA3_224	("SHA3-224"),
	/** SHA-256 */
	SHA3_256	("SHA3-256"),
	/** SHA-384 */
	SHA3_384	("SHA3-384"),
	/** SHA-512 */
	SHA3_512	("SHA3-512"),
	/** HMAC MD5 */
	HMAC_MD5	("HmacMD5"),
	/** HMAC 1 */
	HMAC_SHA1	("HmacSha1"),
	/** HMAC 224 */
	HMAC_SHA224	("HmacSha224"),
	/** HMAC 256 */
	HMAC_SHA256	("HmacSha256"),
	/** HMAC 384 */
	HMAC_SHA384	("HmacSha384"),
	/** HMAC 512 */
	HMAC_SHA512	("HmacSha512");
	
	/** 算法 */
	private String algorithm;
	
	/**	
	 * 	构造函数-自定义算法类型
	 * 	
	 * 	@author Pan
	 * 	@param 	algorithm	算法
	 */
	DigestType(String algorithm) {
		this.algorithm = algorithm;
	}
	
	/**
	 * 	获取对称加密算法的类型字符串
	 * 	
	 * 	@author Pan
	 * 	@return String	对称加密算法类型字符串
	 */
	public String getAlgorithm() {
		return algorithm;
	}
}
