package com.gitee.apanlh.util.algorithm.signature;

import java.io.Serializable;

/**	
 * 	Jwt存放信息对象
 * 	@author Pan
 */
public class JwtObject implements Serializable {

	private static final long serialVersionUID = -4760670897189830843L;
	
	/** 生成后的token值 */
	private String token;
	/** 返回校验信息 */
	private String signMsg;
	/** 返回校验状态码  */
	private Integer signCode;
	/** 异常原因 */
	private String exceptionMsg;
	/** 校验值 */
	private boolean verifyFlag;
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	public JwtObject() {
		super();
	}
	
	/**	
	 * 	初始化构造函数
	 * 	
	 * 	@author Pan
	 * 	@param 	token		token
	 * 	@param 	signMsg		错误签名信息
	 * 	@param 	signCode	错误签名状态
	 * 	@param 	verifyFlag  验证状态
	 */
	public JwtObject(String token, String signMsg, Integer signCode, boolean verifyFlag) {
		super();
		this.token = token;
		this.signMsg = signMsg;
		this.signCode = signCode;
		this.verifyFlag = verifyFlag;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getSignMsg() {
		return signMsg;
	}

	public void setSignMsg(String signMsg) {
		this.signMsg = signMsg;
	}

	public Integer getSignCode() {
		return signCode;
	}

	public void setSignCode(Integer signCode) {
		this.signCode = signCode;
	}

	public String getExceptionMsg() {
		return exceptionMsg;
	}

	public void setExceptionMsg(String exceptionMsg) {
		this.exceptionMsg = exceptionMsg;
	}

	public boolean isVerifyFlag() {
		return verifyFlag;
	}

	public void setVerifyFlag(boolean verifyFlag) {
		this.verifyFlag = verifyFlag;
	}
	
}
