package com.gitee.apanlh.util.algorithm.encrypt.symmetric;

/**
 * 	对称加密编码接口
 * 	<br>包含密钥，向量，盐值等
 * 	
 * 	@author Pan
 */
public interface SymmetricEncode {
	
	/**	
	 * 	获取原始密钥字节数组
	 * 	
	 * 	@author Pan
	 * 	@return	byte[]
	 */
	byte[] getKey();
	
	/**	
	 * 	将密钥编码十六进制字符串
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	String getKeyToHex();
	
	/**	
	 * 	将密钥编码Base64
	 * 	
	 * 	@author Pan
	 * 	@return	byte[]
	 */
	byte[] getKeyToBase64();
	
	/**	
	 * 	将密钥编码Base64字符串
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	String getKeyToBase64Str();
	
	/**	
	 * 	获取原始向量字节数组
	 * 	
	 * 	@author Pan
	 * 	@return	byte[]
	 */
	byte[] getIv();
	
	/**	
	 * 	将向量编码十六进制字符串
	 * 	
	 * 	@author Pan
	 * 	@return	byte[]
	 */
	String getIvToHex();
	
	/**	
	 * 	将向量编码Base64
	 * 	
	 * 	@author Pan
	 * 	@return	byte[]
	 */
	byte[] getIvToBase64();
	
	/**	
	 * 	将向量编码Base64字符串
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	String getIvToBase64Str();
	
	/**	
	 * 	获取原始密码字节数组
	 * 	
	 * 	@author Pan
	 * 	@return	byte[]
	 */
	byte[] getPassword();
	
	/**	
	 * 	将密码编码十六进制字符串
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	String getPasswordToHex();
	
	/**	
	 * 	将密码编码Base64
	 * 	
	 * 	@author Pan
	 * 	@return	byte[]
	 */
	byte[] getPasswordToBase64();
	
	/**	
	 * 	将密码编码Base64字符串
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	String getPasswordToBase64Str();
}
