package com.gitee.apanlh.util.algorithm.encrypt.asymmetric;

/**
 * 非对称加密类型算法类型枚举
 * 
 * <p>该枚举包含常用的对称加密算法类型，用于调用KeyGenerator.getInstance方法。</p>
 * 
 * <p>注意：可用的算法类型可能因密码提供者而异。</p>
 * 
 * <p>示例用法：</p>
 * <code>SymmetricType.SM2</code>
 * <code>SymmetricType.RSA</code>
 * 
 * <p>枚举值与KeyGenerator中的算法类型对应。</p>
 * <p>注：由于不同的JDK版本和密码提供者，可能支持的算法类型会有所不同。</p>
 * 
 * <p>以下枚举值基于常见的对称加密算法类型：</p>
 * <ul>
 *     <li>RSA - Rivest-Shamir-Adleman</li>
 *     <li>SM2 - 椭圆曲线公钥密码算法</li>
 * </ul>
 * 
 * 	@author Pan
 * 	@see 	<a href="https://docs.oracle.com/en/java/javase/11/docs/specs/security/standard-names.html#keygenerator-algorithms">KeyGenerator Algorithms</a>
 */
public enum AsymmetricType {
	
	/** RSA算法 RSA/ECB/PKCS1Padding */
	RSA("RSA"),
	/** BC默认实现 RSA算法 RSA/None/PKCS1Padding */
	RSA_BC("RSA"),
	/** SM2国密算法 */
	SM2("SM2");
	
	/** 算法 */
	private String algorithm;
	
	/**	
	 * 	构造函数-自定义算法类型
	 * 	
	 * 	@author Pan
	 * 	@param 	algorithm	算法
	 */
	AsymmetricType(String algorithm) {
		this.algorithm = algorithm;
	}
	
	/**
	 * 	获取对称加密算法的类型字符串
	 * 	
	 * 	@author Pan
	 * 	@return String	对称加密算法类型字符串
	 */
	public String getAlgorithm() {
		return algorithm;
	}
}
