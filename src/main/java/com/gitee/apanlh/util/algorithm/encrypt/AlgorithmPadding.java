package com.gitee.apanlh.util.algorithm.encrypt;

/**
 *	加密/解密填充模式
 *	 
 * 	@author Pan
 */
public enum AlgorithmPadding {
	
	/** 无填充 */
	NONE("NoPadding"),
	/**
	 * 非标准填充方式，特殊实现Zero模式，未满足Block长度时使用0填充，0补码
	 */
	ZERO("NoPadding"),
	/**
	 * This padding for block ciphers is described in 5.2 Block Encryption
	 * Algorithms in the W3C's "XML Encryption Syntax and Processing" document.
	 */
	ISO10126("ISO10126Padding"),
	/**
	 * Optimal Asymmetric Encryption Padding scheme defined in PKCS1
	 */
	OAEP("OAEPPadding"),
	/**
	 * The padding scheme described in PKCS #1, used with the RSA algorithm
	 */
	PKCS1("PKCS1Padding"),
	/**
	 * PKCS5Padding
	 * The padding scheme described in RSA Laboratories, "PKCS #5: Password-Based
	 * Encryption Standard," version 1.5, November 1993.
	 */
	PKCS5("PKCS5Padding"),
	/** PKCS7Padding */
	PKCS7("PKCS7Padding"),
	/**
	 * The padding scheme defined in the SSL Protocol Version 3.0, November 18,
	 * 1996, section 5.2.3.2 (CBC block cipher)
	 */
	SSL3("SSL3Padding");
	
	/** 模式 */
	private String mode;
		
	/**	
	 * 	构造函数-定义模式
	 * 	
	 * 	@author Pan
	 * 	@param  mode 模式
	 */
	AlgorithmPadding(String mode) {
		this.mode = mode;
	}

	/**
	 * 	获取模式
	 * 
	 * 	@author Pan
	 * 	@return String
	 */
	public String getMode() {
		return mode;
	}
}
