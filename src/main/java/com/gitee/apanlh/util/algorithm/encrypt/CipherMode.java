package com.gitee.apanlh.util.algorithm.encrypt;

import javax.crypto.Cipher;

/**
 * 	加密器模式枚举类，用于表示加密器的操作模式，包括加密和解密
 * 	<br>ENCRYPT：加密操作模式，用于加密数据
 * 	<br>DECRYPT：解密操作模式，用于解密数据
 * 	<br>加密器模式指定了加密器的具体操作，根据模式的不同，加密器可以用于加密或解密数据
 * 	s
 * 	@author Pan
 */
public enum CipherMode {
	
	/** 加密操作模式，用于加密数据 */
	ENCRYPT(Cipher.ENCRYPT_MODE),
	/** 解密操作模式，用于解密数据 */
	DECRYPT(Cipher.DECRYPT_MODE);
	
	/** 类型值 */
	private int mode;
	
	/**	
	 * 	构造函数
	 * 	<br>自定义类型
	 * 	
	 * 	@author Pan
	 * 	@param 	mode	类型
	 */
	CipherMode(int mode) {
		this.mode = mode;
	}
	
	/**
	 * 	获取类型值
	 * 	
	 * 	@author Pan
	 * 	@return	int
	 */
	public int getMode() {
		return mode;
	}
}
