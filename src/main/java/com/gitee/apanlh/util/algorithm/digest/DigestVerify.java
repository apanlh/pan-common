package com.gitee.apanlh.util.algorithm.digest;

import java.io.File;

/**	
 * 	签名摘要对比接口
 * 	<br>定义验证摘要等
 * 	
 * 	@author Pan
 */
public interface DigestVerify {

	/**	
	 * 	摘要校验方法
	 * 	<br>会将明文内容进行摘要算法后与摘要内容进行对比
	 * 	
	 * 	@author Pan
	 * 	@param 	plaintextContent 	明文内容
	 * 	@param 	digestContent 		摘要内容
	 * 	@return	boolean
	 */
	boolean verify(byte[] plaintextContent, byte[] digestContent);

	/**	
	 * 	文件的散列值比较
	 * 	<br>会将文件中进行散列算法，并进行比较
	 * 
	 * 	@author Pan
	 * 	@param 	file 	文件对象
	 * 	@param 	file2 	文件对象2
	 * 	@return	boolean
	 */
	boolean verify(File file, File file2);
	
	/**	
	 * 	摘要校验方法
	 * 	<br>会将明文内容进行摘要算法后与摘要内容进行对比
	 * 	<br>默认将明文转换Base64
	 * 	
	 * 	@author Pan
	 * 	@param 	plaintextContent 	明文内容
	 * 	@param 	digestContent 		摘要内容
	 * 	@return	boolean
	 */
	boolean verifyBase64(byte[] plaintextContent, byte[] digestContent);

	/**	
	 * 	摘要校验方法
	 * 	<br>会将明文内容进行摘要算法后与摘要内容进行对比
	 * 	<br>默认将明文转换Base64
	 * 
	 * 	@author Pan
	 * 	@param 	plaintextContent 	明文内容
	 * 	@param 	digestContent 		摘要内容
	 * 	@return	boolean
	 */
	boolean verifyBase64(byte[] plaintextContent, String digestContent);
	
	/**	
	 * 	摘要校验方法(字符串类型)
	 * 	<br>会将明文内容进行摘要算法后与摘要内容进行对比
	 * 	<br>将忽略大小写匹配
	 * 	<br>默认将明文转换Base64
	 * 
	 * 	@author Pan	
	 * 	@param 	plaintextContent 	明文内容
	 * 	@param 	digestContent 		摘要内容
	 * 	@return	boolean
	 */
	boolean verifyBase64(String plaintextContent, String digestContent);
	
	/**	
	 * 	文件的散列值比较
	 * 	<br>会将文件中进行散列算法，并对摘要内容进行比较
	 * 	<br>默认将明文转换Base64
	 * 
	 * 	@author Pan
	 * 	@param 	file 			文件对象
	 * 	@param 	digestContent 	摘要内容
	 * 	@return	boolean
	 */
	boolean verifyBase64(File file, String digestContent);
	
	/**	
	 * 	摘要校验方法
	 * 	<br>会将明文内容进行摘要算法后与摘要内容进行对比
	 * 	<br>默认将明文转换Hex(16进制)
	 * 
	 * 	@author Pan
	 * 	@param 	plaintextContent 	明文内容
	 * 	@param 	digestContent 		摘要内容
	 * 	@return	boolean
	 */
	boolean verifyHex(byte[] plaintextContent, String digestContent);
	
	/**	
	 * 	摘要校验方法(字符串类型)
	 * 	<br>会将明文内容进行摘要算法后与摘要内容进行对比
	 * 	<br>将忽略大小写匹配
	 * 	<br>默认将明文转换Hex(16进制)
	 * 
	 * 	@author Pan	
	 * 	@param 	plaintextContent 	明文内容
	 * 	@param 	digestContent 		摘要内容
	 * 	@return	boolean
	 */
	boolean verifyHex(String plaintextContent, String digestContent);
	
	/**	
	 * 	文件的散列值比较
	 * 	<br>会将文件中进行散列算法，并对摘要内容进行比较
	 * 	<br>默认将明文转换Hex(16进制)
	 * 
	 * 	@author Pan
	 * 	@param 	file 			文件对象
	 * 	@param 	digestContent 	摘要内容
	 * 	@return	boolean
	 */
	boolean verifyHex(File file, String digestContent);
}
