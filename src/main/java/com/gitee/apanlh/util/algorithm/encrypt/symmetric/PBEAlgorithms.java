package com.gitee.apanlh.util.algorithm.encrypt.symmetric;

/**
 * 	PBE算法枚举（目前支持Bouncy Castle 1.60以上版本）
 * 	<br>Bouncy Castle版本可能会随着更新而改变，建议查看具体版本支持的算法。
 * 	<br>在不同版本中可能支持的算法会有所不同。
 * 	
 * 	@author Pan
 */
public enum PBEAlgorithms {
	
	/** PBEWITHMD2ANDDES */
	MD2_DES("PBEWITHMD2ANDDES"),
	
	/** PBEWithMD5AndDES */
	MD5_DES("PBEWithMD5AndDES"),
	/** PBEWithMD5AndRC2 */
    MD5_RC2("PBEWithMD5AndRC2"),
    /** PBEWITHMD5AND128BITAES-CBC-OPENSSL */
    MD5_128_AEC_CBC("PBEWITHMD5AND128BITAES-CBC-OPENSSL"),
    /** PBEWITHMD5AND192BITAES-CBC-OPENSSL */
    MD5_192_AEC_CBC("PBEWITHMD5AND192BITAES-CBC-OPENSSL"),
    /** PBEWITHMD5AND256BITAES-CBC-OPENSSL */
    MD5_256_AEC_CBC("PBEWITHMD5AND256BITAES-CBC-OPENSSL"),
    
    /** PBEWithSHAAnd2-KeyTripleDES-CBC */
    SHA_2_KEY_TRIPLE_DES_CBC("PBEWithSHAAnd2-KeyTripleDES-CBC"),
    /** PBEWithSHAAnd3-KeyTripleDES-CBC */
    SHA_3_KEY_TRIPLE_DES_CBC("PBEWithSHAAnd3-KeyTripleDES-CBC"),
    /** PBEWithSHAAnd40BitRC2-CBC */
    SHA_40_RC2_CBC("PBEWithSHAAnd40BitRC2-CBC"),
    /** PBEWithSHAAnd128BitRC2-CBC */
    SHA_128_RC2_CBC("PBEWithSHAAnd128BitRC2-CBC"),
    /** PBEWithSHAAnd40BitRC4 */
    SHA_40_BIT_RC4("PBEWithSHAAnd40BitRC4"),
    /** PBEWithSHAAnd128BitRC4 */
    SHA_128_BIT_RC4("PBEWithSHAAnd128BitRC4"),
    /** PBEWITHSHAAND128BITAES-CBC-BC */
    SHA_128_AES_CBC("PBEWITHSHAAND128BITAES-CBC-BC"),
    /** PBEWITHSHAAND192BITAES-CBC-BC */
	SHA_192_AES_CBC("PBEWITHSHAAND192BITAES-CBC-BC"),
	/** PBEWITHSHAAND256BITAES-CBC-BC */
	SHA_256_AES_CBC("PBEWITHSHAAND256BITAES-CBC-BC"),
	/** PBEWITHSHAANDTWOFISH-CBC */
	SHA_TWO_FISH_CBC("PBEWITHSHAANDTWOFISH-CBC"),
    
	/** PBEWithSHA1AndDES */
    SHA1_DES("PBEWithSHA1AndDES"),
    /** PBEWithSHA1AndDESede */
    SHA1_DESEDE("PBEWithSHA1AndDESede"),
    /** PBEWithSHA1AndRC2 */
    SHA1_RC2("PBEWithSHA1AndRC2"),
    /** PBEWithSHAAndIDEA-CBC */
    SHA1_AES_IDEA_CBC("PBEWithSHAAndIDEA-CBC"),
    
    /** PBEWITHSHA256AND128BITAES-CBC-BC */
	SHA256_128_AES_CBC("PBEWITHSHA256AND128BITAES-CBC-BC"),
	/** PBEWITHSHA256AND192BITAES-CBC-BC */
	SHA256_192_AES_CBC("PBEWITHSHA256AND192BITAES-CBC-BC"),
	/** PBEWITHSHA256AND256BITAES-CBC-BC */
	SHA256_256_AES_CBC("PBEWITHSHA256AND256BITAES-CBC-BC");
    
	/** 算法 */
	private String algorithm;
	
	/**	
	 * 	构造函数
	 * 	<br>自定义类型
	 * 	
	 * 	@author Pan
	 * 	@param 	algorithm	算法名称
	 */
	PBEAlgorithms(String algorithm) {
		this.algorithm = algorithm;
	}
	
	/**	
	 * 	获取算法名称
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getAlgorithm() {
		return algorithm;
	}
}
