package com.gitee.apanlh.util.algorithm.encrypt.asymmetric;

/**
 * 	非对称算法对公钥/私钥编码接口
 * 	
 * 	@author Pan
 */
public interface AsymmetricEncode {
	
	/**	
	 * 	获取原始公钥字节数组
	 * 	
	 * 	@author Pan
	 * 	@return	byte[]
	 */
	byte[] getPublicEncode();
	
	/**	
	 * 	将公钥编码十六进制字符串
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	String getPublicEncodeToHex();
	
	/**	
	 * 	将公钥编码Base64
	 * 	
	 * 	@author Pan
	 * 	@return	byte[]
	 */
	byte[] getPublicEncodeToBase64();
	
	/**	
	 * 	将公钥编码Base64字符串
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	String getPublicEncodeToBase64Str();
	
	
	/**	
	 * 	获取原始私钥字节数组
	 * 	
	 * 	@author Pan
	 * 	@return	byte[]
	 */
	byte[] getPrivateEncode();
	
	/**	
	 * 	将私钥编码十六进制字符串
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	String getPrivateEncodeToHex();
	
	/**	
	 * 	将私钥编码Base64
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	byte[] getPrivateEncodeToBase64();
	
	/**	
	 * 	将私钥编码Base64字符串
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	String getPrivateEncodeToBase64Str();
	
}
