package com.gitee.apanlh.util.algorithm.digest;

/**
 * 	自定义实现默认自带摘要算法
 * 	<br>例如：BouncyCastle等
 * 	<br>提供外部继承接口，以便重写自定义摘要逻辑
 * 	
 * 	@author Pan
 */
public abstract class CustomDigestAbstract extends DigestAbstract {
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	CustomDigestAbstract() {
		super();
	}
}
