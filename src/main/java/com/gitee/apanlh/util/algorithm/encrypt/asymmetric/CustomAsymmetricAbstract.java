package com.gitee.apanlh.util.algorithm.encrypt.asymmetric;

/**
 * 	非对称算法加密/解密抽象类
 * 	<br>默认实现基本功能
 * 	<br>外部可自定义继承此类编写额外加解密方法
 * 	
 * 	@author Pan
 */
public abstract class CustomAsymmetricAbstract extends AsymmetricAbstract {

}
