package com.gitee.apanlh.util.algorithm.digest;

import com.gitee.apanlh.exp.DigestException;

/**
 * 	MD5摘要类
 * 	
 * 	@author Pan
 */
public class MD5 extends JdkDigestAbstract {
	
	/**
	 * 	默认构造函数
	 * 	<br>初始化指定摘要类型
	 * 	
	 * 	@author Pan
	 * 	@throws DigestException 如果未找到对应摘要算法则抛出
	 */
	public MD5() {
		super(DigestType.MD5);
	}
	
	@Override
	public void check() {
		if (!getDigestType().equals(DigestType.MD5)) {
			throw new DigestException("digest type error cause: expected value[{}] but actual value is[{}]", DigestType.MD5.getAlgorithm(), getDigestType().getAlgorithm());
		}
	}
	
	/**	
	 * 	构建MD5摘要类
	 * 	
	 * 	@author Pan
	 * 	@return	MD5
	 */
	public static MD5 create() {
		return new MD5();
	}
}
