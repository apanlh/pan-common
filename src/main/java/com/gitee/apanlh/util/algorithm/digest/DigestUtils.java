package com.gitee.apanlh.util.algorithm.digest;

import com.gitee.apanlh.util.base.Empty;


/**	
 * 	摘要工具类
 * 	<br>整合了MD5、SHA、HMAC、SM3摘要算法的工具类
 * 	<br>使用该工具类可以方便地构建不同类型的摘要实例，并进行摘要计算
 * 	<br>支持的摘要算法包括：
 * 	<br>MD5
 * 	<br>SHA1、SHA224、SHA256、SHA384、SHA512、SHA3-224
 * 	<br>SHA3-256、SHA3-384、SHA3-512
 * 	<br>HMAC-MD5、HMAC-SHA1、HMAC-SHA224、HMAC-SHA256、HMAC-SHA384、HMAC-SHA512
 * 	<br>SM3
 * 	<p>
 *	<br>示例用法：
 * 	<pre>{@code
 * 	//	MD5
 * 	MD5 md5 = DigestUtils.md5();
 * 	md5.digestToHex("pan");
 * 	md5.digest("pan");
 * 		
 *	//	SHA
 *	SHA sha = DigestUtils.shaWith256();
 * 	sha.digestToHex("pan");
 * 	sha.digest("pan");
 * 	}</pre>
 * 	
 * 	<br>注意：为了保证摘要的安全性，请妥善保管密钥
 * 	@author Pan
 */
public class DigestUtils {
		
	/**
	 * 	构造函数
	 * 	
	 * 	@author Pan
	 */
	private DigestUtils() {
		//	不允许外部示例
		super();
	}
	
	/**	
	 * 	构建MD5摘要
	 * 	
	 * 	@author Pan
	 * 	@return	MD5
	 */
	public static MD5 mD5() {
		return MD5.create();
	}
	
	/**	
	 * 	构建Sha摘要
	 * 	<br>自定义摘要类型
	 * 	
	 * 	@author Pan
	 * 	@param 	digestType	摘要类型
	 * 	@return	SHA
	 */
	public static SHA sha(DigestType digestType) {
		return SHA.create(digestType);
	}
	
	/**	
	 * 	构建Sha摘要
	 * 	<br>Sha1摘要类型
	 * 	
	 * 	@author Pan
	 * 	@return	SHA
	 */
	public static SHA shaWith1() {
		return sha(DigestType.SHA_1);
	}
	
	/**	
	 * 	构建Sha摘要
	 * 	<br>Sha224摘要类型
	 * 	
	 * 	@author Pan
	 * 	@return	SHA
	 */
	public static SHA shaWith224() {
		return sha(DigestType.SHA_224);
	}
	
	/**	
	 * 	构建Sha摘要
	 * 	<br>Sha256摘要类型
	 * 	
	 * 	@author Pan
	 * 	@return	SHA
	 */
	public static SHA shaWith256() {
		return sha(DigestType.SHA_256);
	}
	
	/**	
	 * 	构建Sha摘要
	 * 	<br>Sha384摘要类型
	 * 	
	 * 	@author Pan
	 * 	@return	SHA
	 */
	public static SHA shaWith384() {
		return sha(DigestType.SHA_384);
	}
	
	/**	
	 * 	构建Sha摘要
	 * 	<br>Sha512摘要类型
	 * 	
	 * 	@author Pan
	 * 	@return	SHA
	 */
	public static SHA shaWith512() {
		return sha(DigestType.SHA_512);
	}
	
	
	/**	
	 * 	构建Sha3摘要
	 * 	<br>自定义摘要类型
	 * 	
	 * 	@author Pan
	 * 	@param 	digestType	摘要类型
	 * 	@return	SHA3
	 */
	public static SHA3 sha3(DigestType digestType) {
		return SHA3.create(digestType);
	}
	
	/**	
	 * 	构建Sha3摘要
	 * 	<br>Sha3-224摘要类型
	 * 	
	 * 	@author Pan
	 * 	@return	SHA3
	 */
	public static SHA3 sha3With224() {
		return sha3(DigestType.SHA3_224);
	}
	
	/**	
	 * 	构建Sha3摘要
	 * 	<br>Sha3-256摘要类型
	 * 	
	 * 	@author Pan
	 * 	@return	SHA3
	 */
	public static SHA3 sha3With256() {
		return sha3(DigestType.SHA3_256);
	}
	
	/**	
	 * 	构建Sha3摘要
	 * 	<br>Sha3-384摘要类型
	 * 	
	 * 	@author Pan
	 * 	@return	SHA3
	 */
	public static SHA3 sha3With384() {
		return sha3(DigestType.SHA3_384);
	}
	
	/**	
	 * 	构建Sha3摘要
	 * 	<br>Sha3-512摘要类型
	 * 	
	 * 	@author Pan
	 * 	@return	SHA3
	 */
	public static SHA3 sha3With512() {
		return sha3(DigestType.SHA3_512);
	}
	
	/**	
	 * 	构建HMac摘要
	 * 	<br>默认密钥，需要获取密钥请调用{@link HMac#getKeyBytes()} 或 {@link HMac#getKey()} 
	 * 	<br>MD5摘要类型
	 * 	
	 * 	@author Pan
	 * 	@return	HMac
	 */
	public static HMac hmacWithMd5() {
		return hmac(Empty.arrayByte(), DigestType.MD5);
	}
	
	/**	
	 * 	构建HMac摘要
	 * 	<br>默认密钥，需要获取密钥请调用{@link HMac#getKeyBytes()} 或 {@link HMac#getKey()} 
	 * 	<br>SHA1摘要类型
	 * 	
	 * 	@author Pan
	 * 	@return	HMac
	 */
	public static HMac hmacWithSha1() {
		return hmac(Empty.arrayByte(), DigestType.HMAC_SHA1);
	}
	
	/**	
	 * 	构建HMac摘要
	 * 	<br>默认密钥，需要获取密钥请调用{@link HMac#getKeyBytes()} 或 {@link HMac#getKey()} 
	 * 	<br>SHA224摘要类型
	 * 	
	 * 	@author Pan
	 * 	@return	HMac
	 */
	public static HMac hmacWithSha224() {
		return hmac(Empty.arrayByte(), DigestType.HMAC_SHA224);
	}
	
	/**	
	 * 	构建HMac摘要
	 * 	<br>默认密钥，需要获取密钥请调用{@link HMac#getKeyBytes()} 或 {@link HMac#getKey()} 
	 * 	<br>SHA256摘要类型
	 * 	
	 * 	@author Pan
	 * 	@return	HMac
	 */
	public static HMac hmacWithSha256() {
		return hmac(Empty.arrayByte(), DigestType.HMAC_SHA256);
	}
	
	/**	
	 * 	构建HMac摘要
	 * 	<br>默认密钥，需要获取密钥请调用{@link HMac#getKeyBytes()} 或 {@link HMac#getKey()} 
	 * 	<br>SHA384摘要类型
	 * 	
	 * 	@author Pan
	 * 	@return	HMac
	 */
	public static HMac hmacWithSha384() {
		return hmac(Empty.arrayByte(), DigestType.HMAC_SHA384);
	}
	
	/**	
	 * 	构建HMac摘要
	 * 	<br>默认密钥，需要获取密钥请调用{@link HMac#getKeyBytes()} 或 {@link HMac#getKey()} 
	 * 	<br>SHA512摘要类型
	 * 	
	 * 	@author Pan
	 * 	@return	HMac
	 */
	public static HMac hmacWithSha512() {
		return hmac(Empty.arrayByte(), DigestType.HMAC_SHA512);
	}
	
	/**	
	 * 	构建HMac摘要
	 * 	<br>自定义密钥
	 * 	<br>MD5摘要类型
	 * 	
	 * 	@author Pan
	 *  @param 	key			密钥
	 * 	@return	HMac
	 */
	public static HMac hmacWithMd5(byte[] key) {
		return hmac(key, DigestType.MD5);
	}
	
	/**	
	 * 	构建HMac摘要
	 * 	<br>自定义密钥
	 * 	<br>SHA1摘要类型
	 * 	
	 * 	@author Pan
	 *  @param 	key			密钥
	 * 	@return	HMac
	 */
	public static HMac hmacWithSha1(byte[] key) {
		return hmac(key, DigestType.HMAC_SHA1);
	}
	
	/**	
	 * 	构建HMac摘要
	 * 	<br>自定义密钥
	 * 	<br>SHA224摘要类型
	 * 	
	 * 	@author Pan
	 *  @param 	key			密钥
	 * 	@return	HMac
	 */
	public static HMac hmacWithSha224(byte[] key) {
		return hmac(key, DigestType.HMAC_SHA224);
	}
	
	/**	
	 * 	构建HMac摘要
	 * 	<br>自定义密钥
	 * 	<br>SHA256摘要类型
	 * 	
	 * 	@author Pan
	 *  @param 	key			密钥
	 * 	@return	HMac
	 */
	public static HMac hmacWithSha256(byte[] key) {
		return hmac(key, DigestType.HMAC_SHA256);
	}
	
	/**	
	 * 	构建HMac摘要
	 * 	<br>自定义密钥
	 * 	<br>SHA384摘要类型
	 * 	
	 * 	@author Pan
	 *  @param 	key			密钥
	 * 	@return	HMac
	 */
	public static HMac hmacWithSha384(byte[] key) {
		return hmac(key, DigestType.HMAC_SHA384);
	}
	
	/**	
	 * 	构建HMac摘要
	 * 	<br>自定义密钥
	 * 	<br>SHA512摘要类型
	 * 	
	 * 	@author Pan
	 *  @param 	key			密钥
	 * 	@return	HMac
	 */
	public static HMac hmacWithSha512(byte[] key) {
		return hmac(key, DigestType.HMAC_SHA512);
	}
	
	/**	
	 * 	构建HMac摘要
	 * 	<br>自定义密钥
	 * 	<br>自定义摘要类型
	 * 	
	 * 	@author Pan
	 *  @param 	key			密钥
	 * 	@param 	digestType	摘要类型
	 * 	@return	HMac
	 */
	public static HMac hmac(byte[] key, DigestType digestType) {
		return HMac.create(key, digestType);
	}
	
	/**	
	 * 	构建HMac摘要
	 * 	<br>自定义密钥
	 * 	<br>MD5摘要类型
	 * 	
	 * 	@author Pan
	 *  @param 	key			字符串密钥
	 * 	@return	HMac
	 */
	public static HMac hmacWithMd5(String key) {
		return hmac(key, DigestType.MD5);
	}
	
	/**	
	 * 	构建HMac摘要
	 * 	<br>自定义密钥
	 * 	<br>SHA1摘要类型
	 * 	
	 * 	@author Pan
	 *  @param 	key			字符串密钥
	 * 	@return	HMac
	 */
	public static HMac hmacWithSha1(String key) {
		return hmac(key, DigestType.HMAC_SHA1);
	}
	
	/**	
	 * 	构建HMac摘要
	 * 	<br>自定义密钥
	 * 	<br>SHA224摘要类型
	 * 	
	 * 	@author Pan
	 *  @param 	key			字符串密钥
	 * 	@return	HMac
	 */
	public static HMac hmacWithSha224(String key) {
		return hmac(key, DigestType.HMAC_SHA224);
	}
	
	/**	
	 * 	构建HMac摘要
	 * 	<br>自定义密钥
	 * 	<br>SHA256摘要类型
	 * 	
	 * 	@author Pan
	 *  @param 	key			字符串密钥
	 * 	@return	HMac
	 */
	public static HMac hmacWithSha256(String key) {
		return hmac(key, DigestType.HMAC_SHA256);
	}
	
	/**	
	 * 	构建HMac摘要
	 * 	<br>自定义密钥
	 * 	<br>SHA384摘要类型
	 * 	
	 * 	@author Pan
	 *  @param 	key			字符串密钥
	 * 	@return	HMac
	 */
	public static HMac hmacWithSha384(String key) {
		return hmac(key, DigestType.HMAC_SHA384);
	}
	
	/**	
	 * 	构建HMac摘要
	 * 	<br>自定义密钥
	 * 	<br>SHA512摘要类型
	 * 	
	 * 	@author Pan
	 *  @param 	key			字符串密钥
	 * 	@return	HMac
	 */
	public static HMac hmacWithSha512(String key) {
		return hmac(key, DigestType.HMAC_SHA512);
	}
	
	/**	
	 * 	构建HMac摘要
	 * 	<br>自定义密钥
	 * 	<br>自定义摘要类型
	 * 	
	 * 	@author Pan
	 *  @param 	key			字符串密钥
	 * 	@param 	digestType	摘要类型
	 * 	@return	HMac
	 */
	public static HMac hmac(String key, DigestType digestType) {
		return HMac.create(key, digestType);
	}
	
	/**	
	 * 	构建Sm3摘要
	 * 	
	 * 	@author Pan
	 * 	@return	SM3
	 */
	public static SM3 sm3() {
		return SM3.create();
	}
}
