package com.gitee.apanlh.util.algorithm.encrypt.symmetric;

/**
 * 对称加密类型算法类型枚举
 * 
 * <p>该枚举包含常用的对称加密算法类型，用于调用KeyGenerator.getInstance方法。</p>
 * 
 * <p>注意：可用的算法类型可能因密码提供者而异。</p>
 * 
 * <p>示例用法：</p>
 * <code>SymmetricType.AES</code>
 * <code>SymmetricType.DES</code>
 * <code>SymmetricType.DESEDE</code>
 * <code>SymmetricType.BLOWFISH</code>
 * <code>SymmetricType.RC2</code>
 * <code>SymmetricType.RC4</code>
 * 
 * <p>枚举值与KeyGenerator中的算法类型对应。</p>
 * <p>注：由于不同的JDK版本和密码提供者，可能支持的算法类型会有所不同。</p>
 * 
 * <p>以下枚举值基于常见的对称加密算法类型：</p>
 * <ul>
 *     <li>AES - 高级加密标准（Advanced Encryption Standard）</li>
 *     <li>DES - 数据加密标准（Data Encryption Standard）</li>
 *     <li>DESEDE - 三重DES（Triple DES）</li>
 *     <li>BLOWFISH - Blowfish算法</li>
 *     <li>RC2 - Rivest Cipher 2</li>
 *     <li>RC4 - Rivest Cipher 4</li>
 * </ul>
 * 
 * 	@author Pan
 * 	@see 	<a href="https://docs.oracle.com/en/java/javase/11/docs/specs/security/standard-names.html#keygenerator-algorithms">KeyGenerator Algorithms</a>
 */
public enum SymmetricType {
	
	/** AES 高级加密标准 默认AES/ECB/PKCS5Padding */
	AES("AES"),
	/** DES 数据加密标准 默认 DES/ECB/PKCS5Padding */
	DES("DES"),
	/** DESEDE 三重DES 默认DESede/CBC/PKCS5Padding */
	DESEDE("DESede"),
//	/** Blowfish算法 */
//	BLOWFISH("Blowfish"),
//	/** Rivest Cipher 2算法 */
//	RC2("RC2"),
//	/** Rivest Cipher 4算法 */
//	RC4("RC4"),
	/** PBE算法 */
	PBE("PBE"),
	/** SM4算法 */
	SM4("SM4");
	
	/** 算法 */
	private String algorithm;
	
	/**	
	 * 	构造函数-自定义算法类型
	 * 	
	 * 	@author Pan
	 * 	@param 	algorithm	算法
	 */
	SymmetricType(String algorithm) {
		this.algorithm = algorithm;
	}
	
	/**
	 * 	获取对称加密算法的类型字符串
	 * 	
	 * 	@author Pan
	 * 	@return String	对称加密算法类型字符串
	 */
	public String getAlgorithm() {
		return algorithm;
	}
}
