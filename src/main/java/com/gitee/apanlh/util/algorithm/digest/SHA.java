package com.gitee.apanlh.util.algorithm.digest;

import com.gitee.apanlh.exp.DigestException;
import com.gitee.apanlh.util.base.Eq;

/**
 * 	SHA摘要算法
 * 	<br>使用更多摘要算法请到{@link DigestUtils}类中查看
 * 	<br>SHA-2散列算法
 * 	
 * 	@author Pan
 */
public class SHA extends JdkDigestAbstract {
	
	/**
	 * 	默认构造函数
	 * 	<br>指定SHA2-256摘要算法
	 * 	
	 * 	@author Pan
	 */
	public SHA() {
		super(DigestType.SHA_256);
	}
	
	/**
	 * 	构造函数
	 * 	<br>自定义指定SHA2范围的摘要类型
	 * 	
	 * 	@author Pan
	 * 	@param 	digestType		摘要类型
	 * 	@throws 	DigestException 如果未找到对应摘要算法则抛出
	 */
	public SHA(DigestType digestType) {
		super(digestType);
	}
	
	@Override
	public void check() {
		if (!Eq.enumsOr(getDigestType(), 
				DigestType.SHA_1,
				DigestType.SHA_224, 
				DigestType.SHA_256, 
				DigestType.SHA_384, 
				DigestType.SHA_512)) {
			throw new DigestException("digest type error cause: expected type[{}] but actual type is[{}]", "SHA-1/SHA-224/SHA-256/SHA-384/SHA-512", getDigestType().getAlgorithm());
		}
	}
	
	/**	
	 * 	构建Sha摘要类
	 * 	<br>默认指定SHA2-256摘要算法
	 * 	
	 * 	@author Pan
	 * 	@return	SHA
	 */
	public static SHA create() {
		return new SHA();
	}
	
	/**	
	 * 	构建Sha摘要类
	 * 	<br>指定SHA2摘要类型
	 * 	
	 * 	@author Pan
	 * 	@param 	digestType	摘要类型
	 * 	@return	SHA
	 */
	public static SHA create(DigestType digestType) {
		return new SHA(digestType);
	}
}
