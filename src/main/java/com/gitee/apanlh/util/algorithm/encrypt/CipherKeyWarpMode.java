package com.gitee.apanlh.util.algorithm.encrypt;

import javax.crypto.Cipher;

/**
 * 	密钥包装模式枚举类，用于表示密钥包装和解包装的操作模式
 *  <br>WRAP：密钥包装操作模式，用于将密钥包装成加密的密文
 *  <br>UNWRAP：密钥解包操作模式，用于从加密的密文中解包装出原始密钥
 * 	<br>密钥包装是一种将密钥进行加密和解密的技术，用于在密钥交换或存储过程中保护密钥的安全性
 * 
 * 	@author Pan
 */
public enum CipherKeyWarpMode {
	
	/** 密钥包装操作模式，用于将密钥包装成加密的密文 */
	WRAP(Cipher.WRAP_MODE),
	/** 密钥解包操作模式，用于从加密的密文中解包装出原始密钥 */
	UNWRAP(Cipher.UNWRAP_MODE);
	
	/** 类型值 */
	private int mode;
	
	/**	
	 * 	构造函数
	 * 	<br>自定义类型
	 * 	
	 * 	@author Pan
	 * 	@param 	mode	类型
	 */
	CipherKeyWarpMode(int mode) {
		this.mode = mode;
	}
	
	/**
	 * 	获取类型值
	 * 	
	 * 	@author Pan
	 * 	@return	int
	 */
	public int getMode() {
		return mode;
	}
}
