package com.gitee.apanlh.util.algorithm.encrypt;

import com.gitee.apanlh.util.encode.Base64Type;

/**
 * 	加密/解密接口
 * 	<br>定义了常规加解密方法，支持对称和非对称加密
 * 	
 * 	@author Pan
 */
public interface Encrypt {
	
	/**	
	 * 	对字节数组进行加密计算
	 * 	
	 * 	@author Pan
	 * 	@param 	content	内容
	 * 	@return byte[]	加密后的结果
	 */
	byte[] encrypt(byte[] content);
    
	/**	
	 * 	对字符串进行加密计算
	 * 	
	 * 	@author Pan
	 * 	@param 	content	内容
	 * 	@return byte[]	加密后的结果
	 */
	byte[] encrypt(String content);
	
	 /**	
	 * 	对字节数组进行加密计算
	 * 	<br>以十六进制字符串形式返回加密结果
	 * 	<br>默认小写结果
	 * 	
	 * 	@author Pan
	 * 	@param 	content	内容
     * 	@return String	加密后的结果
     */
	String encryptToHex(byte[] content);
	
	/**	
	 * 	对字节数组进行加密计算
	 * 	<br>以十六进制字符串形式返回加密结果
     *  <br>自定义对加密结果大小写
     * 	
     * 	@author Pan
     * 	@param 	content		内容
     * 	@param 	toLowerCase	true为小写, false为大写
     * 	@return String		加密后的结果
     */
	String encryptToHex(byte[] content, boolean toLowerCase);
	
	 /**	
	 * 	对字符串进行加密计算
	 * 	<br>以十六进制字符串形式返回加密结果
     *  <br>默认小写结果
     * 	
     * 	@author Pan
     * 	@param 	content		内容
     * 	@return String		加密后的结果
     */
	String encryptToHex(String content);
	
	/**	
	 * 	对字符串进行加密计算
	 * 	<br>以十六进制字符串形式返回加密结果
     *  <br>自定义对加密结果大小写
     * 	
     * 	@author Pan
     * 	@param 	content		内容
     * 	@param 	toLowerCase	true为小写, false为大写
     * 	@return String		加密后的结果
     */
	String encryptToHex(String content, boolean toLowerCase);
	
	/**	
	 * 	对字节数组进行加密计算
	 * 	<br>以Base64编码返回加密结果(RFC4648)
     * 	
     * 	@author Pan
     * 	@param 	content		内容
     * 	@return String		加密后的结果
     */
	byte[] encryptToBase64(byte[] content);

	/**
	 * 	对字节数组进行加密计算
	 * 	<br>自定义Base64编码返回加密结果
     *
     * 	@author Pan
     * 	@param 	content		内容
     * 	@param 	base64Type	Base64编码类型
     * 	@return String		加密后的结果
     */
	byte[] encryptToBase64(byte[] content, Base64Type base64Type);

	/**	
	 * 	对字符串进行加密计算
	 * 	<br>以Base64编码返回加密结果(RFC4648)
     * 	
     * 	@author Pan
     * 	@param 	content		内容
     * 	@return String		加密后的结果
     */
	byte[] encryptToBase64(String content);

	/**
	 * 	对字符串进行加密计算
	 * 	<br>自定义Base64编码返回加密结果
     *
     * 	@author Pan
     * 	@param 	content		内容
	 * 	@param 	base64Type	Base64编码类型
     * 	@return String		加密后的结果
     */
	byte[] encryptToBase64(String content, Base64Type base64Type);

	/**	
	 * 	对字节数组进行加密计算
	 * 	<br>以Base64编码返回加密结果(RFC4648)
     * 	
     * 	@author Pan
     * 	@param 	content		内容
     * 	@return String		加密后的结果
     */
	String encryptToBase64Str(byte[] content);

	/**
	 * 	对字节数组进行加密计算
	 * 	<br>自定义Base64编码返回加密结果
     *
     * 	@author Pan
     * 	@param 	content		内容
	 * 	@param 	base64Type	Base64编码类型
     * 	@return String		加密后的结果
     */
	String encryptToBase64Str(byte[] content, Base64Type base64Type);

	/**	
	 * 	对字符串进行加密计算
	 * 	<br>以Base64编码返回加密结果(RFC4648)
     *
     * 	@author Pan
     * 	@param 	content		内容
     * 	@return String		加密后的结果
     */
	String encryptToBase64Str(String content);

	/**
	 * 	对字符串进行加密计算
	 * 	<br>自定义Base64编码返回加密结果
     *
     * 	@author Pan
     * 	@param 	content		内容
	 * 	@param 	base64Type	Base64编码类型
     * 	@return String		加密后的结果
     */
	String encryptToBase64Str(String content, Base64Type base64Type);

	/**	
	 * 	对字节数组进行解密计算
	 * 	
	 * 	@author Pan
	 * 	@param 	content	内容
	 * 	@return byte[]	解密后的结果
	 */
	byte[] decrypt(byte[] content);
    
	/**	
	 * 	对十六进制字符串进行解密计算
	 * 	
	 * 	@author Pan
	 * 	@param 	content	内容
	 * 	@return byte[]	解密后的结果
	 */
	byte[] decryptFromHex(String content);
	
	/**	
	 * 	对十六进制字符串进行解密计算
	 * 	
	 * 	@author Pan
	 * 	@param 	content	内容
	 * 	@return String	解密后的结果
	 */
	String decryptFromHexStr(String content);

	/**	
	 * 	对Base64字节数组进行解密计算
	 * 	
	 * 	@author Pan
	 * 	@param 	content	内容
	 * 	@return byte[]	解密后的结果
	 */
	byte[] decryptFromBase64(byte[] content);

	/**	
	 * 	对Base64字节数组进行解密计算
	 * 	<br>自定义Base64编码类型
	 * 	
	 * 	@author Pan
	 * 	@param 	content		内容
	 * 	@param 	base64Type	Base64编码类型
	 * 	@return byte[]	解密后的结果
	 */
	byte[] decryptFromBase64(byte[] content, Base64Type base64Type);
	
	/**	
	 * 	对Base64字符串进行解密计算
	 * 	
	 * 	@author Pan
	 * 	@param 	content	内容
	 * 	@return byte[]	解密后的结果
	 */
	byte[] decryptFromBase64(String content);
	
	/**	
	 * 	对Base64字符串进行解密计算
	 * 	<br>自定义Base64编码类型
	 * 
	 * 	@author Pan
	 * 	@param 	content	内容
	 * 	@param 	base64Type	Base64编码类型
	 * 	@return byte[]	解密后的结果
	 */
	byte[] decryptFromBase64(String content, Base64Type base64Type);
	
	/**	
	 * 	对Base64字节数组进行解密计算
	 * 	
	 * 	@author Pan
	 * 	@param 	content	内容
	 * 	@return String	解密后的结果
	 */
	String decryptFromBase64Str(byte[] content);
	
	/**	
	 * 	对Base64字节数组进行解密计算
	 * 	<br>自定义Base64编码类型
	 * 	
	 * 	@author Pan
	 * 	@param 	content		内容
	 * 	@param 	base64Type	Base64编码类型
	 * 	@return String	解密后的结果
	 */
	String decryptFromBase64Str(byte[] content, Base64Type base64Type);
	
	/**	
	 * 	对Base64字符串进行解密计算
	 * 	
	 * 	@author Pan
	 * 	@param 	content	内容
	 * 	@return String	解密后的结果
	 */
	String decryptFromBase64Str(String content);
	
	/**	
	 * 	对Base64字符串进行解密计算
	 * 	<br>自定义Base64编码类型
	 * 
	 * 	@author Pan
	 * 	@param 	content	内容
	 * 	@param 	base64Type	Base64编码类型
	 * 	@return String	解密后的结果
	 */
	String decryptFromBase64Str(String content, Base64Type base64Type);
}
