package com.gitee.apanlh.util.algorithm.encrypt.asymmetric;

import org.bouncycastle.crypto.engines.SM2Engine.Mode;

/**
 * 	用于SM2加解密模式枚举类
 * 	<br>主要为了避免重名
 * 	
 * 	@author Pan
 */
public enum SM2Mode {
	
	/** C1C2C3模式 */
	C1C2C3(Mode.C1C2C3),
	/** C1C3C2模式 */
	C1C3C2(Mode.C1C3C2);
	
	/** SM2模式 */
	private Mode mode;
	
	/**
	 * 	构造函数-初始化模式
	 * 	
	 * 	@author Pan
	 * 	@param 	mode	SM2模式
	 */
	SM2Mode(Mode mode) {
		this.mode = mode;
	}

	/**
	 * 	获取模式
	 * 	
	 * 	@author Pan
	 * 	@return	Mode
	 */
	public Mode getMode() {
		return mode;
	}
}
