package com.gitee.apanlh.util.algorithm.encrypt.asymmetric;

import com.gitee.apanlh.util.algorithm.encrypt.CipherMode;

import javax.crypto.Cipher;
import java.security.Key;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.spec.KeySpec;

/**
 *  默认JDK实现非对称加密抽象类
 *
 *  @author Pan
 */
public class JdkCastleAsymmetricAbstract extends CustomAsymmetricAbstract implements AsymmetricEncode {

    @Override
    public Cipher initCipher(byte[] key, byte[] iv, CipherMode cipherMode) {
        return null;
    }

    @Override
    public byte[] doFinal(CipherMode cipherMode, byte[] content) {
        return new byte[0];
    }

    @Override
    public KeyPair initKeyPair(int keyLength) {
        return null;
    }

    @Override
    public Provider getProvider() {
        return null;
    }

    @Override
    public PublicKey getPublicKey() {
        return null;
    }

    @Override
    public PrivateKey getPrivateKey() {
        return null;
    }

    @Override
    public Key getPublicKey(byte[] publicKey) {
        return null;
    }

    @Override
    public Key getPrivateKey(byte[] privateKey) {
        return null;
    }

    @Override
    public Key getPublicKey(KeySpec publicKey) {
        return null;
    }

    @Override
    public Key getPrivateKey(KeySpec privateKey) {
        return null;
    }

    @Override
    public Key generateKey(byte[] publicKey, byte[] privateKey, CipherMode cipherMode) {
        return null;
    }

    @Override
    public Key generateKey(KeySpec publicKey, KeySpec privateKey, CipherMode cipherMode) {
        return null;
    }

    @Override
    public byte[] getPublicEncode() {
        return new byte[0];
    }

    @Override
    public String getPublicEncodeToHex() {
        return null;
    }

    @Override
    public byte[] getPublicEncodeToBase64() {
        return new byte[0];
    }

    @Override
    public String getPublicEncodeToBase64Str() {
        return null;
    }

    @Override
    public byte[] getPrivateEncode() {
        return new byte[0];
    }

    @Override
    public String getPrivateEncodeToHex() {
        return null;
    }

    @Override
    public byte[] getPrivateEncodeToBase64() {
        return new byte[0];
    }

    @Override
    public String getPrivateEncodeToBase64Str() {
        return null;
    }
}
