package com.gitee.apanlh.util.algorithm.digest;

/**	
 * 	摘要接口
 * 	<br>定义了对摘要的基本方法
 * 	
 * 	@author Pan
 * 	@see	JdkDigestAbstract
 */
public interface Digest {
	
	/**	
	 * 	对字节数组进行摘要计算
	 * 	
	 * 	@author Pan
	 * 	@param 	content	内容
	 * 	@return byte[]	摘要后的结果
	 */
	byte[] digest(byte[] content);
    
	/**	
	 * 	对字符串进行摘要计算
	 * 	
	 * 	@author Pan
	 * 	@param 	content	内容
	 * 	@return byte[]	摘要后的结果
	 */
	byte[] digest(String content);
	
	 /**	
	 * 	对字节数组进行摘要计算
	 * 	<br>以十六进制字符串形式返回摘要结果
	 * 	<br>默认小写结果
	 * 	
	 * 	@author Pan
	 * 	@param 	content	内容
     * 	@return String	摘要后的结果
     */
	String digestToHex(byte[] content);
	
	/**	
	 * 	对字节数组进行摘要计算
	 * 	<br>以十六进制字符串形式返回摘要结果
     *  <br>自定义对摘要结果大小写
     * 	
     * 	@author Pan
     * 	@param 	content		内容
     * 	@param 	toLowerCase	true为小写, false为大写
     * 	@return String		摘要后的结果
     */
	String digestToHex(byte[] content, boolean toLowerCase);
	
	 /**	
	 * 	对字符串进行摘要计算
	 * 	<br>以十六进制字符串形式返回摘要结果
     *  <br>默认小写结果
     * 	
     * 	@author Pan
     * 	@param 	content		内容
     * 	@return String		摘要后的结果
     */
	String digestToHex(String content);
	
	/**	
	 * 	对字符串进行摘要计算
	 * 	<br>以十六进制字符串形式返回摘要结果
     *  <br>自定义对摘要结果大小写
     * 	
     * 	@author Pan
     * 	@param 	content		内容
     * 	@param 	toLowerCase	true为小写, false为大写
     * 	@return String		摘要后的结果
     */
	String digestToHex(String content, boolean toLowerCase);
	
	/**	
	 * 	对字节数组进行摘要计算
	 * 	<br>以Base64编码返回摘要结果(RFC4648)
     * 	
     * 	@author Pan
     * 	@param 	content		内容
     * 	@return String		摘要后的结果
     */
	byte[] digestToBase64(byte[] content);
    
	/**	
	 * 	对字符串进行摘要计算
	 * 	<br>以Base64编码返回摘要结果(RFC4648)
     * 	
     * 	@author Pan
     * 	@param 	content		内容
     * 	@return String		摘要后的结果
     */
	byte[] digestToBase64(String content);
	
	/**	
	 * 	对字节数组进行摘要计算
	 * 	<br>以Base64编码返回摘要结果(RFC4648)
     * 	
     * 	@author Pan
     * 	@param 	content		内容
     * 	@return String		摘要后的结果
     */
	String digestToBase64Str(byte[] content);
    
	/**	
	 * 	对字符串进行摘要计算
	 * 	<br>以Base64编码返回摘要结果(RFC4648)
     * 	
     * 	@author Pan
     * 	@param 	content		内容
     * 	@return String		摘要后的结果
     */
	String digestToBase64Str(String content);
}
