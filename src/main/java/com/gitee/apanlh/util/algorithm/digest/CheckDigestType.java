package com.gitee.apanlh.util.algorithm.digest;

import com.gitee.apanlh.exp.DigestException;

/**
 * 	定义了检测包含摘要类型范围接口
 * 	
 * 	@author Pan
 */
public interface CheckDigestType {
	
	/**	
	 * 	检测摘要类型
	 * 	
	 * 	@author Pan
	 * 	@throws DigestException 如果对应的摘要类，没有指定对应的摘要类型，则抛出
	 */
	void check() throws DigestException;
}
