package com.gitee.apanlh.util.algorithm.encrypt;

/**
 * 	常见加解密器模式(分组模式)
 *  <a href="https://blog.csdn.net/u013073067/article/details/87086562">https://blog.csdn.net/u013073067/article/details/87086562</a>
 *
 *  @author Pan
 */
public enum AlgorithmMode {
	
	/** 无模式 */
	NONE("NONE"),
	/** Electronic CodeBook mode 电子密码本模式 */ 
	ECB("ECB"),
	/** Cipher Block Chaining mode 加密块链模式 */
	CBC("CBC"),
	/** Propagating cipher-block chaining 填充密码块链接模式 */
	PCBC("PCBC"),
	/** Cipher FeedBack mode 密文反馈模式 */
	CFB("CFB"),
	/** Output FeedBack mode 输出反馈模式 */
	OFB("CTR"),
	/** CountTeR mode 计数器模式 */
	CTR("CTR"),
	/** Cipher Text Stealing 密码文本窃取模式 */
	CTS("CTS");
	
	/** 模式 */
	private String mode;
		
	/**	
	 * 	构造函数-定义模式
	 * 	
	 * 	@author Pan
	 * 	@param  mode 模式
	 */
	AlgorithmMode(String mode) {
		this.mode = mode;
	}

	/**
	 * 	获取模式
	 * 
	 * 	@author Pan
	 * 	@return String
	 */
	public String getMode() {
		return mode;
	}
}
