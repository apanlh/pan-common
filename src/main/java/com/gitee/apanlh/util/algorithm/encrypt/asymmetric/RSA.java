package com.gitee.apanlh.util.algorithm.encrypt.asymmetric;

import java.math.BigInteger;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * 	RSA非对称加密
 * 	<br>RSA公钥/私钥/签名加密解密
 *	<br>使用该类需要加载BouncyCastle库
 *	<br>默认实现了加解密方式
 *	TODO 区分BC以及JDK实现不同方式
 *  TODO BC是none 而JDK是 ECB
 * 	@author Pan
 */
public class RSA extends BouncyCastleAsymmetricAbstract {

	/**
	 * 	构造函数-使用默认RSA算法
	 * 	<br>默认使用1024位长度
	 * 	
	 * 	@author Pan
	 */
	public RSA() {
		super(AsymmetricType.RSA);
	}
	
	/**	
	 * 	构造函数-使用默认RSA算法
	 * 	<br>如果只传递公钥可用于加密
	 * 	<br>如果只传递私钥可用于解密
	 * 
	 * 	@author Pan
	 * 	@param 	publicKey	公钥字节
	 * 	@param 	privateKey	私钥字节
	 */
	public RSA(byte[] publicKey, byte[] privateKey) {
		super(publicKey, privateKey, AsymmetricType.RSA);
	}
	
	/**	
	 * 	构造函数-使用默认RSA算法
	 * 	<br>如果字符串为hex或者base64编码格式将自动还原无需额外操作
	 * 	<br>如果只传递公钥可用于加密
	 * 	<br>如果只传递私钥可用于解密
	 * 	
	 * 	@author Pan
	 * 	@param 	publicKey	公钥字符串
	 * 	@param 	privateKey	私钥字符串
	 */
	public RSA(String publicKey, String privateKey) {
		super(publicKey, privateKey, AsymmetricType.RSA);
	}
	
	/**	
	 * 	构造函数-根据公共指数或者公钥/私钥特征来进行还原
	 * 	<br>公共指数必须传递
	 * 	<br>根据加密或者解密的需求来任意传递公钥特征或私钥特征(任意一个即可)
	 * 
	 * 	@author Pan
	 * 	@param 	modulus			公共指数
	 * 	@param 	publicExponent	公钥特征值
	 * 	@param 	privateExponent	私钥特征值
	 */
	public RSA(BigInteger modulus, BigInteger publicExponent, BigInteger privateExponent) {
		super(modulus, publicExponent, privateExponent, AsymmetricType.RSA);
	}
	
	/**	
	 * 	构造函数-根据公共指数或者公钥/私钥对象进行加解密
	 * 	<br>根据加密或者解密的需求来任意传递(任意一个即可)
	 * 
	 * 	@author Pan
	 * 	@param 	publicKey	公钥对象
	 * 	@param 	privateKey	私钥对象
	 */
	public RSA(PublicKey publicKey, PrivateKey privateKey) {
		super(publicKey, privateKey, AsymmetricType.RSA);
	}
}
