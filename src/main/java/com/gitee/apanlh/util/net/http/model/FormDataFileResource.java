package com.gitee.apanlh.util.net.http.model;

/**
 * 	FormData文件资源
 * 	<br>便于文件传输
 * 	<br>在实体类字段中可以使用此类型，方便用于byte[]没有来源文件名称问题
 * 	
 * 	@author Pan
 */
public class FormDataFileResource {
	
	/** 文件内容 */
	private byte[] bytes; 
	/** 来源文件名称 */
	private String originalFilename;

	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	public FormDataFileResource() {
		super();
	}
	
	/**	
	 * 	构造函数
	 * 	<br>自定义内容及来源文件名称
	 * 	
	 * 	@author Pan
	 * 	@param 	bytes				内容
	 * 	@param 	originalFilename	来源文件名称
	 */
	public FormDataFileResource(byte[] bytes, String originalFilename) {
		super();
		this.bytes = bytes;
		this.originalFilename = originalFilename;
	}
	
	/**	
	 * 	获取字节数组	
	 * 	
	 * 	@author Pan
	 * 	@return	byte[]
	 */
	public byte[] getBytes() {
		return bytes;
	}
	
	/**	
	 * 	设置字节数组	
	 * 	
	 * 	@author Pan
	 * 	@param	bytes 字节数组
	 */
	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	/**	
	 * 	获取来源文件名称	
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getOriginalFilename() {
		return originalFilename;
	}

	/**	
	 * 	设置来源文件名称
	 * 	
	 * 	@author Pan
	 * 	@param	originalFilename	来源文件名称
	 */
	public void setOriginalFilename(String originalFilename) {
		this.originalFilename = originalFilename;
	}
	
	/**	
	 * 	创建FormDataFileResource
	 * 	
	 * 	@author Pan
	 * 	@return	FormDataFileResource
	 */
	public static FormDataFileResource create() {
		return new FormDataFileResource();
	}
	
	/**	
	 * 	创建FormDataFileResource
	 * 	<br>自定义内容及来源文件名称
	 * 
	 * 	@author Pan
	 * 	@param 	bytes				内容
	 * 	@param 	originalFilename	来源文件名称
	 * 	@return	FormDataFileResource
	 */
	public static FormDataFileResource create(byte[] bytes, String originalFilename) {
		return new FormDataFileResource(bytes, originalFilename);
	}
}
