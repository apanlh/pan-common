package com.gitee.apanlh.util.net.http.io;

import java.io.InputStream;

/**
 * 	HTTP错误输入流
 * 	
 * 	@author Pan
 */
public class HttpErrorInputStream extends HttpInputStream {

	/**	
	 * 	构造函数-引用原有HTTP错误输入流
	 * 	
	 * 	@author Pan
	 * 	@param 	inputStream	输入流
	 */
	public HttpErrorInputStream(InputStream inputStream) {
		super(inputStream);
	}
	
}
