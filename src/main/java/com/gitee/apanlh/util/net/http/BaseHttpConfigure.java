package com.gitee.apanlh.util.net.http;

import com.gitee.apanlh.exp.HttpConfigException;
import com.gitee.apanlh.exp.HttpException;

/**
 * 	基础HTTTConfigure配置接口
 * 	
 * 	@author Pan
 */
public interface BaseHttpConfigure {
	
	/**	
	 * 	自定义装配方法
	 * 	
	 * 	@author Pan
	 * 	@param 	httpRequest 	HTTP请求对象
	 * 	@param 	httpClient  	HTTP客户端对象
	 * 	@return	HttpRequest 	HTTP请求对象
	 * 	@throws HttpException    装配时处理异常，则抛出该异常
	 */
	HttpRequest configure(HttpRequest httpRequest, HttpClient httpClient) throws HttpConfigException;
}
