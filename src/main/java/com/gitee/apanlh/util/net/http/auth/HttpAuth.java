package com.gitee.apanlh.util.net.http.auth;

/**
 * 	HTTP请求认证抽象类
 * 	
 * 	@author Pan
 */
public abstract class HttpAuth {
	
	/** 请求头Key */
	private String headerKey;
	/** 请求头value */
	private String headerValue;
	
	/**	
	 * 	创建Bearer
	 * 	<br>请求头示例:authorization=Bearer 123456
	 *  
	 * 	@author Pan
	 * 	@param 	token	认证请求头
	 * 	@return	HttpAuthBearer
	 */
	public static HttpAuthBearer bearer(String token) {
		return HttpAuthBearer.create(token);
	}
	
	/**	
	 * 	创建Basic
	 * 	<br>请求头示例:authorization=Basic NDI6NDEy
	 *  
	 * 	@author Pan
	 * 	@param 	userName	用户名
	 * 	@param 	password	密码
	 * 	@return	HttpAuthBasic
	 */
	public static HttpAuthBasic basic(String userName, String password) {
		return HttpAuthBasic.create(userName, password);
	}
	
	/**	
	 * 	创建自定义认证
	 * 	<br>不会添加authorization
	 * 	<br>key为请求头
	 * 	<br>value=请求头值
	 * 	<br>key = token value = 123456，请求头示例: token:123456
	 *  
	 * 	@author Pan
	 * 	@param 	key		用户名
	 * 	@param 	value	密码
	 * 	@return	HttpAuthCustomer
	 */
	public static HttpAuthCustom custom(String key, String value) {
		return HttpAuthCustom.create(key, value);
	}
	
	/**	
	 * 	获取请求头key
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getHeaderKey() {
		return headerKey;
	}

	/**	
	 * 	获取请求头value
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getHeaderValue() {
		return headerValue;
	}
	
	/**	
	 * 	设置请求头key
	 * 	
	 * 	@author Pan
	 * 	@param 	headerKey	请求头键
	 */
	public void setHeaderKey(String headerKey) {
		this.headerKey = headerKey;
	}
	
	/**	
	 * 	设置请求头value
	 * 	
	 * 	@author Pan
	 * 	@param 	headerValue	请求头值
	 */
	public void setHeaderValue(String headerValue) {
		this.headerValue = headerValue;
	}
	
	/**	
	 * 	设置请求头key和请求头value
	 * 	
	 * 	@author Pan
	 * 	@param 	key		键
	 * 	@param 	value	值
	 */
	public void setHeader(String key, String value) {
		this.headerKey = key;
		this.headerValue = value;
	}
}
