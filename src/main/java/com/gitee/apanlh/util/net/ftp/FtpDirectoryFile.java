package com.gitee.apanlh.util.net.ftp;

import com.gitee.apanlh.exp.FtpFileException;
import com.gitee.apanlh.util.base.CollUtils;
import com.gitee.apanlh.util.base.Empty;
import com.gitee.apanlh.util.base.StringUtils;
import com.gitee.apanlh.util.valid.ValidParam;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPFileFilter;

import java.util.Arrays;
import java.util.List;

/**	
 * 	FTP工作目录空间
 * 
 * 	@author Pan
 */
class FtpDirectoryFile implements FtpCommandDirectoryFile {

	/** FTP命令器 */
	private FtpCommandExecutor commandExecutor;
	/** 目录地址对象 */
	private FtpCommandDirectoryPath directoryPath;
	/** 执行器 */
	private FTPClient executor;
	
	/**
	 * 	构造函数-默认
	 * 	@author Pan
	 */
	FtpDirectoryFile() {}
	
	/**	
	 * 	构造函数-获取执行器
	 * 	
	 * 	@author Pan
	 *	@param 	commandExecutors	命令执行器
	 */
	public FtpDirectoryFile(FtpCommandExecutor commandExecutors) {
		this.commandExecutor = commandExecutors;
		this.executor = commandExecutors.getExecutor();
	}
	
	@Override
	public FTPFile getFile(String findFileName) {
		FTPFile[] files = getFiles();
		for (int i = 0, len = files.length; i < len; i++) {
			FTPFile ftpFile = files[i];
			if (ftpFile.isFile() && StringUtils.eq(ftpFile.getName(), findFileName)) {
				return ftpFile;
			}
		}
		return null;
	}

	@Override
	public FTPFile[] getFiles() {
		try {
			return executor.listFiles();
		} catch (Exception e) {
			throw new FtpFileException(e.getMessage(), e);
		}
	}
	
	@Override
	public FTPFile[] getFiles(String pathname) {
		try {
			return executor.listFiles(pathname);
		} catch (Exception e) {
			throw new FtpFileException(e.getMessage(), e);
		}
	}
	
	@Override
	public FTPFile[] getFiles(String pathname, FTPFileFilter fileFilter) {
		try {
			return executor.listFiles(pathname, fileFilter);
		} catch (Exception e) {
			throw new FtpFileException(e.getMessage(), e);
		}
	}
	
	@Override
	public List<FTPFile> getFiles(List<String> findFileName) {
		FTPFile[] fileArray = getFiles();
		List<FTPFile> listFiles = null;
		
		for (int i = 0, len = fileArray.length; i < len; i++) {
			FTPFile ftpFile = fileArray[i];
			//	验证是否为文件类型
			if (ftpFile.isFile()) {
				String fileName = ftpFile.getName();
				//	搜索需要寻找的文件目标
				for (int j = 0, findLen = findFileName.size(); j < findLen; j++) {
					if (StringUtils.eq(fileName, findFileName.get(j))) {
						if (listFiles == null) {
							listFiles = CollUtils.newArrayList(findFileName.size());
						}
						listFiles.add(ftpFile);
					}
				}
			}
		}
		return listFiles;
	}
	
	@Override
	public boolean isExists(String fileName) {
		if (ValidParam.isEmpty(fileName)) {
			return false;
		}
		return isExists(directoryPath.getCurrentPath(), fileName);
	}
	
	@Override
	public boolean isExists(String targetDir, final String fileName) {
		if (ValidParam.isEmpty(fileName)) {
			return false;
		}
		directoryPath.changeWorkingDirectory(targetDir);
		return getFiles(fileName) != null;
	}
	
	@Override
	public int[] isExists(String targetDir, final List<String> needFindFileNames) {
		if (CollUtils.isEmpty(needFindFileNames)) {
			return Empty.arrayInt();
		}
		directoryPath.changeWorkingDirectory(targetDir);
		
		List<FTPFile> listFindFtpFile = getFiles(needFindFileNames);
		//	存放未匹配上的文件下标
		int [] missingArr;
		//	出现全部不匹配
		if (CollUtils.isEmpty(listFindFtpFile)) {
			int size = needFindFileNames.size();
			missingArr = new int[size];
			int i = 0;
			while (i < size) {
				missingArr[i] = i;
				i ++;
			}
			return missingArr;
		}
		
		//	全匹配
		int needFindLen = needFindFileNames.size();
		int findLen = listFindFtpFile.size();
		if (needFindLen == findLen) {
			return Empty.arrayInt();
		}
		
		//	部分不匹配
		missingArr = new int[needFindLen - findLen];
		int missingArrIndex = 0;
		for (int i = 0; i < needFindLen; i++) {
			String needFindName = needFindFileNames.get(i);
			boolean continueFlag = false;

			for (int j = 0; j < findLen; j++) {
				//	找到相同匹配的值则跳出主循环
				if (needFindName.equals(listFindFtpFile.get(j).getName())) {
					continueFlag = true;
					break;
				}
			}
			if (continueFlag) {
				continue;
			}
			//	添加不同匹配的值
			missingArr[missingArrIndex++] = i;
		}
		return missingArr;
	}
	
	@Override
	public boolean isAllExists(final String[] needFindFileNames) {
		if (needFindFileNames == null || needFindFileNames.length == 0) {
			return false;
		}
		return isAllExists(directoryPath.getCurrentPath(), needFindFileNames);
	}
	
	@Override
	public boolean isAllExists(String targetDir, final String[] needFindFileNames) {
		if (needFindFileNames == null || needFindFileNames.length == 0) {
			return false;
		}
		return isAllExists(targetDir, Arrays.asList(needFindFileNames));
	}
	
	@Override
	public boolean isAllExists(String targetDir, final List<String> needFindFileNames) {
		if (CollUtils.isEmpty(needFindFileNames)) {
			return false;
		}
		directoryPath.changeWorkingDirectory(targetDir);
		
		List<FTPFile> listFindFtpFile = getFiles(needFindFileNames);
		if (CollUtils.isEmpty(listFindFtpFile)) {
			return false;
		}
		//	简化对比流程 只对比是否长度一致,此getFiles()方法已经找出相对应的文件
		return listFindFtpFile.size() == needFindFileNames.size();
	}
	
	@Override
	public void beforeConfigure() {
		//	初始化加载
		this.directoryPath = commandExecutor.getDirectory().getDirectoryPath();
	}

	@Override
	public void afterConfigure() {
		//	空实现
	}
}
