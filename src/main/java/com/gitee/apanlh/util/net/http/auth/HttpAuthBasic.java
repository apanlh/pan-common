package com.gitee.apanlh.util.net.http.auth;

import com.gitee.apanlh.util.base.StringUtils;
import com.gitee.apanlh.util.encode.Base64Utils;

/**
 * 	Basic请求认证
 * 	
 * 	@author Pan
 */
public class HttpAuthBasic extends HttpAuth {
	
	/** 用户名 */
	private String userName;
	/** 密码 */
	private String password;
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	public HttpAuthBasic() {
		super();
	}
	
	/**	
	 * 	构造函数-初始化用户密码
	 * 	<br>base64编码格式(用户名:密码)
	 * 
	 * 	@author Pan
	 * 	@param 	userName	用户名
	 * 	@param 	password	密码
	 */
	public HttpAuthBasic(String userName, String password) {
		this.userName = userName;
		this.password = password;
		
		String merge = StringUtils.append(userName, ":", password);
		super.setHeader(HttpAuthProtocol.AUTHORIZATION, StringUtils.append(HttpAuthProtocol.BASIC.getType(), " ", Base64Utils.encodeToStr(merge)));
	}

	/**	
	 * 	获取用户名	
	 * 
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getUserName() {
		return userName;
	}

	/**	
	 * 	获取密码
	 * 
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getPassword() {
		return password;
	}
	
	/**	
	 * 	设置用户名	
	 * 
	 * 	@author Pan
	 * 	@param 	userName	用户名
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**	
	 * 	设置密码
	 * 
	 * 	@author Pan
	 * 	@param 	password	密码
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**	
	 * 	创建Basic
	 * 	
	 * 	@author Pan
	 * 	@param 	userName	用户名
	 * 	@param 	password	密码
	 * 	@return	HttpAuthBasic
	 */
	public static HttpAuthBasic create(String userName, String password) {
		return new HttpAuthBasic(userName, password);
	}
}
