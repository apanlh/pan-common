package com.gitee.apanlh.util.net.http.handler;

import com.gitee.apanlh.exp.HttpException;

import java.util.List;

/**	
 * 	初始化加载拦截器
 * 	
 * 	@author Pan
 */
interface InitializeInterceptors<T extends HttpInterceptor> {
	
	/**	
	 * 	加载执行器
	 * 	
	 * 	@author Pan
	 * 	@throws HttpException 异常抛出
	 */
	void loadExecute() throws HttpException;
	
	/**	
	 * 	单个加载
	 * 	
	 * 	@author Pan
	 *  @param  t 对象
	 * 	@throws HttpException 异常抛出
	 */
	void load(T t) throws HttpException;
	
	/**	
	 * 	多个加载
	 * 	
	 * 	@author Pan
	 * 	@param 	listHttpInterceptor	拦截集合
	 * 	@throws HttpException 异常抛出
	 */
	void load(List<HttpInterceptor> listHttpInterceptor) throws HttpException;
}
