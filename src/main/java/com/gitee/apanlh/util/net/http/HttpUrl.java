package com.gitee.apanlh.util.net.http;

import com.gitee.apanlh.util.base.StringUtils;

/**	
 * 	请求地址对象
 * 	
 * 	@author Pan
 */
public class HttpUrl {
	
	/** 请求地址  */
	private StringBuilder urlBuilder;
	/** 添加或者修改标识 */
	private boolean hasModified;
	
	/**
	 * 	构造函数-初始化请求地址
	 * 	
	 * 	@author Pan
	 * 	@param 	url 字符串
	 */
	public HttpUrl(String url) {
		this.urlBuilder = StringUtils.createBuilder(url);
	}

	/**
	 * 	获取-URL地址
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String get() {
		return urlBuilder.toString();
	}
	
	/**
	 * 	将会修改当前字符串
	 * 	
	 * 	@author Pan
	 * 	@param 	str 字符串
	 */
	public void modify(String str) {
		hasModified = true;
		urlBuilder.setLength(0);
		urlBuilder.append(str);
	}
	
	/**
	 * 	获取-URLBuilder
	 * 	
	 * 	@author Pan
	 * 	@return	StringBuilder
	 */
	public StringBuilder getBuilder() {
		return urlBuilder;
	}
	
	/**
	 * 	添加-请求地址
	 * 	
	 * 	@author Pan
	 * 	@param 	str 字符串
	 * 	@return	HttpUrl	HttpUrl对象
	 */
	public HttpUrl append(String str) {
		hasModified = true;
		urlBuilder.append(str);
		return this;
	}
	
	@Override
	public String toString() {
		return get();
	}
	
	/**	
	 * 	表示字符串是否修改或者添加过
	 * 	
	 * 	@author Pan
	 * 	@return	boolean	true是
	 */
	public boolean hasModified() {
		return hasModified;
	}

	/**
	 * 	构建HTTPURL对象
	 * 	
	 * 	@author Pan
	 * 	@param 	url		请求地址
	 * 	@return	HttpUrl
	 */
	public static HttpUrl create(String url) {
		return new HttpUrl(url);
	}
}
