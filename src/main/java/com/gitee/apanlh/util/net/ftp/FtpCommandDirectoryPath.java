package com.gitee.apanlh.util.net.ftp;

/**	
 * 	目录相关指令
 * 
 * 	@author Pan
 */
public interface FtpCommandDirectoryPath extends FtpConfigureHandler {
	
	/**	
	 * 	切换工作目录
	 * 	
	 * 	@author Pan
	 * 	@param 	targetDir	目标目录
	 * 	@return	boolean
	 */
	boolean changeWorkingDirectory(String targetDir);
	
	/**	
	 * 	获取当前目录
	 * 
	 * 	@author Pan
	 * 	@return	String
	 */
	String getCurrentPath();
	
	/**	
	 * 	获取当前所有的工作空间
	 * 
	 * 	@author Pan
	 * 	@return	String[]
	 */
	String[] getCurrentPaths();
	
	/**	
	 * 	获取目录
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	String getDirectoryPath();
	
	/**	
	 * 	获取目录下所有的工作空间
	 * 	
	 * 	@author Pan
	 * 	@return	String[]
	 */
	String[] getDirectoryPathAll();
}
