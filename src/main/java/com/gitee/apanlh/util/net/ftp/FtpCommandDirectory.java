package com.gitee.apanlh.util.net.ftp;

/**	
 * 	获取目录读取以及文件对象
 * 
 * 	@author Pan
 */
public interface FtpCommandDirectory extends FtpConfigureHandler {

	/**	
	 * 	获取目录地址对象
	 * 	
	 * 	@author Pan
	 * 	@return	FtpCommandDirectoryPath
	 */
	FtpCommandDirectoryPath getDirectoryPath();
	
	/**	
	 * 	获取目录文件对象
	 * 	
	 * 	@author Pan
	 * 	@return	FtpCommandDirectoryFile
	 */
	FtpCommandDirectoryFile getDirectoryFile();
}
