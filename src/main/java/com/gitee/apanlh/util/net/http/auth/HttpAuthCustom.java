package com.gitee.apanlh.util.net.http.auth;

/**
 * 	基础认证形式，Key-Value形式
 * 	<br>自定义认证
 * 	
 * 	@author Pan
 */
public class HttpAuthCustom extends HttpAuth {
	
	/** 键 */
	private String key;
	/** 值 */
	private String value;
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */	
	public HttpAuthCustom() {
		super();
	}
	
	/**	
	 * 	构造函数-初始化键值
	 * 	
	 * 	@author Pan
	 * 	@param 	key		键
	 * 	@param 	value	值
	 */
	public HttpAuthCustom(String key, String value) {
		this.key = key;
		this.value = value;
		
		setHeader(key, value);
	}

	/**	
	 * 	获取键
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getKey() {
		return key;
	}

	/**	
	 * 	获取值
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getValue() {
		return value;
	}

	/**	
	 * 	设置键
	 * 	
	 * 	@author Pan
	 * 	@param 	key	键
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**	
	 * 	设置值
	 * 	
	 * 	@author Pan
	 * 	@param 	value	值
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**	
	 * 	创建APIKey
	 * 	
	 * 	@author Pan
	 * 	@param 	key		键
	 * 	@param 	value	值
	 * 	@return	ApiKey
	 */
	public static HttpAuthCustom create(String key, String value) {
		return new HttpAuthCustom(key, value);
	}
}
