package com.gitee.apanlh.util.net.http.io;

import com.gitee.apanlh.util.io.IOUtils;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

/**
 * 	自定义输入流
 * 	
 * 	@author Pan
 */
public abstract class HttpInputStream extends InputStream implements Closeable {

	/** 输入流 */
	private InputStream inputStream;

	HttpInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	/**
	 * 	读取数据默认关闭输入流
	 * 	
	 * 	@author Pan
	 * 	@return	InputStream
	 */
	public byte[] readBytes() {
		return IOUtils.read(inputStream);
	}

	@Override
	public int read() throws IOException {
		return inputStream.read();
	}

	@Override
	public int read(byte[] b) throws IOException {
		return read(b, 0, b.length);
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		return inputStream.read(b, off, len);
	}

	@Override
	public void close() throws IOException {
		IOUtils.close(inputStream);
	}
}
