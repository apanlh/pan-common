package com.gitee.apanlh.util.net.http.handler;

/**
 *	拦截器上下文，用于在请求/响应过程中传递数据
 *	<br>该上下文对象会在每个请求开始前创建
 * 	
 * 	@author Pan
 */
public class HttpInterceptorContext {
	
	/** Request拦截器链，参数解析前 */
    private HttpRequestPreInterceptorChain requestPreInterceptorChain;
	/** Request拦截器链，参数解析后 */
    private HttpRequestInterceptorChain requestInterceptorChain;
    /** Response拦截器链 */
    private HttpResponseInterceptorChain responseInterceptorChain;
    
    /**
     * 	构造函数-初始化拦截器链
     * 	
     * 	@author Pan
     */
    public HttpInterceptorContext() {
    	this.requestPreInterceptorChain = new HttpRequestPreInterceptorChain();
		this.requestInterceptorChain = new HttpRequestInterceptorChain();
		this.responseInterceptorChain = new HttpResponseInterceptorChain();
    }
    
    /**	
     * 	获取-请求参数解析前拦截器
     * 	
     * 	@author Pan
     * 	@return	HttpRequestPreInterceptorChain
     */
    public HttpRequestPreInterceptorChain getRequestPreInterceptorChain() {
		return requestPreInterceptorChain;
	}
    
	/**	
	 * 	获取-请求参数解析后拦截器
	 * 	
	 * 	@author Pan
	 * 	@return	HttpRequestInterceptorChain
	 */
	public HttpRequestInterceptorChain getRequestInterceptorChain() {
		return requestInterceptorChain;
	}

	/**	
	 * 	获取-请求响应后拦截器
	 * 	
	 * 	@author Pan
	 * 	@return	HttpResponseInterceptorChain
	 */
	public HttpResponseInterceptorChain getResponseInterceptorChain() {
		return responseInterceptorChain;
	}
	
	/**	
	 * 	添加-HTTP请求参数解析前拦截器
	 * 	
	 * 	@author Pan
	 * 	@param 	requestPreInterceptor 请求参数解析前拦截器
	 * 	@return	HttpRequestPreInterceptorChain
	 */
	public HttpRequestPreInterceptorChain addRequestPreInterceptor(HttpRequestPreInterceptor requestPreInterceptor) {
		requestPreInterceptorChain.addInterceptor(requestPreInterceptor);
		return requestPreInterceptorChain;
	}
	
	/**	
	 * 	添加-HTTP请求参数解析后拦截器
	 * 	
	 * 	@author Pan
	 * 	@param 	requestInterceptor	请求参数解析后拦截器
	 * 	@return	HttpRequestInterceptorChain
	 */
	public HttpRequestInterceptorChain addRequestInterceptor(HttpRequestInterceptor requestInterceptor) {
		requestInterceptorChain.addInterceptor(requestInterceptor);
		return requestInterceptorChain;
	}
	
	/**	
	 * 	添加-HTTP响应拦截器
	 * 	
	 * 	@author Pan
	 * 	@param 	repResponseInterceptor	响应拦截器
	 * 	@return	HttpResponseInterceptorChain
	 */
	public HttpResponseInterceptorChain addResponseInterceptor(HttpResponseInterceptor repResponseInterceptor) {
		responseInterceptorChain.addInterceptor(repResponseInterceptor);
		return responseInterceptorChain;
	}
}
