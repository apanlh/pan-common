package com.gitee.apanlh.util.net.http;

/**
 * 	基础HTTP响应状态接口
 * 
 * 	@author Pan
 */
public interface BaseHttpStatus {
	
	/**	
	 * 	获取响应消息
	 * 	<br>非响应体消息
	 * 	<br>比如 HTTP/1.1 200 OK， 这里的OK即响应消息
	 * 	<br>比如 HTTP/1.1 404 NOT FOUND， 这里的NOT FOUND即响应消息
	 * 
	 * 	@author Pan
	 * 	@return	String
	 */
	String getMessage();
	
	 
	 /**	
     * 	获取错误响应消息
	 * 	<br>将检测是否错误响应
	 * 	<br>如4xx及5xx响应将记录ERROR级别的错误日志
	 * 
     * 	@author Pan
     * 	@return	String
     */
    String getErrorMsg();
    
	/**	
	 * 	获取响应状态
	 * 	
	 * 	@author Pan
	 * 	@return	Integer
	 */
	Integer getCode();
	
	/**	
     * 	验证是否成功请求
     * 	<br>出现2xx系列都为成功
     * 	
     * 	@author Pan
     * 	@return	boolean
     */
    boolean isSuc();
    
    /**	
     * 	验证是否为请求失败
     * 	<br>如果出现4系列或者5系列则认定失败
     * 	
     * 	@author Pan
     * 	@return	boolean
     */
    boolean isError();
}
