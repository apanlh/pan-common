package com.gitee.apanlh.util.net.http.auth;

import com.gitee.apanlh.util.base.StringUtils;

/**
 * 	Bearer认证协议
 * 	
 * 	@author Pan
 */
public class HttpAuthBearer extends HttpAuth {
	
	/** token */
	private String token;
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	public HttpAuthBearer() {
		super();
	}
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 * 	@param 	token	认证token
	 */
	public HttpAuthBearer(String token) {
		this.token = token;
		
		setHeader(HttpAuthProtocol.AUTHORIZATION, StringUtils.append(HttpAuthProtocol.BASIC.getType(), " ", token));
	}
	
	/**	
	 * 	获取token
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getToken() {
		return token;
	}

	/**	
	 * 	设置token
	 * 	
	 * 	@author Pan
	 * 	@param 	token	认证token
	 */
	public void setToken(String token) {
		this.token = token;
	}
	
	/**	
	 * 	创建Bearer
	 * 	
	 * 	@author Pan
	 * 	@param 	token	认证token
	 * 	@return	HttpAuthBasic
	 */
	public static HttpAuthBearer create(String token) {
		return new HttpAuthBearer(token);
	}
}
