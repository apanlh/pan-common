package com.gitee.apanlh.util.net.http.handler.body.cast;

import com.gitee.apanlh.util.base.Empty;
import com.gitee.apanlh.util.encode.StrEncodeUtils;
import com.gitee.apanlh.util.net.http.HttpBody;
import com.gitee.apanlh.util.reflection.ClassConvertUtils;

/**
 * 	提供一些请求参数解析后的转换方法
 * 	
 * 	@author Pan
 */
public class HttpRequestBodyCast extends HttpBodyCast {
	
	/**
	 * 	默认构造函数
	 * 	<br>继承父类
	 * 	
	 * 	@author Pan
	 */
	public HttpRequestBodyCast() {
		super();
	}
	
	/**
	 * 	构造函数-加载HttpBody
	 * 	<br>继承父类
	 * 	
	 * 	@author Pan	
	 * 	@param 	httpBody	请求体
	 */
	public HttpRequestBodyCast(HttpBody httpBody) {
		super(httpBody);
	}
	
	/**	
	 * 	转换byte[]类型
	 * 	<br>根据类型选择强转类型，如果当前请求类型不匹配则返回null
	 * 	
	 * 	@author Pan
	 * 	@return	byte[]
	 */
	public byte[] getBodyBytes() {
		HttpBody httpBody = super.getHttpBody();
		if (httpBody.getRequestBody() == null) {
			return Empty.arrayByte();
		}
		return ClassConvertUtils.castArrayByte(httpBody.getRequestBody());
	}
	
	/**	
	 * 	转换JSON类型(String)
	 * 	<br>根据类型选择强转类型，如果当前请求类型不匹配则返回null
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getBodyJson() {
		HttpBody httpBody = super.getHttpBody();
		if (!httpBody.hasJson()) {
			return null;
		}
		return ClassConvertUtils.castString(httpBody.getRequestBody());
	}
	
	/**	
	 * 	转换FORM类型(String)
	 * 	<br>根据类型选择强转类型，如果当前请求类型不匹配则返回null
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getBodyForm() {
		HttpBody httpBody = super.getHttpBody();
		if (!httpBody.hasForm()) {
			return null;
		}
		return ClassConvertUtils.castString(httpBody.getRequestBody());
	}
	
	/**	
	 * 	转换FORM类型(String)
	 * 	<br>根据类型选择强转类型，如果当前请求类型不匹配则返回null
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getBodyFormData() {
		HttpBody httpBody = super.getHttpBody();
		if (!httpBody.hasFormData()) {
			return null;
		}
		return StrEncodeUtils.utf8EncodeToStr(ClassConvertUtils.castArrayByte(httpBody.getRequestBody()));
	}
}
