package com.gitee.apanlh.util.net.http;

/**
 * 	Http状态枚举
 * 	<br>分为5类，1xx、2xx、3xx、4xx、5xx，每类下面又有若干个具体的状态码。
 * 	
 * 	@author Pan
 */
public enum HttpStatusEnums {
	
	/** 信息性状态码 1xx Informational */
	CONTINUE(						100, "Continue", 						"请求者应继续进行请求"),
	SWITCHING_PROTOCOLS(			101, "Switching Protocols", 			"请求者已要求服务器切换协议，服务器已确认并准备切换"),
	PROCESSING(						102, "Processing", 						"WebDAV请求可能包含许多涉及文件操作的子请求，此状态代码表示已经接收到并正在处理请求"),
	EARLY_HINTS(					103, "Early Hints", 					"表示服务器已经发送了部分响应主体，因为它希望在等待请求完成之前发送头部，使客户端可以开始显示响应主体的形式"),

	/** 成功状态码 2xx Success */
    OK(								200, "OK", 								"请求已成功处理"),
    CREATED(						201, "Created", 						"请求已经被实现，而且有一个新的资源已经依据请求的需要而建立"),
    ACCEPTED(						202, "Accepted", 						"服务器已接受请求，但尚未处理完成"),
    NON_AUTHORITATIVE_INFORMATION(	203, "Non-Authoritative Information", 	"服务器已经成功处理了请求，但返回了可能来自另一来源的信息"),
    NO_CONTENT(						204, "No Content", 						"服务器成功处理了请求，但没有返回任何内容"),
    RESET_CONTENT(					205, "Reset Content", 					"服务器成功处理了请求，但没有返回任何内容。与204响应不同，此响应要求请求者重置文档视图"),
    PARTIAL_CONTENT(				206, "Partial Content", 				"服务器已经成功处理了部分GET请求"),
    MULTI_STATUS(					207, "Multi-Status", 					"表示对请求的响应，可以包含有关资源状态的消息实体或指针"),

    /** 重定向状态码 3xx Redirection */
    MULTIPLE_CHOICES(				300, "Multiple Choices", 				"请求的资源有多种表示形式，而且根据请求的目的和用户代理的能力，可以自行判断应该哪种表示形式"),
    MOVED_PERMANENTLY(				301, "Moved Permanently", 				"请求的资源已被永久移动到新URI，返回信息会包括新的URI，浏览器会自动定向到新URI"),
    FOUND(							302, "Found", 							"请求的资源已被临时移动到新URI，返回信息会包括新的URI，浏览器会自动定向到新URI"),
    SEE_OTHER(						303, "See Other", 						"对应当前请求的响应可以在另一个URI上被找到，客户端应当采用GET的方式访问那个资源"),
    NOT_MODIFIED(					304, "Not Modified", 					"如果客户端发送了一个带条件的GET请求且该请求已被允许，则服务器返回304 Not Modified"),
    USE_PROXY(						305, "Use Proxy", 						"被请求的资源必须通过指定的代理才能被访问"),
    TEMPORARY_REDIRECT(				307, "Temporary Redirect", 				"请求的资源临时从不同的URI响应请求，由于该请求可能是POST或者包含了一些实体内容，所以服务器在重定向的时候，不会将实际的请求转发给新的地址，而是发送一个带有 Location头信息的响应在响应中告诉客户端下一个请求应该访问哪个URI"),
    PERMANENT_REDIRECT(				308, "Permanent Redirect", 				"请求的资源已永久移动到新URI，任何未保存的书签将需要更新为新的URI"),

    /** 客户端错误状态码 4xx Client Error */
	BAD_REQUEST(					400, "Bad Request", 					"请求报文存在语法错误"),
	UNAUTHORIZED(					401, "Unauthorized", 					"请求需要HTTP认证"),
	PAYMENT_REQUIRED(				402, "Payment Required", 				"保留状态码，该状态码被预留，将来可能会使用"),
	FORBIDDEN(						403, "Forbidden", 						"服务器拒绝执行该请求"),
	NOT_FOUND(						404, "Not Found", 						"服务器无法根据客户端的请求找到资源"),
	METHOD_NOT_ALLOWED(				405, "Method Not Allowed", 				"客户端请求中的方法被禁止"),
	NOT_ACCEPTABLE(					406, "Not Acceptable", 					"服务器无法根据客户端请求的内容特性完成请求"),
	PROXY_AUTHENTICATION_REQUIRED(	407, "Proxy Authentication Required", 	"请求者首先需要使用代理服务器进行授权"),
	REQUEST_TIMEOUT(				408, "Request Timeout", 				"请求超时"),
	CONFLICT(						409, "Conflict", 						"由于请求和资源的当前状态之间存在冲突，请求无法完成"),
	GONE(							410, "Gone", 							"所请求的资源已经不再可用，而且没有任何已知的转发地址"),
	LENGTH_REQUIRED(				411, "Length Required", 				"服务器无法处理请求，因为请求的内容长度大于服务器可以处理的范围"),
	PRECONDITION_FAILED(			412, "Precondition Failed", 			"请求头中指定的一些前提条件失败"),
	PAYLOAD_TOO_LARGE(				413, "Payload Too Large", 				"请求的实体过大，服务器无法处理"),
	URI_TOO_LONG(					414, "URI Too Long", 					"请求的URI长度超过了服务器能够解释的长度，因此服务器拒绝对该请求提供服务"),
	UNSUPPORTED_MEDIA_TYPE(			415, "Unsupported Media Type", 			"请求的格式不受请求页面的支持"),
	RANGE_NOT_SATISFIABLE(			416, "Range Not Satisfiable", 			"如果请求中包含了 Range请求头，并且Range中指定的任何数据范围都与当前资源的可用范围不重合，同时请求中又没有定义 If-Range请求头，那么服务器就应当返回416状态码"),
	EXPECTATION_FAILED(				417, "Expectation Failed", 				"请求头中包含了一些期望但服务器无法满足的内容"),

	/** 服务器错误状态码 5xx Server Error  */
	INTERNAL_SERVER_ERROR(			500, "Internal Server Error", 			"服务器内部错误"),
	NOT_IMPLEMENTED(				501, "Not Implemented", 				"服务器不支持请求的功能，无法完成请求"),
	BAD_GATEWAY(					502, "Bad Gateway", 					"服务器作为网关或代理，从上游服务器收到无效响应"),
	SERVICE_UNAVAILABLE(			503, "Service Unavailable", 			"由于临时的服务器维护或者过载，服务器当前无法处理请求"),
	GATEWAY_TIMEOUT(				504, "Gateway Timeout", 				"服务器作为网关或代理，但是没有及时从上游服务器收到请求"),
	HTTP_VERSION_NOT_SUPPORTED(		505, "HTTP Version not supported", 		"服务器不支持请求中所用的HTTP协议版本"),
	VARIANT_ALSO_NEGOTIATES(		506, "Variant Also Negotiates", 		"服务器有一个内部配置错误：被请求的变量资源被配置为参与透明协商处理，但服务器没有定义任何协商配置数据"),
	INSUFFICIENT_STORAGE(			507, "Insufficient Storage", 			"服务器无法存储完成请求所必须的内容"),
	LOOP_DETECTED(					508, "Loop Detected", 					"服务器在处理请求时检测到无限循环"),
	NOT_EXTENDED(					510, "Not Extended", 					"获得所请求资源的访问控制列表所需的策略没有被满足"),
	NETWORK_AUTHENTICATION_REQUIRED(511, "Network Authentication Required", "客户端需要进行身份验证才能获得网络访问权限"),
	
	/** 分别五种类型区分 */
	TYPE_INFORMATIONAL(	1),
	TYPE_SUCCESSFUL(	2),
	TYPE_REDIRECTION(	3),
	TYPE_CLIENT_ERROR(	4),
	TYPE_SERVER_ERROR(	5);
	
	/** 状态 */
	private int code;
	/** 状态类型 */
	private int type;
	/** 短语 */
	private String phrase;
	/** 描述 */
	private String description;

	/**	
	* 	构造函数-自定义状态类型 
	* 	
	* 	@author Pan
	* 	@param 	type		HTTP状态类型
	*/
	HttpStatusEnums(int type) {
		this.type = type;
	}
	
	/**	
	* 	构造函数-HTTP状态及短语
	* 	
	* 	@author Pan
	* 	@param 	code		状态
	* 	@param 	phrase		短语
	* 	@param 	description	描述
	*/
	HttpStatusEnums(int code, String phrase, String description) {
	   this.code = code;
	   this.phrase = phrase;
	   this.description = description;
	}

	/**	
	 * 	获取HTTP状态
	 * 	
	 * 	@author Pan
	 * 	@return	int
	 */
	public int getCode() {
		return code;
	}
	
	/**	
	 * 	获取HTTP类型
	 * 	
	 * 	@author Pan
	 * 	@return	int
	 */
	public int getType() {
		return type;
	}

	/**	
	 * 	获取当前HTTP状态短语
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getPhrase() {
		return phrase;
	}

	/**	
	 * 	获取当前HTTP状态描述
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getDescription() {
		return description;
	}
	
}
