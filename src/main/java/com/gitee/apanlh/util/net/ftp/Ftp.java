package com.gitee.apanlh.util.net.ftp;

import com.gitee.apanlh.util.check.CheckImport;
import com.gitee.apanlh.util.check.CheckLibrary;
import com.gitee.apanlh.util.io.IOUtils;

import java.io.Closeable;

/**
 * 	创建FTP
 * 
 * 	@author Pan
 */
public class Ftp implements Closeable {
	
	static {
		CheckImport.library(CheckLibrary.APACHE_NET);
	}
	
	/** 连接客户端 */
	private FtpConnection connection;
	
	/**	
	 * 	构造函数-默认端口，匿名登录，构建默认FTP配置
	 * 	
	 * 	@author Pan
	 * 	@param 	ip			地址
	 */
	public Ftp(String ip) {
		this.connection = new FtpConnection(ip);
	}

	/**	
	 * 	构造函数-匿名登录，构建默认FTP配置
	 * 	
	 * 	@author Pan
	 * 	@param 	ip			地址
	 * 	@param 	port		端口
	 */
	public Ftp(String ip, Object port) {
		this.connection = new FtpConnection(ip, port);
	}

	/**
	 * 	构造函数-用户无密码登录，构建默认FTP配置
	 * 	
	 * 	@author Pan
	 * 	@param 	ip			地址
	 * 	@param 	port		端口
	 * 	@param 	username	用户名
	 */
	public Ftp(String ip, Object port, String username) {
		this.connection = new FtpConnection(ip, port, username);
	}

	/**
	 * 	构造函数-用户密码登录，构建默认FTP配置
	 * 	
	 * 	@author Pan
	 * 	@param 	ip			地址
	 * 	@param 	port		端口
	 * 	@param 	username	用户名
	 * 	@param 	password	密码
	 */
	public Ftp(String ip, Object port, String username, String password) {
		this.connection = new FtpConnection(ip, port, username, password);
	}

	/**	
	 * 	构造函数-用户密码登录，自定义FTP配置
	 * 	
	 * 	@author Pan
	 * 	@param 	ip			地址
	 * 	@param 	port		端口
	 * 	@param 	username	用户名
	 * 	@param 	password	密码
	 * 	@param 	ftpConfig	FTP连接配置
	 */
	public Ftp(String ip, Object port, String username, String password, FtpConfig ftpConfig) {
		this.connection = new FtpConnection(ip, port, username, password, ftpConfig);
	}

	/**	
	 * 	连接FTP获取执行器
	 * 	<br>如果连接失败会自动关闭流连接
	 * 	
	 * 	@author Pan
	 * 	@return	FtpCommandExecutor
	 */
	public FtpCommandExecutor getExecutor() {
		return connection.connect();
	}
	
	/**	
	 * 	获取FTP连接信息
	 * 	
	 * 	@author Pan
	 * 	@return	FtpConnection
	 */
	public FtpConnection getConnection() {
		return connection;
	}
	
	/**
	 * 	重写关闭方法
	 * 	
	 * 	@author Pan
	 */
	@Override
	public void close() {
		IOUtils.close(connection);
	}
	
	/**	
	 * 	默认端口，匿名登录，构建默认FTP配置
	 * 	
	 * 	@author Pan
	 * 	@param 	ip			地址
	 * 	@return	Ftp
	 */
	public static Ftp create(String ip) {
		return new Ftp(ip);
	}

	/**
	 * 	匿名登录，构建默认FTP配置
	 * 	
	 * 	@author Pan
	 * 	@param 	ip			地址
	 * 	@param 	port		端口
	 * 	@return	Ftp
	 */
	public static Ftp create(String ip, Object port) {
		return new Ftp(ip, port);
	}
	
	/**
	 * 	用户无密码登录，构建默认FTP配置
	 * 	
	 * 	@author Pan
	 * 	@param 	ip			地址
	 * 	@param 	port		端口
	 * 	@param 	username	用户名
	 * 	@return	Ftp
	 */
	public static Ftp create(String ip, Object port, String username) {
		return new Ftp(ip, port, username);
	}
	
	/**
	 * 	用户密码登录，构建默认FTP配置
	 * 	
	 * 	@author Pan
	 * 	@param 	ip			地址
	 * 	@param 	port		端口
	 * 	@param 	username	用户名
	 * 	@param 	password	密码
	 * 	@return	Ftp
	 */
	public static Ftp create(String ip, Object port, String username, String password) {
		return new Ftp(ip, port, username, password);
	}
	
	/**	
	 * 	用户密码登录，自定义FTP配置
	 * 	<br>连接后使用{@link Ftp#getExecutor()}方法
	 * 
	 * 	@author Pan
	 * 	@param 	ip			地址
	 * 	@param 	port		端口
	 * 	@param 	username	用户名
	 * 	@param 	password	密码
	 * 	@param 	ftpConfig	FTP连接配置
	 * 	@return	Ftp
	 */
	public static Ftp create(String ip, Object port, String username, String password, FtpConfig ftpConfig) {
		return new Ftp(ip, port, username, password, ftpConfig);
	}
}
	
