package com.gitee.apanlh.util.net.http;

import com.gitee.apanlh.web.http.HttpMethod;

/**	
 * 	HTTP请求对象
 * 	
 * 	@author Pan
 */
public class HttpRequest {
	
	/** 请求地址 */
	private HttpUrl url;
	/** 请求方法 */
	private HttpMethod method;
	/** 请求体 */
	private HttpBody body;
	/** 请求头 */
	private HttpRequestHeader header;
	/** 请求配置 */
	private HttpConfig config;
	
	/**
	 * 	构造-默认
	 * 	
	 */
	HttpRequest() {
		super();
	}
	
	/**
	 * 	构造-初始化对象
	 * 	
	 * 	@author Pan
	 * 	@param 	url		HttpUrl对象
	 * 	@param 	method	HttpMethod方法枚举
	 */
	HttpRequest(HttpUrl url, HttpMethod method) {
		this.url = url;
		this.method = method;
		this.header = HttpRequestHeader.create();
	}
	
	/**	
	 * 	获取HTTP请求地址
	 * 	
	 * 	@author Pan
	 * 	@return	HttpUrl
	 */
	public HttpUrl getUrl() {
		return this.url;
	}
	
	/**	
	 * 	获取HTTP请求方法类型
	 * 	
	 * 	@author Pan
	 * 	@return	HttpMethod
	 */
	public HttpMethod getMethod() {
		return this.method;
	}
	
	/**	
	 * 	获取HTTP配置
	 * 	
	 * 	@author Pan
	 * 	@return	HttpConfig
	 */
	public HttpConfig getConfig() {
		return this.config;
	}
	
	/**	
	 * 	获取HTTPBody
	 * 	
	 * 	@author Pan
	 * 	@return	HttpConfig
	 */
	public HttpBody getBody() {
		return this.body;
	}
	
	/**	
	 * 	获取请求头
	 * 	
	 * 	@return	HttpRequestHeader
	 */
	public HttpRequestHeader getHeader() {
		return header;
	}

	/**	
	 * 	设置-Request请求体
	 * 	
	 * 	@author Pan
	 * 	@param 	httpBody	请求体
	 */
	public void setBody(HttpBody httpBody) {
		this.body = httpBody;
	}
	
	/**	
	 * 	填充-Request配置
	 * 	
	 * 	@author Pan
	 * 	@param 	httpConfig	请求配置
	 * 	@return	HttpBody
	 */
	public HttpConfig setConfig(HttpConfig httpConfig) {
		this.config = httpConfig;
		return config;
	}

	/**	
	 * 	设置请求头
	 * 	
	 * 	@author Pan
	 * 	@param 	header	请求头对象
	 */
	public void setHeader(HttpRequestHeader header) {
		this.header = header;
	}
	
	/**	
	 * 	构建-Request对象
	 * 	
	 * 	@author Pan
	 * 	@param 	url		请求地址
	 * 	@param 	method	请求方法
	 * 	@return	HttpRequest
	 */
	public static HttpRequest create(HttpUrl url, HttpMethod method) {
		return new HttpRequest(url, method);
	}
}
