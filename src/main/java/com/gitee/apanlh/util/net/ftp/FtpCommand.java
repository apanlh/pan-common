package com.gitee.apanlh.util.net.ftp;

import com.gitee.apanlh.exp.FtpException;
import com.gitee.apanlh.util.base.ArrayUtils;
import com.gitee.apanlh.util.base.StringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

/**	
 * 	命令对象
 * 	<br>目前用于描述命令状态等信息
 * 	
 * 	@author Pan
 */
public class FtpCommand {
	
	/** FTP客户端信息 */ 
	private FTPClient executor;
	/** 当前命令状态码 */
	private int commandCode;
	/** 当前命令执行状态 */
	private boolean commandStatus;
	/** 当前命令详情信息 */
	private StringBuilder commandDetails;

	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	FtpCommand() {
		super();
	}
	
	/**	
	 * 	构造函数-创建命令对象
	 * 	
	 * 	@author Pan
	 * 	@param 	executor	FTP客户端
	 */
	public FtpCommand(FTPClient executor) {
		this.executor = executor;
	}
	
	/**	
	 * 	执行指令返回成功失败
	 * 	
	 * 	@author Pan
	 * 	@param 	commandStr	执行命令字符串
	 * 	@param 	params		参数
	 * 	@return	boolean
	 */
	public boolean doCommand(String commandStr, String params) {
		try {
			return executor.doCommand(commandStr, params);
		} catch (Exception e) {
			throw new FtpException(e.getMessage() , e);
		}
	}
	
	/**
	 * 	执行指令返回转换数组
	 * 	
	 * 	@author Pan
	 * 	@param 	commandStr	命令
	 * 	@param 	params		参数
	 * 	@return	String[]
	 */
	public String[] doCommandAsArray(String commandStr, String params) {
		try {
			return executor.doCommandAsStrings(commandStr, params);
		} catch (Exception e) {
			throw new FtpException(e.getMessage() , e);
		}
	}
	
	/**
	 * 	执行指令返回转换字符串
	 * 	<br>如果为空将返回字符串("null")
	 * 	
	 * 	@author Pan
	 * 	@param 	commandStr	命令
	 * 	@param 	params		参数
	 * 	@return	String
	 */
	public String doCommandAsStringJoin(String commandStr, String params) {
		return ArrayUtils.toString(doCommandAsArray(commandStr, params));
	}
	
	/**	
	 * 	获取结果信息
	 * 	
	 * 	@author Pan
	 */
	void createCommandResult() {
		if (commandDetails == null) {
			commandDetails = new StringBuilder();
		}
		//	重置并添加返回结果值
		commandDetails.setLength(0);
		commandDetails.append(StringUtils.format("{}, code:{}, msg:{}", this.commandStatus, this.commandCode, ArrayUtils.join(this.executor.getReplyStrings(), "\n")));
	}
	
	/**	
	 * 	获取命令指令的消息结果
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getCommandDetails() {
		return commandDetails.toString();
	}
	
	/**	
	 * 	获取当前命令执行结果
	 * 	
	 * 	@author Pan
	 * 	@return	boolean
	 */
	public boolean getCommandStatus() {
		return commandStatus;
	}
	
	/**	
	 * 	获取当前指令
	 * 	
	 * 	@author Pan
	 * 	@return	int
	 */
	public int getCommandCode() {
		return commandCode;
	}
	
	/**	
	 * 	当前命令是否完成
	 * 	生成命令响应详情结果
	 * 	
	 * 	@author Pan
	 * 	@param 	connectionCommand	是否为连接指令
	 * 	@return	boolean
	 */
	public boolean isCompletion(boolean connectionCommand) {
		return isCompletion(connectionCommand, true);
	}
	
	/**	
	 * 	当前命令是否完成
	 * 	自定义生成命令详情结果
	 * 	#mark 后期可增加相对应状态码标识识别
	 * 	
	 * 	<br> 200/300状态码识别
	 * 	
	 * 	@author Pan
	 * 	@param 	connectionCommand	是否为连接指令
	 * 	@param 	createResult		是否生成命令详情结果
	 * 	@return	boolean
	 */
	public boolean isCompletion(boolean connectionCommand, boolean createResult) {
		//	获取命令执行结果消息;
		try {
			//	都会更改commandCode的状态值
			if (connectionCommand) {
				commandCode = executor.getReplyCode();
			} else {
				commandCode = executor.getReply();
			}
			//	生成详情结果信息
			if (createResult) {
				createCommandResult();
			}
			commandStatus = FTPReply.isPositiveCompletion(commandCode);
		} catch (Exception e) {
			throw new FtpException(e.getMessage(), e);
		}
		return commandStatus;
	}
	
	/**	
	 * 	获取客户端
	 * 	
	 * 	@author Pan
	 * 	@return	FTPClient
	 */
	FTPClient getExecutor() {
		return executor;
	}
}
