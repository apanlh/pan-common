package com.gitee.apanlh.util.net.ftp;

/**	
 * 	FTP连接方式
 * 
 * 	@author Pan
 */
public enum FtpConnectMode {
	/** 主动模式 */
	ACTIVE,
	/** 被动模式 */
	PASSIVE,
	;
}
