package com.gitee.apanlh.util.net.http.handler;

import com.gitee.apanlh.exp.HttpException;
import com.gitee.apanlh.util.net.http.HttpClient;
import com.gitee.apanlh.util.net.http.HttpRequest;
import com.gitee.apanlh.util.net.http.handler.body.cast.HttpRequestBodyCast;

/**
 * 	HTTP请求拦截器(参数解析后)
 * 	<br>实现该接口能够在HTTP请求之前进行处理但参数已经解析了
 * 	<br>如果使用的是Restful风格或者QueryParam方式请使用{@link HttpRequestPreInterceptorChain},因为即使修改了请求体，也不会再次刷新URL资源
 * 	<br>支持函数式
 * 	
 * 	@author Pan
 */
@FunctionalInterface
public interface HttpRequestInterceptor extends HttpInterceptor {
	
	/**
	 * 	处理HTTP请求拦截
	 * 	<br>如果使用的是Restful风格或者QueryParam方式请使用{@link HttpRequestPreInterceptorChain},因为即使修改了请求体，也不会再次刷新URL资源
	 *  <br>如果要获取参数，请使用{@link HttpRequestBodyCast}类中的方法
	 *  
	 * 	@author Pan
	 * 	@param 	httpRequest 	HTTP请求对象
	 * 	@param 	httpClient  	HTTP客户端对象
	 * 	@param 	bodyCast  		HTTPBody转换对象
	 * 	@param 	chain       	HTTP拦截器链
	 * 	@return HttpRequest 	HTTP请求对象
	 * 	@throws HttpException 	处理异常，则抛出该异常
	 */
	HttpRequest chain(HttpRequest httpRequest, HttpClient httpClient, HttpRequestBodyCast bodyCast, HttpInterceptorChain chain) throws HttpException;
}
