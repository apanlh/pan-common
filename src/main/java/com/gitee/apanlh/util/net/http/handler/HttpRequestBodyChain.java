package com.gitee.apanlh.util.net.http.handler;

import com.gitee.apanlh.exp.HttpException;
import com.gitee.apanlh.util.net.http.HttpClient;
import com.gitee.apanlh.util.net.http.HttpRequest;
import com.gitee.apanlh.util.net.http.handler.body.HttpBodyParseContext;
import com.gitee.apanlh.util.net.http.handler.body.cast.HttpRequestBodyCast;

/**	
 * 	HTTP配置拦截器，装配实现对HTTP在请求前对请求体进行参数处理
 * 	<br>将会开启HTTPClient连接
 * 	
 * 	@author Pan
 */
public class HttpRequestBodyChain implements HttpRequestInterceptor {
	
	@Override
	public HttpRequest chain(HttpRequest httpRequest, HttpClient httpClient, HttpRequestBodyCast bodyCast, HttpInterceptorChain chain) throws HttpException {
		//	等待解析器完成之后添加URL(避免QueryParam及PathVariable导致URL错误)，并开启连接
		HttpBodyParseContext bodyParseContext = HttpRequestInterceptorChain.getBodyParseContext();
		HttpRequest requestParse = bodyParseContext.parse(httpRequest, httpClient);
		//	避免泄露
		try {
			httpClient.open();
		} catch (Exception e) {
			httpClient.close();
			throw e;
		}
		return chain.proceed(requestParse, httpClient);
	}
}
