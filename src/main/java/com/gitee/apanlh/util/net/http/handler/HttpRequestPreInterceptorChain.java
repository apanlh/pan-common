package com.gitee.apanlh.util.net.http.handler;

import com.gitee.apanlh.exp.HttpException;
import com.gitee.apanlh.util.net.http.HttpClient;
import com.gitee.apanlh.util.net.http.HttpRequest;

import java.util.List;

/**
 * 	HTTP请求拦截器责任链，按照顺序执行HTTP请求拦截器
 * 	<br>在HTTP参数解析前执行
 * 	
 * 	@author Pan
 */
public class HttpRequestPreInterceptorChain extends HttpInterceptorChain {

	/**
	 * 	构造函数-默认初始化拦截链
	 * 	<br>有序集合
	 * 	<br>在HTTP参数解析前执行
	 * 
	 * 	@author Pan
	 */
	HttpRequestPreInterceptorChain() {
		super();
	}
	
	/**
	 * 	构造函数-添加一个HTTP拦截器
	 * 	<br>继承父类
	 * 	
	 * 	@author Pan
	 * 	@param 	interceptor  需要添加的拦截器
	 */
	public HttpRequestPreInterceptorChain(HttpInterceptor interceptor) {
		super(interceptor);
	}
	
	/**	
	 * 	构造函数-初始化链
	 * 	<br>继承父类
	 * 
	 * 	@author Pan
	 * 	@param 	interceptors	一个或多个链
	 */
	public HttpRequestPreInterceptorChain(List<HttpInterceptor> interceptors) {
		super(interceptors);
	}
	
	/**
	 * 	执行HTTP请求拦截器责任
	 * 	<br>返回null则终止执行接下来所有链
	 * 	<br>在HTTP请求发送前执行
	 * 
	 * 	@param 	httpRequest 	HTTP请求对象
	 * 	@param 	client			HTTP客户端
	 * 	@return HttpRequest		HTTP请求对象
	 * 	@throws HttpException 	处理异常，则抛出该异常
	 */
	@Override
	public HttpRequest proceed(HttpRequest httpRequest, HttpClient client) throws HttpException {
		while (super.hasNext()) {
			HttpRequest proceed = super.proceed(httpRequest, client);
			//	如果返回null停止执行
			if (proceed == null) {
				break;
			}
		}
		return httpRequest;
	}
}
