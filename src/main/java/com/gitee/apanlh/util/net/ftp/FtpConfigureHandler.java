package com.gitee.apanlh.util.net.ftp;

/**	
 * 	配置执行装配
 * 
 * 	@author Pan
 */
public interface FtpConfigureHandler {
	
	/**
	 * 	在某之前装配
	 * 	@author Pan
	 */
	void beforeConfigure();
	
	/**
	 * 	在某之后装配
	 * 	@author Pan
	 */
	void afterConfigure();
}
