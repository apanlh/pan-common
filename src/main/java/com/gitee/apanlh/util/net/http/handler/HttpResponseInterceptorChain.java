package com.gitee.apanlh.util.net.http.handler;

import com.gitee.apanlh.exp.HttpException;
import com.gitee.apanlh.util.net.http.HttpResponse;

import java.util.List;

/**
 * 	HTTP响应拦截器责任链，按照顺序执行HTTP响应拦截器
 * 	
 * 	@author Pan
 */
public class HttpResponseInterceptorChain extends HttpInterceptorChain {

	/**
	 * 	构造函数-默认初始化拦截链
	 * 	<br>有序集合
	 * 	
	 * 	@author Pan
	 */
	HttpResponseInterceptorChain() {
		super();
	}
	
	/**
	 * 	构造函数-添加一个HTTP拦截器
	 * 	<br>继承父类
	 * 	
	 * 	@author Pan
	 * 	@param 	interceptor  需要添加的拦截器
	 */
	public HttpResponseInterceptorChain(HttpInterceptor interceptor) {
		super(interceptor);
	}
	
	/**	
	 * 	构造函数-初始化链
	 * 	<br>继承父类
	 * 
	 * 	@author Pan
	 * 	@param 	interceptors	一个或多个链
	 */
	public HttpResponseInterceptorChain(List<HttpInterceptor> interceptors) {
		super(interceptors);
	}

	/**
	 * 	执行HTTP响应拦截器责任链。
	 * 	<br>返回null则终止执行接下来所有链
	 * 	
	 * 	@param 	httpResponse 	HTTP响应对象
	 * 	@return httpResponse	HTTP响应对象
	 * 	@throws HttpException 	处理异常，则抛出该异常
	 */
	@Override
	public HttpResponse proceed(HttpResponse httpResponse) throws HttpException {
		while (super.hasNext()) {
			HttpResponse proceed = super.proceed(httpResponse);
			//	如果返回null停止执行
			if (proceed == null) {
				break;
			}
		}
		return httpResponse;
	}
}
