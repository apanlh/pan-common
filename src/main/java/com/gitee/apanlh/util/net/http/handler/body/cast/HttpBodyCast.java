package com.gitee.apanlh.util.net.http.handler.body.cast;

import com.gitee.apanlh.util.net.http.HttpBody;

/**
 * 	提供一些对HttpBody强转方法
 * 	<br>HttpBody强转父类
 * 	
 * 	@author Pan
 */
public class HttpBodyCast {
	
	/** HttpBody对象 */
	private HttpBody httpBody;
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	public HttpBodyCast() {
		super();
	}
	
	/**	
	 * 	构造函数-加载HttpBody
	 * 	
	 * 	@author Pan
	 * 	@param 	httpBody	HttpBody对象
	 */
	public HttpBodyCast(HttpBody httpBody) {
		this.httpBody = httpBody;
	}

	/**	
	 * 	获取HttpBody请求体
	 * 	
	 * 	@author Pan
	 * 	@return	HttpBody
	 */
	HttpBody getHttpBody() {
		return httpBody;
	}
}
