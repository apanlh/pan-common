package com.gitee.apanlh.util.net.http.handler.body.cast;

import com.gitee.apanlh.util.net.http.HttpBody;
import com.gitee.apanlh.util.reflection.ClassConvertUtils;

import java.util.Map;

/**
 * 	提供一些请求参数解析前的转换方法
 * 	
 * 	@author Pan
 */
public class HttpRequestPreBodyCast extends HttpBodyCast {
	
	/**
	 * 	默认构造函数
	 * 	<br>继承父类
	 * 	
	 * 	@author Pan
	 */
	public HttpRequestPreBodyCast() {
		super();
	}
	
	/**
	 * 	构造函数-加载HttpBody
	 * 	<br>继承父类
	 * 	
	 * 	@author Pan
	 * 	@param 	httpBody	HTTP请求体
	 */
	public HttpRequestPreBodyCast(HttpBody httpBody) {
		super(httpBody);
	}
	
	/**	
	 * 	自定义转换类型
	 * 	
	 * 	@author Pan
	 *  @param 	<T>		数据类型
	 * 	@param 	clazz	类
	 * 	@return T
	 */
	public <T> T get(Class<T> clazz) {
		return ClassConvertUtils.cast(getHttpBody().getRequestBody());
	}

	/**
	 * 	转换Map
	 * 	
	 * 	@author Pan
	 * 	@return Map
	 */
	public Map<String, Object> getMap() {
		return ClassConvertUtils.castMap(getHttpBody().getRequestBody());
	}
}
