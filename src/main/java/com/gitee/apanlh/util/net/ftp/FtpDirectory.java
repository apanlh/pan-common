package com.gitee.apanlh.util.net.ftp;

/**	
 * 	FTP目录操作对象(目录地址,目录文件)
 * 
 * 	@author Pan
 */
class FtpDirectory implements FtpCommandDirectory {
	
	/** 目录地址 */
	private FtpCommandDirectoryPath directoryPath;
	/** 目录文件 */
	private FtpCommandDirectoryFile directoryFile;
	
	/**
	 * 	构造函数-默认
	 * 	@author Pan
	 */
	FtpDirectory() {
		
	}
	
	/**	
	 * 	构造函数-初始化目录对象
	 * 	
	 * 	@author Pan
	 * 	@param 	commandExecutors	命令执行器
	 */
	public FtpDirectory(FtpCommandExecutor commandExecutors) {
		this.directoryPath = new FtpDirectoryPath(commandExecutors);
		this.directoryFile = new FtpDirectoryFile(commandExecutors);
	}

	@Override
	public void afterConfigure() {
		directoryPath.afterConfigure();
		directoryFile.afterConfigure();
	}
	
	@Override
	public void beforeConfigure() {
		directoryPath.beforeConfigure();
		directoryFile.beforeConfigure();
	}

	@Override
	public FtpCommandDirectoryPath getDirectoryPath() {
		return directoryPath;
	}

	@Override
	public FtpCommandDirectoryFile getDirectoryFile() {
		return directoryFile;
	}
}
