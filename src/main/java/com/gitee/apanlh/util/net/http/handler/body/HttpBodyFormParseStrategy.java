package com.gitee.apanlh.util.net.http.handler.body;

import com.gitee.apanlh.exp.HttpBodyParseException;
import com.gitee.apanlh.util.net.http.HttpBody;
import com.gitee.apanlh.util.net.http.HttpClient;
import com.gitee.apanlh.util.net.http.HttpRequest;

import java.util.Map;

/**
 * 	HTTPBody请求体解析
 * 	<br>FORM表单格式请求
 * 	
 * 	@author Pan
 */
public class HttpBodyFormParseStrategy implements HttpBodyParseStrategy {

	@Override
	public HttpRequest parse(HttpRequest httpRequest, HttpClient httpClient) throws HttpBodyParseException {
		return toNameValuePair(httpRequest);
	}
	
	 /**	
     * 	将对象转化请求方式为NameValuePair格式(值对节点类型)
     * 	<br>顺序添加节点
     * 	
     * 	@author Pan
     * 	@param 	httpRequest	HTTP请求对象
     * 	@return	HttpRequest
     */
	HttpRequest toNameValuePair(HttpRequest httpRequest) {
		HttpBody httpBody = httpRequest.getBody();
		Map<String, ?> paramMap = beanToLinkedMap(httpBody);
		boolean hasParamUrlEncode = httpRequest.getConfig().hasParamUrlEncode();

		httpBody.setRequestBody(toNameValuePair(paramMap, hasParamUrlEncode));
		httpRequest.setBody(httpBody);
		return httpRequest;
	}
}
