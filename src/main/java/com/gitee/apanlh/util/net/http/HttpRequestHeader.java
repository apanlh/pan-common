package com.gitee.apanlh.util.net.http;

import com.gitee.apanlh.util.base.MapUtils;

import java.util.Map;

/**
 * 	HTTP请求头
 * 	
 * 	@author Pan
 */
public class HttpRequestHeader extends HttpHeader<String> {
	
	/**
	 * 	构造函数-继承抽象类
	 * 	
	 * 	@author Pan
	 */
	public HttpRequestHeader() {
		super();
	}
	
	/**
	 * 	构造函数-继承抽象类
	 * 	<br>自定义Map
	 * 	
	 * 	@author Pan
	 * 	@param 	map	Map
	 */
	public HttpRequestHeader(Map<String, String> map) {
		super(map);
	}

	/**	
	 * 	创建-默认使用LinkedHashMap
	 * 	
	 * 	@author Pan
	 * 	@return	HttpHeader
	 */
	public static HttpRequestHeader create() {
		return new HttpRequestHeader();
	}
	
	/**	
	 * 	创建-添加key-value
	 * 	<br>默认添加LinkedHashMap
	 * 	
	 * 	@author Pan
	 * 	@param 	key		键
	 * 	@param 	value	值
	 * 	@return	HttpHeader
	 */
	public static HttpRequestHeader create(String key, String value) {
		return new HttpRequestHeader(MapUtils.newLinkedHashMap(newMap -> newMap.put(key, value)));
	}
	
	/**	
	 * 	创建-自定义Map
	 * 
	 * 	@author Pan
	 * 	@param 	map		请求头
	 * 	@return	HttpHeader
	 */
	public static HttpRequestHeader create(Map<String, String> map) {
		return new HttpRequestHeader(map);
	}
}
