package com.gitee.apanlh.util.net.http;

/**
 * 	HTTP请求体/响应体类型枚举
 * 	<br>用于标识HTTP请求的数据格式
 * 
 * 	@author Pan
 */
public enum HttpBodyTypeEnum {

	/** 查询参数格式 */
	QUERY_PARAMS,
	/** x-www-form-urlencoded表单格式 */
	FORM, 
	/** multipart表单格式 */
	FORM_DATA, 
	/** JSON格式 */
	JSON, 
	/** URL形式 RESTful风格 */
	PATH_VARIABLE,
	;
}
