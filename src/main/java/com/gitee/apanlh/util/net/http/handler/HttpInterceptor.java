package com.gitee.apanlh.util.net.http.handler;

/**
 * 	HTTP拦截器接口
 * 	<br>再请求前和HTTP响应后进行拦截处理。
 * 	<br>如果需要使用HTTP拦截器请额外实现继承或该接口
 * 	
 * 	@author Pan
 */
public interface HttpInterceptor {
	 
}
