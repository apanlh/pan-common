package com.gitee.apanlh.util.net.ftp;

import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPFileFilter;

import java.util.List;

/**	
 * 	FTP文件相关接口
 * 
 * 	@author Pan
 */
public interface FtpCommandDirectoryFile extends FtpConfigureHandler {
	
	/**	
	 * 	在当前目录下获取特定文件
	 * 	
	 * 	@author Pan
	 * 	@param 	findFileName	需要找寻的文件目标
	 * 	@return	FTPFile
	 */
	FTPFile getFile(String findFileName);
	
	/**
	 * 	获取当前目录下所有文件
	 * 	
	 * 	@author Pan
	 * 	@return	FTPFile[]
	 */
	FTPFile[] getFiles();
	
	/**	
	 * 	获取指定目录下所有文件
	 * 	
	 * 	@author Pan
	 * 	@param 	path		路径
	 * 	@return	FTPFile[]
	 */
	FTPFile[] getFiles(String path);
	
	/**	
	 * 	获取指定目录下所有文件
	 * 	
	 * 	@author Pan
	 * 	@param 	path		路径
	 * 	@param 	fileFilter	文件过滤器
	 * 	@return	FTPFile[]
	 */
	FTPFile[] getFiles(String path, FTPFileFilter fileFilter);
	
	/**	
	 * 	在当前目录下获取一个或多个特定文件	
	 * 
	 * 	@author Pan
	 * 	@param 	findFileName	需要找寻的文件目标
	 * 	@return	List
	 */
	List<FTPFile> getFiles(List<String> findFileName);
	
	/**	
	 * 	检查FTP中是否包含某个文件
	 * 	
	 * 	@author Pan
	 * 	@param 	fileName	被查找文件名
	 * 	@return	boolean		true为存在
	 */
	boolean isExists(String fileName);
	
	/**
	 * 	检查FTP中是否包含某个文件
	 * 	<br>自定义目标目录
	 * 	
	 * 	@author Pan
	 * 	@param 	targetDir	目标目录
	 * 	@param 	fileName	被查找文件名
	 * 	@return	boolean		true为存在
	 */
	boolean isExists(String targetDir, final String fileName);
	
	
	/**	
	 * 	检查FTP中是否包含一个或多个某文件
	 * 	<br>根据需要搜索文件名的List(needFindFileNames)返回List(needFindFileNames)中不存在该文件名的List下标值
	 * 	
	 * 	@author Pan
	 * 	@param 	targetDir			目标目录
	 * 	@param 	needFindFileNames	被查找一个或多个文件名
	 * 	@return	int[]				返回传递进来List中的下标(代表已存在文件)
	 */
	int[] isExists(String targetDir, final List<String> needFindFileNames);
	
	/**
	 * 	在FTP默认目录空间下是否存在一个或全部文件
	 * 	
	 * 	@author Pan
	 * 	@param 	needFindFileNames	被查找文件名
	 * 	@return	boolean	true为存在
	 */
	boolean isAllExists(final String[] needFindFileNames);
	
	/**	
	 * 	检查FTP中是否包含全部文件
	 * 	<br>检查是否文件全部存在
	 * 	
	 * 	@author Pan
	 * 	@param 	targetDir			目标目录
	 * 	@param 	needFindFileNames	被查找文件名
	 * 	@return	boolean	true为存在
	 */
	boolean isAllExists(String targetDir, final String[] needFindFileNames);
	
	/**	
	 * 	检查FTP中是否包含全部文件
	 * 	<br>检查是否文件全部存在
	 * 	
	 * 	@author Pan
	 * 	@param 	targetDir			目标目录
	 * 	@param 	needFindFileNames	被查找文件名
	 * 	@return	boolean	true为存在
	 */
	boolean isAllExists(String targetDir, final List<String> needFindFileNames);
}
