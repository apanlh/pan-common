package com.gitee.apanlh.util.net.http.handler;

import com.gitee.apanlh.exp.HttpException;
import com.gitee.apanlh.util.net.http.HttpResponse;

/**
 * 	HTTP请求拦截器接口
 * 	<br>实现该接口能够在HTTP响应之后进行处理
 * 	<br>支持函数式
 * 	
 * 	@author Pan
 */
@FunctionalInterface
public interface HttpResponseInterceptor extends HttpInterceptor {
	
	/**	
	 * 	处理HTTP响应拦截
	 * 	
	 * 	@author Pan
	 * 	@param 	httpResponse 	HTTP响应对象
	 * 	@param 	chain       	HTTP拦截器链
	 * 	@return HttpResponse 	HTTP响应对象
	 * 	@throws HttpException 	处理异常，则抛出该异常
	 */
	HttpResponse chain(HttpResponse httpResponse, HttpInterceptorChain chain) throws HttpException;
}
