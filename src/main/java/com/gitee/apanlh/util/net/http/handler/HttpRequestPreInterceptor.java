package com.gitee.apanlh.util.net.http.handler;

import com.gitee.apanlh.exp.HttpException;
import com.gitee.apanlh.util.net.http.HttpClient;
import com.gitee.apanlh.util.net.http.HttpRequest;
import com.gitee.apanlh.util.net.http.handler.body.cast.HttpRequestPreBodyCast;

/**
 * 	HTTP请求拦截器(参数解析前)
 * 	<br>实现该接口能够在HTTP请求参数解析之前进行处理
 * 	<br>支持函数式
 * 	
 * 	@author Pan
 */
@FunctionalInterface
public interface HttpRequestPreInterceptor extends HttpInterceptor {
	
	/**
	 * 	处理HTTP请求拦截(参数解析前)
	 * 	<br>获取到的所有参数都是构造后的原始参数
	 * 	
	 * 	@author Pan
	 * 	@param 	httpRequest 	HTTP请求对象
	 * 	@param 	httpClient  	HTTP客户端对象
	 * 	@param 	bodyCast  		HTTPBody转换对象
	 * 	@param 	chain       	HTTP拦截器链
	 * 	@return HttpRequest 	HTTP请求对象
	 * 	@throws HttpException 	处理异常，则抛出该异常
	 */
	HttpRequest chain(HttpRequest httpRequest, HttpClient httpClient, HttpRequestPreBodyCast bodyCast, HttpInterceptorChain chain) throws HttpException;
}
