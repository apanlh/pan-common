package com.gitee.apanlh.util.net.http;

import com.gitee.apanlh.util.base.MapUtils;

import java.util.Map;

/**	
 * 	HTTP-Header
 * 	
 * 	@author Pan
 *  @param  <V>     值类型
 */
public abstract class HttpHeader<V> {
	
	/** 头信息 */
	private Map<String, V> headers;
	
	/**	
	 * 	默认构造-创建请求头
	 * 	<br>默认使用LinkedHashMap
	 * 
	 * 	@author Pan
	 */
	HttpHeader() {
		this(MapUtils.newLinkedHashMap());
	}
	
	/**	
	 * 	构造函数-创建请求头
	 * 	添加实例Map的构造函数
	 * 	
	 * 	@author Pan
	 * 	@param 	map	map
	 */
	HttpHeader(Map<String, V> map) {
		this.headers = map;
	}
	
	/**	
	 * 	添加请求头
	 * 	
	 * 	@author Pan
	 * 	@param 	key		键
	 * 	@param 	value	值
	 * 	@return	HttpHeader
	 */
	public HttpHeader<V> add(String key, V value) {
		headers.put(key, value);
		return this;
	}
	
	/**		
	 * 	获取键
	 * 	
	 * 	@author Pan
	 * 	@param 	key	键
	 * 	@return	String
	 */
	public V get(String key) {
		return headers.get(key);
	}
	
	/**	
	 * 	移除请求头
	 * 	
	 * 	@author Pan
	 * 	@param 	key	键
	 * 	@return	HttpHeader
	 */
	public HttpHeader<V> remove(String key) {
		headers.remove(key);
		return this;
	}
	
	/**	
	 * 	获取请求头
	 * 	
	 * 	@author Pan
	 * 	@return	Map
	 */
	public Map<String, V> getHeaderMap() {
		return this.headers;
	}
	
	
	@Override
	public String toString() {
		return this.headers.toString();
	}
}
