package com.gitee.apanlh.util.net.http.auth;

/**
 * 	HTTP请求认证协议
 * 	
 * 	@author Pan
 */
public enum HttpAuthProtocol {
	
	//	HTTP标准类型
	/** Basic认证*/
    BASIC("Basic"),
    /** Digest认证*/
    DIGEST("Digest"),
    /** Bearer认证*/
    BEARER("Bearer"),

	//	HTTP非标准类型
    /** 自定义认证请求头 */
	CUSTOM(""),
	;
	
	/** 认证请求头 */
	public static final String AUTHORIZATION = "Authorization";
	/** 认证类型 */
	private String type;

    /**	
     * 	构造函数-自定义认证类型
     * 	
     * 	@author Pan
     * 	@param 	type	类型
     */
    HttpAuthProtocol(String type) {
        this.type = type;
    }

	/**	
	 * 	获取认证类型
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getType() {
		return type;
	}
}
