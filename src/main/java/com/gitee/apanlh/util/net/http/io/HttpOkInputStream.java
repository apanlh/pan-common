package com.gitee.apanlh.util.net.http.io;

import java.io.InputStream;

/**	
 * 	HTTP成功输入流
 * 	
 * 	@author Pan
 */
public class HttpOkInputStream extends HttpInputStream {

	/**	
	 * 	构造函数-引用原有HTTP输入流
	 * 	
	 * 	@author Pan
	 * 	@param 	inputStream	输入流
	 */
	public HttpOkInputStream(InputStream inputStream) {
		super(inputStream);
	}
	
}
