package com.gitee.apanlh.util.net.ftp;

import com.gitee.apanlh.util.encode.CharsetCode;
import com.gitee.apanlh.util.unit.BuffSize;
import org.apache.commons.net.ftp.FTP;

/**	
 * 	FTP配置
 * 
 * 	@author Pan
 */
public class FtpConfig {
	
	/** 连接方式(默认主动) */
	private FtpConnectMode connectMode = FtpConnectMode.ACTIVE;
	/** 文件类型 */
	private int fileTypeMode;
	/** 文件传输模式 */
	private int fileTransferMode;
	/** 接收时的缓冲区 */
	private int receiveBufferSize;
	/** 发送时的缓冲区 */
	private int sendBufferSize;
	/** 编码 */
	private String encoding;
	/** 全局超时(包含终端连接、传输命令、传输超时)*/
	private int globalTimeOut;
	/** 连接FTP终端超时(毫秒) */
	private int connectTimeout;
	/** 传输命令超时(毫秒) */
	private int commandTimeout;
	/** 数据传输超时(毫秒) */
	private int dataTimeout;
	/** 默认工作空间 */
	private String workingDirectory;
	
	/**	
	 * 	设置FTP连接默认配置
	 * 	
	 * 	@author Pan
	 * 	@param 	ftpConnection 	ftp连接对象
	 * 	@return	FtpConfig
	 */
	static FtpConfig getDefaultConfig(FtpConnection ftpConnection) {
		FtpConfig defaultConfig = getDefaultConfig();
		ftpConnection.setConfig(defaultConfig);
		return defaultConfig;
	}
	
	/**	
	 * 	设置默认配置
	 * 	
	 * 	@author Pan
	 * 	@return	FtpConfig
	 */
	public static FtpConfig getDefaultConfig() {
		FtpConfig cfg = new FtpConfig();
		cfg.setEncoding(CharsetCode.UTF_8);
		cfg.setGlobalTimeOut(FtpStarterConfigure.DEFAULT_TIMEOUT);
		//	默认缓冲8K
		cfg.setReceiveBufferSize(BuffSize.SIZE_8K);
		cfg.setSendBufferSize(BuffSize.SIZE_8K);
		//	二进制、字节流形式传输
		cfg.setFileTypeMode(FTP.BINARY_FILE_TYPE);
		cfg.setFileTransferMode(FTP.STREAM_TRANSFER_MODE);
		//	主动模式
		cfg.setConnectMode(FtpConnectMode.ACTIVE);
		return cfg;
	}
	
	/**
	 * 	默认构造函数
	 * 	@author Pan
	 */
	public FtpConfig() {
		super();
	}
	
	/**	
	 * 	获取默认工作空间
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getWorkingDirectory() {
		return workingDirectory;
	}

	/**	
	 * 	获取工作空间地址
	 * 	
	 * 	@author Pan
	 * 	@param 	workingDirectory	工作空间
	 */
	public void setWorkingDirectory(String workingDirectory) {
		this.workingDirectory = workingDirectory;
	}

	/**	
	 * 	获取FTP连接模式
	 * 	
	 * 	@author Pan
	 * 	@return	FtpConnectMode
	 */
	public FtpConnectMode getConnectMode() {
		return connectMode;
	}

	/**	
	 * 	设置 FTP连接模式
	 * 	
	 * 	@author Pan
	 * 	@param 	connectMode	连接模式
	 */
	public void setConnectMode(FtpConnectMode connectMode) {
		this.connectMode = connectMode;
	}

	/**	
	 * 	获取文件类型
	 * 	
	 * 	@author Pan
	 * 	@return	int
	 */
	public int getFileTypeMode() {
		return fileTypeMode;
	}

	/**	
	 * 	设置文件类型
	 * 	
	 * 	@author Pan
	 * 	@param 	fileTypeMode	文件类型模式
	 */
	public void setFileTypeMode(int fileTypeMode) {
		this.fileTypeMode = fileTypeMode;
	}

	/**	
	 * 	获取传输文件类型
	 * 	
	 * 	@author Pan
	 * 	@return	int
	 */
	public int getFileTransferMode() {
		return fileTransferMode;
	}

	/**	
	 * 	设置传输文件类型
	 * 	
	 * 	@author Pan
	 * 	@param 	fileTransferMode	文件传输类型
	 */
	public void setFileTransferMode(int fileTransferMode) {
		this.fileTransferMode = fileTransferMode;
	}

	/**	
	 * 	获取传输接收缓冲区大小
	 * 	
	 * 	@author Pan
	 * 	@return	int
	 */
	public int getReceiveBufferSize() {
		return receiveBufferSize;
	}

	/**	
	 * 	设置传输接收缓冲区大小
	 * 
	 * 	@author Pan
	 * 	@param 	receiveBufferSize	传输接收缓冲区大小
	 */
	public void setReceiveBufferSize(int receiveBufferSize) {
		this.receiveBufferSize = receiveBufferSize;
	}

	/**	
	 * 	获取传输发送缓冲区大小
	 * 
	 * 	@author Pan
	 * 	@return	int
	 */
	public int getSendBufferSize() {
		return sendBufferSize;
	}

	/**	
	 * 	设置传输发送缓冲区大小
	 * 	
	 * 	@author Pan
	 * 	@param 	sendBufferSize	传输发送缓冲区大小
	 */
	public void setSendBufferSize(int sendBufferSize) {
		this.sendBufferSize = sendBufferSize;
	}

	/**	
	 * 	获取编码格式
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getEncoding() {
		return encoding;
	}

	/**	
	 * 	设置编码格式
	 * 	
	 * 	@author Pan
	 * 	@param 	encoding	编码格式
	 */
	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	/**	
	 * 	获取全局超时(毫秒)
	 * 	
	 * 	@author Pan
	 * 	@return	int
	 */
	public int getGlobalTimeOut() {
		return globalTimeOut;
	}

	/**	
	 * 	设置	全局超时(毫秒)
	 * 
	 * 	@author Pan
	 * 	@param 	globalTimeOut	全局超时(毫秒)
	 */
	public void setGlobalTimeOut(int globalTimeOut) {
		this.globalTimeOut = globalTimeOut;
	}

	/**	
	 * 	获取建立连接超时
	 * 	
	 * 	@author Pan
	 * 	@return	int
	 */
	public int getConnectTimeout() {
		return connectTimeout;
	}

	/**
	 * 	设置建立连接超时(毫秒)
	 * 	
	 * 	@author Pan
	 * 	@param 	connectTimeout	连接超时时间
	 */
	public void setConnectTimeout(int connectTimeout) {
		this.connectTimeout = connectTimeout;
	}

	/**	
	 * 	获取控制命令超时
	 * 	
	 * 	@author Pan
	 * 	@return	int
	 */
	public int getCommandTimeout() {
		return commandTimeout;
	}

	/**	
	 * 	设置控制命令超时(毫秒)
	 * 	
	 * 	@author Pan
	 * 	@param 	commandTimeout	命令超时时间
	 */
	public void setCommandTimeout(int commandTimeout) {
		this.commandTimeout = commandTimeout;
	}

	/**	
	 * 	获取传输文件超时
	 * 
	 * 	@author Pan
	 * 	@return	int
	 */
	public int getDataTimeout() {
		return dataTimeout;
	}

	/**	
	 * 	设置传输文件超时(毫秒)
	 * 	
	 * 	@author Pan
	 * 	@param 	dataTimeout	传输文件超时时间
	 */
	public void setDataTimeout(int dataTimeout) {
		this.dataTimeout = dataTimeout;
	}
	
	/**	
	 * 	创建FTP配置
	 * 	
	 * 	@author Pan
	 * 	@return	FtpConfig
	 */
	public static FtpConfig create() {
		return new FtpConfig();
	}
}
