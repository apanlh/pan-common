package com.gitee.apanlh.util.net.ftp;

import java.io.InputStream;
import java.util.List;

/**	
 * 	FTP文件操作相关
 * 
 * 	@author Pan
 */
public interface FtpCommandFileExecutor extends FtpConfigureHandler {

	/**	
	 * 	文件上传至FTP服务器
	 * 	<br> 默认目录下
	 * 	<br> 默认为二进制类型
	 * 	<br> 默认为流类型
	 * 	
	 * 	@author Pan
	 * 	@param 	fileName	需要从FTP下载的文件名
	 * 	@param 	bytes 		字节流
	 * 	@return	boolean 	true成功
	 */
	boolean upload(String fileName, byte[] bytes);
	
	/**	
	 * 	文件上传至FTP服务器
	 * 	<br> 默认为二进制类型
	 * 	<br> 默认为流类型
	 * 	
	 * 	@author Pan
	 * 	@param 	targetDir	FTP服务器目录
	 * 	@param 	fileName	需要从FTP下载的文件名
	 * 	@param 	bytes 		字节流
	 * 	@return	boolean 	true成功
	 */
	boolean upload(String targetDir, String fileName, byte[] bytes);
	
	/**	
	 * 	文件上传至FTP服务器
	 * 	<br> 默认为二进制类型
	 * 	<br> 默认为流类型
	 * 
	 * 	@author Pan
	 * 	@param 	fileName	需要从FTP下载的文件名
	 * 	@param 	is 			输入流
	 * 	@return	boolean 	true成功
	 */
	boolean upload(String fileName, InputStream is);
	
	/**	
	 * 	文件上传至FTP服务器
	 * 	<br> 默认为二进制类型
	 * 	<br> 默认为流类型
	 * 
	 * 	@author Pan
	 * 	@param 	targetDir	FTP服务器目录
	 * 	@param 	fileName	需要从FTP下载的文件名
	 * 	@param 	is 			输入流
	 * 	@return	boolean 	true成功
	 */
	boolean upload(String targetDir, String fileName, InputStream is);
	
	/**	
	 * 	从FTP默认目录下开始下载文件
	 * 	<br>需自行关闭输出流
	 * 	
	 * 	@author Pan
	 * 	@param 	fileName	需要从ftp下载的文件名
	 * 	@return	byte[]
	 */
	byte[] download(String fileName);
	
	/**		
	 * 	从FTP下载
	 * 	<br>需自行关闭输出流
	 * 	
	 * 	@author Pan
	 * 	@param 	targetDir	FTP服务器目录
	 * 	@param 	fileName	需要从FTP下载的文件名
	 * 	@return	byte[]
	 */
	byte[] download(String targetDir, String fileName);
	
	/**	
	 * 	在默认目录下删除指定文件
	 * 	
	 * 	@author Pan
	 * 	@param 	fileName	删除文件名
	 * 	@return	boolean		true删除成功
	 */
	boolean delFile(String fileName);
	
	/**
	 * 	删除指定目录下指定文件
	 * 	
	 * 	@author Pan
	 * 	@param 	targetDir	FTP服务器目录
	 * 	@param 	fileName	删除文件名
	 * 	@return	boolean		true删除成功
	 */
	boolean delFile(String targetDir, String fileName);
	
	/**	
	 * 	在默认目录下删除一个或多个文件
	 * 	<br>在出现删除时失败时会继续删除相关文件但是结果返回false
	 * 	
	 * 	@author Pan
	 * 	@param 	filesName	删除一个或多个文件名
	 * 	@return	boolean		true删除成功
	 */
	boolean delFiles(String[] filesName);
	
	/**	
	 * 	在指定目录下删除一个或多个文件-{@code String[]}
	 * 	<br>在出现删除时失败时会继续删除相关文件但是结果返回false
	 * 	
	 * 	@author Pan
	 * 	@param 	targetDir	指定目录
	 * 	@param 	filesName	删除一个或多个文件名
	 * 	@return	boolean		true删除成功
	 */
	boolean delFiles(String targetDir, String[] filesName);
	
	/**	
	 * 	在指定目录下删除一个或多个文件-{@code List<String>}
	 * 	<br>在出现删除时失败时会继续删除相关文件但是结果返回false
	 * 	
	 * 	@author Pan
	 * 	@param 	targetDir	指定目录
	 * 	@param 	filesName	删除一个或多个文件名
	 * 	@return	boolean		true删除成功
	 */
	boolean delFiles(String targetDir, List<String> filesName);
}
