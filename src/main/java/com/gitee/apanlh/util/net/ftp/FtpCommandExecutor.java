package com.gitee.apanlh.util.net.ftp;

import org.apache.commons.net.ftp.FTPClient;

/**	
 * 	FTP命令器
 * 
 * 	@author Pan
 */
public class FtpCommandExecutor implements FtpConfigureHandler {
	
	/** FTP命令 */
	private FtpCommand command;
	/** FTP执行器 */
	private FTPClient executor;
	/** FTP目录操作相关 */
	private FtpCommandDirectory directoryExecutor;
	/** FTP文件操作相关 */
	private FtpCommandFileExecutor fileExecutor;
	
	/**
	 * 	构造函数-默认
	 * 
	 * 	@author Pan
	 */
	FtpCommandExecutor() {
		super();
	}
	
	/**	
	 * 	构造函数-创建执行器
	 * 	
	 * 	@author Pan
	 * 	@param 	executor	FTP客户端
	 */
	public FtpCommandExecutor(FTPClient executor) {
		this.executor = executor;
		this.command = new FtpCommand(executor);
		this.directoryExecutor = new FtpDirectory(this);
		this.fileExecutor = new FtpFile(this);
	}
	
	/**	
	 * 	获取执行器
	 * 	
	 * 	@author Pan
	 * 	@return FTPClient
	 */
	protected FTPClient getExecutor() {
		return executor;
	}

	/**	
	 * 	获取命令信息
	 * 	
	 * 	@author Pan
	 * 	@return	FtpCommand
	 */
	public FtpCommand getCommand() {
		return command;
	}
	
	/**
	 * 	获取目录操作对象
	 * 	
	 * 	@author Pan
	 * 	@return	FtpCommandDirectory
	 */
	protected FtpCommandDirectory getDirectory() {
		return directoryExecutor;
	}

	/**	
	 * 	获取目录地址执行器
	 * 	
	 * 	@author Pan
	 * 	@return	FtpCommandDirectoryPath
	 */
	public FtpCommandDirectoryPath getDirPathExecutor() {
		return directoryExecutor.getDirectoryPath();
	}

	/**	
	 * 	获取目录文件执行器
	 * 	
	 * 	@author Pan
	 * 	@return	FtpCommandDirectoryFile
	 */
	public FtpCommandDirectoryFile getDirFileExecutor() {
		return directoryExecutor.getDirectoryFile();
	}
	
	/**	
	 * 	获取文件执行器
	 * 	
	 * 	@author Pan
	 * 	@return	FtpCommandFileExecutor
	 */
	public FtpCommandFileExecutor getFileExecutor() {
		return fileExecutor;
	}
	
	@Override
	public void beforeConfigure() {
		directoryExecutor.beforeConfigure();
		fileExecutor.beforeConfigure();
	}

	@Override
	public void afterConfigure() {
		directoryExecutor.afterConfigure();
		fileExecutor.afterConfigure();
	}
}
