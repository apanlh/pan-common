package com.gitee.apanlh.util.net.http.handler.body;

import com.gitee.apanlh.exp.HttpBodyParseException;
import com.gitee.apanlh.util.dataformat.JsonUtils;
import com.gitee.apanlh.util.net.http.HttpBody;
import com.gitee.apanlh.util.net.http.HttpClient;
import com.gitee.apanlh.util.net.http.HttpRequest;

/**
 * 	HTTPBody请求体解析
 * 	<br>JSON格式请求
 * 	
 * 	@author Pan
 */
public class HttpBodyJsonParseStrategy implements HttpBodyParseStrategy {

	@Override
	public HttpRequest parse(HttpRequest httpRequest, HttpClient httpClient) throws HttpBodyParseException {
		HttpBody httpBody = httpRequest.getBody();
		//	忽略解析
		if (!httpBody.hasParameterParse()) {
			httpBody.setRequestBody(JsonUtils.toJson(httpBody.getRequestBody()));
		}
		httpRequest.setBody(httpBody);
		return httpRequest;
	}
}
