package com.gitee.apanlh.util.net.ftp;

/**	
 * 	FTP状态
 * 
 * 	@author Pan
 */
public enum FtpStatus {
	
	DEL_SUC_STATUS(250);
	
	FtpStatus(Integer status) {
		this.status = status;
	}
	
	/** 状态 */
	private Integer status;

	/**	
	 * 	获取状态 
	 * 	
	 * 	@author Pan
	 * 	@return	Integer
	 */
	public Integer getStatus() {
		return status;
	}
	
	/**	
	 * 	验证是否删除成功状态
	 * 	<br>返回true为成功
	 * 	
	 * 	@author Pan
	 * 	@param 	resultCode	返回装填
	 * 	@return	boolean
	 */
	public static boolean isDelSuc(Integer resultCode) {
		return DEL_SUC_STATUS.getStatus().equals(resultCode);
	}
}
