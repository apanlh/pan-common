package com.gitee.apanlh.util.file;

/**
 * 	文件归档
 * 	@author Pan
 */
public class FileArchiveUtils {
	
	/** 后缀类型 */
	public static final String ZIP_SUFFIX = ".zip";
	public static final String RAR_SUFFIX = ".rar";
	
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private FileArchiveUtils() {
		//	不允许外部实例
		super();
	}
	
}
