package com.gitee.apanlh.util.file;

/**	
 * 	文件类型匹配
 * 	<br>用于文件搜索等
 * 
 * 	@author Pan
 */
public enum FileMatchType {

	//	文件匹配类型
	/** 文件类型 */
	FILE("file"),
	/** 目录类型 */
	DIRECTORY(""),
	/** 所有类型 */
	ALL(""),
	
	//	-----------后缀匹配
	/** JAR类型 */
	JAR(".jar"),
	/** WAR类型 */
	WAR(".war"),
	/** ZIP类型 */
	ZIP(".zip"),
	/** RAR类型 */
	RAR(".rar"),
	/** GZIP类型 */
	GZIP(".gzip"),
	/** ISO类型 */
	ISO(".iso"),
	/** 7Z类型 */
	SEVEN_ZIP(".7z"),
	/** tar类型 */
	TAR(".tar"),
	/** tar.gz类型 */
	TAR_GZ(".tar.gz");
	
	/** 值 */
	private String value;
	
	/**	
	 * 	构造函数
	 * 	<br>自定义别名
	 * 	
	 * 	@author Pan
	 * 	@param 	value	值
	 */
	FileMatchType(String value) {
		this.value = value;
	}
	
	/**		
	 * 	获取值
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getValue() {
		return value;
	}
}
