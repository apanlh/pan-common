package com.gitee.apanlh.util.file;

import com.gitee.apanlh.exp.FileDeleteException;
import com.gitee.apanlh.exp.IORuntimeException;
import com.gitee.apanlh.exp.NotFoundException;
import com.gitee.apanlh.util.base.ArrayUtils;
import com.gitee.apanlh.util.base.CollUtils;
import com.gitee.apanlh.util.base.Empty;
import com.gitee.apanlh.util.base.StringUtils;
import com.gitee.apanlh.util.io.FileIOUtils;
import com.gitee.apanlh.util.io.IOUtils;
import com.gitee.apanlh.util.valid.Assert;
import com.gitee.apanlh.util.valid.ValidParam;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

/**
 * 	文件工具类
 * 
 *	@author Pan
 */
public class FileUtils {
	
	public static final String CLASS_SUFFIX = ".class";
	
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private FileUtils() {
		//	不允许外部实例
		super();
	}
	
	/**
	 * 	创建目录
	 * 	<br>路径不存在则自动创建路径
	 * 	
	 * 	@author Pan
	 * 	@param  filePath	目录地址
	 * 	@return boolean		true创建成功
	 */
	public static boolean create(String filePath) {
		return create(filePath, null, true, false);
	}
	
	/**
	 * 	创建目录+文件
	 * 	<br>路径不存在则自动创建路径
	 * 	<br>如果文件已存在则忽略并返回false
	 * 	
	 * 	@author Pan
	 * 	@param  filePath	目录地址
	 * 	@param  fileName	文件名称
	 * 	@return boolean		true创建成功
	 */
	public static boolean create(String filePath, String fileName) {
		return create(filePath, fileName, false, false);
	}
	
	/**
	 * 	创建目录+文件
	 * 	<br>路径不存在则自动创建路径
	 * 	<br>自定义当文件已存在的处理(覆盖、忽略)
	 * 	
	 * 	@author Pan
	 * 	@param  filePath	目录地址
	 * 	@param  fileName	文件名称
	 * 	@param  overwrite	true文件覆盖
	 * 	@return boolean		true创建成功
	 */
	public static boolean create(String filePath, String fileName, boolean overwrite) {
		return create(filePath, fileName, false, overwrite);
	}
	
	/**
	 * 	创建文件
	 * 	<br>路径不存在则自动创建路径
	 * 	<br>自定义当文件已存在的处理(覆盖、忽略)
	 * 	
	 * 	@author Pan
	 * 	@param  filePath	目录地址
	 * 	@param  fileName	文件名称
	 * 	@param  createModel	true创建目录模式,false创建目录+文件模式
	 * 	@param  overwrite	true文件覆盖,false忽略
	 * 	@return boolean		true创建成功
	 */
	public static boolean create(String filePath, String fileName, boolean createModel, boolean overwrite) {
		Assert.isNotEmpty(filePath);
		
		//	修复路径
		String reviseFilePath = reviseFilePath(filePath);
		
		//	创建目录
		boolean exist = existPath(reviseFilePath);
		if (exist && createModel) {
			return true;
		}
		if (!exist) {
			boolean createDirectors = createDirectors(reviseFilePath);
			if (createModel) {
				return createDirectors;
			}
		}
		
		Assert.isNotEmpty(fileName);
		try {
			//	文件覆盖
			File file = new File(StringUtils.append(reviseFilePath, reviseFileName(fileName)));
			if (overwrite && file.exists()) {
				del(file);
			}
			return file.createNewFile();
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage(), e);
		}
	}
	
	/**
	 * 	创建单层目录
	 * 
	 * 	@author Pan
	 * 	@param  filePath	目录文件
	 * 	@return	boolean		true创建成功
	 */
	public static boolean createDirectory(String filePath) {
		return new File(filePath).mkdir();
	}
	
	/**
	 * 	创建多层级目录
	 * 
	 * 	@author Pan
	 * 	@param  filePath	目录文件
	 * 	@return	boolean		true创建成功
	 */
	public static boolean createDirectors(String filePath) {
		return new File(filePath).mkdirs();
	}

	/**	
	 * 	创建Path对象
	 * 	<br>可以是目录也可以是文件的绝对路径
	 * 	
	 * 	@author Pan
	 * 	@param 	file		文件对象
	 * 	@return	Path
	 */
	public static Path createPath(File file) {
		Assert.isNotNull(file);

		return Paths.get(file.getPath());
	}
	
	/**	
	 * 	创建Path对象
	 * 	<br>可以是目录也可以是文件的绝对路径
	 * 	
	 * 	@author Pan
	 * 	@param 	filePath	目录地址
	 * 	@return	Path
	 */
	public static Path createPath(String filePath) {
		Assert.isNotNull(filePath);
		
		return Paths.get(filePath);
	}
	
	/**	
	 * 	将原始文件拷贝至目标文件地址
	 * 	<br>如果目标文件已存在或目标目录不存在则抛出异常
	 * 	
	 * 	@author Pan	
	 * 	@param 	source	来源文件
	 * 	@param 	target	目标文件
	 * 	@return	File
	 */
	public static File copy(String source, String target) {
		return copy(new File(source), new File(target));
	}
	
	/**	
	 * 	将原始文件拷贝至目标文件地址
	 * 	<br>如果目标文件已存在或目标目录不存在则抛出异常
	 * 	
	 * 	@author Pan	
	 * 	@param 	source	来源文件
	 * 	@param 	target	目标文件
	 * 	@return	File
	 */
	public static File copy(String source, File target) {
		return copy(new File(source), target);
	}
	
	/**	
	 * 	将原始文件拷贝至目标文件地址
	 * 	<br>如果目标文件已存在或目标目录不存在则抛出异常
	 * 	
	 * 	@author Pan	
	 * 	@param 	source	来源文件
	 * 	@param 	target	目标文件
	 * 	@return	File
	 */
	public static File copy(File source, String target) {
		return copy(source, new File(target));
	}
	
	/**	
	 * 	将原始文件拷贝至目标文件地址
	 * 	<br>如果目标文件已存在或目标目录不存在则抛出异常
	 * 	
	 * 	@author Pan	
	 * 	@param 	source	来源文件
	 * 	@param 	target	目标文件
	 * 	@return	File
	 */
	public static File copy(File source, File target) {
		try {
			Files.copy(source.toPath(), target.toPath());
			return target;
		} catch (Exception e) {
			throw new IORuntimeException(e.getMessage(), e);
		}
	}
	
	/**	
	 * 	将原始文件移动至目标文件地址
	 * 	<br>如果目标文件已存在或目标目录不存在则抛出异常
	 * 	
	 * 	@author Pan	
	 * 	@param 	source	来源文件
	 * 	@param 	target	目标文件
	 * 	@return	File
	 */
	public static File move(String source, String target) {
		return move(new File(source), new File(target));
	}
	
	/**	
	 * 	将原始文件移动至目标文件地址
	 * 	<br>如果目标文件已存在或目标目录不存在则抛出异常
	 * 	
	 * 	@author Pan	
	 * 	@param 	source	来源文件
	 * 	@param 	target	目标文件
	 * 	@return	File
	 */
	public static File move(String source, File target) {
		return move(new File(source), target);
	}
	
	/**	
	 * 	将原始文件移动至目标文件地址
	 * 	<br>如果目标文件已存在或目标目录不存在则抛出异常
	 * 	
	 * 	@author Pan	
	 * 	@param 	source	来源文件
	 * 	@param 	target	目标文件
	 * 	@return	File
	 */
	public static File move(File source, String target) {
		return move(source, new File(target));
	}
	
	/**	
	 * 	将原始文件移动至目标文件地址
	 * 	<br>如果目标文件已存在或目标目录不存在则抛出异常
	 * 	
	 * 	@author Pan	
	 * 	@param 	source	来源文件
	 * 	@param 	target	目标文件
	 * 	@return	File
	 */
	public static File move(File source, File target) {
		try {
			Files.move(source.toPath(), target.toPath());
			return target;
		} catch (Exception e) {
			throw new IORuntimeException(e.getMessage(), e);
		}
	}
	
	/**
	 * 	删除文件或目录
	 * 	
	 * 	@author Pan
	 * 	@param  filePath	文件路径
	 * 	@return	boolean		true成功
	 */
	public static boolean del(String filePath) {
		return del(createPath(filePath));
	}
	
	/**	
	 * 	递归删除文件夹
	 * 	<br>以及文件夹目录所有的文件与目录
	 * 
	 * 	@author Pan
	 * 	@param  file	文件对象
	 * 	@return	boolean
	 */
	public static boolean del(File file) {
		if (file == null) {
			return false;
		}
        if (file.isDirectory()) {
            String[] children = file.list();
            if (ArrayUtils.isEmpty(children)) {
            	return false;
            }
            
            //	递归删除目录中的子目录下
            for (int i = 0, childrenLen = children.length; i < childrenLen; i++) {
                boolean success = del(new File(file, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // 	目录下以无文件	删除目录
        return del(createPath(file));
    }
	
	/**	
	 * 	删除文件或目录
	 * 	<br>如果删除目录时该目录还存在其他目录则无法删除
	 * 	
	 * 	@author Pan
	 * 	@param 	path	地址
	 * 	@return	boolean
	 */
	public static boolean del(Path path) {
		try {
			Files.delete(path);
			return true;
		} catch (Exception e) {
			throw new FileDeleteException(e.getMessage(), e);
		}
	}

	/**
	 * 	从首行开始删除文件
	 * 	<br>自定义删除行数
	 * 	<br>建议文件较小时才用此方法
	 * 	
	 * 	@author Pan
	 * 	@param  filePath	文件位置
	 * 	@param  delCount 	删除行数(1为首行)
	 */
	public static void delLine(String filePath, int delCount) {
		if (delCount <= 0) {
			return ;
		}
		
		BufferedReader br = null;
		BufferedWriter bw = null;
		try {
			File file = new File(filePath);
			
			br = IOUtils.getReader(file);
			List<String> list = FileIOUtils.readLines(br);
			
			if (ValidParam.isEmpty(list)) {
				return ;
			}
			
			//	重新生成文件
			if (list.size() <= delCount) {
				FileIOUtils.writeToFile(Empty.arrayByte(), file);
				return ;
			}
			
			int currentDel = 0;
			while (currentDel < delCount) {
				list.remove(0);
				currentDel++;
			}
			
			bw = FileIOUtils.getWriter(file);
			for (int i = 0, len = list.size(); i < len; i++) {
				bw.write(list.get(i));
				bw.newLine();
				bw.flush();
			}
		} catch (Exception e) {
			throw new FileDeleteException(e.getMessage(), e);
		} finally {
			IOUtils.close(bw, br);
		}	
	}
	
	/**
	 * 	检查目录是否存在
	 * 	
	 * 	@author Pan
	 * 	@param  filePath	文件目录
	 * 	@return boolean		true存在
	 */
	public static boolean existPath(String filePath) {
		return existPath(filePath, false);
	}
	
	/**
	 * 	检查目录是否存在
	 * 	<br>自定义创建目录模式(创建目录)
	 * 	<br>如果isCreate为true则返回值是创建目录是否成功
	 * 	<br>如果isCreate为false则返回值是目录是否存在
	 * 
	 * 	@author Pan
	 * 	@param  filePath	文件目录
	 * 	@param  isCreate 	创建模式true则返回值是创建目录是否成功, false则返回值是目录是否存在
	 * 	@return boolean
	 */
	public static boolean existPath(String filePath, boolean isCreate) {
		Assert.isNotEmpty(filePath);
		
		File file = new File(reviseFilePath(filePath));
		boolean exists = file.exists();
		
		if (isCreate && !exists) {
			return file.mkdirs();
		}
		return exists;
	}
	
	/**
	 * 	检查文件是否存在
	 * 
	 * 	@author Pan
	 * 	@param  filePath	文件目录
	 * 	@param  fileName	文件名称
	 * 	@return boolean		true存在
	 */
	public static boolean existFile(String filePath, String fileName) {
		return existFile(filePath, fileName, false);
	}
	
	/**
	 * 	检查文件是否存在
	 * 	<br>自定义目录创建模式(创建文件)
	 * 	<br>如果isCreate为true则返回值是创建目录是否成功
	 * 	<br>如果isCreate为false则返回值是目录是否存在
	 * 
	 * 	@author Pan
	 * 	@param  filePath	文件目录
	 * 	@param  fileName	文件名称
	 * 	@param  isCreate 	创建模式true则返回值是创建文件是否成功, false则返回值是文件是否存在
	 * 	@return boolean
	 */
	public static boolean existFile(String filePath, String fileName, boolean isCreate) {
		Assert.isNotEmpty(filePath);
		Assert.isNotEmpty(fileName);
		
		String reviseFilePath = reviseFilePath(filePath);
		String reviseFileName = reviseFileName(fileName);
		
		File file = new File(StringUtils.append(reviseFilePath, reviseFileName));
		boolean exists = file.exists();
		
		if (isCreate && !exists) {
			return create(reviseFilePath, reviseFileName);
		}
		return exists;
	}

	/**		
	 * 	获取File对象
	 * 	
	 * 	@author Pan
	 * 	@param 	pathname	路径名称
	 * 	@return	String
	 */
	public static File getFile(String pathname) {
		return new File(pathname);
	}
	
	/**		
	 * 	获取File对象当前的路径
	 * 	
	 * 	@author Pan
	 * 	@param 	file	文件
	 * 	@return	String
	 */
	public static String getFilePath(File file) {
		if (file == null) {
			return null;
		}
		
		String filePath = file.getPath();
		if (filePath == null) {
			return null;
		}
		return filePath.substring(0, filePath.length() - file.getName().length());
	}
	
	/**	
	 * 	获取指定文件目录下的所有文件对象
	 * 	<br>单层目录
	 * 
	 * 	@author Pan
	 * 	@param 	filePath	目录地址
	 * 	@return	List
	 */
	public static List<File> getFiles(String filePath) {
		return getFiles(filePath, FileMatchType.ALL);
	}
	
	/**	
	 * 	获取指定文件目录下，文件类型
	 * 	<br>自定义文件类型
	 * 	<br>单层目录
	 * 
	 * 	@author Pan
	 * 	@param 	filePath	目录地址
	 * 	@param 	fileType	文件类型
	 * 	@return	List
	 */
	public static List<File> getFiles(String filePath, FileMatchType fileType) {
		Assert.isNotEmpty(filePath);
		
		String reviseFilePath = reviseFilePath(filePath);
		
		File file = new File(reviseFilePath);
		if (!file.exists()) {
			return Empty.list();
		}
		
		File[] files = file.listFiles();
		if (ArrayUtils.isEmpty(files)) {
			return Empty.list();
		}
		
		List<File> newArrayList = CollUtils.newArrayList(files.length);
		
		for (int i = 0; i < files.length; i++) {
			File f = files[i];
			switch (fileType) {
				case FILE:
					if (f.isFile()) {
						newArrayList.add(f);
					}
					break;
				case DIRECTORY:
					if (f.isDirectory()) {
						newArrayList.add(f);
					}
					break;
				case ALL: 
					newArrayList.add(f);
					break;
				default:
					//	匹配后缀
					if (f.getName().endsWith((fileType.getValue()))) {
						newArrayList.add(f);
					}
					break;
			}
		}
		return newArrayList;
	}

	/**
	 * 	截取文件名称 /aaa/bbb/cc/dd/ee/666.png ------ 666.png
	 *
	 *	@author Pan
	 * 	@param 	name 文件路径/文件名/
	 * 	@return 没有文件路径的名称
	 */
	public static String subName(String name) {
		return subName(name, false);
	}

	/**
	 * 	截取文件名称 /aaa/bbb/cc/dd/ee/666.png ------ 666.png
	 *	<br>开启忽略后缀/aaa/bbb/cc/dd/ee/666.png ------ 666
	 *
	 *	@author Pan
	 * 	@param 	name 			文件路径/文件名/
	 * 	@param 	ignoreSuffix 	是否忽略后缀
	 * 	@return 没有文件路径的名称
	 */
	public static String subName(String name, boolean ignoreSuffix) {
		if (name == null) {
			return null;
		}
		int unix = name.lastIndexOf('/');
		int windows = name.lastIndexOf('\\');

		if (unix == -1 && windows == -1) {
			return name;
		}

		int index = Math.max(unix, windows);
		String substring = name.substring(index + 1);

		if (ignoreSuffix) {
			String suffix = getSuffix(substring, false);
			return substring.substring(0, suffix == null ? substring.length() : suffix.length());
		}
		return substring;
	}

	/**
	 * 	获取文件后缀
	 * 	<br>默认不包含.
	 * 	<br>123.txt=txt
	 *
	 * 	@author Pan
	 * 	@param  file		文件对象
	 * 	@return String
	 */
	public static String getSuffix(File file) {
		return getSuffix(file.getName(), false);
	}

	/**
	 * 	获取文件后缀
	 * 	<br>自定义是否包含.
	 * 	<br>123.txt(包含.模式)=.txt
	 *
	 * 	@author Pan
	 * 	@param  file		文件对象
	 * 	@param  ignore		true返回时包含.(123.txt -- .txt)
	 * 	@return String
	 */
	public static String getSuffix(File file, boolean ignore) {
		return getSuffix(file.getName(), ignore);
	}

	/**
	 * 	获取文件后缀
	 * 	<br>默认不包含.
	 * 	<br>123.txt=txt
	 *
	 * 	@author Pan
	 * 	@param  name		名称(不限文件绝对路径、文件相对路径、文件名称)
	 * 	@return String
	 */
	public static String getSuffix(String name) {
		return getSuffix(name, false);
	}

	/**
	 * 	获取文件后缀
	 * 	<br>自定义是否包含.
	 * 	<br>123.txt(包含.模式)=.txt
	 *
	 * 	@author Pan
	 * 	@param  name		名称(不限文件绝对路径、文件相对路径、文件名称)
	 * 	@param  ignore		true返回时包含.(123.txt = .text)
	 * 	@return String
	 */
	public static String getSuffix(String name, boolean ignore) {
		if (ValidParam.isEmpty(name)) {
			return null;
		}
		int separatorIndex = name.lastIndexOf(".");
		if (separatorIndex == -1) {
			return null;
		}
		if (ignore) {
			return name.substring(separatorIndex).toLowerCase();
		}
		return name.substring(separatorIndex + 1).toLowerCase();
	}

	/**
	 * 	获取文件类型-图片相关
	 *
	 * 	@author Pan
	 * 	@param  bytes	字节
	 * 	@return String
	 */
	public static String getExtendOfPhoto(byte[] bytes) {
		if (ValidParam.isEmpty(bytes)) {
			return null;
		}
		//	FF D8 FF
		if ((bytes[0] == (byte) 0xff) && (bytes[1] == (byte) 0xd8) && (bytes[2] == (byte) 0xff)
				&& (bytes[6] == 74) && (bytes[7] == 70) && (bytes[8] == 73) && (bytes[9] == 70)) {
			return "jpg";
		}
		//	137,80,78,71
		if ((bytes[0] == -119) && (bytes[1] == 80) && (bytes[2] == 78) && (bytes[3] == 71)) {
			return "png";
		}
		if ((bytes[0] == 71) && (bytes[1] == 73) && (bytes[2] == 70) && (bytes[3] == 56)
				&& ((bytes[4] == 55) || (bytes[4] == 57)) && (bytes[5] == 97)) {
			return "gif";
		}
		if ((bytes[0] == 66) && (bytes[1] == 77)) {
			return "bmp";
		}
		//	0x49,0x49,0x2a,0x00
		if ((bytes[0] == 73) && (bytes[1] == 73) && (bytes[2] == 42) && (bytes[3] == 0)) {
			return "tif";
		}
		//	第七第八位是宽高，忽略验证
//		&& (bytes[6] == 32 || bytes[6] == 0) && (bytes[7] == 32 || bytes[7] == 0)
		if ((bytes[0] == 0) && (bytes[1] == 0) && (bytes[2] == 1) && (bytes[3] == 0)
				&& (bytes[4] == 1) && (bytes[5] == 0)) {
			return "ico";
		}
		if ((bytes[0] == 82) && (bytes[1] == 73) && (bytes[2] == 70) && (bytes[3] == 70)) {
			return "webp";
		}
		return null;
	}

	/**
	 * 	获取指定目录下所有文件的绝对路径(路径+文件目录或文件名称)
	 * 	<br>单层目录
	 * 
	 * 	@author Pan
	 * 	@param 	filePath	目录地址
	 * 	@return	List
	 */
	public static List<String> getFilesAbsolutePath(String filePath) {
		return getFilesAbsolutePath(filePath, FileMatchType.ALL);
	}
	
	/**	
	 * 	获取指定目录下所有文件的绝对路径(路径+文件目录或文件名称)
	 * 	<br>单层目录
	 * 	<br>自定义文件类型
	 * 	
	 * 	@author Pan
	 * 	@param 	filePath	目录地址
	 * 	@param 	fileType	文件类型
	 * 	@return	List
	 */
	public static List<String> getFilesAbsolutePath(String filePath, FileMatchType fileType) {
		List<File> list = getFiles(filePath, fileType);
		
		if (CollUtils.isEmpty(list)) {
			return Empty.list();
		}
		
		String reviseFilePath = reviseFilePath(filePath);
		return CollUtils.newArrayList(newArrayList -> list.forEach(file -> newArrayList.add(StringUtils.append(reviseFilePath, file.getName()))), list.size());
	}
	
	/**	
	 * 	获取指定目录下的所有文件名称
	 * 	<br>单层目录
	 * 
	 * 	@author Pan
	 * 	@param 	filePath	目录地址
	 * 	@return	String[]
	 */
	public static List<String> getFilesName(String filePath) {
		Assert.isNotEmpty(filePath);
		
		String reviseFilePath = reviseFilePath(filePath);
		File file = new File(reviseFilePath);
		if (file.exists()) {
			return CollUtils.newArrayList(file.list());
		}
		return Empty.list();
	}
	
	/**	
	 * 	获取指定目录下的所有文件名称
	 * 	<br>单层目录
	 * 
	 * 	@author Pan
	 * 	@param 	filePath	目录地址
	 * 	@param 	fileType	文件匹配类型
	 * 	@return	String[]
	 */
	public static List<String> getFilesName(String filePath, FileMatchType fileType) {
		List<File> list = getFiles(filePath, fileType);
		
		if (CollUtils.isEmpty(list)) {
			return Empty.list();
		}
		return CollUtils.newArrayList(newArrayList -> list.forEach(file -> newArrayList.add(file.getName())), list.size());
	}
	
	/**
	 * 	读取文件行号	
	 * 	
	 * 	@author Pan
	 * 	@param  filePath	文件路径
	 * 	@return	long
	 */
	public static long getTotalLine(String filePath) {
		return getTotalLine(new File(filePath)); 
	}
	
	/**	
	 * 	读取文件行号
	 * 	
	 * 	@author Pan
	 * 	@param  file	文件对象
	 * 	@return	long	
	 */
	public static long getTotalLine(File file) {
		BufferedReader br = null;
		Stream<String> stream = null;
		
		try {
			br = IOUtils.getReader(file);
			stream = br.lines();
			return stream.count();
		} finally {
			if (stream != null) {
				stream.close();
			}
			IOUtils.close(br);
		}
	}
	
	/**	
	 * 	获取文件的Content-Type
	 * 	
	 * 	@author Pan
	 * 	@param 	pathname	路径名称
	 * 	@return	String
	 */
	public static String getContentType(String pathname) {
		return getContentType(new File(pathname));
	}
	
	/**	
	 * 	获取文件的Content-Type
	 * 	
	 * 	@author Pan
	 * 	@param 	file	文件对象
	 * 	@return	String
	 */
	public static String getContentType(File file) {
		try {
			return Files.probeContentType(Paths.get(file.toURI()));
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage(), e);
		}
	}
	
	/**
	 * 	修复文件路径
	 * 	
	 * 	@author Pan
	 * 	@param	str		字符串
	 * 	@return	String
	 */
	public static String reviseFilePath(String str) {
		if (str == null) {
			return null;
		}
		
		if (str.endsWith("\\")) {
			return str;
		}
		if (str.endsWith("//")) {
			return StringUtils.replace(str, "//", "/");
		}
		
		String appendStr = null;
		if (StringUtils.contains(str, "\\")) {
			appendStr = "\\";
		}
		
		if (StringUtils.contains(str, "/")) {
			appendStr = "/";
		}
		return str + appendStr;
	}
	
	/**
	 * 	修复文件名称
	 * 	
	 * 	@author Pan
	 * 	@param	str		字符串
	 * 	@return	String
	 */
	public static String reviseFileName(String str) {
		if (str == null) {
			return null;
		}
		
		if (str.startsWith("//")) {
			return StringUtils.remove(str, "//");
		}
		if (str.startsWith("\\")) {
			return StringUtils.remove(str, "\\");
		}
		return str;
	}
	
	/**
	 * 	检查是否为Class文件名称
	 * 	
	 * 	@author Pan
	 * 	@param 	fileName	文件名称
	 * 	@return	boolean
	 */
	public static boolean isClassFile(String fileName) {
		if (fileName == null) {
			return false;
		}
		return fileName.endsWith(CLASS_SUFFIX);
	}
	
	/**
	 * 	检查是否为Class文件
	 * 	
	 * 	@author Pan
	 * 	@param 	file		文件
	 * 	@return	boolean
	 */
	public static boolean isClassFile(File file) {
		if (file == null) {
			return false;
		}
		return isClassFile(file.getName());
	}
}
