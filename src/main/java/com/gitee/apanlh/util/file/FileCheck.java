package com.gitee.apanlh.util.file;

import com.gitee.apanlh.exp.IORuntimeException;
import com.gitee.apanlh.exp.NotFoundException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileLock;

/**	
 * 	文件检查器
 *  #mark 补充完整
 *
 * 	@author Pan
 */
public class FileCheck {
	
	/** 文件锁 */
	private FileLock fileLock;
	/** 文件对象 */
	private RandomAccessFile file;
	
	public FileCheck(File file, String mode) {
		try {
			this.file = new RandomAccessFile(file, mode);
		} catch (FileNotFoundException e) {
			throw new NotFoundException(e.getMessage(), e);
		}
	}
	
//	public boolean hasCanWrite() {
//		if (fileLock == null) {
//			return true;
//		}
//		return canWrite;
//	}
//	
//	public boolean hasCanRead() {
//		return canRead;
//	}
	
	public void lock() {
        try {
//        	canWrite = false;
        	if (fileLock == null) {
        		fileLock = file.getChannel().lock();
        	}
		} catch (IOException e) {
			throw new IORuntimeException(e.getMessage(), e);
		}
    }
	
	public void unlock() {
        try {
            if (fileLock != null) {
                fileLock.release();
                fileLock = null;
            }
        } catch (IOException e) {
        	throw new IORuntimeException(e.getMessage(), e);
        }
    }
}
