package com.gitee.apanlh.util.reflection.copy;

import com.gitee.apanlh.util.base.CollUtils;

import java.util.List;

/**
 * 	拷贝配置
 * 	<br>在类字段中可以增加{@code @CopyMapping}注解来完成字段映射
 * 	<br>注意字段映射：B类增加字段映射注解则是A类中的某个字段映射到B类某个字段
 * 	
 * 	@author Pan
 */
public class CopyConfig {
	
	/** 忽略拷贝字段 */
	private List<String> ignores;
	/** 目标类 */
	private Class<?> target;
	/** 转换器 */
	private CopyConverter converter;
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	CopyConfig() {
		super();
	}
	
	/**	
	 * 	构造函数-加载忽略项、目标类
	 * 	
	 * 	@author Pan	
	 * 	@param 	target	目标类
	 * 	@param 	ignores	忽略项
	 */
	CopyConfig(Class<?> target, String... ignores) {
		super();
		this.ignores = CollUtils.newArrayList(ignores);
		this.target = target;
		this.converter = new CopyConverter(this);
	}
	
	/**	
	 * 	设置目标类
	 * 	
	 * 	@author Pan
	 * 	@param 	target	目标类
	 */
	public void setTarget(Class<?> target) {
		this.target = target;
	}
		
	/**	
	 * 	获取忽略字段项
	 * 	
	 * 	@author Pan
	 * 	@return	List
	 */
	public List<String> getIgnores() {
		return ignores;
	}

	/**	
	 * 	获取目标类
	 * 	
	 * 	@author Pan
	 * 	@return	Class
	 */
	public Class<?> getTarget() {
		return target;
	}

	/**	
	 * 	获取转换器
	 * 	
	 * 	@author Pan
	 * 	@return	CopyConverter
	 */
	public CopyConverter getConverter() {
		return converter;
	}
	
	/**	
	 * 	创建-自定义忽略项
	 * 	
	 * 	@author Pan	
	 * 	@param 	ignores	忽略项
	 * 	@return	CopyConfig
	 */
	public static CopyConfig create(String... ignores) {
		return new CopyConfig(null, ignores);
	}
}
