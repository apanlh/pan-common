package com.gitee.apanlh.util.reflection;

import java.lang.reflect.Field;
import java.util.List;

/**
 * 	反射字段提供	
 * 	
 * 	@author Pan
 */
public interface FieldProvider {
	
	/**
	 *	根据字段名返回Field
	 *	
	 * 	@author Pan
	 * 	@param 	clazz			类
	 * 	@param 	searchNames		字段名
	 * 	@return	Field
	 */
	Field getField(Class<?> clazz, String searchNames);
	
	/**	
	 * 	获取该类下所有字段
	 * 	
	 * 	@author Pan
	 * 	@param 	clazz	类
	 * 	@return	Field[]
	 */
	Field[] getDeclaredFields(Class<?> clazz);
	
	/**
	 * 	获取该类下的所有字段
	 * 	<br>默认获取静态字段 
	 * 
	 * 	@author Pan
	 * 	@param  <T>      数据类型
	 * 	@param 	t	对象
	 * 	@return	Field[]
	 */
	<T> Field[] getFields(T t);
	
	/**
	 * 	获取该类下的字段
	 * 	<br>自定义是否忽略静态字段
	 * 		
	 * 	@author Pan
	 * 	@param  <T>      数据类型
	 * 	@param 	t	对象
	 * 	@param 	ignoreStaticField	true忽略静态字段
	 * 	@return	Field[]
	 */
	<T> Field[] getFields(T t, boolean ignoreStaticField);
	
	/**
	 * 	获取该类下的字段
	 * 	<br>通过反射缓存对象获取字段
	 * 
	 * 	@author Pan
	 * 	@param  <T>      数据类型
	 * 	@param 	t					对象
	 * 	@param 	fieldCacheObject	反射缓存
	 * 	@return	Field[]
	 */
	<T> Field[] getFields(T t, FieldCacheObject fieldCacheObject);
	
	/**
	 * 	获取该类下的所有字段
	 * 	<br>返回该集合首个元素的所有字段对象
	 * 	<br>默认获取静态字段 
	 * 
	 * 	@author Pan
	 * 	@param  <T>      数据类型
	 * 	@param 	list	集合
	 * 	@return	Field[]
	 */
	<T> Field[] getFields(List<T> list);
	
	/**
	 * 	获取该类下的字段
	 * 	<br>自定义是否忽略静态字段
	 * 		
	 * 	@author Pan
	 * 	@param  <T>      数据类型
	 * 	@param 	list				集合
	 * 	@param 	ignoreStaticField	true忽略静态字段
	 * 	@return	Field[]
	 */
	<T> Field[] getFields(List<T> list, boolean ignoreStaticField);
	
	/**
	 * 	获取该类下的字段
	 * 	<br>通过反射缓存对象获取字段
	 * 
	 * 	@author Pan
	 * 	@param  <T>      数据类型
	 * 	@param 	list				集合
	 * 	@param 	fieldCacheObject	反射缓存
	 * 	@return	Field[]
	 */
	<T> Field[] getFields(List<T> list, FieldCacheObject fieldCacheObject);
	
	/**
	 * 	获取该类下的所有字段
	 * 	<br>默认获取所有字段包含(静态字段) 
	 * 
	 * 	@author Pan
	 * 	@param 	clazz	类
	 * 	@return	Field[]
	 */
	Field[] getFields(Class<?> clazz);
	
	/**
	 * 	获取该类下的字段
	 * 	<br>自定义是否忽略静态字段
	 * 		
	 * 	@author Pan
	 * 	@param 	clazz	类
	 * 	@param 	ignoreStaticField	true忽略静态字段
	 * 	@return	Field[]
	 */
	Field[] getFields(Class<?> clazz, boolean ignoreStaticField);
	
	/**
	 * 	获取该类下的字段
	 * 	<br>通过反射缓存对象获取字段
	 * 		
	 * 	@author Pan
	 * 	@param 	clazz				类
	 * 	@param 	fieldCacheObject	反射缓存
	 * 	@return	Field[]
	 */
	Field[] getFields(Class<?> clazz, FieldCacheObject fieldCacheObject);
}
