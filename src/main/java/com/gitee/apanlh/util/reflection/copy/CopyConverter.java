package com.gitee.apanlh.util.reflection.copy;

import com.gitee.apanlh.util.base.CollUtils;
import com.gitee.apanlh.util.base.StringUtils;
import org.springframework.cglib.core.Converter;

/**	
 * 	拷贝转换器
 * 	
 * 	@author Pan
 */
public class CopyConverter implements Converter {
	
	private static final int SET_LENGTH = 3;
	/** 拷贝配置 */
	private CopyConfig copyConfig;

	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	CopyConverter() {
		super();
	}
	
	/**	
	 * 	构造函数-加载拷贝配置
	 * 
	 * 	@author Pan
	 * 	@param 	copyConfig	拷贝配置
	 */
	public CopyConverter(CopyConfig copyConfig) {
		super();
		this.copyConfig = copyConfig;
	}

	@Override
	public Object convert(Object value, Class fieldType, Object setterName) {
		String lowerFirst = StringUtils.lowerFirst(StringUtils.subStr((String) setterName, SET_LENGTH));
		if (CollUtils.findOne(this.copyConfig.getIgnores(), lowerFirst) != null) {
			return null;
		}
		return value;
	}
}
