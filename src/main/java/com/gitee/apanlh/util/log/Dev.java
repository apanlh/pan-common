package com.gitee.apanlh.util.log;

import com.gitee.apanlh.util.base.StringUtils;

import java.io.BufferedWriter;
import java.io.PrintWriter;

import static java.lang.System.out;

/**	
 * 	测试使用Log
 * 
 * 	@author Pan
 */
public class Dev {
	
	private static final PrintWriter PW = new PrintWriter(new BufferedWriter(new PrintWriter(out)), true);
	
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private Dev() {
		//	不允许外部实例
		super();
	}
	
	@SafeVarargs
	public static <T> void log(T... argument) {
		if (argument == null) {
			log("null");
			return ;
		}
		StringBuilder sb = new StringBuilder();
		int index = 0;
		while (index < argument.length) {
			sb.append("{}");
			index ++;
		}
		log(sb.toString(), argument);
	}
	
	/**	
	 * 	测试使用
	 * 	生产则使用log.info
	 * 	
	 * 	@author Pan
	 * 	@param 	str			字符串
	 * 	@param 	argument	参数
	 */
	public static void log(String str, Object... argument) {
		if (str == null || argument == null) {
			PW.write("null");
			return ;
		}
		
		synchronized (PW) {
			String format = StringUtils.format(str, argument);
			if (format == null) {
				PW.write("null");
				PW.flush();
				return ;
			}
			PW.write(format);
			PW.write('\n');
			PW.flush();
		}
	}
}
