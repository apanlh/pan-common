package com.gitee.apanlh.util.log;

import com.gitee.apanlh.util.cache.local.Cache;
import com.gitee.apanlh.util.cache.local.CacheUtils;
import com.gitee.apanlh.util.reflection.ClassUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 	日志处理
 * 	<br>当前只支持SLF4J
 * 	
 *	@author Pan
 */
public class Log {
	
	private static final Cache<Class<?>, Logger> LOGGER = CacheUtils.cache(64);
	
	/**
	 * 	构造函数
	 * 	@author Pan
	 */
	private Log() {
		//	不允许外部示例
		super();
	}
	
	/**	
	 * 	获取日志工厂
	 * 	
	 * 	@author Pan
	 * 	@return	Logger
	 */
	public static Logger get() {
		return get(ClassUtils.getClassOfCall());
	}
	
	/**	
	 * 	获取指定类日志工厂
	 * 	
	 * 	@author Pan
	 * 	@param 	clazz		类
	 * 	@return	Logger
	 */
	public static Logger get(Class<?> clazz) {
		return LOGGER.get(clazz, () -> LoggerFactory.getLogger(clazz));
	}
	
	/**	
	 * 	获取日志等级
	 * 	
	 * 	@author Pan
	 * 	@return	LogLevel
	 */
	public static LogLevel level() {
		return level(get());
	}
	
	/**	
	 * 	是否开启Info级别日志
	 * 	
	 * 	@author Pan
	 * 	@return	boolean
	 */
	public static boolean isInfoEnabled() {
		return get().isInfoEnabled();
	}
	
	/**	
	 * 	是否开启Debug级别日志
	 * 	
	 * 	@author Pan
	 * 	@return	boolean
	 */
	public static boolean isDebugEnabled() {
		return get().isDebugEnabled();
	}
	
	/**	
	 * 	是否开启Info级别日志
	 * 	
	 * 	@author Pan
	 * 	@return	boolean
	 */
	public static boolean isErrorEnabled() {
		return get().isErrorEnabled();
	}
	
	/**	
	 * 	是否开启Warn级别日志
	 * 	
	 * 	@author Pan
	 * 	@return	boolean
	 */
	public static boolean isWarnEnabled() { 
		return get().isWarnEnabled();
	}
	
	/**	
	 * 	是否开启Trace级别日志
	 * 	
	 * 	@author Pan
	 * 	@return	boolean
	 */
	public static boolean isTraceEnabled(){ 
		return get().isTraceEnabled();
	}
	
	/**	
	 * 	获取指定Logger日志等级
	 * 	
	 * 	@author Pan
	 * 	@param 	logger		日志器
	 * 	@return	LogLevel
	 */
	public static LogLevel level(Logger logger) {
		if (logger.isInfoEnabled()) {
			return LogLevel.INFO;
		}
		if (logger.isDebugEnabled()) {
			return LogLevel.DEBUG;
		}
		if (logger.isErrorEnabled()) {
			return LogLevel.ERROR;
		}
		if (logger.isWarnEnabled()) {
			return LogLevel.WARN;
		}
		if (logger.isTraceEnabled()) {
			return LogLevel.TRACE;
		}
		return null;
	}
	
	/**	
	 * 	根据自定义的日志等级输出内容
	 * 	
	 * 	@author Pan
	 * 	@param 	logLevel	日志等级
	 * 	@param 	content		内容
	 */
	public static void write(LogLevel logLevel, String content) {
		Logger logger = Log.get();
		
		switch (logLevel) {
			case DEBUG:
				logger.debug(content);
				return ;
			case ERROR:
				logger.error(content);
				return ;
			case INFO:
			    logger.info(content);
			    return ;
			case TRACE:
				logger.trace(content);
				return ;
			case WARN:
				logger.warn(content);
		}
	}
	
	/**	
	 * 	根据自定义的日志等级输出内容
	 * 	<br>自定义异常
	 * 	
	 * 	@author Pan
	 * 	@param 	logLevel	日志等级
	 * 	@param 	content		内容
	 * 	@param 	throwable	异常
	 */
	public static void write(LogLevel logLevel, String content, Throwable throwable) {
	    Logger logger = Log.get();
	    
	    switch (logLevel) {
	        case DEBUG:
	            logger.debug(content, throwable);
	            return;
	        case ERROR:
	            logger.error(content, throwable);
	            return;
	        case INFO:
	            logger.info(content, throwable);
	            return;
	        case TRACE:
	            logger.trace(content, throwable);
	            return;
	        case WARN:
	            logger.warn(content, throwable);
	    }
	}
}
