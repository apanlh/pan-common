package com.gitee.apanlh.util.log;

/**	
 * 	日志等级枚举
 * 	
 * 	@author Pan
 */
public enum LogLevel {
	
	/** TRACE：用于追踪和诊断问题，提供最详细的日志记录信息 */
	TRACE,
	/** INFO：用于记录应用程序的运行情况，包括启动、停止等 */
	INFO,
	/** WARN：用于记录可能会引起问题的情况，但不影响应用程序的运行 */
	WARN,
	/** ERROR：用于记录严重的错误，可能导致应用程序崩溃  */
	ERROR,
	/** DEBUG：用于记录调试信息，通常只在开发和测试阶段使用 */
	DEBUG,
	;
}
