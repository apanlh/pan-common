package com.gitee.apanlh.util.sys.time;

import com.gitee.apanlh.util.log.Log;
import com.gitee.apanlh.util.thread.StackUtils;

/**	
 * 	运行计时器
 * 	<br>拓展
 * 	
 * 	@author Pan
 */
public class RunTimer {
	
	/**	开始时间 */
	private Long startTime;
	/** 结束时间  */
	private Long stopTime;
	/** 任务信息名称 */
	private String taskInfo;
	
	/**
	 * 	构造函数
	 * 	
	 * 	@author Pan
	 */
	public RunTimer() {
		//	不允许外部构造，默认添加调用方法名称作为任务信息名称
		this(StackUtils.getMethodName());
	}
	
	/**	
	 * 	添加任务信息-构造函数	
	 * 
	 * 	@author Pan
	 * 	@param 	taskInfo	任务信息
	 */
	public RunTimer(String taskInfo) {
		super();
		this.taskInfo = taskInfo;
		this.startTime = System.currentTimeMillis();
	}
	
	/**	
	 * 	设置任务信息
	 * 	
	 * 	@author Pan
	 * 	@param  taskInfo 任务信息
	 */
	public void setTaskInfo(String taskInfo) {
		if (this.taskInfo == null) {
			this.taskInfo = StackUtils.getMethodName();
		}
		this.taskInfo = taskInfo;
	}
	
	/**
	 * 	记录开始时间
	 * 	
	 * 	@author Pan
	 */
	public void startTime() {
		if (this.startTime == null) {
			this.startTime = System.currentTimeMillis();
		}
	}
	
	/**
	 * 	记录截止时间
	 * 	
	 * 	@author Pan	
	 */
	public void stopTime() {
		this.stopTime = System.currentTimeMillis() - this.startTime;
	}
	
	/**	
	 * 	获取停止时间
	 * 	
	 * 	@author Pan
	 * 	@return	Long
	 */
	Long getStopTime() {
		return this.stopTime;
	}
	
	/**	
	 * 	打印时间
	 * 	
	 * 	@author Pan
	 * 	@return String
	 */
	public String printTime() {
		if (this.stopTime == null) {
			throw new NullPointerException("未设置结束时间!");
		}
		
		if (taskInfo != null) {
			return "任务名:(".concat(taskInfo).concat(")的运行时间为:").concat(this.stopTime.toString()).concat("(ms)。");
		}
		return "运行时间为:".concat(this.stopTime.toString()).concat("(ms)。");
	}
	
	/**	
	 * 	打印记录时间，并输出至日志中
	 * 	<br>如果为记录结束时间将自动设置
	 * 	@author Pan
	 */
	public void printLog() {
		if (stopTime == null) {
			stopTime();
		}
		String printTime = printTime();
		Log.get().info("{}", printTime);
	}
	
	/**
	 * 	清理开始时间及结束时间
	 */
	public void clearTimer() {
		this.startTime = null;
		this.stopTime = null;
	}
	
	/**	
	 * 	创建运行计时器
	 * 	
	 * 	@author Pan
	 * 	@return	RunTimer
	 */
	public static RunTimer create() {
		return new RunTimer();
	}
	
	/**	
	 * 	创建运行计时器
	 * 	<br>自定义任务名称
	 * 	
	 * 	@author Pan
	 * 	@param 	taskInfo 任务信息
	 * 	@return	RunTimer
	 */
	public static RunTimer create(String taskInfo) {
		return new RunTimer(taskInfo);
	}
}
