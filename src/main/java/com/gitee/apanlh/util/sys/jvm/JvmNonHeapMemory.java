package com.gitee.apanlh.util.sys.jvm;

import com.gitee.apanlh.util.unit.ByteReadable;

import java.lang.management.MemoryUsage;

/**	
 * 	JVM虚拟机非堆的内存使用情况
 * 
 * 	@author Pan
 */
public class JvmNonHeapMemory {
	
	/** 返回Java虚拟机最初从操作系统请求进行内存管理的内存量。  初始化量 */
	private String init;
	/** 返回最大的内存量 */
	private String max;
	/** 返回使用的内存量 */
	private String used;
	/** 返回提供的内存量 */
	private String committed;
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	public JvmNonHeapMemory() {
		super();
	}
	
	/**	
	 * 	构造函数
	 * 	
	 * 	@author Pan
	 * 	@param 	init		初始化内存	
	 * 	@param 	max			最大内存
	 * 	@param 	used		使用内存
	 * 	@param 	committed	提交内存
	 */
	public JvmNonHeapMemory(String init, String max, String used, String committed) {
		super();
		this.init = init;
		this.max = max;
		this.used = used;
		this.committed = committed;
	}
	
	public String getInit() {
		return init;
	}
	
	public String getMax() {
		return max;
	}
	
	public String getUsed() {
		return used;
	}

	public String getCommitted() {
		return committed;
	}

	@Override
	public String toString() {
		return "JvmNonHeapMemoryInfo [init=" + init + ", max=" + max + ", used=" + used + ", committed=" + committed + "]";
	}
	
	/**
	 * 	返回Java虚拟机使用的非堆内存的当前内存使用情况。
	 * 
	 * 	@author Pan
	 */
	static JvmNonHeapMemory instance() {
		MemoryUsage nonHeapMemoryUsage = JvmMemory.memoryBean.getNonHeapMemoryUsage();
		
		return new JvmNonHeapMemory(
			ByteReadable.convert(nonHeapMemoryUsage.getInit()),
			ByteReadable.convert(nonHeapMemoryUsage.getMax()),
			ByteReadable.convert(nonHeapMemoryUsage.getUsed()),
			ByteReadable.convert(nonHeapMemoryUsage.getCommitted())
		);
	}

}	
