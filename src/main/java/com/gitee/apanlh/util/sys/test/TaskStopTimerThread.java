package com.gitee.apanlh.util.sys.test;

import com.gitee.apanlh.util.thread.Sleep;

/**	
 * 	任务时间器
 * 	<br>监听任务在某个阈值通知执行器停止执行
 * 	<br>暴力停止线程模式
 * 	
 * 	@author Pan
 */
public class TaskStopTimerThread extends Thread {
	
	/** 执行时间 */	
	private long time;
	/** 休眠间隔 */
	private long interval;
	/** 任务计数器 */
	private TaskCount taskCount;
	/** 任务总数计算器 */
	private TaskStopExecutorThread executorStopThread;
	
	/**
	 * 	默认构造函数
	 *
	 * 	
	 * 	@author Pan
	 */
	public TaskStopTimerThread() {
		super();
	}

	/**	
	 * 	任务时间器-构造函数
	 * 	
	 * 	@author Pan
	 * 	@param 	executorStopThread		执行器
	 * 	@param 	taskCount				任务总数
	 * 	@param 	time					执行时间
	 * 	@param 	interval				此任务器的休眠间隔
	 */
	public TaskStopTimerThread(TaskStopExecutorThread executorStopThread, TaskCount taskCount, long time, long interval) {
		super();
		this.executorStopThread = executorStopThread;
		this.taskCount = taskCount;
		this.time = time;
		this.interval = interval;
		//	设置线程名
		this.setName("timer-stop-".concat(String.valueOf(this.getId())));
	}

	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		if (taskCount.getCurrentTime() == 0L) {
			taskCount.setCurrentTime();
		}
		
		for (;;) {
			if (taskCount.isExpire(time)) {
				//	中断timer线程
				Thread.currentThread().interrupt();
				//	方式2 强制停止
				executorStopThread.stop();
				break;
			}
			Sleep.mills(interval);
		}
	}
}
