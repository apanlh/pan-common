package com.gitee.apanlh.util.sys.test;

/**		
 * 	任务计数器
 * 
 * 	@author Pan
 */
public class TaskCount {
	
	/**	偏差值 */
	private long offsetTime;
	/** 当前时间毫秒数 */
	private long currentTime;
	/** 总计数 */
	private long count;
	/** 是否继续任务 */
	private boolean nextTask;
	/** 结束时计算的总耗时 */
	private long totalTime;
	
	/**
	 * 	默认构造函数
	 * 	<br>默认偏差值0
	 * 	
	 * 	@author Pan
	 */
	public TaskCount() {
		this(0L);
	}
	
	/**	
	 * 	自定义偏差值-构造函数
	 * 	<br>计算任务器与执行器任务休眠的偏差值
	 * 	
	 * 	@author Pan
	 * 	@param 	offsetTime	设置偏差值
	 */
	public TaskCount(long offsetTime) {
		this.count = 0L;
		this.nextTask = true;
		this.offsetTime = offsetTime;
	}
	
	
	public long getCurrentTime() {
		return currentTime;
	}
	
	public void setCurrentTime() {
		this.currentTime = System.currentTimeMillis();
	}
	
	public void setCurrentTime(long time) {
		this.currentTime = time;
	}
	
	public long getCount() {
		return count;
	}
	
	/**
	 * 	自增
	 * 	@author Pan
	 */
	public void increment() {
		count++;
	}
	
	public synchronized void incrementSync() {
		count++;
	}
	
	public boolean getNextTask() {
		return nextTask;
	}

	public void stopNextTask() {
		this.nextTask = false;
	}
	
	public long getOffsetTime() {
		return offsetTime;
	}

	public void setOffsetTime(long offsetTime) {
		this.offsetTime = offsetTime;
	}
	
	public long getTotalTime() {
		return totalTime;
	}

	/**	
	 * 	是否过期
	 *
	 * 	@author Pan
	 * 	@param  threshold	阈值(毫秒)
	 * 	@return boolean
	 */
	public boolean isExpire(long threshold) {
		long time = System.currentTimeMillis() - getCurrentTime();
		boolean isExpire = time >= threshold + getOffsetTime();
		if (isExpire) {
			this.totalTime = time;
		}
		return isExpire;
	}

	@Override
	public String toString() {
		return "TaskCount [offsetTime=" + offsetTime + ", currentTime=" + currentTime + ", count=" + count
				+ ", nextTask=" + nextTask + ", totalTime=" + totalTime + "]";
	}

}
