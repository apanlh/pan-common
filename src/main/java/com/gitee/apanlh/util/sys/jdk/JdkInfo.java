package com.gitee.apanlh.util.sys.jdk;

import com.gitee.apanlh.util.base.StringUtils;

/**
 * 	JDK信息
 * 	
 * 	@author Pan
 */
public class JdkInfo {
	
	/** JDK路径地址 */
	static final String JDK_PATH 								= System.getProperty("sun.boot.class.path");
	/** JDK全路径地址 */
	static final String[] JDK_FULL_PATH 						= StringUtils.split(JDK_PATH, ";");
	/** JDK版本信息 */
	static final String JDK_VERSION 							= System.getProperty("java.version");
	/** JDK系统位数 */
	static final String JDK_BIT 								= System.getProperty("sun.arch.data.model");
	
	/**
	 * 	默认构造函数
	 * 
	 * 	@author Pan
	 */
	private JdkInfo() {
		//	不允许外部实例
		super();
	}
	
	/**		
	 * 	获取JDK信息
	 * 
	 * 	@author Pan
	 * 	@return	String
	 */
	public static String getJdk() {
		return JDK_VERSION;
	}
	
	/**		
	 * 	获取JDK路径地址
	 * 	
	 * 	@author Pan
	 * 	@return	String[]
	 */
	public static String[] getJdkPath() {
		return JDK_FULL_PATH;
	}
	
	/**	
	 * 	获取JDK的版本信息
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public static String getJdkVersion() {
		return JDK_VERSION.substring(JDK_VERSION.indexOf('_') + 1);
	}
	
	/**	
	 * 	获取JDK32位或64位信息
	 * 
	 * 	@author Pan
	 * 	@return	String	
	 */
	public static String getJdkBit() {
		return JDK_BIT;
	}
}
