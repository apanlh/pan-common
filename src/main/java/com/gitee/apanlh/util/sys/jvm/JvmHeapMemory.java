package com.gitee.apanlh.util.sys.jvm;

import com.gitee.apanlh.util.unit.ByteReadable;

import java.lang.management.MemoryUsage;

/**	
 * 	JVM虚拟机堆的内存使用情况
 * 
 * 	@author Pan
 */
public class JvmHeapMemory {
	
	/** 返回Java虚拟机最初从操作系统请求进行内存管理的内存量。  初始化量 */
	private String init;
	/** 返回最大的内存量  */
	private String max;
	/** 返回使用的内存量 */
	private String used;
	/** 返回提供的内存量 */
	private String committed;

	/**
	 * 	默认构造函数
	 * 	
	 *	@author Pan 
	 */
	JvmHeapMemory() {
		super();
	}
	
	/**
	 * 	构造函数
	 * 	
	 * 	@author Pan
	 * 	@param 	init		初始化内存	
	 * 	@param 	max			最大内存
	 * 	@param 	used		使用内存
	 * 	@param 	committed	提交内存
	 */
	JvmHeapMemory(String init, String max, String used, String committed) {
		super();
		this.init = init;
		this.max = max;
		this.used = used;
		this.committed = committed;
	}
	
	public String getInit() {
		return init;
	}

	public String getMax() {
		return max;
	}
	
	public String getUsed() {
		return used;
	}

	public String getCommitted() {
		return committed;
	}

	@Override
	public String toString() {
		return "JvmHeapMemoryInfo [init=" + init + ", max=" + max + ", used=" + used + ", committed=" + committed + "]";
	}
	
	/**
	 * 	返回用于对象分配的堆的当前内存使用情况。
	 * 	@author Pan
	 */
	static JvmHeapMemory instance() {
		MemoryUsage heapMemoryUsage = JvmMemory.memoryBean.getHeapMemoryUsage();
		
		return new JvmHeapMemory(
			ByteReadable.convert(heapMemoryUsage.getInit()),
			ByteReadable.convert(heapMemoryUsage.getMax()),
			ByteReadable.convert(heapMemoryUsage.getUsed()),
			ByteReadable.convert(heapMemoryUsage.getCommitted())
		);
	}
}
