package com.gitee.apanlh.util.sys;

import com.gitee.apanlh.util.base.BigDecimalUtils;
import com.gitee.apanlh.util.unit.ByteReadable;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**	
 * 	系统磁盘信息
 * 	@author Pan
 */
public class SystemDisk {
	
	/** 当前默认地址 */
	private static final String 			USER_DIR 				= System.getProperty("user.dir");
	/** 当前地址的磁盘信息 */
	private static final File 				DEFAULT_SYSTEM_FILE 	= new File(USER_DIR);
	/** FILE信息 */
	private static final FileSystemView	 	SYSTEM_FILE_VIEW 		= FileSystemView.getFileSystemView();
	/** 系统盘符 */
	private static final File[] 			SYSTEM_FILES 			= File.listRoots();

	/** 磁盘信息 */
	private File file;
	/** 磁盘名称 */
	private String diskName;
	/** 此硬盘总空间 */
	private String totalSpace;
	/** 使用空间 */
	private String usedSpace;
	/** 可用空间 */
	private String usableSpace;
	/**  获取磁盘未分配的可用空间 */
	private String freeSpace;
	/** 硬盘资源的利用率 */
	private String usedPercentage;
	
	/**
	 * 	默认构造函数
	 * 	<br>如果不传递指定file则使用默认的file对象
	 * 
	 * 	@author Pan
	 */
	private SystemDisk() {
		this(DEFAULT_SYSTEM_FILE);
	}
	
	/**	
	 * 	指定加载盘符-构造函数
	 * 	
	 * 	@author Pan
	 * 	@param 	file	文件对象
	 */
	private SystemDisk(File file) {
		this.file = file;
		reload();
	}
	
	/**	
	 * 	获取磁盘名称
	 * 	
	 * 	@author Pan
	 * 	@return String
	 */
	public String getName() {
		if (this.diskName == null) {
			this.diskName = SYSTEM_FILE_VIEW.getSystemDisplayName(file);
		}
		return this.diskName;
	}
	
	/**	
	 * 	获取所有系统盘符名称
	 * 	@author Pan
	 * 	@return String[]
	 */
	public String[] getNames() {
		File[] systemFiles = SYSTEM_FILES;
		String[] names = new String[SYSTEM_FILES.length];
		
		for (int i = 0; i < SYSTEM_FILES.length; i++) {
			names[i] = systemFiles[i].getName();
		}
		return names;
	}
	
	/**	
	 * 	获取磁盘总空间
	 * 	
	 * 	@author Pan
	 * 	@return String
	 */
	public String getTotalSpace() {
		this.totalSpace = ByteReadable.convert(file.getTotalSpace());
		return this.totalSpace;
	}
	
	/**	
	 * 	获取硬盘使用空间
	 * 	
	 * 	@author Pan
	 * 	@return String
	 */
	public String getUsedSpace() {
		BigDecimal minusOperation = BigDecimalUtils.minus(file.getTotalSpace(), file.getUsableSpace());
		this.usedSpace = ByteReadable.convert(minusOperation);
		return this.usedSpace;
	}
	
	/**	
	 * 	获取磁盘可用空间
	 * 	
	 * 	@author Pan
	 * 	@return String
	 */
	public String getUsableSpace() {
		this.usableSpace = ByteReadable.convert(file.getUsableSpace());
		return this.usableSpace;
	}
	
	/**
	 * 	获取磁盘未分配的可用空间
	 * 	
	 * 	@author Pan
	 * 	@return String
	 */
	public String getFree() {
		this.freeSpace = ByteReadable.convert(file.getFreeSpace());
		return this.freeSpace;
	}
	
	/**	
	 * 	硬盘资源的利用率
	 * 	
	 * 	@author Pan
	 * 	@return	String
	 */
	public String getUsedPercentage() {
		if (file.getTotalSpace() == 0L) {
			return "0.0%";
		}
		//	获取使用空间
		long use = file.getTotalSpace() - file.getUsableSpace();
		//	格式化
		BigDecimal division = BigDecimalUtils.division(use, file.getTotalSpace(), 4 , RoundingMode.HALF_EVEN);
		BigDecimal multiply = BigDecimalUtils.multiply(division.toString(), 100);

		this.usedPercentage = BigDecimalUtils.toTruncate(multiply.toString()) + "%";
		return this.usedPercentage;
	}

	/**
	 * 	加载硬盘信息
	 * 	
	 * 	@author Pan
	 */
	public void reload() {
		getName();
		getTotalSpace();
		getUsedSpace();
		getUsableSpace();
		getFree();
		getUsedPercentage();
	}

	@Override
	public String toString() {
		return "磁盘名=" + diskName + ", 总空间=" + totalSpace + ", 使用空间="
				+ usedSpace + ", 可用空间=" + usableSpace + ", 未分配空间=" + freeSpace + ", 利用率="
				+ usedPercentage;
	}
	
	/**	
	 * 	获取当前磁盘信息
	 * 	
	 * 	@author Pan
	 * 	@return SystemDisk
	 */
	public static SystemDisk create() {
		return new SystemDisk();
	}
	
	/**	
	 * 	获取指定磁盘信息
	 * 	
	 * 	@author Pan
	 * 	@param 	disk 文件对象
	 * 	@return SystemDisk
	 */
	public static SystemDisk create(File disk) {
		return new SystemDisk(disk);
	}
	
	/**	
	 * 	获取所有系统盘符
	 * 	
	 * 	@author Pan
	 * 	@return File[]
	 */
	public static File[] getDisks() {
		return SYSTEM_FILES;
	}
}
