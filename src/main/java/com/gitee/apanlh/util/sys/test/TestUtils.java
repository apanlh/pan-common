package com.gitee.apanlh.util.sys.test;

import com.gitee.apanlh.util.log.Dev;
import com.gitee.apanlh.util.log.Log;
import com.gitee.apanlh.util.sys.jvm.JvmMemory;
import com.gitee.apanlh.util.sys.time.RunTimer;
import com.gitee.apanlh.util.thread.Sleep;
import com.gitee.apanlh.util.valid.ValidParam;

import java.lang.management.GarbageCollectorMXBean;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**	
 * 	测试相关
 * 	
 * 	@author Pan
 */
public class TestUtils {
	
	/**
	 * 	构造函数
	 * 
	 * 	@author Pan
	 */
	private TestUtils() {
		//	不允许外部实例
		super();
	}
	
	/**	
	 * 	计算在限定毫秒时间内执行次数
	 * 	
	 * 	@author Pan
	 * 	@param 	count		重复次数
	 * 	@param 	time		限定时间
	 * 	@param 	runnable	Runnable接口
	 */
	public static void taskCount(int count, long time, Runnable runnable) {
		taskCount(null, count, time, runnable);
	}
	
	/**
	 * 	计算在限定毫秒时间内执行次数
	 * 	方式1
	 * 	
	 * 	@author Pan
	 * 	@param 	taskName	任务名
	 * 	@param 	count		重复次数
	 * 	@param 	time		限定时间
	 * 	@param 	runnable	Runnable接口
	 */
	public static void taskCount(String taskName, int count, long time, Runnable runnable) {
		boolean emptyName =  ValidParam.isEmpty(taskName);
		for (int i = 0; i < count; i++) {
			if (emptyName) {
				Dev.log("第{}轮在{}(ms)内共执行{}次", i + 1, time, taskCount(time, runnable));
			} else {
				Dev.log("任务[{}], 第{}轮在{}(ms)内共执行{}次", taskName, i + 1, time, taskCount(time, runnable));
			}
		}
	}
	
	/**
	 * 	计算在限定毫秒时间内执行次数
	 * 	方式1:	因为线程不同所以 执行器在执行时都要增加锁 获取是否超时状态 (效率低)
	 * 	
	 * 	@author Pan
	 * 	@param 	time		限定时间
	 * 	@param 	runnable	Runnable接口
	 * 	@return	long
	 */
	public static long taskCount(long time, Runnable runnable) {
		//	休眠间隔
		long interval = 1L;
		//	耗时时间偏移量
		long offset = 0L;
		//	为了更精准一点 计算了休眠所消耗的偏移量时间
		TaskCount taskCount = new TaskCount(offset);
		
		TaskTimerThread taskTimerThread = new TaskTimerThread(taskCount, time, interval);
		TaskExecutorThread taskExecutorThread = new TaskExecutorThread(taskCount, runnable);
		
		taskExecutorThread.start();
		taskTimerThread.start();
		
		//	主线程等待
		while (!taskExecutorThread.isInterrupted() && taskExecutorThread.isAlive()) {
			Sleep.seconds(1L);
		}
		return taskCount.getCount();
	}
	
	/**
	 * 	计算在限定毫秒时间内执行次数
	 * 	
	 * 	@author Pan
	 * 	@param 	count		重复次数
	 * 	@param 	time		限定时间
	 * 	@param 	runnable	Runnable接口
	 */
	public static void taskCountOfStop(int count, long time, Runnable runnable) {
		taskCountOfStop(null, count, time, runnable);
	}
	
	/**	
	 * 	计算在限定毫秒时间内执行次数
	 * 	方式2
	 * 	
	 * 	@author Pan
	 * 	@param 	taskName	任务名
	 * 	@param 	count		重复次数
	 * 	@param 	time		限定时间
	 * 	@param 	runnable	Runnable接口
	 */
	public static void taskCountOfStop(String taskName, int count, long time, Runnable runnable) {
		boolean emptyName =  ValidParam.isEmpty(taskName);
		for (int i = 0; i < count; i++) {
			if (emptyName) {
				Dev.log("第{}轮在{}(ms)内共执行{}次", i + 1, time, taskCountOfStop(time, runnable));
			} else {
				Dev.log("任务[{}], 第{}轮在{}(ms)内共执行{}次", taskName, i + 1, time, taskCountOfStop(time, runnable));
			}
		}
	}
	
	/**	
	 * 	计算在限定毫秒时间内执行次数
	 * 	方式2 暴力stop方式（有未知的风险但数据准确）
	 * 	
	 * 	@author Pan
	 * 	@param 	time		限定时间
	 * 	@param 	runnable	Runnable接口
	 * 	@return	long
	 */
	public static long taskCountOfStop(long time, Runnable runnable) {
		//	休眠间隔
		long interval = 1L;
		//	耗时时间偏移量
		long offset = 0L;
		
		//	为了更精准一点 计算了休眠所消耗的偏移量时间
		TaskCount taskCount = new TaskCount(offset);
		
		TaskStopExecutorThread taskExecutorStopThread = new TaskStopExecutorThread(taskCount, runnable);
		TaskStopTimerThread taskTimerStopThread = new TaskStopTimerThread(taskExecutorStopThread, taskCount, time, interval);
		taskExecutorStopThread.start();
		taskTimerStopThread.start();
		
		//	检测是否存活
		while (!taskExecutorStopThread.isInterrupted() && taskExecutorStopThread.isAlive()) {
			Sleep.mills(500L);
		}
		return taskCount.getCount();
	}
	
	/**	
	 * 	测试单线程
	 * 	<br>计算总耗时
	 * 	{@code
	 * 		testTask(1, new Runnable() {
	 * 			public void run() {
	 * 				// do
	 * 			}
	 * 		});
	 * 	}
	 * 	@author Pan
	 * 	@param  taskCount		执行任务次数
	 * 	@param  task			Runnable接口
	 */
	public static void taskTime(final int taskCount, final Runnable task) {
		taskTime(1, taskCount, task);
	}
	
	/**	
	 * 	测试单线程、多线程耗时
	 * 	<br>计算总耗时
	 * 	<br>示例:
	 * 	{@code
	 * 		testTask(1, 100, new Runnable() {
	 * 			public void run() {
	 * 				// do
	 * 			}
	 * 		});
	 * 	}
	 * 
	 * 	@author Pan
	 * 	@param  threadCount		执行线程数
	 * 	@param  taskCount		执行任务次数
	 * 	@param  task	任务
	 */
	public static void taskTime(final int threadCount, final int taskCount, final Runnable task) {
		Dev.log("开始前>>>>>内存使用情况[{}]", JvmMemory.getMemory());
		ThreadFactory threadFactory = Executors.defaultThreadFactory();
		CountDownLatch startGate = new CountDownLatch(1);
        CountDownLatch endGate = new CountDownLatch(threadCount);
        
        RunTimer runTimer = RunTimer.create();
        runTimer.clearTimer();
        
		try {
			//	线程数
			for (int i = 0; i < threadCount; i++) {
				threadFactory.newThread(() -> {
					try {
						startGate.await();
						runTimer.startTime();
						//	执行数
						for (int j = 0; j < taskCount; j++) {
							task.run();	
						}
						runTimer.stopTime();
						endGate.countDown();
					} catch (Exception e) {
						runTimer.stopTime();
						endGate.countDown();
						Log.get().error("{}", e.getMessage(), e);
					}
				}).start();
			}
		} finally {
			try {
				startGate.countDown();
				endGate.await();
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				Log.get().error("{}", e.getMessage(), e);
			}
		}
		Dev.log("总耗时[{}]", runTimer.printTime());
		Dev.log("清理情况>>>>>");
		for (GarbageCollectorMXBean garbageCollectorMxBean : JvmMemory.getGcInfo()) {
			Dev.log("GC:{}#  time: {}(ms), count: {}", garbageCollectorMxBean.getName(), garbageCollectorMxBean.getCollectionTime(), garbageCollectorMxBean.getCollectionCount());
		}
		Dev.log("结束时>>>>>内存使用情况[{}]", JvmMemory.getMemory());
	}
	
}
