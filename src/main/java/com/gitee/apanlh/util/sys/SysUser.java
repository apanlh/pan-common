package com.gitee.apanlh.util.sys;

import com.gitee.apanlh.util.base.SingletonUtils;
import com.gitee.apanlh.util.base.StringUtils;
import com.gitee.apanlh.util.file.FileUtils;

/**	
 * 	系统用户信息
 * 	
 * 	@author Pan
 */
public class SysUser {
	
	/** 用户名 */
	private String name;
	/** 用户目录 */
	private String homeDir;
	/** 用户临时目录 */
	private String tempDir;
	/** 当前位置 */
	private String currentDir;
	/** 语言 */
	private String language;
	/** 国家 */
	private String country;
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	private SysUser() {
		//	不允许外部实例，默认加载系统用户信息
		load();
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getHomeDir() {
		return this.homeDir;
	}
	
	public String getTempDir() {
		return this.tempDir;
	}
	
	public String getCurrentDir() {
		return this.currentDir;
	}
	
	public String getLanguage() {
		return this.language;
	}
	
	public String getCountry() {
		return this.country;
	}
	
	/**
	 * 	加载配置信息
	 * 	@author Pan
	 */
	private void load() {
		this.name			= 	FileUtils.reviseFilePath(System.getProperty("user.name"));
		this.homeDir 		= 	FileUtils.reviseFilePath(System.getProperty("user.home"));
		this.currentDir 	= 	FileUtils.reviseFilePath(System.getProperty("user.dir"));
		this.tempDir 		= 	FileUtils.reviseFilePath(System.getProperty("java.io.tmpdir"));
		this.language 		=	System.getProperty("user.language");
		this.country 		= 	System.getProperty("user.country");
	}
	
	@Override
	public String toString() {
		return StringUtils.format("Name\t\t[{}]\nHome\t\t[{}]\nCurrentDir\t[{}]\nTempDir\t\t[{}]\nLanguage\t[{}]\nCountry\t\t[{}]", 
			this.name,
			this.homeDir,
			this.currentDir,
			this.tempDir,
			this.language,
			this.country
		);
	}
	
	/**	
	 * 	创建系统用户信息
	 * 	<br>此方法为单例
	 * 	
	 * 	@author Pan
	 * 	@return SysUser
	 */
	public static SysUser create() {
		return SingletonUtils.get(SysUser.class);
	}
}
