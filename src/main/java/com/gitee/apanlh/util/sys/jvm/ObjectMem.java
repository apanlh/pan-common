package com.gitee.apanlh.util.sys.jvm;

import java.lang.instrument.Instrumentation;

/**	
 * 	获取对象占用
 * 
 * 	@author Pan
 */
public class ObjectMem {
	
	private static Instrumentation instrumentation;
	 
    public static void premain(Instrumentation inst) {
        instrumentation = inst;
    }

    /**
     * 	构造函数
     *
     * 	@author Pan
     */
    private ObjectMem() {
        //	不允许外部实例
        super();
    }

    public static Long getSizeOf(Object object) {
        if (instrumentation == null) {
            throw new IllegalStateException("Instrumentation is null");
        }
        return instrumentation.getObjectSize(object);
    }
    
}
