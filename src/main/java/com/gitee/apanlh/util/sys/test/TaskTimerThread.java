package com.gitee.apanlh.util.sys.test;

import com.gitee.apanlh.util.thread.Sleep;

/**	
 * 	任务时间器
 * 	<br>监听任务在某个阈值通知执行器停止执行
 * 	
 * 	@author Pan
 */
public class TaskTimerThread extends Thread {
	
	/** 执行时间 */	
	private long time;
	/** 休眠间隔 */
	private long interval;
	/** 任务计数器 */
	private TaskCount taskCount;
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	public TaskTimerThread() {
		super();
	}

	/**	
	 * 	任务时间器-构造函数
	 * 	<br>自定义任务计数器
	 * 	<br>自定义执行时间
	 * 	<br>自定义休眠间隔
	 * 
	 * 	@author Pan
	 * 	@param 	taskCount	任务计数器
	 * 	@param 	time		执行时间
	 * 	@param 	interval	休眠间隔
	 */
	public TaskTimerThread(TaskCount taskCount, long time, long interval) {
		super();
		this.taskCount = taskCount;
		this.time = time;
		this.interval = interval;
		//	设置线程名
		this.setName("timer-".concat(String.valueOf(this.getId())));
	}

	@Override
	public void run() {
		if (taskCount.getCurrentTime() == 0L) {
			taskCount.setCurrentTime();
		}
		for (;;) {
			if (taskCount.isExpire(time)) {
				//	停止任务
				taskCount.stopNextTask();
				//	中断时间线程
				Thread.currentThread().interrupt();
				break;
			}
			Sleep.mills(interval);
		}
	}
}
