package com.gitee.apanlh.util.sys.test;

/**	
 * 	任务执行器
 * 	<br>用于执行任务
 * 
 * 	@author Pan
 */
public class TaskExecutorThread extends Thread {
	
	/** 任务计数器 */
	private TaskCount taskCount;
	/** 执行方法 */
	private Runnable runnable;
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	public TaskExecutorThread() {
		super();
	}

	/**	
	 * 	构造函数
	 * 	<br>自定义任务计数器
	 * 	<br>Runnable接口
	 * 	
	 * 	@author Pan
	 * 	@param 	taskCount	计数器
	 * 	@param 	runnable	Runnable
	 */	
	public TaskExecutorThread(TaskCount taskCount, Runnable runnable) {
		super();
		this.taskCount = taskCount;
		this.runnable = runnable;
		//	设置线程名
		this.setName("executor-".concat(String.valueOf(this.getId())));
	}
	
	@Override
	public void run() {
		for (;;) {
			synchronized (taskCount) {
				if (!taskCount.getNextTask()) {
					//	中断线程
					Thread.currentThread().interrupt();
					break;
				}
			}
			taskCount.increment();
			//	执行任务
			runnable.run();
		}
	}
}
