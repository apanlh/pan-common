package com.gitee.apanlh.util.sys.jvm;

import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.util.List;

/**	
 * 	JVM内存信息
 * 
 * 	@author Pan
 */
public class JvmMemory {
	
	/** JVM 内存使用情况信息 */
	static MemoryMXBean	memoryBean = ManagementFactory.getMemoryMXBean();
	/** JVM GC信息 */
	static List<GarbageCollectorMXBean> garbageCollectorMXBeans = ManagementFactory.getGarbageCollectorMXBeans();
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	private JvmMemory() {
		//	不允许外部实例
		super();
	}

	/**	
	 * 	堆的内存使用信息
	 * 	
	 * 	@author Pan
	 * 	@return JvmHeapMemory
	 */
	public JvmHeapMemory getHeapInfo() {
		return JvmHeapMemory.instance();
	}

	/**	
	 * 	非堆的内存使用信息
	 * 	
	 * 	@author Pan
	 * 	@return JvmNonHeapMemory
	 */
	public JvmNonHeapMemory getNonHeapInfo() {
		return JvmNonHeapMemory.instance();
	}
	
	/**		
	 * 	用于描述有多少对象被挂起以便回收
	 * 	
	 * 	@author Pan
	 * 	@return int
	 */
	public int getObjectPendingFinalizationCount() {
		return memoryBean.getObjectPendingFinalizationCount();
	}

	@Override
	public String toString() {
		return "objectPendingFinalizationCount=" + getObjectPendingFinalizationCount() + 
				", heapInfo=" + getHeapInfo() + 
				", nonHeapInfo=" + getNonHeapInfo();
	}

	/**	
	 * 	获取堆、非堆的内存使用信息
	 * 	
	 * 	@author Pan
	 * 	@return JvmMemory
	 */
	public static JvmMemory getMemory() {
		return new JvmMemory();
	}
	
	/**	
	 * 	获取GC信息
	 * 	
	 * 	@author Pan
	 * 	@return List
	 */
	public static List<GarbageCollectorMXBean> getGcInfo() {
		return garbageCollectorMXBeans;
	}
}
