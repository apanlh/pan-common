package com.gitee.apanlh.util.sys.test;

/**	
 * 	任务执行器
 * 	<br>用于执行任务
 * 	<br>暴力停止线程模式
 * 	
 * 	@author Pan
 */
public class TaskStopExecutorThread extends Thread {
	
	/** 任务计数器 */
	private TaskCount taskCount;
	/** 执行方法 */
	private Runnable runnable;
	
	/**
	 * 	默认构造函数
	 * 	
	 * 	@author Pan
	 */
	public TaskStopExecutorThread() {
		super();
	}

	/**	
	 * 	任务执行器-构造函数
	 * 	<br>自定义任务计数器
	 * 	<br>Runnable接口
	 * 	
	 * 	@author Pan
	 * 	@param 	taskCount	计数器
	 * 	@param 	runnable	Runnable
	 */	
	public TaskStopExecutorThread(TaskCount taskCount, Runnable runnable) {
		super();
		this.taskCount = taskCount;
		this.runnable = runnable;
		//	设置线程名
		this.setName("executor-stop-".concat(String.valueOf(this.getId())));
	}
	
	@Override
	public void run() {
		try {
			for (;;) {
				taskCount.increment();
				//	执行任务
				runnable.run();
			}
		} catch (Exception e) {
			//	不打印异常--强制停止的异常
			
		}
	}
}
