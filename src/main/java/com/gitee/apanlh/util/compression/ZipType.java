package com.gitee.apanlh.util.compression;

/**	
 * 	ZIP压缩方式
 * 	@author Pan
 */
public enum ZipType {
	// 	GZIP类型
	GZIP,
	// 	DEFLATER
	DEFLATER,
	//	SNAPPY类型
	SNAPPY,
	// 	LZ4 类型
	LZ4,
	// 	Brotli 类型
	BROTLI,
	;
}
