package com.gitee.apanlh.exp;

/**
 * 	图片写入异常
 * 	@author Pan
 */
public class ImageWriteException extends StreamWriteException {
	
	private static final long serialVersionUID = 1L;

	public ImageWriteException(Exception e) {
		super(e);
	}
	
	public ImageWriteException(String msg) {
		super(msg);
	}
	
	public ImageWriteException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
