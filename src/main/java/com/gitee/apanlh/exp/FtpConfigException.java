package com.gitee.apanlh.exp;

/**	
 * 	FTP装配异常
 * 	@author Pan
 */
public class FtpConfigException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public FtpConfigException(Exception e) {
		super(e);
	}
	
	public FtpConfigException(String msg) {
		super(msg);
	}
	
	public FtpConfigException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
