package com.gitee.apanlh.exp;

/**	
 * 	POI读取器异常
 * 	
 * 	@author Pan
 */
public class PoiReaderException extends PoiFileException {

	private static final long serialVersionUID = 1L;

	public PoiReaderException(Exception e) {
		super(e);
	}
	
	public PoiReaderException(String msg) {
		super(msg);
	}
	
	public PoiReaderException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
}
