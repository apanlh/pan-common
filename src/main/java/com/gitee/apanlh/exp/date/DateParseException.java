package com.gitee.apanlh.exp.date;

/**
 *  时间解析异常
 *  
 *  @author Pan
 */
public class DateParseException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public DateParseException(Exception e) {
		super(e);
	}
	
	public DateParseException(String msg) {
		super(msg);
	}
	
	public DateParseException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
