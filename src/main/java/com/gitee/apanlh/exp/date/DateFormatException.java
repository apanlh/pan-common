package com.gitee.apanlh.exp.date;

/**
 * 	时间格式转换异常
 * 	@author Pan
 */
public class DateFormatException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public DateFormatException(Exception e) {
		super(e);
	}
	
	public DateFormatException(String msg) {
		super(msg);
	}
	
	public DateFormatException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
