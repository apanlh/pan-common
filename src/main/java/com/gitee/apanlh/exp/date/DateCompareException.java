package com.gitee.apanlh.exp.date;

/**	
 * 	时间对比异常
 * 	@author Pan
 */
public class DateCompareException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public DateCompareException(Exception e) {
		super(e);
	}
	
	public DateCompareException(String msg) {
		super(msg);
	}
	
	public DateCompareException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
