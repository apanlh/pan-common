package com.gitee.apanlh.exp;

/**	
 * 	初始化加载异常
 * 	@author Pan
 */
public class InitLoadException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public InitLoadException(Exception e) {
		super(e);
	}
	
	public InitLoadException(String msg) {
		super(msg);
	}
	
	public InitLoadException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
