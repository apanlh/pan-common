package com.gitee.apanlh.exp;

/**	
 * 	加密异常
 * 	@author Pan
 */
public class EncryptException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public EncryptException(Exception e) {
		super(e);
	}
	
	public EncryptException(String msg) {
		super(msg);
	}
	
	public EncryptException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
