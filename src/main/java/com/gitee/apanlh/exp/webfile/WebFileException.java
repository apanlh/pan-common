package com.gitee.apanlh.exp.webfile;

/**	
 * 	读取文件异常
 * 	@author Pan
 */
public class WebFileException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public WebFileException(Exception e) {
		super(e);
	}
	
	public WebFileException(String msg) {
		super(msg);
	}
	
	public WebFileException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
