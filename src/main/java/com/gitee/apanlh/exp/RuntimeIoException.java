package com.gitee.apanlh.exp;

/**	
 * 	运行时IO异常
 * 	
 * 	@author Pan
 */
public class RuntimeIoException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public RuntimeIoException(Exception e) {
		super(e);
	}
	
	public RuntimeIoException(String msg) {
		super(msg);
	}
	
	public RuntimeIoException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
