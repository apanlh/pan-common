package com.gitee.apanlh.exp;

/**
 * 	FTP文件异常
 * 	@author Pan
 */
public class FtpFileException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public FtpFileException(Exception e) {
		super(e);
	}
	
	public FtpFileException(String msg) {
		super(msg);
	}
	
	public FtpFileException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
