package com.gitee.apanlh.exp;

/**	
 * 	剪切版异常
 * 	
 * 	@author Pan
 */
public class ClipboardException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public ClipboardException(Exception e) {
		super(e);
	}
	
	public ClipboardException(String msg) {
		super(msg);
	}
	
	public ClipboardException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
