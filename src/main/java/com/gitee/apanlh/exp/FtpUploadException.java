package com.gitee.apanlh.exp;

/**	
 * 	FTP上传异常
 * 	@author Pan
 */
public class FtpUploadException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public FtpUploadException(Exception e) {
		super(e);
	}
	
	public FtpUploadException(String msg) {
		super(msg);
	}
	
	public FtpUploadException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
