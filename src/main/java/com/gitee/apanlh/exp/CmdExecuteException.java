package com.gitee.apanlh.exp;

/**	
 * 	CMD命令行相关执行异常
 * 	
 * 	@author Pan
 */
public class CmdExecuteException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public CmdExecuteException(Exception e) {
		super(e);
	}
	
	public CmdExecuteException(String msg) {
		super(msg);
	}
	
	public CmdExecuteException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
