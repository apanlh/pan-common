package com.gitee.apanlh.exp;

/**	
 * 	IP地址相关异常
 * 	
 * 	@author Pan
 */
public class IpAddrException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public IpAddrException(Exception e) {
		super(e);
	}
	
	public IpAddrException(String msg) {
		super(msg);
	}
	
	public IpAddrException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
