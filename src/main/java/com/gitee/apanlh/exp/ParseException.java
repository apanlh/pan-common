package com.gitee.apanlh.exp;

/**
 * 	对于解析异常都能使用此类
 * 	
 * 	@author Pan
 */
public class ParseException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public ParseException(Exception e) {
		super(e);
	}
	
	public ParseException(String msg) {
		super(msg);
	}
	
	public ParseException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
}
