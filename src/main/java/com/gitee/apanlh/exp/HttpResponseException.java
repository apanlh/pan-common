package com.gitee.apanlh.exp;

/**	
 * 	HTTP请求体解析相关异常
 * 
 * 	@author Pan
 */
public class HttpResponseException extends HttpException {
	
	private static final long serialVersionUID = 1L;

	public HttpResponseException(Exception e) {
		super(e);
	}
	
	public HttpResponseException(String msg) {
		super(msg);
	}
	
	public HttpResponseException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
