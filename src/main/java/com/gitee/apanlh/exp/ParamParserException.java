package com.gitee.apanlh.exp;

/**	
 * 	参数解析器异常
 * 
 * 	@author Pan
 */
public class ParamParserException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ParamParserException(Exception e) {
		super(e);
	}
	
	public ParamParserException(String msg) {
		super(msg);
	}
	
	public ParamParserException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
