package com.gitee.apanlh.exp;

/**	
 * 	编码异常
 * 	@author Pan
 */
public class EncodeException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public EncodeException(Exception e) {
		super(e);
	}
	
	public EncodeException(String msg) {
		super(msg);
	}
	
	public EncodeException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
