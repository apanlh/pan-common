package com.gitee.apanlh.exp;

/**	
 * 	IO关闭异常
 * 	
 * 	@author Pan
 */
public class IOCloseErrorException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public IOCloseErrorException(Exception e) {
		super(e);
	}
	
	public IOCloseErrorException(String msg) {
		super(msg);
	}
	
	public IOCloseErrorException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
