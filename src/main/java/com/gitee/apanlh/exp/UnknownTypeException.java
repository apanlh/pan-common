package com.gitee.apanlh.exp;

/**	
 * 	未知类型异常
 * 
 * 	@author Pan
 */
public class UnknownTypeException extends CustomException {

	private static final long serialVersionUID = 1L;

	public UnknownTypeException(Exception e) {
		super(e);
	}
	
	public UnknownTypeException(String msg) {
		super(msg);
	}
	
	public UnknownTypeException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
