package com.gitee.apanlh.exp;

/**	
 * 	身份证校验异常
 * 	
 * 	@author Pan
 */
public class IdcardValidException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public IdcardValidException(Exception e) {
		super(e);
	}
	
	public IdcardValidException(String msg) {
		super(msg);
	}
	
	public IdcardValidException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
