package com.gitee.apanlh.exp.algorithm;

/**	
 * 	加解密相关异常
 * 	@author Pan
 */
public class AlgorithmException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public AlgorithmException(Exception e) {
		super(e);
	}
	
	public AlgorithmException(String msg) {
		super(msg);
	}
	
	public AlgorithmException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
