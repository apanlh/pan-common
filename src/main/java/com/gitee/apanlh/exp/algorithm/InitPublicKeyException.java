package com.gitee.apanlh.exp.algorithm;

import com.gitee.apanlh.exp.CustomException;
import com.gitee.apanlh.util.base.StringUtils;

/**
 * 	对称加密/解密异常
 * 	
 * 	@author Pan
 */
public class InitPublicKeyException extends CustomException {

	private static final long serialVersionUID = 1L;

	/**	
	 * 	构造函数
	 * 	<br>指定异常
	 * 	
	 * 	@author Pan
	 * 	@param 	e	异常
	 */
	public InitPublicKeyException(Exception e) {
		super(e);
	}
	
	/**	
	 * 	构造函数
	 * 	<br>指定消息
	 * 	
	 * 	@author Pan
	 * 	@param 	msg	 消息
	 */
	public InitPublicKeyException(String msg) {
		super(msg);
	}
	
	/**	
	 * 	构造函数
	 * 	<br>指定格式化消息
	 * 	
	 * 	@author Pan
	 * 	@param 	format	格式化字符串
	 * 	@param 	argument	参数
	 */
	public InitPublicKeyException(String format, Object... argument) {
		super(StringUtils.format(format, argument));
	}
	
	/**	
	 * 	构造函数
	 * 	<br>指定格式化消息
	 * 	<br>自定义抛出异常
	 * 	@author Pan
	 * 	@param 	format	格式化字符串
	 * 	@param 	e			抛出异常
	 * 	@param 	argument	参数
	 */
	public InitPublicKeyException(String format, Exception e, Object... argument) {
		super(StringUtils.format(format, argument), e);
	}
	
	/**	
	 * 	构造函数
	 * 	<br>指定消息及指定异常
	 * 	
	 * 	@author Pan
	 * 	@param 	msg		异常
	 * 	@param 	cause	原因异常
	 */
	public InitPublicKeyException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
