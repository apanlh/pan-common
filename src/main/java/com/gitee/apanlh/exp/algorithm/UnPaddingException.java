package com.gitee.apanlh.exp.algorithm;

import com.gitee.apanlh.util.base.StringUtils;

/**	
 * 	数据去除填充异常
 * 	
 * 	@author Pan
 */
public class UnPaddingException extends SymmetricException {

	private static final long serialVersionUID = 1L;

	/**	
	 * 	构造函数
	 * 	<br>指定异常
	 * 	
	 * 	@author Pan
	 * 	@param 	e	异常
	 */
	public UnPaddingException(Exception e) {
		super(e);
	}
	
	/**	
	 * 	构造函数
	 * 	<br>指定消息
	 * 	
	 * 	@author Pan
	 * 	@param 	msg	 消息
	 */
	public UnPaddingException(String msg) {
		super(msg);
	}
	
	/**	
	 * 	构造函数
	 * 	<br>指定格式化消息
	 * 	
	 * 	@author Pan
	 * 	@param 	format	消息
	 * 	@param 	argument 	参数
	 */
	public UnPaddingException(String format, Object... argument) {
		super(StringUtils.format(format, argument));
	}
	
	/**	
	 * 	构造函数
	 * 	<br>指定消息及指定异常
	 * 	
	 * 	@author Pan
	 * 	@param 	msg		异常
	 * 	@param 	cause	原因异常
	 */
	public UnPaddingException(String msg, Throwable cause) {
		super(msg, cause);
	}
}