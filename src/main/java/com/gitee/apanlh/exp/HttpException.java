package com.gitee.apanlh.exp;

/**	
 * 	HTTP相关异常
 * 
 * 	@author Pan
 */
public class HttpException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public HttpException(Exception e) {
		super(e);
	}
	
	public HttpException(String msg) {
		super(msg);
	}
	
	public HttpException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
