package com.gitee.apanlh.exp;

/**	
 * 	HTTP响应状态相关异常
 * 
 * 	@author Pan
 */
public class HttpStatusException extends HttpException {
	
	private static final long serialVersionUID = 1L;

	public HttpStatusException(Exception e) {
		super(e);
	}
	
	public HttpStatusException(String msg) {
		super(msg);
	}
	
	public HttpStatusException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
