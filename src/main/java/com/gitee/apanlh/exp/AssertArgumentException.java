package com.gitee.apanlh.exp;

/**	
 * 	断言参数异常
 * 	
 * 	@author Pan
 */
public class AssertArgumentException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public AssertArgumentException(Exception e) {
		super(e);
	}
	
	public AssertArgumentException(String msg) {
		super(msg);
	}
	
	public AssertArgumentException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
