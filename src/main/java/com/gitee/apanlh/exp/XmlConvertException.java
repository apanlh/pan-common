package com.gitee.apanlh.exp;

/**
 * 	用于表示XML转换异常
 * 	
 * 	@author Pan
 */
public class XmlConvertException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public XmlConvertException(Exception e) {
		super(e);
	}
	
	public XmlConvertException(String msg) {
		super(msg);
	}
	
	public XmlConvertException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
