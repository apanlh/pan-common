package com.gitee.apanlh.exp;

/**	
 * 	回调运行异常
 * 	
 * 	@author Pan
 */
public class CallRuntimeException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public CallRuntimeException(Exception e) {
		super(e);
	}
	
	public CallRuntimeException(String msg) {
		super(msg);
	}
	
	public CallRuntimeException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
