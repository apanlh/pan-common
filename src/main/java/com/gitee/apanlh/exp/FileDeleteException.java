package com.gitee.apanlh.exp;

/**	
 * 	文件删除异常
 * 	
 * 	@author Pan
 */
public class FileDeleteException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public FileDeleteException(Exception e) {
		super(e);
	}
	
	public FileDeleteException(String msg) {
		super(msg);
	}
	
	public FileDeleteException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
