package com.gitee.apanlh.exp;

import com.gitee.apanlh.util.base.StringUtils;

/**
 *	自定义异常
 *	<br>用于自定义描述异常，也可作为业务异常或其他异常使用
 *	 	
 * 	@author Pan
 */
public class CustomException extends RuntimeException {

	private static final long serialVersionUID = 7969344125770715270L;
	
	/**	
	 * 	构造函数
	 * 	<br>指定异常
	 * 	
	 * 	@author Pan
	 * 	@param 	e	异常
	 */
	public CustomException(Exception e) {
		super(e);
	}
	
	/**	
	 * 	构造函数
	 * 	<br>指定消息
	 * 	
	 * 	@author Pan
	 * 	@param 	msg	 消息
	 */
	public CustomException(String msg) {
		this(msg, "");
	}
	
	/**	
	 * 	构造函数
	 * 	<br>指定格式化消息
	 * 	
	 * 	@author Pan
	 * 	@param 	format	消息
	 * 	@param 	argument 	参数
	 */
	public CustomException(String format, Object... argument) {
		super(StringUtils.format(format, argument));
	}
	
	/**	
	 * 	构造函数
	 * 	<br>指定消息及指定异常
	 * 	
	 * 	@author Pan
	 * 	@param 	msg		异常
	 * 	@param 	cause	原因异常
	 */
	public CustomException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
}
