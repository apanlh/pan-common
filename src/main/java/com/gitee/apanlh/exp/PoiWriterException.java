package com.gitee.apanlh.exp;

/**	
 * 	POI写入器异常
 * 	
 * 	@author Pan
 */
public class PoiWriterException extends PoiFileException {

	private static final long serialVersionUID = 1L;

	public PoiWriterException(Exception e) {
		super(e);
	}
	
	public PoiWriterException(String msg) {
		super(msg);
	}
	
	public PoiWriterException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
}
