package com.gitee.apanlh.exp;

/**	
 * 	POI自定义异常
 * 	@author Pan
 */
public class PoiFileException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public PoiFileException(Exception e) {
		super(e);
	}
	
	public PoiFileException(String msg) {
		super(msg);
	}
	
	public PoiFileException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
}
