package com.gitee.apanlh.exp;

/**	
 * 	流读取相关异常
 * 	<br>包含IO流、文件流等
 * 	
 * 	@author Pan
 */
public class StreamReadException extends StreamException {

	private static final long serialVersionUID = 1L;

	public StreamReadException(Exception e) {
		super(e);
	}
	
	public StreamReadException(String msg) {
		super(msg);
	}
	
	public StreamReadException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
