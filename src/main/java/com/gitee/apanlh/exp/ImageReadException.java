package com.gitee.apanlh.exp;

/**
 * 	图片读取异常
 * 
 * 	@author Pan
 */
public class ImageReadException extends StreamReadException {
	
	private static final long serialVersionUID = 1L;

	public ImageReadException(Exception e) {
		super(e);
	}
	
	public ImageReadException(String msg) {
		super(msg);
	}
	
	public ImageReadException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
