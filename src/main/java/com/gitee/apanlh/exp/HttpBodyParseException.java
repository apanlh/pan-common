package com.gitee.apanlh.exp;

/**	
 * 	HTTP请求体解析相关异常
 * 
 * 	@author Pan
 */
public class HttpBodyParseException extends HttpException {
	
	private static final long serialVersionUID = 1L;

	public HttpBodyParseException(Exception e) {
		super(e);
	}
	
	public HttpBodyParseException(String msg) {
		super(msg);
	}
	
	public HttpBodyParseException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
