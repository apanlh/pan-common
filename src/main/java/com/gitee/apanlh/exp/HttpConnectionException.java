package com.gitee.apanlh.exp;

/**	
 * 	HTTP相关异常
 * 
 * 	@author Pan
 */
public class HttpConnectionException extends HttpException {
	
	private static final long serialVersionUID = 1L;

	public HttpConnectionException(Exception e) {
		super(e);
	}
	
	public HttpConnectionException(String msg) {
		super(msg);
	}
	
	public HttpConnectionException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
