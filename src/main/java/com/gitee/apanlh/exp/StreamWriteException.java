package com.gitee.apanlh.exp;

/**	
 * 	流写入相关异常
 * 	<br>包含IO流、文件流等
 * 	
 * 	@author Pan
 */
public class StreamWriteException extends StreamException {

	private static final long serialVersionUID = 1L;

	public StreamWriteException(Exception e) {
		super(e);
	}
	
	public StreamWriteException(String msg) {
		super(msg);
	}
	
	public StreamWriteException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
