package com.gitee.apanlh.exp;

/**	
 * 	流相关异常
 * 	<br>包含IO流、文件流等
 * 	
 * 	@author Pan
 */
public class StreamException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public StreamException(Exception e) {
		super(e);
	}
	
	public StreamException(String msg) {
		super(msg);
	}
	
	public StreamException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
