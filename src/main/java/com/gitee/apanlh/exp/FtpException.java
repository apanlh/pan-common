package com.gitee.apanlh.exp;

/**
 * 	FTP异常
 * 	@author Pan
 */
public class FtpException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public FtpException(Exception e) {
		super(e);
	}
	
	public FtpException(String msg) {
		super(msg);
	}
	
	public FtpException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
