package com.gitee.apanlh.exp;

/**
 * 	Invoke异常
 * 	@author Pan
 */
public class MethodInvokeException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public MethodInvokeException(Exception e) {
		super(e);
	}
	
	public MethodInvokeException(String msg) {
		super(msg);
	}
	
	public MethodInvokeException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
