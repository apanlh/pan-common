package com.gitee.apanlh.exp;

/**
 * 	Redis自定义异常
 *
 * 	@author Pan
 */
public class RedisException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public RedisException(Exception e) {
		super(e);
	}
	
	public RedisException(String msg) {
		super(msg);
	}
	
	public RedisException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
