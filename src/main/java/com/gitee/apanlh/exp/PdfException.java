package com.gitee.apanlh.exp;

/**	
 * 	PDF相关异常
 * 	
 * 	@author Pan
 */
public class PdfException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public PdfException(Exception e) {
		super(e);
	}
	
	public PdfException(String msg) {
		super(msg);
	}
	
	public PdfException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
