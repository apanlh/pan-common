package com.gitee.apanlh.exp;

/**	
 * 	加载异常
 * 	@author Pan
 */
public class LoadErrorException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public LoadErrorException(Exception e) {
		super(e);
	}
	
	public LoadErrorException(String msg) {
		super(msg);
	}
	
	public LoadErrorException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
