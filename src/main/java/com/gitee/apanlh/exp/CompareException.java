package com.gitee.apanlh.exp;

/**	
 * 	比较异常
 * 	@author Pan
 */
public class CompareException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public CompareException(Exception e) {
		super(e);
	}
	
	public CompareException(String msg) {
		super(msg);
	}
	
	public CompareException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
