package com.gitee.apanlh.exp;

/**	
 * 	实例化异常
 * 	@author Pan
 */
public class InstanceException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public InstanceException(Exception e) {
		super(e);
	}
	
	public InstanceException(String msg) {
		super(msg);
	}
	
	public InstanceException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
