package com.gitee.apanlh.exp.zip;
	
/**	
 * 	ZIP解压异常
 * 	#mark 增加压缩异常类型
 * 	@author Pan
 */
public class ZipDecompressException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public ZipDecompressException(Exception e) {
		super(e);
	}
	
	public ZipDecompressException(String msg) {
		super(msg);
	}
	
	public ZipDecompressException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
