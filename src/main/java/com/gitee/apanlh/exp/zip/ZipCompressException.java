package com.gitee.apanlh.exp.zip;
	
/**	
 * 	ZIP压缩异常
 * 	#mark 增加压缩异常类型
 * 	@author Pan
 */
public class ZipCompressException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public ZipCompressException(Exception e) {
		super(e);
	}
	
	public ZipCompressException(String msg) {
		super(msg);
	}
	
	public ZipCompressException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
