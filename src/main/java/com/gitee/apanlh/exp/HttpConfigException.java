package com.gitee.apanlh.exp;

/**	
 * 	HTTP配置相关异常
 * 
 * 	@author Pan
 */
public class HttpConfigException extends HttpException {
	
	private static final long serialVersionUID = 1L;

	public HttpConfigException(Exception e) {
		super(e);
	}
	
	public HttpConfigException(String msg) {
		super(msg);
	}
	
	public HttpConfigException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
