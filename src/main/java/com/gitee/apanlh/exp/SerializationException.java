package com.gitee.apanlh.exp;

/**	
 * 	序列化异常
 * 	
 * 	@author Pan
 */
public class SerializationException extends StreamException {

	private static final long serialVersionUID = 1L;

	public SerializationException(Exception e) {
		super(e);
	}
	
	public SerializationException(String msg) {
		super(msg);
	}
	
	public SerializationException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
