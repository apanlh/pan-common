package com.gitee.apanlh.exp;

/**
 * 	FTP连接异常
 * 	@author Pan
 */
public class FtpConnectionException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public FtpConnectionException(Exception e) {
		super(e);
	}
	
	public FtpConnectionException(String msg) {
		super(msg);
	}
	
	public FtpConnectionException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
}
