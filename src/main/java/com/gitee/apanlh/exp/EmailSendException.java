package com.gitee.apanlh.exp;

/**	
 * 	邮件发送异常
 * 	@author Pan
 */
public class EmailSendException extends EmailException {

	private static final long serialVersionUID = 1L;

	public EmailSendException(Exception e) {
		super(e);
	}
	
	public EmailSendException(String msg) {
		super(msg);
	}
	
	public EmailSendException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
