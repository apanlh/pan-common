package com.gitee.apanlh.exp;

/**	
 * 	FTP下载异常
 * 	@author Pan
 */
public class FtpDownloadException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public FtpDownloadException(Exception e) {
		super(e);
	}
	
	public FtpDownloadException(String msg) {
		super(msg);
	}
	
	public FtpDownloadException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
