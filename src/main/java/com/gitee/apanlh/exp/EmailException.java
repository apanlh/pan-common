package com.gitee.apanlh.exp;

/**	
 * 	邮件异常
 * 	@author Pan
 */
public class EmailException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public EmailException(Exception e) {
		super(e);
	}
	
	public EmailException(String msg) {
		super(msg);
	}
	
	public EmailException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
