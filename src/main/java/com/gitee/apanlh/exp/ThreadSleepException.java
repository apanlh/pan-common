package com.gitee.apanlh.exp;

/**
 * 	线程睡眠异常
 * 	
 * 	@author Pan
 */
public class ThreadSleepException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public ThreadSleepException(Exception e) {
		super(e);
	}
	
	public ThreadSleepException(String msg) {
		super(msg);
	}
	
	public ThreadSleepException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
