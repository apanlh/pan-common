package com.gitee.apanlh.exp;

/**	
 * 	文件输出异常
 * 	<br>文件落地异常
 * 	
 * 	@author Pan
 */
public class FileOutException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public FileOutException(Exception e) {
		super(e);
	}
	
	public FileOutException(String msg) {
		super(msg);
	}
	
	public FileOutException(String msg, Throwable cause) {
		super(msg, cause);
	}
}

