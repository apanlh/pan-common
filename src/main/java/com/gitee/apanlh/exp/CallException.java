package com.gitee.apanlh.exp;

/**	
 * 	回调异常
 * 	
 * 	@author Pan
 */
public class CallException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public CallException(Exception e) {
		super(e);
	}
	
	public CallException(String msg) {
		super(msg);
	}
	
	public CallException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
