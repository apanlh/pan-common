package com.gitee.apanlh.exp;

/**	
 * 	未找到某某某异常
 * 	<br>适用性比较广泛
 * 	
 * 	@author Pan
 */
public class NotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public NotFoundException(Exception e) {
		super(e);
	}
	
	public NotFoundException(String msg) {
		super(msg);
	}
	
	public NotFoundException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
