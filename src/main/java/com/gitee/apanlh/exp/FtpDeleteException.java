package com.gitee.apanlh.exp;

/**	
 * 	FTP删除异常
 * 	@author Pan
 */
public class FtpDeleteException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public FtpDeleteException(Exception e) {
		super(e);
	}
	
	public FtpDeleteException(String msg) {
		super(msg);
	}
	
	public FtpDeleteException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
