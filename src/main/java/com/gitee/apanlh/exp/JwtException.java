package com.gitee.apanlh.exp;

/**
 * 	JWT异常
 * 	@author Pan
 */
public class JwtException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public JwtException(Exception e) {
		super(e);
	}
	
	public JwtException(String msg) {
		super(msg);
	}
	
	public JwtException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
