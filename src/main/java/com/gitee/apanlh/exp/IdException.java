package com.gitee.apanlh.exp;

/**	
 * 	Id相关异常
 * 	@author Pan
 */
public class IdException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public IdException(Exception e) {
		super(e);
	}
	
	public IdException(String msg) {
		super(msg);
	}
	
	public IdException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
