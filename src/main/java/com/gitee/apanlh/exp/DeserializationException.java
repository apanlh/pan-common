package com.gitee.apanlh.exp;

/**	
 * 	反序列化异常
 * 	
 * 	@author Pan
 */
public class DeserializationException extends StreamException {

	private static final long serialVersionUID = 1L;

	public DeserializationException(Exception e) {
		super(e);
	}
	
	public DeserializationException(String msg) {
		super(msg);
	}
	
	public DeserializationException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
