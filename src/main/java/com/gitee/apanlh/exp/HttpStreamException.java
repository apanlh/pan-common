package com.gitee.apanlh.exp;

/**	
 * 	HTTP流相关异常
 * 
 * 	@author Pan
 */
public class HttpStreamException extends HttpException {
	
	private static final long serialVersionUID = 1L;

	public HttpStreamException(Exception e) {
		super(e);
	}
	
	public HttpStreamException(String msg) {
		super(msg);
	}
	
	public HttpStreamException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
