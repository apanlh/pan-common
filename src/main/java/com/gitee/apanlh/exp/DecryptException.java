package com.gitee.apanlh.exp;

/**	
 * 	解密异常
 * 	@author Pan
 */
public class DecryptException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public DecryptException(Exception e) {
		super(e);
	}
	
	public DecryptException(String msg) {
		super(msg);
	}
	
	public DecryptException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
