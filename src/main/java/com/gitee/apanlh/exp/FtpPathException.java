package com.gitee.apanlh.exp;

/**
 * 	FTP目录地址异常
 * 	@author Pan
 */
public class FtpPathException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public FtpPathException(Exception e) {
		super(e);
	}
	
	public FtpPathException(String msg) {
		super(msg);
	}
	
	public FtpPathException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
