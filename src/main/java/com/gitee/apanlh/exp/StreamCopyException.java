package com.gitee.apanlh.exp;

/**	
 * 	流拷贝相关异常
 * 	<br>包含IO流、文件流等
 * 	
 * 	@author Pan
 */
public class StreamCopyException extends StreamException {

	private static final long serialVersionUID = 1L;

	public StreamCopyException(Exception e) {
		super(e);
	}
	
	public StreamCopyException(String msg) {
		super(msg);
	}
	
	public StreamCopyException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
