package com.gitee.apanlh.annotation.viewresolver;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 	开启解析RESTFul
 * 	
 * 	@author Pan
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnableRest {
	
	String name() default "";
	
	String value() default "";
}
