package com.gitee.apanlh.annotation.viewresolver;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**	
 * 	添加身份认证
 * 	@author Pan
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ResponseAuthParam {
	
	boolean flag() default true;
	
	String value() default "";
	
}
