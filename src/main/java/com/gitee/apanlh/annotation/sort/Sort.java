package com.gitee.apanlh.annotation.sort;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**	
 * 	对象属性字段排序注解
 * 	
 * 	@author Pan
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Sort {
	
	/**	
	 * 	排序 默认值模式
	 * 	
	 * 	@author Pan
	 * 	@return	SortType
	 */
	SortType type() default SortType.VALUE;
}
