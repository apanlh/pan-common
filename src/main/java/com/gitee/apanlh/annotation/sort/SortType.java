package com.gitee.apanlh.annotation.sort;

/**	
 * 	对象属性字段排序类型
 * 	
 * 	@author Pan
 */
public enum SortType {
	
	VALUE,
	TIME,
	;
}
