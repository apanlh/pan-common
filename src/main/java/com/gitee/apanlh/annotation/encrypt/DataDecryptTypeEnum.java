package com.gitee.apanlh.annotation.encrypt;


/**	
 *  用于接收前端解析时
 *  以什么形式解密
 * 	@author Pan
 *
 */
public enum DataDecryptTypeEnum {
	SM2, 
	SM3, 
	SM4,
	;
}
