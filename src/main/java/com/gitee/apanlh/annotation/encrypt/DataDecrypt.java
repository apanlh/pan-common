package com.gitee.apanlh.annotation.encrypt;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**	
 * 	数据解密类型
 * 	
 * 	@author Pan
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataDecrypt {
	
	/**	
	 * 	解密类型
	 * 
	 * 	@author Pan
	 * 	@return	DataDecryptTypeEnum
	 */
	DataDecryptTypeEnum type();
	
	/**	
	 * 	私钥
	 * 
	 * 	@author Pan
	 * 	@return	String
	 */
	String privateKey() default "";
	
	/**	
	 * 	是否为非对称加密
	 * 
	 * 	@return	boolean
	 */
	boolean hasRsaEncrypt() default false;
	
}
