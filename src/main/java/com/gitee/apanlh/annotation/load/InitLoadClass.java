package com.gitee.apanlh.annotation.load;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**	
 * 	初始化Bean加载器
 * 	@author Pan
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface InitLoadClass {

	/**
	 *  bean名称
	 *
	 *  @return String
	 */
	String beanName() default "";
	
	/**
	 * 初始化时是否加载实例类
	 *
	 * @return boolean
	 */
	boolean initInstance() default false;
	
	/**
	 *  初始化时是否加载实例类(默认构造函数)并存入单例类
	 *
	 *  @return boolean
	 */
	boolean initSingleton() default false;
}
