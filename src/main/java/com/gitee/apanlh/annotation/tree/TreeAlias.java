package com.gitee.apanlh.annotation.tree;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**	
 * 	树结构属性别名
 * 	@author Pan
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TreeAlias {
	
	/**
	 *  别名
	 *
	 *  @return String
	 */
	String alias() default "";

}
